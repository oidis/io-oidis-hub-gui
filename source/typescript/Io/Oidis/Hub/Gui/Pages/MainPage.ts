/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";

export class MainPage extends BasePanel {
    public readonly dashboard : Button;
    public readonly hubInfo : Label;

    constructor() {
        super();
        this.dashboard = new Button();
        this.hubInfo = new Label();
    }

    protected innerHtml() : string {
        return `
<section style="background: var(--bs-dark);">
    <div class="container-fluid">
        <div class="row"></div>
        <div class="row" style="text-align: center;margin: 10%;">
            <div class="col"><img class="img-fluid d-xl-flex" src="resource/graphics/Io/Oidis/Hub/Gui/Logo.png" style="margin: auto;" /></div>
        </div>
        <div class="row" style="text-align: center;">
            <div class="col"><button class="btn btn-outline-warning" type="button" style="width: 30%;margin-top: 39px;" data-oidis-bind="${this.dashboard}">Dashboard</button></div>
        </div>
        <div class="row">
            <div class="col">
                <p style="text-align: center;margin-top: 40px;color: var(--bs-light);" data-oidis-bind="${this.hubInfo}">Copyright ${StringUtils.YearFrom(2019)} Oidis</p>
            </div>
        </div>
    </div>
</section>`;
    }
}
