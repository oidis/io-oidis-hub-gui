/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { NewPasswordPanel } from "../Panels/SSO/NewPasswordPanel.js";
import { ResetPasswordPanel } from "../Panels/SSO/ResetPasswordPanel.js";
import { SignInPanel } from "../Panels/SSO/SignInPanel.js";
import { SignUpPanel } from "../Panels/SSO/SignUpPanel.js";
import { TwoFactorChallengePanel } from "../Panels/SSO/TwoFactorChallengePanel.js";
import { BasePageLayout, ITabDescriptor } from "../Primitives/BasePageLayout.js";

export class SSOPage extends BasePageLayout {
    public declare readonly signIn : SignInPanel;
    public declare readonly signUp : SignUpPanel;
    public declare readonly resetPassword : ResetPasswordPanel;
    public declare readonly newPassword : NewPasswordPanel;
    public declare readonly twoFactorChallenge : TwoFactorChallengePanel;

    protected getTabsConfiguration() : ITabDescriptor[] {
        return <any>{
            signIn            : {
                link     : "/login",
                altLinks : ["/signIn", "/accountActivation", "/accountActivation/{token}"],
                content  : SignInPanel,
                isDefault: true
            },
            signUp            : {
                link    : "/signUp",
                altLinks: ["/invite", "/invite/{token}"],
                content : SignUpPanel
            },
            resetPassword     : {
                content: ResetPasswordPanel
            },
            newPassword       : {
                content: NewPasswordPanel
            },
            twoFactorChallenge: {
                link   : "/twoFactorChallenge",
                content: TwoFactorChallengePanel
            }
        };
    }

    protected innerHtml() : string {
        return `
<section class="ScreenSection" data-oidis-bind="${this.screenSection}">
    <div class="container-fluid d-flex h-100 flex-column">
        <div class="row text-dark ${GeneralCSS.DNONE}" data-oidis-bind="${this.headerStrip}" style="background: var(--bs-yellow);">
            <div class="col text-center"><span> </span></div>
        </div>
        <div class="row"><div class="col" data-oidis-bind="${this.systemState}" style="padding: 0;"></div></div>
        <div class="row">
            <div class="col-xl-12 text-center" style="margin-bottom: 30px;margin-top: 30px;"><img class="img-fluid" src="resource/graphics/Io/Oidis/Hub/Gui/LogoSymbol.png" /></div>
        </div>
        <div class="row justify-content-center flex-fill" style="min-height: 500px;" data-oidis-bind="${this.tabsContent}">
            <div class="col">
                <div class="row h-100" data-oidis-bind="${this.panelsContainer}">${this.getTabContent()}</div>
            </div>
        </div>
        <div class="row justify-content-center flex-fill" data-oidis-bind="${this.skeletonLoader}"></div>
    </div>
    <div class="toast-container position-fixed bottom-0 end-0 p-3" style="z-index: 11;" data-oidis-bind="${this.toasts}"></div>
</section>
<section class="HiddenElement PrintSection" data-oidis-bind="${this.printSection}"></section>`;
    }
}
