/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { AutocompleteSearch } from "../UserControls/AutocompleteSearch.js";

export class AdminInitPage extends BasePanel {
    public readonly status : Label;
    public readonly hubInfo : Label;
    public readonly statusContent : GuiCommons;
    public readonly authContent : GuiCommons;
    public readonly activateContent : GuiCommons;
    public readonly confirmContent : GuiCommons;
    public readonly authToken : TextField;
    public readonly authenticate : Button;
    public readonly user : TextField;
    public readonly pass : TextField;
    public readonly repass : TextField;
    public readonly find : AutocompleteSearch;
    public readonly activate : Button;
    public readonly countDown : Label;

    constructor() {
        super();
        this.status = new Label();
        this.hubInfo = new Label();
        this.statusContent = new GuiCommons();
        this.authContent = new GuiCommons();
        this.activateContent = new GuiCommons();
        this.confirmContent = new GuiCommons();
        this.authToken = new TextField();
        this.authenticate = new Button();
        this.user = new TextField();
        this.pass = new TextField();
        this.repass = new TextField();
        this.find = new AutocompleteSearch();
        this.activate = new Button();
        this.countDown = new Label();
    }

    protected innerCode() : string {
        this.status.Text("Loading, please wait ...");
        this.hubInfo.Text(this.getEnvironmentArgs().getProjectConfig().target.copyright);

        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="container-fluid d-flex flex-column h-100">
        <div class="row" style="text-align: center;margin: 7%;">
            <div class="col Logo"><img class="img-fluid d-xl-flex" src="resource/graphics/Io/Oidis/Hub/Gui/LogoSymbol.png" style="margin: auto;" /></div>
        </div>
        <div class="row" style="text-align: center;" data-oidis-bind="${this.authContent}">
            <div class="col">
                <h3 style="margin-bottom: 20px;">Admin account activation is needed</h3>
                <div class="row justify-content-center" style="margin-bottom: 20px;">
                    <div class="col-12 col-md-6">
                        <div class="input-group"><span class="input-group-text">Authorization token</span><input class="form-control" type="text" data-oidis-bind="${this.authToken}" autocomplete="off" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.authenticate}">Authorize</button></div>
                    </div>
                </div>
                <div class="card" style="margin-bottom: 10px;">
                    <div class="card-body">
                        <h6 class="text-start text-muted card-subtitle mb-2">What is authorization token?</h6>
                        <p class="text-start card-text">This page is displayed because current instance is in its initial state, where database does not contain any user with Admin rights. To create or define user with Admin rights it is required to verify instance owner. For verification is used &quot;Authorization token&quot; which is available only in application stdout. Go to the server terminal a find stdout for this instance where you can reach required token. After successfull authorization you will be able to create new admin user or specify admin rights to one of already existing user.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ${GeneralCSS.DNONE}" style="text-align: center;" data-oidis-bind="${this.activateContent}">
            <div class="col">
                <h3 style="margin-bottom: 20px;">Define admin account</h3>
                <div class="row justify-content-center" style="margin-bottom: 10px;">
                    <div class="col-12 col-md-6">
                        <div class="row">
                            <div class="col">
                                <form>
                                    <div class="input-group" style="margin-bottom: 10px;"><span class="input-group-text" style="width: 42px;"><svg class="bi bi-person-badge" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                                                <path d="M6.5 2a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1zM11 8a3 3 0 1 1-6 0 3 3 0 0 1 6 0"></path>
                                                <path d="M4.5 0A2.5 2.5 0 0 0 2 2.5V14a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2.5A2.5 2.5 0 0 0 11.5 0zM3 2.5A1.5 1.5 0 0 1 4.5 1h7A1.5 1.5 0 0 1 13 2.5v10.795a4.2 4.2 0 0 0-.776-.492C11.392 12.387 10.063 12 8 12s-3.392.387-4.224.803a4.2 4.2 0 0 0-.776.492z"></path>
                                            </svg></span><input class="form-control" type="text" data-oidis-bind="${this.user}" placeholder="user name" autocomplete="off" /></div>
                                    <div class="input-group" style="margin-bottom: 11px;"><span class="input-group-text" style="width: 42px;"><svg class="bi bi-lock" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                                                <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2m3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2M5 8h6a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1"></path>
                                            </svg></span><input class="form-control" type="password" data-oidis-bind="${this.pass}" placeholder="password" autocomplete="off" /></div>
                                    <div class="input-group"><span class="input-group-text" style="width: 42px;"><svg class="bi bi-check-circle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"></path>
                                                <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05"></path>
                                            </svg></span><input class="form-control" type="password" data-oidis-bind="${this.repass}" placeholder="password verify" autocomplete="off" /></div>
                                </form>
                            </div>
                        </div>
                        <div class="row align-items-center" style="margin-top: 15px;margin-bottom: 15px;">
                            <div class="col">
                                <hr />
                            </div>
                            <div class="col-auto">
                                <p class="text-secondary" style="margin: 0;">or</p>
                            </div>
                            <div class="col">
                                <hr />
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col" data-oidis-bind="${this.find}"></div>
                        </div>
                        <div class="row">
                            <div class="col"><button class="btn btn-success btn-lg" type="button" data-oidis-bind="${this.activate}"><svg class="bi bi-shield-check" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-bottom: 4px;margin-right: 12px;">
                                        <path d="M5.338 1.59a61.44 61.44 0 0 0-2.837.856.481.481 0 0 0-.328.39c-.554 4.157.726 7.19 2.253 9.188a10.725 10.725 0 0 0 2.287 2.233c.346.244.652.42.893.533.12.057.218.095.293.118a.55.55 0 0 0 .101.025.615.615 0 0 0 .1-.025c.076-.023.174-.061.294-.118.24-.113.547-.29.893-.533a10.726 10.726 0 0 0 2.287-2.233c1.527-1.997 2.807-5.031 2.253-9.188a.48.48 0 0 0-.328-.39c-.651-.213-1.75-.56-2.837-.855C9.552 1.29 8.531 1.067 8 1.067c-.53 0-1.552.223-2.662.524zM5.072.56C6.157.265 7.31 0 8 0s1.843.265 2.928.56c1.11.3 2.229.655 2.887.87a1.54 1.54 0 0 1 1.044 1.262c.596 4.477-.787 7.795-2.465 9.99a11.775 11.775 0 0 1-2.517 2.453 7.159 7.159 0 0 1-1.048.625c-.28.132-.581.24-.829.24s-.548-.108-.829-.24a7.158 7.158 0 0 1-1.048-.625 11.777 11.777 0 0 1-2.517-2.453C1.928 10.487.545 7.169 1.141 2.692A1.54 1.54 0 0 1 2.185 1.43 62.456 62.456 0 0 1 5.072.56"></path>
                                        <path d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"></path>
                                    </svg>Enable admin account</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ${GeneralCSS.DNONE}" style="text-align: center;" data-oidis-bind="${this.confirmContent}">
            <div class="col">
                <div class="card" style="margin-bottom: 10px;">
                    <div class="card-body">
                        <h4 class="card-title">Admin account enabled</h4>
                        <p class="card-text">Admin account has been successfully enabled, providing server restart... In case of that server is running on local machine or container is not configured for auto-restart you must start this instance manually.<br /><br />Redirect to home page will be done automatically in <span data-oidis-bind="${this.countDown}">10</span>s.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center ${GeneralCSS.DNONE}" style="text-align: center;" data-oidis-bind="${this.statusContent}">
            <div class="col-12 col-md-6">
                <div class="alert alert-danger" role="alert"><span data-oidis-bind="${this.status}"><strong>Something goes wrong...</strong></span></div>
            </div>
        </div>
        <div class="row mt-auto">
            <div class="col">
                <p style="text-align: center;margin-top: 40px;" data-oidis-bind="${this.hubInfo}">Copyright 2019-2024 Oidis</p>
            </div>
        </div>
    </div>
</section>`;
    }
}
