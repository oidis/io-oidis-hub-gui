/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";

export class InitPage extends BasePanel {
    public readonly status : Label;
    public readonly hubInfo : Label;

    constructor() {
        super();
        this.status = new Label();
        this.hubInfo = new Label();
    }

    protected innerCode() : string {
        this.status.Text("Loading, please wait ...");
        this.hubInfo.Text(this.getEnvironmentArgs().getProjectConfig().target.copyright);

        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="container-fluid d-flex flex-column h-100">
        <div class="row" style="text-align: center;margin: 10%;">
            <div class="col Logo"><img class="img-fluid d-xl-flex" src="resource/graphics/Io/Oidis/Hub/Gui/LogoSymbol.png" style="margin: auto;" /></div>
        </div>
        <div class="row" style="text-align: center;">
            <div class="col">
                <p data-oidis-bind="${this.status}"></p>
            </div>
        </div>
        <div class="row mt-auto">
            <div class="col">
                <p style="text-align: center;margin-top: 40px;" data-oidis-bind="${this.hubInfo}">Copyright ${StringUtils.YearFrom(2019)} Oidis</p>
            </div>
        </div>
    </div>
</section>`;
    }
}
