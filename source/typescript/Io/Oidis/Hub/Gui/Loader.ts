/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogAdapterType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogAdapterType.js";
import { SentryAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/SentryAdapter.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { isBrowser } from "@io-oidis-commons/Io/Oidis/Commons/Utils/EnvironmentHelper.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { HttpResolver } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/HttpResolver.js";
import { Loader as Parent } from "@io-oidis-services/Io/Oidis/Services/Loader.js";
import "./ForceCompile.js";
import { IProject } from "./Interfaces/IProject.js";
import { ForwardingAdapter } from "./LogProcessor/Adapters/ForwardingAdapter.js";
import { InMemoryAdapter } from "./LogProcessor/Adapters/InMemoryAdapter.js";
import { UserContext } from "./Structures/UserContext.js";

export class Loader extends Parent {
    private readonly memoryAdapter : InMemoryAdapter;
    private forwardingAdapter : ForwardingAdapter;
    private context : UserContext;

    public static getInstance() : Loader {
        return <Loader>super.getInstance();
    }

    constructor() {
        super();

        this.memoryAdapter = new InMemoryAdapter();
        if (isBrowser) {
            LogIt.getLogger().AddAdapter(this.memoryAdapter);
        }
        this.context = this.initContext();
    }

    public getAppConfiguration() : IProject {
        return <IProject>this.getEnvironmentArgs().getProjectConfig();
    }

    public ApplicationLog() : ILoggerTrace[] {
        return this.memoryAdapter.getLogs();
    }

    public setLogsContext($value : string) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.forwardingAdapter)) {
            this.forwardingAdapter.setContext($value);
        }
        (<SentryAdapter>LogIt.getLogger().getAdapter(LogAdapterType.SENTRY)).setContext($value);
    }

    public getContext() : UserContext {
        return this.context;
    }

    protected async onEnvironmentLoad() : Promise<void> {
        await super.onEnvironmentLoad();
        if (isBrowser) {
            this.forwardingAdapter = new ForwardingAdapter();
            LogIt.getLogger().AddAdapter(this.forwardingAdapter);
        }
    }

    protected initContext() : UserContext {
        return new UserContext();
    }

    protected initResolver() : HttpResolver {
        return new HttpResolver("/");
    }
}
