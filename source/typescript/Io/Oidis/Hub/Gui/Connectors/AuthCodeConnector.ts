/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { Loader } from "../Loader.js";

export class AuthCodeConnector extends BaseConnector {

    public static getClient() : AuthCodeConnector {
        const instance : AuthCodeConnector =
            new AuthCodeConnector(true, Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp"));
        AuthCodeConnector.getClient = () : AuthCodeConnector => {
            return instance;
        };
        return instance;
    }

    public SendAuthCode($id : string, $force : boolean = false) : Promise<void> {
        return this.asyncInvoke("SendAuthCode", $id, $force, null);
    }

    public RequestAuthCode($id : string, $email : string, $verifyEmail : string,
                           $verifyPhone : string) : Promise<IAuthCodeRequestResult> {
        return this.asyncInvoke("RequestAuthCode", $id, $email, $verifyEmail, $verifyPhone);
    }

    public Validate($id : string, $code : string) : Promise<IAuthCodeValidateResult> {
        return this.asyncInvoke("Validate", $id, $code);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.HUB_CONNECTOR;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Utils.AuthCodeManager";
        return namespaces;
    }
}

export interface IAuthCodeRequestResult {
    status : boolean;
    message? : string;
    skipped? : boolean;
    size : number;
}

export interface IAuthCodeValidateResult {
    codeEnabled : boolean;
    codePassed? : boolean;
    error? : string;
    emailTemplate? : string;
    phoneTemplate? : string;
}

// generated-code-start
/* eslint-disable */
export const IAuthCodeRequestResult = globalThis.RegisterInterface(["status", "message", "skipped", "size"]);
export const IAuthCodeValidateResult = globalThis.RegisterInterface(["codeEnabled", "codePassed", "error", "emailTemplate", "phoneTemplate"]);
/* eslint-enable */
// generated-code-end
