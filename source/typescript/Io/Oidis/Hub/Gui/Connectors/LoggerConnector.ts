/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { BaseConnector as ServicesBaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { Loader } from "../Loader.js";

export class LoggerClient extends ServicesBaseConnector {

    public static getClient() : LoggerClient {
        const instance : LoggerClient =
            new LoggerClient(true, Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp"));
        LoggerClient.getClient = () : LoggerClient => {
            return instance;
        };
        return instance;
    }

    public async ProcessTrace($trace : ILoggerTrace) : Promise<void> {
        return this.asyncInvoke("ProcessTrace", $trace);
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Connectors.LoggerConnector";
        return namespaces;
    }
}
