/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import {
    AuthManagerConnector as Parent, IAuthFindGroupsOptions, IAuthFindOptions, IAuthFindUsersOptions,
    IModelListFilter,
    IModelListResult
} from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { Loader } from "../Loader.js";
import { Group } from "../Models/Group.js";
import { Organization } from "../Models/Organization.js";
import { User } from "../Models/User.js";

export class AuthManagerConnector extends Parent {

    public static getClient() : AuthManagerConnector {
        const instance : AuthManagerConnector =
            new AuthManagerConnector(true, Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp"));
        AuthManagerConnector.getClient = () : AuthManagerConnector => {
            return instance;
        };
        return instance;
    }

    public async FindUsers($idOrUserNameFilter : string | any, $options? : IAuthFindUsersOptions) : Promise<IModelListResult<User>> {
        return super.FindUsers($idOrUserNameFilter, $options);
    }

    public async FindGroups($idOrNameFilter : string | any, $options? : IAuthFindGroupsOptions) : Promise<IModelListResult<Group>> {
        return super.FindGroups($idOrNameFilter, $options);
    }

    public FindOrganizations($filter : string | any, $options? : IAuthFindOptions) : Promise<IModelListResult<Organization>> {
        return super.FindOrganizations($filter, $options);
    }

    public getSystemGroups() : Promise<ISystemGroups> {
        return this.asyncInvoke("getSystemGroupsInterface");
    }

    /// TODO: enable to have open args or at least possibility to specify array of groups
    public HasGroup($group : Group | string, $user : User | string = null) : Promise<boolean> {
        if (ObjectValidator.IsObject($group)) {
            $group = (<Group>$group).Id;
        }
        if (ObjectValidator.IsEmptyOrNull($user)) {
            $user = this.CurrentToken();
        } else if (ObjectValidator.IsObject($user)) {
            $user = (<User>$user).Id;
        }
        return this.asyncInvoke("HasGroup", $group, $user);
    }

    public getActiveGroupsList() : Promise<string[]> {
        return this.asyncInvoke("getActiveGroupsList");
    }

    public getOrganizationProfile($idOrOwnerId : string | User, $withData : boolean = false) : Promise<any> {
        if (ObjectValidator.IsObject($idOrOwnerId)) {
            $idOrOwnerId = <any>(<User>$idOrOwnerId).DefaultGroup;
        }
        return super.getOrganizationProfile(<string>$idOrOwnerId, $withData);
    }

    public getOrganizationChart($organizationId : string, $filter? : IModelListFilter) : Promise<IModelListResult<User>> {
        return super.getOrganizationChart($organizationId, $filter);
    }

    public async ExportAuthMethods() : Promise<IGroupsDump[]> {
        return this.asyncInvoke("ExportAuthMethods");
    }

    public async ImportAuthMethods($data : IGroupsDump[]) : Promise<void> {
        return this.asyncInvoke("ImportAuthMethods", $data);
    }

    public async getPermissionsReport() : Promise<IPermissionsReport> {
        return this.asyncInvoke("getPermissionsReport");
    }

    public async ResetDefaultGroups() : Promise<void> {
        return this.asyncInvoke("ResetDefaultGroups");
    }

    public async ResetDefaultAuthMethods() : Promise<void> {
        return this.asyncInvoke("ResetDefaultAuthMethods");
    }

    public async DeleteRedundantGroups() : Promise<void> {
        return this.asyncInvoke("DeleteRedundantGroups");
    }

    public async ResetCache() : Promise<void> {
        return this.asyncInvoke("ResetCache");
    }

    public async SyncSystemGroups() : Promise<void> {
        return this.asyncInvoke("forceSyncSystemGroups");
    }

    public async AuthenticateAdmin($token : string) : Promise<string> {
        return this.asyncInvoke("AuthenticateAdmin", StringUtils.getSha1($token));
    }

    public async RegisterAdmin($name : string, $pass? : string) : Promise<void> {
        return this.asyncInvoke("RegisterAdmin", $name, StringUtils.getSha1($pass));
    }

    public async Generate2FAKey() : Promise<I2FAKey> {
        return this.asyncInvoke("Generate2FAKey");
    }

    public Validate2FA($code : string, $secret? : string) : Promise<boolean> {
        return this.asyncInvoke("Validate2FA", $code, $secret);
    }

    public Has2FAActivated() : Promise<boolean> {
        return this.asyncInvoke("Has2FAActivated");
    }

    public getServiceAccounts() : Promise<IServiceAccounts> {
        return this.asyncInvoke("getServiceAccountsInterface");
    }

    public SwitchToAccount($account : User) : Promise<string> {
        return this.asyncInvoke("SwitchToAccount", $account);
    }
}

export interface ISystemGroups {
    Admin : Group;
    SysAdmin : Group;
    Agent : Group;
    OrganizationOwner : Group;
    Member : Group;
}

export interface IPermissionsReport {
    allDefaultMethods : string[];
    appMethods : string[];
    sysMethods : string[];
    features : string[];
    sysPermsRelicts : any;
    sysGroups : string[];
    usersForCheck : IPermissionsReportUsers[];
    groupsForCheck : IPermissionsReportGroups[];
    groupsCleanUpCandidates : string[];
    uniqueMethods : string[];
    defaultGroups : IDefaultGroupOwner[];
    redundantGroups : string[];
    groupsWithDuplicity : string[];
}

export interface IPermissionsReportUsers {
    id : string;
    name : string;
    email : string;
    active : boolean;
    deleted : boolean;
    customAuthMethods : string[];
}

export interface IPermissionsReportGroups {
    id : string;
    name : string;
    assignedTo : string[];
    isRedundant : boolean;
    hidden : boolean;
    deleted : boolean;
    customAuthMethods : string[];
}

export interface IGroupsDump {
    Id : string;
    AuthorizedMethods : string[];
}

export interface IDefaultGroupOwner {
    UserName : string;
    UserId : string;
    Id : string;
}

export interface I2FAKey {
    secret : string;
    url : string;
}

export interface IServiceAccounts {
    AgentsRegister : User;
    Contributor : User;
    Viewer : User;
}

// generated-code-start
/* eslint-disable */
export const ISystemGroups = globalThis.RegisterInterface(["Admin", "SysAdmin", "Agent", "OrganizationOwner", "Member"]);
export const IPermissionsReport = globalThis.RegisterInterface(["allDefaultMethods", "appMethods", "sysMethods", "features", "sysPermsRelicts", "sysGroups", "usersForCheck", "groupsForCheck", "groupsCleanUpCandidates", "uniqueMethods", "defaultGroups", "redundantGroups", "groupsWithDuplicity"]);
export const IPermissionsReportUsers = globalThis.RegisterInterface(["id", "name", "email", "active", "deleted", "customAuthMethods"]);
export const IPermissionsReportGroups = globalThis.RegisterInterface(["id", "name", "assignedTo", "isRedundant", "hidden", "deleted", "customAuthMethods"]);
export const IGroupsDump = globalThis.RegisterInterface(["Id", "AuthorizedMethods"]);
export const IDefaultGroupOwner = globalThis.RegisterInterface(["UserName", "UserId", "Id"]);
export const I2FAKey = globalThis.RegisterInterface(["secret", "url"]);
export const IServiceAccounts = globalThis.RegisterInterface(["AgentsRegister", "Contributor", "Viewer"]);
/* eslint-enable */
// generated-code-end
