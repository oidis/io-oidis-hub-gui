/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { IModelListFilter, IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { BaseConnector as ServicesBaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { IRegistryItem } from "../Interfaces/IRegistryContent.js";

export class RegistryConnector extends ServicesBaseConnector {

    public getAgents() : Promise<IRegistryItem[]> {
        return this.asyncInvoke("getAgents");
    }

    public getConfigs() : Promise<IRegistryItem[]> {
        return this.asyncInvoke("getConfigs");
    }

    public getUpdates() : Promise<IRegistryItem[]> {
        return this.asyncInvoke("getUpdates");
    }

    public getDownloads() : Promise<IRegistryItem[]> {
        return this.asyncInvoke("getDownloads");
    }

    public async getFileEntries($filter? : IModelListFilter) : Promise<IModelListResult<IRegistryItem>> {
        return this.asyncInvoke("getFileEntries", $filter);
    }

    public async UploadFile($data : IFileTransferProtocol) : Promise<boolean> {
        return this.asyncInvoke("UploadFile", $data);
    }

    public async RegisterFile($name : string) : Promise<void> {
        return this.asyncInvoke("RegisterFile", $name);
    }

    public async IsInDatabase() : Promise<boolean> {
        return this.asyncInvoke("IsInDatabase");
    }

    public async Reindex() : Promise<boolean> {
        return this.asyncInvoke("Reindex");
    }

    public async DeteleConfig($appName : string, $configName : string, $version : string) : Promise<void> {
        return this.asyncInvoke("DeteleConfig", $appName, $configName, $version);
    }

    public async FileEntryToFS(...$ids : string[]) : Promise<void> {
        return this.asyncInvoke("FileEntryToFS", ...$ids);
    }

    public async FileEntryToDB(...$ids : string[]) : Promise<void> {
        return this.asyncInvoke("FileEntryToDB", ...$ids);
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Connectors.RegistryConnector";
        namespaces[WebServiceClientType.WEB_SOCKETS] = "Io.Oidis.Hub.Connectors.RegistryConnector";
        return namespaces;
    }
}
