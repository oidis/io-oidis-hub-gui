/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { BaseConnector as ServicesBaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";

export class ReportsConnector extends ServicesBaseConnector {

    public getReportingApps() : Promise<string[]> {
        return this.asyncInvoke("getReportingApps");
    }

    public getReports($appId : string) : Promise<string[]> {
        return this.asyncInvoke("getReports", $appId);
    }

    public getReport($id : string) : Promise<string> {
        return this.asyncInvoke("getReport", $id);
    }

    public ProcessLogs($options? : ILogsSource) : Promise<ILogsRecords> {
        return this.asyncInvoke("ProcessLogs", $options);
    }

    public TestLogs() : Promise<void> {
        return this.asyncInvoke("TestLogs");
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Connectors.ReportsConnector";
        namespaces[WebServiceClientType.WEB_SOCKETS] = "Io.Oidis.Hub.Connectors.ReportsConnector";
        return namespaces;
    }
}

export interface ILogsSource {
    location? : string;
    data? : ILoggerTrace[] | string;
    levels? : LogLevel[];
    from? : number;
    to? : number;
    offset? : number;
    limit? : number;
    isAscending? : boolean;
    filter? : string;
}

export interface ILogsRecords {
    traces : ILoggerTrace[];
    offset : number;
    limit : number;
    size : number;
    counters : ILogsRecordsCounts;
}

export interface ILogsRecordsCounts {
    info : number;
    warning : number;
    error : number;
    debug : number;
}

// generated-code-start
/* eslint-disable */
export const ILogsSource = globalThis.RegisterInterface(["location", "data", "levels", "from", "to", "offset", "limit", "isAscending", "filter"]);
export const ILogsRecords = globalThis.RegisterInterface(["traces", "offset", "limit", "size", "counters"]);
export const ILogsRecordsCounts = globalThis.RegisterInterface(["info", "warning", "error", "debug"]);
/* eslint-enable */
// generated-code-end
