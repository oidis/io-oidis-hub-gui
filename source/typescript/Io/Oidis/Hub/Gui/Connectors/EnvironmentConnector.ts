/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IModelListFilter, IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { EnvironmentConnector as Parent } from "@io-oidis-services/Io/Oidis/Services/Connectors/EnvironmentConnector.js";
import { IConnectorInfo } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IConnectorInfo.js";
import { ILiveContentErrorPromise } from "@io-oidis-services/Io/Oidis/Services/Interfaces/ILiveContentPromise.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";
import { IFAQTopic } from "../DAO/FAQDao.js";
import { FeedbackType } from "../Enums/FeedbackType.js";
import { IReleaseNotesConfig } from "../Interfaces/IReleaseNotesConfig.js";
import { ISendmailConfiguration } from "../Interfaces/ISendmailConfiguration.js";
import { Loader } from "../Loader.js";
import { AppSettings } from "../Models/AppSettings.js";
import { IWhatsNewNote } from "../Models/WhatsNewDao.js";

export class EnvironmentConnector extends Parent {

    public static getInstanceSingleton() : EnvironmentConnector {
        const instance : EnvironmentConnector =
            new this(true, Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp"));
        this.getInstanceSingleton = () : EnvironmentConnector => {
            return instance;
        };
        return instance;
    }

    public async SendFeedback($email : string, $category : FeedbackType, $message : string, $uuid : string = null,
                              $isPublic : boolean = false) : Promise<boolean> {
        return this.asyncInvoke("SendFeedback", $email, $category, $message, $uuid, $isPublic);
    }

    public getSendMailConfig() : Promise<ISendmailConfiguration> {
        return this.asyncInvoke("getSendMailConfig");
    }

    public setSendMailConfig($config : ISendmailConfiguration) : Promise<void> {
        return this.asyncInvoke("setSendMailConfig", $config);
    }

    public getReleaseNotesConfig() : Promise<IReleaseNotesConfig> {
        return this.asyncInvoke("getReleaseNotesConfig");
    }

    public getReleaseNotesFor($version : string) : Promise<IWhatsNewNote> {
        return this.asyncInvoke("getReleaseNotesFor", $version);
    }

    public async getFAQTopics() : Promise<IFAQTopic[]> {
        return this.asyncInvoke("getFAQTopics");
    }

    public getEventLoopRecords($type : EventLoopRecordType, $filter : IModelListFilter = null) : Promise<IModelListResult> {
        return this.asyncInvoke("getEventLoopRecords", $type, $filter);
    }

    public setEventLoopRecords($uuid : string, $type : EventLoopRecordType, $action : EventLoopActionType) : Promise<void> {
        return this.asyncInvoke("setEventLoopRecords", $uuid, $type, $action);
    }

    public CleanEventLoopRecords() : Promise<void> {
        return this.asyncInvoke("CleanEventLoopRecords");
    }

    public async getBuildTime() : Promise<number> {
        return this.asyncInvoke("getBuildTime");
    }

    public async getUpTime() : Promise<number> {
        return this.asyncInvoke("getUpTime");
    }

    public async PushNotification($event : IPushNotificationEvent) : Promise<void> {
        return this.asyncInvoke("PushNotification", $event);
    }

    public AttachToPushNotifications() : IPushNotificationsPromise {
        const callbacks : any = {
            onChange($event : IPushNotificationEvent) : any {
                // declare default callback
            },
            onError: null,
            then() : any {
                // declare default callback
            }
        };
        LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(), "AttachToPushNotifications")
            .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                this.errorHandler(callbacks.onError, $error, $args);
            })
            .Then(($result : any) : void => {
                if (!ObjectValidator.IsEmptyOrNull($result)) {
                    if (ObjectValidator.IsObject($result) && ObjectValidator.IsSet($result.type)) {
                        if ($result.type === EventType.ON_CHANGE || $result.type === EventType.ON_START) {
                            callbacks.onChange($result.data);
                        } else {
                            callbacks.then();
                        }
                    } else {
                        LogIt.Debug("Incorrect result received: {0}", $result);
                        this.errorHandler(callbacks.onError, new ErrorEventArgs("Unrecognized type received"), []);
                    }
                }
            });
        const promise : IPushNotificationsPromise = {
            OnChange: ($callback : ($event : IPushNotificationEvent) => void) : IPushNotificationsPromise => {
                callbacks.onChange = $callback;
                return promise;
            },
            OnError : ($callback : ($args : ErrorEventArgs) => void) : IPushNotificationsPromise => {
                callbacks.onError = $callback;
                return promise;
            },
            Then    : ($callback : () => void) : void => {
                callbacks.then = $callback;
            }
        };
        return promise;
    }

    public async Exit($code : number = 0) : Promise<void> {
        return this.asyncInvoke("Exit", $code);
    }

    public async getConnectorsInfo($filter? : string, $options? : IModelListFilter) : Promise<IModelListResult<IConnectorInfo>> {
        return this.asyncInvoke("getConnectorsInfo", $filter, $options);
    }

    public async ResetConfigsCache() : Promise<void> {
        return this.asyncInvoke("ResetConfigsCache");
    }

    public getAppSettings() : Promise<AppSettings> {
        return this.asyncInvoke("getAppSettings");
    }

    public SaveAppSettings($settings : AppSettings) : Promise<void> {
        return this.asyncInvoke("SaveAppSettings", $settings);
    }

    public async SyncAppSettings($resetDB : boolean = false) : Promise<void> {
        return this.asyncInvoke("SyncAppSettings", $resetDB);
    }

    public async getProjectConfig($key : any) : Promise<string> {
        return this.asyncInvoke("getProjectConfig", $key);
    }
}

export class EventLoopRecordType extends BaseEnum {
    public static readonly EMAIL_RECORD : string = "EmailRecord";
    public static readonly WEBHOOK_RECORD : string = "WebHookRecord";
}

export interface IWebhookEventRecord {
    uuid : string;
    data : any;
    type : EventLoopRecordType;
}

export class EventLoopActionType extends BaseEnum {
    public static readonly DELETE : string = "delete";
    public static readonly ACTIVATE : string = "activate";
}

export interface IEmailEventRecord {
    uuid : string;
    data : any;
}

export interface IPushNotificationEvent {
    type : string;
    data? : any;
}

export interface IPushNotificationsPromise extends ILiveContentErrorPromise {
    OnChange($callback : ($event : IPushNotificationEvent) => void) : IPushNotificationsPromise;

    Then($callback : () => void) : void;
}

// generated-code-start
export const IWebhookEventRecord = globalThis.RegisterInterface(["uuid", "data", "type"]);
export const IEmailEventRecord = globalThis.RegisterInterface(["uuid", "data"]);
export const IPushNotificationEvent = globalThis.RegisterInterface(["type", "data"]);
export const IPushNotificationsPromise = globalThis.RegisterInterface(["OnChange", "Then"], <any>ILiveContentErrorPromise);
// generated-code-end
