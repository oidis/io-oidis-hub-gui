/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { Loader } from "../Loader.js";

export class SystemLogConnector extends BaseConnector {

    public static getClient() : SystemLogConnector {
        const instance : SystemLogConnector =
            new SystemLogConnector(true, Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp"));
        SystemLogConnector.getClient = () : SystemLogConnector => {
            return instance;
        };
        return instance;
    }

    public async getLogsList($filter : ISystemLogFilter = null) : Promise<IModelListResult> {
        return this.asyncInvoke("getLogsList", $filter);
    }

    public async Clean() : Promise<number> {
        return this.asyncInvoke("Clean");
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.LogProcessor.SystemLog";
        return namespaces;
    }
}

export interface ISystemLogTrace {
    message : string;
    onBehalf? : string;
    traceBack? : string;
    level? : LogLevel;
    tags? : string[];
    token? : string;
}

export interface ISystemLogFilter {
    date : number;
    traceBack? : string;
    tags? : string[];
    limit? : number;
    offset? : number;
    ascending? : boolean;
}

// generated-code-start
export const ISystemLogTrace = globalThis.RegisterInterface(["message", "onBehalf", "traceBack", "level", "tags", "token"]);
export const ISystemLogFilter = globalThis.RegisterInterface(["date", "traceBack", "tags", "limit", "offset", "ascending"]);
// generated-code-end
