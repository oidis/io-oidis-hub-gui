/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { BaseConnector as Parent, IFetchProgress } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { Loader } from "../Loader.js";

export class WebhooksManagerConnector extends Parent {

    public static getClient() : WebhooksManagerConnector {
        const instance : WebhooksManagerConnector =
            new WebhooksManagerConnector(true, Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp"));
        this.getClient = () : WebhooksManagerConnector => {
            return instance;
        };
        return instance;
    }

    public async getWebhooksInfo() : Promise<IWebhooksInfo> {
        return this.asyncInvoke("getWebhooksInfo");
    }

    public async setAdapterState($type : string, $active : boolean) : Promise<void> {
        return this.asyncInvoke("setAdapterState", $type, $active);
    }

    public async ReactivateAdapterRecords($type : string) : Promise<void> {
        return this.asyncInvoke("ReactivateAdapterRecords", $type);
    }

    public async LoopStart($fetchTick : number = null) : Promise<void> {
        return this.asyncInvoke("StartLoop", $fetchTick);
    }

    public async LoopStop() : Promise<void> {
        return this.asyncInvoke("StopLoop");
    }

    public async MasterStop($active : boolean) : Promise<boolean> {
        return this.asyncInvoke("MasterStop", $active);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.HUB_CONNECTOR;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Webhooks.WebhooksManagerConnector";
        return namespaces;
    }
}

export enum IWebhookLoopStatus {
    /**
     * Loop is idle and waiting for another tick.
     */
    Idle,
    /**
     * Processing already received webhooks.
     */
    Running,
    /**
     * Stopped, newly received webhook can start the processing loop it again.
     */
    Stopped
}

export interface IAdapterInfo {
    name : string;
    active : boolean;
    processed : number;
    registered : number;
}

export interface IWebhooksInfo {
    enabled : boolean;
    masterStopActive : boolean;
    adapters : IAdapterInfo[];
    tick : number;
    status : IWebhookLoopStatus;
    bucketSize : number;
}

// generated-code-start
export const IAdapterInfo = globalThis.RegisterInterface(["name", "active", "processed", "registered"]);
export const IWebhooksInfo = globalThis.RegisterInterface(["enabled", "masterStopActive", "adapters", "tick", "status", "bucketSize"]);
// generated-code-end
