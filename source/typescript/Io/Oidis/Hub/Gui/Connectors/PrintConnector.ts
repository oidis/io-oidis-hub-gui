/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/WebServiceClientType.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { IPrintAPI } from "../Bindings/PrintBinding.js";
import { IExportStatus } from "../Interfaces/IExportStatus.js";
import { IPDFOptions } from "../Interfaces/IPDFOptions.js";
import { Loader } from "../Loader.js";

export class PrintConnector extends BaseConnector {

    public static getClient() : PrintConnector {
        const instance : PrintConnector =
            new PrintConnector(true, Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp"));
        PrintConnector.getClient = () : PrintConnector => {
            return instance;
        };
        return instance;
    }

    public async GeneratePDF($input : string, $options : IPDFOptions = null) : Promise<IExportStatus> {
        return this.asyncInvoke("GeneratePDF", $input, $options);
    }

    public async getPrintOptions($id : string) : Promise<IPrintAPI> {
        return this.asyncInvoke("getPrintOptions", $id);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.WEB_SOCKETS;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.WEB_SOCKETS] = "Io.Oidis.Hub.Utils.PrintManager";
        return namespaces;
    }
}
