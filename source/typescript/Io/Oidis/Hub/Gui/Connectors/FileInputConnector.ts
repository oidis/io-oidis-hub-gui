/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { Loader } from "../Loader.js";

export class FileInputConnector extends BaseConnector {

    public static getClient() : FileInputConnector {
        const instance : FileInputConnector =
            new FileInputConnector(true, Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp"));
        FileInputConnector.getClient = () : FileInputConnector => {
            return instance;
        };
        return instance;
    }

    public async Upload($data : IFileTransferProtocol) : Promise<boolean> {
        return this.asyncInvoke("Upload", $data);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.HUB_CONNECTOR;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.IOApi.Handlers.FileInputHandler";
        return namespaces;
    }
}
