/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BaseViewer.js";
import { DashboardPage } from "../../Pages/DashboardPage.js";
import { BaseHubPageViewerArgs } from "../../ViewersArgs/Pages/BaseHubPageViewerArgs.js";

export abstract class DashboardPageViewer extends BaseViewer {

    /* dev:start */
    protected static getTestViewerArgs() : BaseHubPageViewerArgs {
        const args : BaseHubPageViewerArgs = new BaseHubPageViewerArgs();
        args.HeaderText("Registry");
        args.FooterText("Copyright 2019 Oidis");

        return args;
    }

    /* dev:end */

    protected constructor($args? : BaseHubPageViewerArgs) {
        super($args);
        this.setInstance(new DashboardPage());
    }

    public getInstance() : DashboardPage {
        return <DashboardPage>super.getInstance();
    }

    public ViewerArgs($args? : BaseHubPageViewerArgs) : BaseHubPageViewerArgs {
        return <BaseHubPageViewerArgs>super.ViewerArgs($args);
    }

    protected argsHandler($instance : any, $args : BaseHubPageViewerArgs) : void {
        $instance.header.Text($args.HeaderText());
        $instance.footer.Text($args.FooterText());
    }

    protected testImplementation($instance? : any, $args? : any) : void {
        this.addTestButton("test", () : void => {
            LogIt.Debug("Some test");
        });
    }
}
