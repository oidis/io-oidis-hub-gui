/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BaseViewer.js";
import { SSOPage } from "../../Pages/SSOPage.js";
import { BaseHubPageViewerArgs } from "../../ViewersArgs/Pages/BaseHubPageViewerArgs.js";

export class SSOPageViewer extends BaseViewer {

    /* dev:start */
    protected static getTestViewerArgs() : BaseHubPageViewerArgs {
        const args : BaseHubPageViewerArgs = new BaseHubPageViewerArgs();
        args.HeaderText("Authentication");
        args.DefaultContent("Loading data ...");
        args.FooterText("Copyright 2019-2021 Oidis");

        return args;
    }

    /* dev:end */

    constructor($args? : BaseHubPageViewerArgs) {
        super($args);
        this.setInstance(new SSOPage());
    }

    public getInstance() : SSOPage {
        return <SSOPage>super.getInstance();
    }

    /* dev:start */
    protected testImplementation($instance : SSOPage) : string {
        // $instance.StyleClassName("TestCss");

        // $instance.logIn.getEvents().setOnClick(() : void => {
        //     if (ObjectValidator.IsEmptyOrNull($instance.userName.Value()) ||
        //         ObjectValidator.IsEmptyOrNull($instance.password.Value())) {
        //         $instance.setStatus("Please specify user name and password");
        //     }
        // });

        return "" +
            "<style>" +
            ".TestCss .SSOPage {}" +
            "</style>";
    }

    /* dev:end */
}
