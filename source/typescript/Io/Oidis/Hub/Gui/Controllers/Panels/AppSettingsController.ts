/*! ******************************************************************************************************** *
 *
 * Copyright 2023 NXP
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { BasePanelController } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanelController.js";
import { EnvironmentConnector } from "../../Connectors/EnvironmentConnector.js";
import { AppSettings } from "../../Models/AppSettings.js";
import { AppSettingsPanel } from "../../Panels/Dashboard/AppSettingsPanel.js";

@Resolver("/appSettings")
export class AppSettingsController<Panel extends AppSettingsPanel> extends BasePanelController<Panel> {
    protected readonly dataProcessor : EnvironmentConnector;

    constructor($instance : Panel) {
        super($instance);
        this.dataProcessor = EnvironmentConnector.getInstanceSingleton();
    }

    public async FetchData() : Promise<void> {
        const instance : AppSettingsPanel = this.getInstanceOwner();

        const settings : AppSettings = await this.dataProcessor.getAppSettings();
        instance.mailEngine.Value(settings.Sendmail.Enabled);
        instance.adminsList.Value(settings.Sendmail.AdminsList.join(","));
        instance.blackList.Value(settings.Sendmail.BlackList.join(","));
        instance.groupAccess.Value(settings.DefaultGroupAccess.join(","));
        instance.accessMethods.Value(settings.DefaultAccessibleMethods.join(","));
    }

    protected async resolver($instance? : AppSettingsPanel) : Promise<void> {
        EventsBinding.SingleClick($instance.saveSettings, async () : Promise<void> => {
            try {
                await this.dataProcessor.SaveAppSettings(this.processSettings($instance, await this.dataProcessor.getAppSettings()));
                await this.FetchData();
            } catch (ex) {
                LogIt.Error(ex);
            }
        });
        EventsBinding.SingleClick($instance.resetSettings, async () : Promise<void> => {
            try {
                await this.dataProcessor.SyncAppSettings(true);
                await this.FetchData();
            } catch (ex) {
                LogIt.Error(ex);
            }
        });
    }

    protected processSettings($instance : AppSettingsPanel, $model : AppSettings) : AppSettings {
        $model.Sendmail.Enabled = $instance.mailEngine.Value();
        $model.Sendmail.AdminsList = this.inputToArray($instance.adminsList.Value());
        $model.Sendmail.BlackList = this.inputToArray($instance.blackList.Value());
        $model.DefaultGroupAccess = this.inputToArray($instance.groupAccess.Value());
        $model.DefaultAccessibleMethods = this.inputToArray($instance.accessMethods.Value());
        return $model;
    }

    protected inputToArray($value : string) : string[] {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            return StringUtils.Split($value, ",");
        }
        return [];
    }
}
