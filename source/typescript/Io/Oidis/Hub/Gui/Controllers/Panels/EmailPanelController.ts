/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { BasePanelController } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanelController.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { ReportServiceConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/ReportServiceConnector.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { AuthManagerConnector } from "../../Connectors/AuthManagerConnector.js";
import {
    EnvironmentConnector,
    EventLoopActionType,
    EventLoopRecordType,
    IEmailEventRecord
} from "../../Connectors/EnvironmentConnector.js";
import { ISendmailConfiguration } from "../../Interfaces/ISendmailConfiguration.js";
import { Loader } from "../../Loader.js";
import { DashboardPage } from "../../Pages/DashboardPage.js";
import { EmailPanel } from "../../Panels/Dashboard/EmailPanel.js";
import { EventLoopRecord } from "../../UserControls/EventLoopTable.js";

@Resolver("/email")
export class EmailPanelController extends BasePanelController {
    private env : EnvironmentConnector;
    private report : ReportServiceConnector;

    constructor($instance : EmailPanel) {
        super($instance);
        const selfConfig : string = Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp");
        this.env = new EnvironmentConnector(true, selfConfig);
        this.report = new ReportServiceConnector(true, selfConfig);
    }

    public async FetchData() : Promise<void> {
        const instance : EmailPanel = this.getInstanceOwner();
        const config : ISendmailConfiguration = await this.env.getSendMailConfig();
        instance.serviceType.Value(config.serviceType);
        instance.user.Value(config.user);
        instance.pass.Value("");
        instance.location.Value(config.location);
        instance.port.Value(config.port + "");
        instance.secure.Value(config.secure);
        instance.adminsList.Value(config.admins.join(","));
        instance.blackList.Value(config.blackList.join(","));
        instance.fetchTick.Value(config.fetchTick + "");
        await instance.inactiveTable.ShowData();
    }

    public async AccessValidator() : Promise<boolean> {
        return AuthManagerConnector.getClient().IsAuthorizedFor(EmailPanelController.ClassName());
    }

    protected getInstanceOwner() : EmailPanel {
        return <EmailPanel>super.getInstanceOwner();
    }

    protected async resolver($instance : EmailPanel) : Promise<void> {
        $instance.send.getEvents().setOnClick(async () : Promise<void> => {
            const config : ISendmailConfiguration = <any>{
                location   : $instance.location.Value(),
                port       : StringUtils.ToInteger($instance.port.Value()),
                secure     : $instance.secure.Value(),
                serviceType: $instance.serviceType.Value(),
                user       : $instance.user.Value()
            };
            if (!ObjectValidator.IsEmptyOrNull($instance.pass.Value())) {
                config.pass = $instance.pass.Value();
            }
            await this.env.setSendMailConfig(config);
            this.report.SendMail(
                $instance.to.Value(),
                $instance.subject.Value(),
                $instance.body.Value(),
                [], "", "",
                $instance.from.Value())
                .OnError(($error : ErrorEventArgs) : void => {
                    LogIt.Error($error.Message());
                    (<DashboardPage>$instance.Parent()).ShowMessage("Failed to send email", ToastType.ERROR);
                })
                .Then(async ($status : boolean) : Promise<void> => {
                    if ($status) {
                        (<DashboardPage>$instance.Parent()).ShowMessage("Email send successfully", ToastType.SUCCESS);
                    } else {
                        (<DashboardPage>$instance.Parent()).ShowMessage("Failed to send email", ToastType.ERROR);
                    }
                });
        });
        $instance.inactiveTable.BindData(async ($offset : number, $limit : number) : Promise<IModelListResult> => {
            return this.env.getEventLoopRecords(EventLoopRecordType.EMAIL_RECORD, {
                limit : $limit,
                offset: $offset
            });
        }, async ($data : IEmailEventRecord, $item : EventLoopRecord) : Promise<void> => {
            $item.Data($data.uuid);
            $item.recordId.Text($data.uuid);
            $item.recordData.Text(JSON.stringify($data.data));
        }, async ($item : EventLoopRecord) : Promise<void> => {
            EventsBinding.SingleClick($item.delete, async () : Promise<void> => {
                await this.env.setEventLoopRecords($item.Data(), EventLoopRecordType.EMAIL_RECORD, EventLoopActionType.DELETE);
                await $instance.inactiveTable.ShowData(true);
            });
            EventsBinding.SingleClick($item.activate, async () : Promise<void> => {
                await this.env.setEventLoopRecords($item.Data(), EventLoopRecordType.EMAIL_RECORD, EventLoopActionType.ACTIVATE);
                await $instance.inactiveTable.ShowData();
            });
        });
    }
}
