/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { BasePanelController } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanelController.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import {
    EnvironmentConnector,
    EventLoopActionType,
    EventLoopRecordType,
    IWebhookEventRecord
} from "../../Connectors/EnvironmentConnector.js";
import { IAdapterInfo, IWebhookLoopStatus, IWebhooksInfo, WebhooksManagerConnector } from "../../Connectors/WebhooksManagerConnector.js";
import { WebhooksPanel } from "../../Panels/Dashboard/WebhooksPanel.js";
import { EventLoopRecord } from "../../UserControls/EventLoopTable.js";
import { WebhookAdapterTableRecord } from "../../UserControls/WebhookAdapterTable.js";

@Resolver("/webhooks")
export class WebhooksPanelController extends BasePanelController<WebhooksPanel> {
    private client : WebhooksManagerConnector;
    private autoFetchTimer : number;
    private countDownIndex : number;
    private env : EnvironmentConnector;
    private tablesInitiated : boolean;

    constructor($instance : WebhooksPanel) {
        super($instance);
        this.client = WebhooksManagerConnector.getClient();
        this.env = EnvironmentConnector.getInstanceSingleton();
        this.tablesInitiated = false;
        this.autoFetchTimer = null;
        this.countDownIndex = 0;
    }

    public async FetchData() : Promise<void> {
        await this.autoFetch(true);
    }

    protected async resolver($instance? : WebhooksPanel) : Promise<void> {
        $instance.refreshRate.Value("10");
        EventsBinding.SingleClick($instance.refresh, async () : Promise<void> => {
            if ($instance.refresh.Value()) {
                await this.autoFetch();
            }
        });
        EventsBinding.SingleClick($instance.stop, async () : Promise<void> => {
            await this.client.LoopStop();
            await this.FetchData();
        }, 5000);
        EventsBinding.SingleClick($instance.start, async () : Promise<void> => {
            await this.client.LoopStart(StringUtils.ToInteger($instance.fetchTick.Value()));
            $instance.getEvents().FireAsynchronousMethod(async () : Promise<void> => {
                await this.FetchData();
            }, 1000);
        }, 5000);
        EventsBinding.SingleClick($instance.restart, async () : Promise<void> => {
            $instance.stop.Enabled(false);
            await this.client.LoopStop();
            await this.client.LoopStart(StringUtils.ToInteger($instance.fetchTick.Value()));
            $instance.getEvents().FireAsynchronousMethod(async () : Promise<void> => {
                await this.FetchData();
                $instance.stop.Enabled(true);
            }, 1000);
        }, 5000);
        EventsBinding.SingleClick($instance.masterStop, async () : Promise<void> => {
            if (await this.client.MasterStop(true)) {
                await this.FetchData();
                $instance.start.Visible(false);
                $instance.masterStop.Visible(false);
                $instance.disableMasterStop.Visible(true);
            } else {
                $instance.masterStop.Visible(true);
                $instance.disableMasterStop.Visible(false);
            }
        }, 5000);
        EventsBinding.SingleClick($instance.disableMasterStop, async () : Promise<void> => {
            if (await this.client.MasterStop(false)) {
                $instance.start.Visible(true);
                $instance.stop.Visible(false);
                $instance.masterStop.Visible(true);
                $instance.disableMasterStop.Visible(false);
            } else {
                $instance.masterStop.Visible(false);
                $instance.disableMasterStop.Visible(true);
            }
        }, 5000);
        EventsBinding.SingleClick($instance.reloadTables, async () : Promise<void> => {
            await $instance.hooks.ShowData(true);
            await $instance.adapters.ShowData(true);
        });
        EventsBinding.SingleClick($instance.removeOldRecords, async () : Promise<void> => {
            await this.env.CleanEventLoopRecords();
            await $instance.hooks.ShowData(true);
            await $instance.adapters.ShowData(true);
        });

        $instance.hooks.BindData(async ($offset : number, $limit : number) : Promise<IModelListResult> => {
            return this.env.getEventLoopRecords(EventLoopRecordType.WEBHOOK_RECORD, {
                limit : $limit,
                offset: $offset
            });
        }, async ($data : IWebhookEventRecord, $item : EventLoopRecord) : Promise<void> => {
            $item.Data($data.uuid);
            $item.recordId.Text($data.uuid);
            $item.recordData.Text(JSON.stringify($data.data));
        }, async ($item : EventLoopRecord) : Promise<void> => {
            EventsBinding.SingleClick($item.delete, async () : Promise<void> => {
                await this.env.setEventLoopRecords($item.Data(), EventLoopRecordType.WEBHOOK_RECORD, EventLoopActionType.DELETE);
                await $instance.hooks.ShowData(true);
                this.countDownIndex = 0;
            });
            EventsBinding.SingleClick($item.activate, async () : Promise<void> => {
                await this.env.setEventLoopRecords($item.Data(), EventLoopRecordType.WEBHOOK_RECORD, EventLoopActionType.ACTIVATE);
                await $instance.hooks.ShowData();
                this.countDownIndex = 0;
            });
        });

        $instance.adapters.BindData(async () : Promise<IModelListResult> => {
            const data : IWebhooksInfo = await this.client.getWebhooksInfo();
            return {data: data.adapters, size: data.adapters.length, limit: data.adapters.length, offset: 0};
        }, async ($data : IAdapterInfo, $item : WebhookAdapterTableRecord) : Promise<void> => {
            $item.Data($data);
            $item.type.Text($data.name);
            $item.status.Text(JSON.stringify({active: $data.active, processed: $data.processed, registered: $data.registered}));
            $item.deactivate.Visible($data.active);
            $item.activate.Visible(!$data.active);
        }, async ($item : WebhookAdapterTableRecord) : Promise<void> => {
            EventsBinding.SingleClick($item.activate, async () : Promise<void> => {
                await this.client.setAdapterState($item.Data().name, true);
                await $instance.adapters.ShowData();
            });
            EventsBinding.SingleClick($item.deactivate, async () : Promise<void> => {
                await this.client.setAdapterState($item.Data().name, false);
                await $instance.adapters.ShowData();
            });
            EventsBinding.SingleClick($item.reactivateRecords, async () : Promise<void> => {
                await this.client.ReactivateAdapterRecords($item.Data().name);
                await $instance.hooks.ShowData();
                this.countDownIndex = 0;
            });
        });
    }

    private async autoFetch($force : boolean = false) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull(this.autoFetchTimer)) {
            clearTimeout(this.autoFetchTimer);
        }
        const instance : WebhooksPanel = this.getInstanceOwner();
        const interval : number = StringUtils.ToInteger(instance.refreshRate.Value());
        instance.countDown.Max(interval);
        if (instance.Visible() && (this.countDownIndex === interval || $force)) {
            const state : IWebhooksInfo = await this.client.getWebhooksInfo();
            instance.fetchTick.Value(state.tick + "");
            instance.bucketSize.Value(state.bucketSize + "");
            switch (state.status) {
            case IWebhookLoopStatus.Idle:
                instance.status.Content("IDLE");
                instance.stop.Visible(true);
                instance.start.Visible(false);
                break;
            case IWebhookLoopStatus.Running:
                instance.status.Content("RUNNING");
                instance.stop.Visible(true);
                instance.start.Visible(false);
                break;
            case IWebhookLoopStatus.Stopped:
                instance.status.Content("STOPPED");
                instance.stop.Visible(false);
                instance.start.Visible(true);
                break;
            default:
                instance.status.Content(IWebhookLoopStatus[state.status]);
                (<any>instance.status).setAttribute(true, "btn-info");
                break;
            }
            (<any>instance.status).setAttribute(state.status === IWebhookLoopStatus.Idle, "btn-warning");
            (<any>instance.status).setAttribute(state.status === IWebhookLoopStatus.Running, "btn-success");
            (<any>instance.status).setAttribute(state.status === IWebhookLoopStatus.Stopped, "btn-danger");

            await instance.adapters.ShowData();
            await instance.hooks.ShowData();
            this.countDownIndex = 0;
        }
        if (instance.refresh.Value() && interval > 0) {
            this.autoFetchTimer = this.getEventsManager().FireAsynchronousMethod(async () : Promise<void> => {
                await this.autoFetch();
            }, true, 1000);
            this.countDownIndex++;
            instance.countDown.Visible(true);
            instance.countDown.Value(this.countDownIndex);
        } else {
            instance.countDown.Visible(false);
        }
    }
}
