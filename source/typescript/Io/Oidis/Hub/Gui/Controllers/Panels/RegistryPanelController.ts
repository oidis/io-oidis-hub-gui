/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { FileHandler } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/Handlers/FileHandler.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { FileUploadBinding } from "../../Bindings/FileUploadBinding.js";
import { EnvironmentConnector } from "../../Connectors/EnvironmentConnector.js";
import { RegistryConnector } from "../../Connectors/RegistryConnector.js";
import { IRegistryItem } from "../../Interfaces/IRegistryContent.js";
import { DashboardPage } from "../../Pages/DashboardPage.js";
import { RegistryPanel } from "../../Panels/Dashboard/RegistryPanel.js";
import { BasePanelController } from "../../Primitives/BasePanelController.js";
import { RegistryItem } from "../../UserControls/RegistryItemsTable.js";

/// TODO: enable edit dialog for registry items
/// TODO: enable private app packages -> package download internal feature
@Resolver("/registry")
export class RegistryPanelController extends BasePanelController {
    private registry : RegistryConnector;
    private readonly projects : string[];
    private readonly versions : string[];
    private readonly platforms : string[];
    private updates : IRegistryItem[];
    private configs : IRegistryItem[];
    private filter : any;
    private readonly persistence : IPersistenceHandler;

    constructor($instance : RegistryPanel) {
        super($instance);
        this.registry = new RegistryConnector(true, this.getHttpManager().CreateLink("/connector.config.jsonp"));

        this.projects = [];
        this.versions = [];
        this.platforms = [];
        this.updates = [];
        this.configs = [];
        this.filter = {
            platform: "",
            project : "",
            text    : "",
            version : ""
        };
        this.persistence = PersistenceFactory.getPersistence(this.getClassName());
        const filter : string = this.persistence.Variable(PersistenceType.FILTER);
        if (!ObjectValidator.IsEmptyOrNull(filter)) {
            this.filter = filter;
        }
    }

    public async FetchData() : Promise<void> {
        await this.fetchConfigs();
        await this.fetchUpdates();
        await this.fetchDownloads();
        await this.getInstanceOwner().fileEntries.ShowData(true);
        this.updateMenu();
    }

    protected getInstanceOwner() : RegistryPanel {
        return <RegistryPanel>super.getInstanceOwner();
    }

    protected async beforeLoad($instance : RegistryPanel) : Promise<void> {
        if (ObjectValidator.IsEmptyOrNull(this.persistence.Variable(PersistenceType.TAB))) {
            this.persistence.Variable(PersistenceType.TAB, $instance.SelectedTab());
        } else {
            $instance.SelectedTab(this.persistence.Variable(PersistenceType.TAB));
        }
        if (!FileHandler.IsSupported()) {
            LogIt.Error("Missing required FileHandler API, so upload is not possible");
            $instance.uploadGroup.Visible(false);
        } else if (!this.context.IsAdmin) {
            $instance.uploadGroup.Visible(false);
        } else {
            const fileUpload : FileUploadBinding = new FileUploadBinding();
            const parent : DashboardPage = <DashboardPage>$instance.Parent();
            fileUpload.Bind({
                button  : $instance.upload,
                field   : $instance.fileSource,
                progress: $instance.uploadProgress
            }, {
                onData          : async ($files : FileList) : Promise<void> => {
                    if ($files.length === 1) {
                        await fileUpload.Upload("RegistryFile");
                        await this.registry.RegisterFile($files[0].name);
                    } else {
                        parent.ShowMessage("Only single file upload is supported", ToastType.ERROR);
                    }
                    await this.fetchDownloads();
                },
                onEmptySelection: () : void => {
                    parent.ShowMessage("File for upload must be selected", ToastType.ERROR);
                },
                onProgressChange: ($status : boolean) : void => {
                    $instance.uploadGroup.Visible(!$status);
                    $instance.progressEnvelop.Visible($status);
                }
            });
        }

        $instance.fileEntries.PagesLimit(5);
        $instance.fileEntries.ItemsPerPage(25);
    }

    protected async resolver($instance : RegistryPanel) : Promise<void> {
        $instance.ascending.Value(this.persistence.Variable(PersistenceType.ASCENDING));

        this.updateMenu();
        [$instance.configsTab, $instance.updatesTab, $instance.downloadsTab, $instance.filesTab].forEach(($element : GuiCommons) : void => {
            $element.getEvents().setOnClick(() : void => {
                this.persistence.Variable(PersistenceType.TAB, $instance.SelectedTab());
            });
        });
        $instance.clear.getEvents().setOnClick(() : void => {
            this.filter.project = "";
            this.filter.version = "";
            this.filter.platform = "";
            this.filter.text = "";
            $instance.projectName.Text("all");
            $instance.version.Text("all");
            $instance.platform.Text("all");
            $instance.searchFilter.Value("");
            this.fillUpdatesTable();
            this.fillConfigsTable();
            this.savePersistence();
        });
        $instance.searchFilter.getEvents().setEvent(EventType.ON_KEY_PRESS, () : void => {
            this.filter.text = $instance.searchFilter.Value();
            if (StringUtils.Length(this.filter.text) >= 3) {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.fillUpdatesTable();
                    this.fillConfigsTable();
                    this.savePersistence();
                }, true, 100);
            }
        });
        [$instance.ascending, $instance.refresh].forEach(($element : GuiCommons) : void => {
            $element.getEvents().setOnClick(async () : Promise<void> => {
                await this.FetchData();
                this.savePersistence();
            });
        });

        if (!await this.registry.IsInDatabase()) {
            $instance.reindexHolder.Visible(true);
            $instance.reindex.getEvents().setOnClick(async () : Promise<void> => {
                $instance.reindex.Enabled(false);
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    $instance.reindexHolder.Visible(false);
                }, 500);
                if (await this.registry.Reindex()) {
                    const parent : DashboardPage = <DashboardPage>$instance.Parent();
                    parent.ShowMessage("Reindex successfully completed", ToastType.SUCCESS);
                } else {
                    LogIt.Error("Failed to reindex registry items.");
                }
            });
        }
        EventsBinding.SingleClick($instance.resetConfigs, async () : Promise<void> => {
            await EnvironmentConnector.getInstanceSingleton().ResetConfigsCache();
        });
        $instance.fileEntries.BindData(($offset : number, $limit : number) : Promise<IModelListResult> => {
            return this.registry.getFileEntries({
                limit : $limit,
                offset: $offset
            });
        }, async ($data : IRegistryItem, $item : RegistryItem) : Promise<void> => {
            $item.type.Text($data.type);
            $item.info.Text($data.info + ", Path: " + $data.link);
            $item.Data($data);
        }, async ($item : RegistryItem) : Promise<void> => {
            EventsBinding.SingleClick($item.convert, async () : Promise<void> => {
                if ($item.Data().type === "Blob") {
                    await this.registry.FileEntryToFS($item.Data().name);
                } else {
                    await this.registry.FileEntryToDB($item.Data().name);
                }
                await $instance.fileEntries.ShowData();
            });
        });
    }

    private async fetchConfigs() : Promise<void> {
        this.configs = await this.registry.getConfigs();
        if (!this.getInstanceOwner().ascending.Value()) {
            this.configs.reverse();
        }
        this.configs.forEach(($item : IRegistryItem) : void => {
            if (!this.projects.includes($item.name)) {
                this.projects.push($item.name);
            }
            if (!this.versions.includes($item.version)) {
                this.versions.push($item.version);
            }
        });
        this.fillConfigsTable();
    }

    private async fetchUpdates() : Promise<void> {
        this.updates = await this.registry.getUpdates();
        if (!this.getInstanceOwner().ascending.Value()) {
            this.updates.reverse();
        }
        this.updates.forEach(($item : IRegistryItem) : void => {
            if (!this.projects.includes($item.name)) {
                this.projects.push($item.name);
            }
            if (!this.versions.includes($item.version)) {
                this.versions.push($item.version);
            }
            if (!this.platforms.includes($item.platform)) {
                this.platforms.push($item.platform);
            }
        });
        this.fillUpdatesTable();
    }

    private fillUpdatesTable() : void {
        let content : string = "";
        if (!ObjectValidator.IsEmptyOrNull(this.updates)) {
            this.updates.forEach(($item : IRegistryItem) : void => {
                let displayItem : boolean = true;
                if (!ObjectValidator.IsEmptyOrNull(this.filter.project) && $item.name !== this.filter.project) {
                    displayItem = false;
                }
                if (!ObjectValidator.IsEmptyOrNull(this.filter.version) && $item.version !== this.filter.version) {
                    displayItem = false;
                }
                if (!ObjectValidator.IsEmptyOrNull(this.filter.platform) && $item.platform !== this.filter.platform) {
                    displayItem = false;
                }
                if (!ObjectValidator.IsEmptyOrNull(this.filter.text) && !StringUtils.ContainsIgnoreCase($item.link, this.filter.text)) {
                    displayItem = false;
                }
                if (displayItem) {
                    content += `
                            <tr>
                                <td><a href="${$item.link}" target="_blank">${$item.link}</a> ${$item.info}</td>
                            </tr>`;
                }
            });
        }
        if (ObjectValidator.IsEmptyOrNull(content)) {
            content = "No updates found";
        }
        this.getInstanceOwner().updatesTable.Content(content);
    }

    private fillConfigsTable() : void {
        let content : string = "";
        if (!ObjectValidator.IsEmptyOrNull(this.configs)) {
            this.configs.forEach(($item : IRegistryItem) : void => {
                let displayItem : boolean = true;
                if (!ObjectValidator.IsEmptyOrNull(this.filter.project) && $item.name !== this.filter.project) {
                    displayItem = false;
                }
                if (!ObjectValidator.IsEmptyOrNull(this.filter.version) && $item.version !== this.filter.version) {
                    displayItem = false;
                }
                if (!ObjectValidator.IsEmptyOrNull(this.filter.text) && !StringUtils.ContainsIgnoreCase($item.link, this.filter.text)) {
                    displayItem = false;
                }
                if (displayItem) {
                    content += `
                            <tr>
                                <td><a href="${$item.link}" target="_blank">${$item.link}</a></td>
                            </tr>`;
                }
            });
        }
        if (ObjectValidator.IsEmptyOrNull(content)) {
            content = "No configs found";
        }
        this.getInstanceOwner().configsTable.Content(content);
    }

    private async fetchDownloads() : Promise<void> {
        let content : string = "";
        const items : IRegistryItem[] = await this.registry.getDownloads();
        if (!ObjectValidator.IsEmptyOrNull(items)) {
            items.forEach(($item : IRegistryItem) : void => {
                content += `
                            <tr>
                                <td><a href="${$item.link}" target="_blank">${$item.link}</a></td>
                            </tr>`;
            });
        } else {
            content = "No downloads found";
        }
        this.getInstanceOwner().downloadsTable.Content(content);
    }

    private updateMenu() : void {
        const instance : RegistryPanel = this.getInstanceOwner();
        const setData : any = ($source : string[], $target : GuiCommons) : void => {
            $target.Clear();
            for (const menuItem of $source) {
                const item : DropDownListItem = new DropDownListItem();
                item.Text(menuItem);
                item.getEvents().setOnClick(($event : EventArgs) : void => {
                    const text : string = $event.Owner().Text();
                    switch ($target) {
                    case instance.projectNameMenu:
                        instance.projectName.Text(text);
                        this.filter.project = text;
                        break;
                    case instance.versionMenu:
                        instance.version.Text(text);
                        this.filter.version = text;
                        break;
                    case instance.platformMenu:
                        instance.platform.Text(text);
                        this.filter.platform = text;
                        break;
                    default:
                        LogIt.Error("Undefined menu target: {0}", $event.Owner().Parent());
                        break;
                    }
                    this.fillUpdatesTable();
                    this.fillConfigsTable();
                    this.savePersistence();
                });
                $target.AddChild(item);
            }
            switch ($target) {
            case instance.projectNameMenu:
                if (!ObjectValidator.IsEmptyOrNull(this.filter.project)) {
                    instance.projectName.Text(this.filter.project);
                }
                break;
            case instance.versionMenu:
                if (!ObjectValidator.IsEmptyOrNull(this.filter.version)) {
                    instance.version.Text(this.filter.version);
                }
                break;
            case instance.platformMenu:
                if (!ObjectValidator.IsEmptyOrNull(this.filter.platform)) {
                    instance.platform.Text(this.filter.platform);
                }
                break;
            default:
                // no default action
                break;
            }
        };
        this.projects.sort();
        this.versions.sort();
        this.platforms.sort();
        setData(this.projects, this.getInstanceOwner().projectNameMenu);
        setData(this.versions, this.getInstanceOwner().versionMenu);
        setData(this.platforms, this.getInstanceOwner().platformMenu);
        if (!ObjectValidator.IsEmptyOrNull(this.filter.text)) {
            instance.searchFilter.Value(this.filter.text);
        }
    }

    private savePersistence() : void {
        this.persistence.Variable(PersistenceType.FILTER, this.filter);
        this.persistence.Variable(PersistenceType.ASCENDING, this.getInstanceOwner().ascending.Value());
    }
}

class PersistenceType extends BaseEnum {
    public static readonly FILTER : string = "filter";
    public static readonly ASCENDING : string = "ascending";
    public static readonly TAB : string = "tab";
}
