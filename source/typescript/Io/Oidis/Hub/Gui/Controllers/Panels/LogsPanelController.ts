/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ITraceBackEntry, StackTraceCallSite } from "@io-oidis-commons/Io/Oidis/Commons/Utils/TraceBack.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { ILogsRecords, ReportsConnector } from "../../Connectors/ReportsConnector.js";
import { SystemLogConnector } from "../../Connectors/SystemLogConnector.js";
import { Loader } from "../../Loader.js";
import { LogEntry } from "../../Models/LogEntry.js";
import { LogsPanel } from "../../Panels/Dashboard/LogsPanel.js";
import { BasePanelController } from "../../Primitives/BasePanelController.js";
import { LogsTableItem } from "../../UserControls/LogsTable.js";
import { MonitorItem } from "../../UserControls/MonitorTable.js";
import { Convert } from "../../Utils/Convert.js";

@Resolver("/logs")
export class LogsPanelController extends BasePanelController {
    private reports : ReportsConnector;
    private logs : SystemLogConnector;

    /// TODO: enable drag-and-drop
    /// TODO: files dropped into hub should be stored in DB under user account for persistence
    /// TODO: enable tabs for multiple logs with close button
    /// TODO: enable logs page persistence
    /// TODO: enable to show browser logs also after date selection
    /// TODO: reports page should redirect to logs tab if file report is selected
    constructor($instance : LogsPanel) {
        super($instance);
        this.reports = new ReportsConnector(true, this.getHttpManager().CreateLink("/connector.config.jsonp"));
        this.logs = SystemLogConnector.getClient();
    }

    public async FetchData() : Promise<void> {
        const instance : LogsPanel = this.getInstanceOwner();
        instance.infoCount.Text("");
        instance.warningCount.Text("");
        instance.errorCount.Text("");
        instance.debugCount.Text("");
        if (instance.source.Value() === "database") {
            instance.monitorContent.Visible(true);
            await instance.monitoringTable.ShowData();
        } else {
            instance.logsContent.Visible(true);
            await instance.logsTable.ShowData();
        }
    }

    protected getInstanceOwner() : LogsPanel {
        return <LogsPanel>super.getInstanceOwner();
    }

    protected async beforeLoad($instance : LogsPanel) : Promise<void> {
        const feSource : DropDownListItem = new DropDownListItem();
        const beSource : DropDownListItem = new DropDownListItem();
        const remoteSource : DropDownListItem = new DropDownListItem();
        const monitoringSource : DropDownListItem = new DropDownListItem();
        $instance.sourceMenu.Clear();
        $instance.sourceMenu.AddChild(feSource);
        $instance.sourceMenu.AddChild(beSource);
        $instance.sourceMenu.AddChild(monitoringSource);
        /// TODO: remote source must be implemented in more robust way and should enable also file browser entry
        // $instance.sourceMenu.AddChild(remoteSource);

        const selectSource : any = async ($type : DropDownListItem) : Promise<void> => {
            $instance.sourceSelector.Content($type.Text());
            $instance.date.Readonly(true);
            $instance.source.Value("");
            $instance.source.Readonly(true);
            $instance.monitorContent.Visible(false);
            $instance.logsContent.Visible(false);
            switch ($type) {
            case feSource:
                $instance.date.Value(Convert.DateToCalendarFormat(new Date()));
                $instance.source.Value("browser");
                $instance.logsContent.Visible(true);
                break;
            case beSource:
                $instance.date.Readonly(false);
                $instance.source.Value("./log");
                $instance.logsContent.Visible(true);
                break;
            case remoteSource:
                $instance.date.Readonly(false);
                $instance.source.Readonly(false);
                $instance.logsContent.Visible(true);
                break;
            case monitoringSource:
                $instance.date.Readonly(false);
                $instance.source.Value("database");
                $instance.monitorContent.Visible(true);
                break;
            }
            await this.FetchData();
        };

        feSource.Text("Front-end");
        EventsBinding.SingleClick(feSource, async () : Promise<void> => {
            selectSource(feSource);
        });
        beSource.Text("Back-end");
        EventsBinding.SingleClick(beSource, async () : Promise<void> => {
            selectSource(beSource);
        });
        remoteSource.Text("Path");
        EventsBinding.SingleClick(remoteSource, async () : Promise<void> => {
            selectSource(remoteSource);
        });
        monitoringSource.Text("System Logs");
        EventsBinding.SingleClick(monitoringSource, async () : Promise<void> => {
            selectSource(monitoringSource);
        });
        selectSource(feSource);

        $instance.date.Value(Convert.DateToCalendarFormat(new Date()));

        $instance.monitorContent.Visible(false);
        $instance.logsContent.Visible(false);
        $instance.monitoringTable.ItemsPerPage(15);
        $instance.monitoringTable.PagesLimit(5);
        $instance.logsTable.ItemsPerPage(300);
        $instance.logsTable.PagesLimit(5);

        $instance.logsTable.BindData(async ($offset : number, $limit : number) : Promise<IModelListResult> => {
            const data : IModelListResult = {data: [], offset: 0, limit: -1, size: 0};
            try {
                const levels : LogLevel[] = [];
                if ($instance.info.Value()) {
                    levels.push(LogLevel.INFO);
                }
                if ($instance.warning.Value()) {
                    levels.push(LogLevel.WARNING);
                }
                if ($instance.error.Value()) {
                    levels.push(LogLevel.ERROR);
                }
                if ($instance.debug.Value()) {
                    levels.push(LogLevel.DEBUG, LogLevel.VERBOSE);
                }
                const records : ILogsRecords = await this.reports.ProcessLogs({
                    data       : $instance.source.Value() === "browser" ? Loader.getInstance().ApplicationLog() : null,
                    filter     : $instance.searchText.Value(),
                    from       : Date.parse($instance.date.Value()),
                    isAscending: $instance.ascending.Value(),
                    levels,
                    location   : $instance.source.Value(),
                    to         : Date.parse($instance.date.Value()),
                    limit      : $limit,
                    offset     : $offset
                });
                data.data = records.traces;
                data.size = records.size;
                data.limit = records.limit;
                data.offset = records.offset;
                $instance.infoCount.Text(records.counters.info + "");
                $instance.warningCount.Text(records.counters.warning + "");
                $instance.errorCount.Text(records.counters.error + "");
                $instance.debugCount.Text(records.counters.debug + "");
                $instance.infoCount.Visible(true);
                $instance.warningCount.Visible(true);
                $instance.errorCount.Visible(true);
                $instance.debugCount.Visible(true);
            } catch (ex) {
                LogIt.Error(ex);
                this.ShowMessage("Fetch of log records has failed", ToastType.ERROR);
            }
            return data;
        }, async ($data : ILoggerTrace, $item : LogsTableItem) : Promise<void> => {
            switch ($data.level) {
            case LogLevel.ERROR:
                $item.setError(true);
                break;
            case LogLevel.WARNING:
                $item.setWarning(true);
                break;
            case LogLevel.DEBUG:
                $item.setDebug(true);
                break;
            case LogLevel.VERBOSE:
                $item.setVerbose(true);
                break;
            }

            const message : string = $data.message;
            if (!ObjectValidator.IsEmptyOrNull($data.entryPoint)) {
                $item.entryPointToggle.Visible(true);
                if (ObjectValidator.IsString($data.entryPoint)) {
                    $item.entryPoint.Text(<string>$data.entryPoint);
                } else {
                    $item.entryPoint.Text(`
at ${(<ITraceBackEntry>$data.entryPoint).at}<br>from ${(<ITraceBackEntry>$data.entryPoint).from}`);
                }
            }
            if (!ObjectValidator.IsEmptyOrNull($data.trace)) {
                $item.traceToggle.Visible(true);
                let stackTrace : string = "";
                $data.trace.forEach(($data : any) : void => {
                    const callSite : StackTraceCallSite = new StackTraceCallSite($data.properties);
                    if (!ObjectValidator.IsEmptyOrNull(stackTrace)) {
                        stackTrace += "<br>";
                    }
                    stackTrace += "at ";
                    let methodUndefined : boolean = true;
                    if (!ObjectValidator.IsEmptyOrNull(callSite.getFunctionName())) {
                        stackTrace += callSite.getFunctionName();
                        methodUndefined = false;
                    } else if (!ObjectValidator.IsEmptyOrNull(callSite.getTypeName())) {
                        stackTrace += callSite.getTypeName();
                        methodUndefined = false;
                    }
                    if (!methodUndefined) {
                        stackTrace += " (";
                    }
                    stackTrace += callSite.getFileName();
                    if (!ObjectValidator.IsEmptyOrNull(callSite.getLineNumber())) {
                        stackTrace += ":" + callSite.getLineNumber();
                    }
                    if (!ObjectValidator.IsEmptyOrNull(callSite.getColumnNumber())) {
                        stackTrace += ":" + callSite.getColumnNumber();
                    }
                    if (!methodUndefined) {
                        stackTrace += ")";
                    }
                });
                $item.trace.Text(stackTrace);
            }
            $item.message.Text(message);
            $item.time.Text(<string>$data.time);
            let time : string = <string>$data.time;
            if (ObjectValidator.IsDigit($data.time)) {
                time = Convert.TimeToLocalFormat(<number>$data.time);
            }
            $item.time.Text(time);
        });

        $instance.monitoringTable.BindData(async ($offset : number, $limit : number) : Promise<IModelListResult> => {
            let data : IModelListResult = {data: [], offset: 0, limit: -1, size: 0};
            try {
                let traceBack : any = "";
                const tags : string[] = [];
                if (!ObjectValidator.IsEmptyOrNull($instance.searchText.Value())) {
                    traceBack = {$regex: ".*" + StringUtils.Replace($instance.searchText.Value(), " ", ".*") + ".*", $options: "i"};
                } else {
                    /// TODO: searchText is too basic and should be extended for an ability to have predefined filtering options
                    // traceBack = {$regex: "AuthManager.*", $options: "i"};
                }
                if (!$instance.info.Value() ||
                    !$instance.warning.Value() ||
                    !$instance.error.Value() ||
                    !$instance.debug.Value()) {
                    if ($instance.info.Value()) {
                        tags.push(LogLevel[LogLevel.INFO]);
                    }
                    if ($instance.warning.Value()) {
                        tags.push(LogLevel[LogLevel.WARNING]);
                    }
                    if ($instance.error.Value()) {
                        tags.push(LogLevel[LogLevel.ERROR]);
                    }
                    if ($instance.debug.Value()) {
                        tags.push(LogLevel[LogLevel.DEBUG]);
                        tags.push(LogLevel[LogLevel.VERBOSE]);
                    }
                }

                data = await this.logs.getLogsList({
                    ascending: $instance.ascending.Value(),
                    date     : $instance.date.InstanceOwner().valueAsNumber,
                    limit    : $limit,
                    offset   : $offset,
                    tags,
                    traceBack
                });
                $instance.infoCount.Visible(false);
                $instance.warningCount.Visible(false);
                $instance.errorCount.Visible(false);
                $instance.debugCount.Visible(false);
            } catch (ex) {
                LogIt.Error(ex);
                this.ShowMessage("Fetch of monitoring data has failed", ToastType.ERROR);
            }
            return data;
        }, async ($data : LogEntry, $item : MonitorItem) : Promise<void> => {
            if ($data.Tags.includes(LogLevel[LogLevel.ERROR])) {
                $item.setError(true);
            } else if ($data.Tags.includes(LogLevel[LogLevel.WARNING])) {
                $item.setWarning(true);
            }
            const time : string = Convert.TimeToLocalFormat($data.Created);
            if (ObjectValidator.IsEmptyOrNull($instance.date.Value())) {
                $item.date.Text(Convert.DateToCalendarFormat(new Date($data.Created)) + " " + time);
            } else {
                $item.date.Text(time);
            }
            $item.message.Text($data.Message);
            $item.user.Text($data.User);
        });
    }

    protected async resolver($instance : LogsPanel) : Promise<void> {
        [
            $instance.load, $instance.ascending, $instance.info, $instance.warning, $instance.error, $instance.debug,
            $instance.searchButton
        ].forEach(($element : GuiCommons) : void => {
            EventsBinding.SingleClick($element, async () : Promise<void> => {
                await this.FetchData();
            });
        });
        $instance.date.getEvents().setEvent(EventType.ON_CHANGE, async () : Promise<void> => {
            await this.FetchData();
        });

        EventsBinding.SingleClick($instance.clearMonitor, async () : Promise<void> => {
            try {
                const removed : number = await this.logs.Clean();
                if (removed > 0) {
                    this.ShowMessage("Removed " + removed + " system log records.", ToastType.SUCCESS);
                } else {
                    this.ShowMessage("System logs are already cleaned up.", ToastType.INFO);
                }
            } catch (ex) {
                LogIt.Error(ex);
                this.ShowMessage("Clean up of system logs has failed.", ToastType.ERROR);
            }
        });
        EventsBinding.SingleClick($instance.generateLogs, async () : Promise<void> => {
            const dummy : number = new Date().getTime();
            LogIt.Warning("Test FE Warning - " + dummy);
            LogIt.Error("Test FE Error - " + dummy);
            await this.reports.TestLogs();
        });
    }
}
