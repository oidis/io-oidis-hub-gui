/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { BasePanelController } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanelController.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import {
    AgentsRegisterConnectorAsync,
    IAgentInfo, IAgentMetadata,
    IAgentStatus
} from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";
import {
    AutocompleteBinding,
    IAutocompleteData,
    IAutocompleteUserControls
} from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/AutocompleteBinding.js";
import { DialogBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/DialogBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { AgentsPanel } from "../../Panels/Dashboard/AgentsPanel.js";
import { AgentsRecord } from "../../UserControls/AgentsTable.js";

@Resolver("/agents")
export class AgentsPanelController extends BasePanelController {
    private registry : AgentsRegisterConnectorAsync;
    private autoFetchTimer : number;
    private countDownIndex : number;

    constructor($instance : AgentsPanel) {
        super($instance);

        this.registry = new AgentsRegisterConnectorAsync(true, this.getHttpManager().CreateLink("/connector.config.jsonp"));
        this.autoFetchTimer = null;
        this.countDownIndex = 0;
    }

    public async FetchData() : Promise<void> {
        await this.autoFetch(true);
    }

    protected getInstanceOwner() : AgentsPanel {
        return <AgentsPanel>super.getInstanceOwner();
    }

    protected async resolver($instance : AgentsPanel) : Promise<void> {
        const statusKeys : string[] = ["Idle", "Running", "Stopped"];
        $instance.refreshRate.Value("15");
        [
            $instance.idle, $instance.running, $instance.stopped,
            $instance.searchButton
        ].forEach(($element : GuiCommons) : void => {
            $element.getEvents().setOnClick(async () : Promise<void> => {
                await this.FetchData();
            });
        });
        EventsBinding.SingleClick($instance.refresh, async () : Promise<void> => {
            if ($instance.refresh.Value()) {
                await this.autoFetch();
            }
        });
        $instance.refreshRate.getEvents().setEvent(EventType.ON_CHANGE, async () : Promise<void> => {
            if ($instance.refresh.Value()) {
                await this.autoFetch();
            }
        });

        EventsBinding.SingleClick($instance.clearFilter, async () : Promise<void> => {
            $instance.searchValue.Value("");
            $instance.searchValue.Readonly(false);
            $instance.searchAutocomplete.Data(null);
            await this.FetchData();
            $instance.clearFilter.Visible(false);
        });

        const selectUC : IAutocompleteUserControls = {
            dropdownSelector: $instance.autocompleteSelector,
            field           : $instance.searchValue,
            menu            : $instance.searchAutocomplete
        };
        const binding : AutocompleteBinding = new AutocompleteBinding();
        binding.Bind(selectUC, async ($query : any) : Promise<IAutocompleteData[]> => {
            const regex : RegExp = new RegExp($query.$regex, $query.$options);
            const output : IAutocompleteData[] = [];
            (await this.registry.getAgentsList()).forEach(($agent : IAgentInfo) : void => {
                if (!ObjectValidator.IsEmptyOrNull(regex.exec(JSON.stringify($agent)))) {
                    output.push({
                        text : $agent.name,
                        value: $agent.id
                    });
                }
            });
            return output;
        });
        $instance.searchByText.getEvents().setOnClick(() : void => {
            selectUC.field.Value("");
            selectUC.field.Readonly(false);
            selectUC.menu.Data(null);
            binding.Show(selectUC, false);
        });
        [
            $instance.searchByPlatform, $instance.searchByPool, $instance.searchByTag, $instance.searchByVersion
        ].forEach(($type : Button) : void => {
            EventsBinding.SingleClick($type, async () : Promise<void> => {
                selectUC.field.Value("");
                /// TODO: enable if filter should be strictly defined
                // selectUC.field.Readonly(true);
                selectUC.menu.Data(null);
                selectUC.menu.Clear();

                const values : string[] = [];
                (await this.registry.getAgentsList()).forEach(($agent : IAgentInfo) : void => {
                    let value : string = null;
                    switch ($type) {
                    case $instance.searchByPlatform:
                        value = $agent.platform;
                        break;
                    case $instance.searchByPool:
                        value = $agent.poolName;
                        break;
                    case $instance.searchByTag:
                        value = $agent.tag;
                        break;
                    case $instance.searchByVersion:
                        value = $agent.version;
                        break;
                    default:
                        LogIt.Warning("Unsupported menu type");
                        break;
                    }

                    if (!ObjectValidator.IsEmptyOrNull(value) && !values.includes(value)) {
                        values.push(value);
                        const item : DropDownListItem = new DropDownListItem();
                        item.Text(value);
                        item.Value(value);
                        selectUC.menu.AddChild(item);
                        item.getEvents().setOnClick(async () : Promise<void> => {
                            selectUC.menu.Data(item.Value());
                            selectUC.field.Value(item.Text());
                            binding.Show(selectUC, false);
                            $instance.clearFilter.Visible(true);
                            await this.FetchData();
                        });
                    }
                });
                binding.Show(selectUC, true);
            });
        });
        $instance.agentsTable.BindData(async () : Promise<IModelListResult> => {
            const output : IModelListResult = {data: [], limit: -1, offset: 0, size: 0};
            const counters : any = {
                idle   : 0,
                running: 0,
                stopped: 0
            };
            const search : string = $instance.searchValue.Value();
            (await this.registry.getAgentsList()).forEach(($agent : IAgentInfo) : void => {
                if (ObjectValidator.IsEmptyOrNull(search) || StringUtils.ContainsIgnoreCase(JSON.stringify($agent), search)) {
                    let printRecord : boolean = false;
                    if (!ObjectValidator.IsEmptyOrNull($agent.status)) {
                        switch ($agent.status) {
                        case IAgentStatus.Idle:
                            counters.idle++;
                            if ($instance.idle.Value()) {
                                printRecord = true;
                            }
                            break;
                        case IAgentStatus.Running:
                            counters.running++;
                            if ($instance.running.Value()) {
                                printRecord = true;
                            }
                            break;
                        case IAgentStatus.Stopped:
                            counters.stopped++;
                            if ($instance.stopped.Value()) {
                                printRecord = true;
                            }
                            break;
                        default:
                            // default value already initiated
                            break;
                        }
                    } else {
                        counters.running++;
                        printRecord = true;
                    }
                    if (printRecord) {
                        output.data.push($agent);
                    }
                }
            });

            $instance.agentsCount.Text((counters.idle + counters.running + counters.stopped) + "");
            $instance.idleCount.Text(counters.idle);
            $instance.runningCount.Text(counters.running);
            $instance.stoppedCount.Text(counters.stopped);

            output.size = output.data.length;
            return output;
        }, async ($data : IAgentInfo, $item : AgentsRecord) : Promise<void> => {
            let location : string = $data.domain;
            if (!ObjectValidator.IsEmptyOrNull($data.poolName) && $data.poolName !== "Default") {
                location += " (" + $data.poolName + ")";
            }
            if (!ObjectValidator.IsEmptyOrNull($data.tag)) {
                location += " - " + $data.tag;
            }
            $item.agentId = $data.id;
            $item.setStatus($data.status);
            $item.Data($data);
            $item.name.Text($data.name);
            $item.version.Text($data.version);
            $item.platform.Text($data.platform);
            $item.location.Text(location);
            $item.startTime.Text(Convert.TimeToGMTformat($data.startTime) +
                "(" + Convert.TimeToLongString(new Date().getTime() - new Date($data.startTime).getTime()) + ")");

            $item.statusMenu.Clear();
            [IAgentStatus.Idle, IAgentStatus.Running, IAgentStatus.Stopped].forEach(($status : IAgentStatus) : void => {
                const item : DropDownListItem = new DropDownListItem();
                item.Text(statusKeys[$status]);
                item.Value($status);
                EventsBinding.SingleClick(item, async () : Promise<void> => {
                    $instance.statusConfirmDialog.Data({agentId: $item.Data().id, status: $status, selector: $item.statusSelector});
                    $instance.statusConfirmDialog.Visible(true);
                });
                $item.statusMenu.AddChild(item);
            });
            $item.statusSelector.Value($data.status);
            $item.statusSelector.Content(statusKeys[$data.status]);
        }, async ($item : AgentsRecord) : Promise<void> => {
            /// TODO: how to fix events assigment after agent restart?
            EventsBinding.SingleClick($item.stop, async () : Promise<void> => {
                $instance.killDialog.Data($item.Data());
                $instance.killDialog.Visible(true);
            });
            EventsBinding.SingleClick($item.openMetadata, async () : Promise<void> => {
                let configData : string = "";
                const metadata : IAgentMetadata = await this.registry.getAgentsMetadata($item.agentId);
                for (const item of Object.keys(metadata.configuration)) {
                    configData += `${item} = ${metadata.configuration[item]}\n`;
                }
                let envData : string = "";
                for (const item of Object.keys(metadata.environment)) {
                    envData += `${item} = ${metadata.environment[item]}\n`;
                }
                $instance.metadataDialog.Data(null);
                $instance.agentMetadata.InstanceOwner().innerHTML = "" +
                    "<h5>Configuration:</h5>" + configData + "\n" +
                    "<h5>Environment:</h5>" + envData;
                $instance.metadataDialog.Visible(true);
            });
        });

        new DialogBinding().Bind({
            close : [$instance.closeKill, $instance.cancelKill],
            holder: $instance.killDialog,
            submit: $instance.killConfirm
        });
        [$instance.closeKill, $instance.cancelKill].forEach(($button : Button) : void => {
            $button.getEvents().setOnClick(() : void => {
                $instance.killDialog.Data(null);
            });
        });
        EventsBinding.SingleClick($instance.killConfirm, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull($instance.killDialog.Data())) {
                try {
                    const protocol : LiveContentWrapper = new LiveContentWrapper("Io.Oidis.Localhost.Connectors.RuntimeHandler");
                    await this.registry.ForwardMessage($instance.killDialog.Data().id, {
                        protocol: protocol.getProtocolForInvokeMethod($instance.killDialog.Data().id, "Exit", 130),
                        taskId  : this.getUID()
                    });
                    /// TODO: why it did not show actual list and waits for next refresh?
                    await this.autoFetch();
                } catch (ex) {
                    LogIt.Error(ex);
                }
            }
            $instance.killDialog.Data(null);
            $instance.killDialog.Visible(false);
        });
        new DialogBinding().Bind({
            close : [$instance.closeStatus, $instance.cancelStatus],
            holder: $instance.statusConfirmDialog,
            submit: $instance.statusConfirm
        });
        [$instance.closeStatus, $instance.cancelStatus].forEach(($button : Button) : void => {
            $button.getEvents().setOnClick(() : void => {
                $instance.statusConfirmDialog.Data(null);
            });
        });
        EventsBinding.SingleClick($instance.statusConfirm, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull($instance.statusConfirmDialog.Data())) {
                try {
                    const data : any = $instance.statusConfirmDialog.Data();
                    data.selector.Value(data.status);
                    data.selector.Content(statusKeys[data.status]);
                    await this.registry.setAgentStatus(data.agentId, data.status);
                } catch (ex) {
                    LogIt.Error(ex);
                }
            }
            $instance.statusConfirmDialog.Data(null);
            $instance.statusConfirmDialog.Visible(false);
            await this.autoFetch();
        });
        new DialogBinding().Bind({
            close : [$instance.closeMetadata, $instance.cancelMetadata],
            holder: $instance.metadataDialog,
            submit: $instance.cancelMetadata
        });
        [$instance.closeMetadata, $instance.cancelMetadata].forEach(($button : Button) : void => {
            $button.getEvents().setOnClick(() : void => {
                $instance.metadataDialog.Data(null);
            });
        });
    }

    private async autoFetch($force : boolean = false) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull(this.autoFetchTimer)) {
            clearTimeout(this.autoFetchTimer);
        }
        const instance : AgentsPanel = this.getInstanceOwner();
        const interval : number = StringUtils.ToInteger(instance.refreshRate.Value());
        instance.countDown.Max(interval);
        if (instance.Visible() && (this.countDownIndex === interval || $force)) {
            await instance.agentsTable.ShowData();
            this.countDownIndex = 0;
        }
        if (instance.refresh.Value() && interval > 0) {
            this.autoFetchTimer = this.getEventsManager().FireAsynchronousMethod(async () : Promise<void> => {
                await this.autoFetch();
            }, true, 1000);
            this.countDownIndex++;
            instance.countDown.Visible(true);
            instance.countDown.Value(this.countDownIndex);
        } else {
            instance.countDown.Visible(false);
        }
    }
}
