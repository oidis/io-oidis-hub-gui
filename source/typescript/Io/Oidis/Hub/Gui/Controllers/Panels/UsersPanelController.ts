/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { IUserProfileOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { FileFetch, IFileFetchOptions } from "@io-oidis-services/Io/Oidis/Services/Utils/FileFetch.js";
import { DialogBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/DialogBinding.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { FileUploadBinding } from "../../Bindings/FileUploadBinding.js";
import { UserSearchBinding } from "../../Bindings/UserSearchBinding.js";
import { AuthManagerConnector, I2FAKey, IPermissionsReport } from "../../Connectors/AuthManagerConnector.js";
import { AuthToken } from "../../Models/AuthToken.js";
import { Group } from "../../Models/Group.js";
import { User } from "../../Models/User.js";
import { UsersPanel } from "../../Panels/Dashboard/UsersPanel.js";
import { BasePanelController } from "../../Primitives/BasePanelController.js";
import { PermissionInputGroup } from "../../UserControls/PermissionInputGroup.js";
import { PermissionsItem } from "../../UserControls/PermissionsItem.js";
import { TokensTableRecord } from "../../UserControls/TokensTableRecord.js";
import { BarCodeEngine } from "../../Utils/BarCodeEngine.js";
import { Convert } from "../../Utils/Convert.js";

@Resolver("/users", ["/users/{userId}"])
export class UsersPanelController<Panel extends UsersPanel> extends BasePanelController<Panel> {
    protected currentProfile : User;
    protected localization : IUsersPanelLocalization;
    private auth : AuthManagerConnector;
    private userId : string;
    private authenticated : boolean;
    private persistence : IPersistenceHandler;
    private faSecret : string;

    constructor($instance : Panel) {
        super($instance);
        this.auth = AuthManagerConnector.getClient();

        this.userId = "";
        this.authenticated = false;
        this.persistence = PersistenceFactory.getPersistence(this.getClassName());
        this.faSecret = "";
        this.currentProfile = null;

        this.localization = {
            permissionsAdd             : "Add",
            noTokensCreated            : "No tokens created ...",
            failedToGenerate2FACode    : "Failed to generate 2FA QR Code.",
            failedToGenerated2FAKey    : "Failed to generate 2FA Key.",
            onlySingleFileSupported    : "Only single file is supported",
            imageMustBeSelected        : "Image file must be selected",
            unsupportedCharactersInName: "User name contains some of unsupported characters: ",
            profileUpdated             : "Profile updated",
            codeMustBeEntered          : "Code must be entered.",
            failedToValidateAuthCode   : "Failed to validated authentication code, please try again."
        };
    }

    public async FetchUserProfile($name : string) : Promise<void> {
        const profile : User = await this.auth.getUserProfile(true, $name);
        this.currentProfile = profile;
        const instance : Panel = this.getInstanceOwner();
        instance.profileId.Value("");
        instance.lastLogin.Value("");
        instance.profileImg.Source("resource/graphics/Io/Oidis/Hub/Gui/UserImage.png");
        this.processProfileData(profile);
        if (!ObjectValidator.IsEmptyOrNull(profile)) {
            this.persistence.Variable("profile", profile.Id);
            this.userId = profile.Id;
            if (this.userId !== this.context.getUserProfile().Id) {
                instance.profileSearch.searchField.Value(this.userId);
                instance.profileSearch.Data(profile);
                instance.profileSearch.clear.Visible(true);
            } else {
                instance.profileSearch.searchField.Value("");
                instance.profileSearch.Data(null);
                instance.profileSearch.clear.Visible(false);
            }
            instance.profileId.Value(profile.Id);
            instance.lastLogin.Value(<any>profile.AuditLog.LastLogIn);
            if (!ObjectValidator.IsEmptyOrNull(profile.Image)) {
                instance.profileImg.Source(<any>profile.Image);
            }
            if (this.context.IsAdmin || this.context.IsSysAdmin) {
                if (profile.Activated) {
                    instance.deactivateProfile.Visible(true);
                    instance.activateProfile.Visible(false);
                    instance.restoreProfile.Visible(false);
                    instance.deleteProfile.Visible(false);
                } else if (!profile.Deleted) {
                    instance.deactivateProfile.Visible(false);
                    instance.activateProfile.Visible(true);
                    instance.restoreProfile.Visible(false);
                    instance.deleteProfile.Visible(true);
                } else {
                    instance.deactivateProfile.Visible(false);
                    instance.activateProfile.Visible(false);
                    instance.restoreProfile.Visible(true);
                    instance.deleteProfile.Visible(false);
                }
            }

            if (instance.groups.Enabled()) {
                instance.groups.Clear();
                const groups : string[] = [];
                const allUserMethods : string[] = [];
                profile.Groups.forEach(($group : Group) : void => {
                    groups.push($group.Name);
                    $group.AuthorizedMethods.forEach(($method : string) : void => {
                        if (!allUserMethods.includes($method)) {
                            allUserMethods.push($method);
                        }
                    });
                    const groupItem : PermissionsItem = new PermissionsItem();
                    groupItem.Text($group.Name);
                    groupItem.Value({user: profile.Id, group: $group.Name});
                    groupItem.getEvents().setOnClick(async ($event : EventArgs) : Promise<void> => {
                        await this.auth.RemoveGroup($event.Owner().Value().group, $event.Owner().Value().user);
                        await this.FetchUserProfile($name);
                    });
                    instance.groups.AddChild(groupItem);
                });
                if (!ObjectValidator.IsEmptyOrNull(profile.DefaultGroup.AuthorizedMethods)) {
                    const permsReport : IPermissionsReport = await this.auth.getPermissionsReport();
                    profile.DefaultGroup.AuthorizedMethods.forEach(($method : string) : void => {
                        if (!permsReport.allDefaultMethods.includes($method) && !allUserMethods.includes($method)) {
                            const groupItem : PermissionsItem = new PermissionsItem();
                            groupItem.Text($method);
                            groupItem.setStyle("btn-outline-info");
                            if ($method === "*") {
                                groupItem.setStyle("btn-outline-warning");
                            } else if (StringUtils.Contains($method, "*")) {
                                groupItem.setStyle("btn-outline-primary");
                            }
                            groupItem.Value({
                                group  : profile.DefaultGroup.Id,
                                method : $method,
                                methods: profile.DefaultGroup.AuthorizedMethods
                            });
                            EventsBinding.SingleClick(groupItem, async () : Promise<void> => {
                                await this.auth.setAuthorizedMethods(groupItem.Value().group,
                                    groupItem.Value().methods.filter(($method : string) : boolean => {
                                        return $method !== groupItem.Value().method;
                                    }), false);
                                await this.FetchUserProfile($name);
                            });
                            instance.groups.AddChild(groupItem);
                        }
                    });
                }
                let showGroups : boolean = false;
                const availableGroups : PermissionInputGroup = new PermissionInputGroup();
                availableGroups.Text(this.localization.permissionsAdd);
                EventsBinding.SingleClick(availableGroups, async () : Promise<void> => {
                    if (!ObjectValidator.IsEmptyOrNull(availableGroups.Data()) || groups.includes(availableGroups.Value())) {
                        await this.auth.AddGroup(availableGroups.Data().group, availableGroups.Data().user);
                    } else if (!ObjectValidator.IsEmptyOrNull(availableGroups.Value())) {
                        await this.auth.setAuthorizedMethods(profile.DefaultGroup.Id,
                            [availableGroups.Value()], true);
                    }
                    await this.FetchUserProfile($name);
                });
                (await this.auth.getActiveGroupsList()).forEach(($groupName : string) : void => {
                    if (!groups.includes($groupName)) {
                        showGroups = true;
                        availableGroups.AddItem($groupName, {user: profile.Id, group: $groupName});
                    }
                });
                if (showGroups) {
                    instance.groups.AddChild(availableGroups);
                }
            }

            if (instance.profileTokens.Enabled()) {
                if (ObjectValidator.IsEmptyOrNull(profile.AuthTokens)) {
                    instance.profileTokens.Content(this.localization.noTokensCreated);
                } else {
                    instance.profileTokens.Clear();
                    profile.AuthTokens.forEach(($token : AuthToken) : void => {
                        const record : TokensTableRecord = new TokensTableRecord();
                        let tokenName : string = $token.Id;
                        if (!ObjectValidator.IsEmptyOrNull($token.Name)) {
                            tokenName = $token.Name;
                        }
                        record.Data({
                            date: $token.Date,
                            id  : $token.Id,
                            name: tokenName,
                            pass: $token.Token
                        });
                        record.name.Value(tokenName);

                        record.name.getEvents().setEvent(EventType.ON_KEY_UP, ($eventArgs : EventArgs) : void => {
                            this.getEventsManager().FireAsynchronousMethod(async () : Promise<void> => {
                                const tokenNameField : TextField = $eventArgs.Owner();
                                const value : string = tokenNameField.Value();
                                if (value !== tokenNameField.Parent().Data().name) {
                                    await this.auth.UpdateAuthToken(tokenNameField.Parent().Data().id, {
                                        name: value
                                    }, this.userId);
                                }
                            }, true);
                        });

                        record.ShowTokenValue(this.authenticated);
                        record.token.Text(!this.authenticated ? "*****" : $token.Token);

                        EventsBinding.SingleClick(record.showButton, async () : Promise<void> => {
                            if (this.context.IsAdmin) {
                                this.authenticated = true;
                                await this.FetchUserProfile(this.userId);
                            } else {
                                instance.tokenDialog.Visible(true);
                            }
                        });

                        record.copyButton.getEvents().setOnClick(async () : Promise<void> => {
                            try {
                                await navigator.clipboard.writeText(JSON.stringify(record.Data(), null, 2));
                            } catch (ex) {
                                LogIt.Error("Failed to write data into clipboard.", ex);
                            }
                        });

                        let date : string = "";
                        if (!ObjectValidator.IsEmptyOrNull($token.Expire)) {
                            const time : Date = new Date($token.Date + $token.Expire);
                            let month : string = (time.getMonth() + 1) + "";
                            if (time.getMonth() < 10) {
                                month = "0" + month;
                            }
                            let day : string = time.getDate() + "";
                            if (time.getDate() < 10) {
                                day = "0" + day;
                            }
                            date = time.getFullYear() + "-" + month + "-" + day;
                            record.date.Value(date);
                        }
                        record.date.getEvents().setEvent(EventType.ON_CHANGE, async () : Promise<void> => {
                            const dateField : TextField = record.date;
                            let expire : number = null;
                            if (!ObjectValidator.IsEmptyOrNull(dateField.Value())) {
                                const dateParts : string[] = StringUtils.Split(dateField.Value(), "-");
                                const date : Date = new Date();
                                date.setFullYear(StringUtils.ToInteger(dateParts[0]),
                                    StringUtils.ToInteger(dateParts[1]) - 1, StringUtils.ToInteger(dateParts[2]));
                                expire = date.getTime() - dateField.Parent().Data().date;
                            }
                            await this.auth.UpdateAuthToken(dateField.Parent().Data().id, {
                                expireTime: expire
                            }, this.userId);
                        });

                        record.revokeButton.getEvents().setOnClick(async ($eventArgs : EventArgs) : Promise<void> => {
                            await this.auth.RevokeAuthToken($eventArgs.Owner().Parent().Data().id, this.userId);
                            await this.FetchUserProfile(this.userId);
                        });
                        this.getInstanceOwner().profileTokens.AddChild(record);
                    });
                }
            }

            this.faSecret = "";
            if (!ObjectValidator.IsEmptyOrNull(profile.TwoFASecret)) {
                instance.faIsInactive.Visible(false);
                instance.faIsActive.Visible(true);
            } else {
                instance.faIsActive.Visible(false);
                instance.faIsInactive.Visible(true);
                try {
                    const key : I2FAKey = await this.auth.Generate2FAKey();
                    this.faSecret = key.secret;
                    instance.faCodeValue.Text(key.secret);
                    try {
                        await BarCodeEngine.Generate(this.getInstanceOwner().qrCode.InstanceOwner(),
                            key.url, {format: "QRCODE"});
                        instance.faQRCodeContent.Visible(true);
                        instance.faCodeContent.Visible(false);
                    } catch (ex) {
                        LogIt.Error(ex);
                        this.ShowMessage(this.localization.failedToGenerate2FACode, ToastType.ERROR);
                        instance.faQRCodeContent.Visible(false);
                        instance.faCodeContent.Visible(true);
                    }
                } catch (ex) {
                    LogIt.Error(ex);
                    this.ShowMessage(this.localization.failedToGenerated2FAKey, ToastType.ERROR);
                }
            }
        } else if ($name !== null) {
            LogIt.Warning("User profile not found for: " + $name);
        }
    }

    public async FetchData() : Promise<void> {
        const userId : string = this.RequestArgs().GET().getItem("userId");
        if (!ObjectValidator.IsEmptyOrNull(userId)) {
            await this.FetchUserProfile(userId);
        } else {
            if (!ObjectValidator.IsEmptyOrNull(this.persistence.Variable("profile"))) {
                await this.FetchUserProfile(this.persistence.Variable("profile"));
            } else {
                await this.FetchUserProfile(this.context.getUserProfile().Id);
            }
        }
    }

    /// TODO: add colors for expired token and token with expire in some limit (based on email notification)
    protected async resolver($instance : UsersPanel) : Promise<void> {
        $instance.profilePassNew.Value("");
        if ($instance.profileSearch.Enabled()) {
            new UserSearchBinding().Bind($instance.profileSearch, async ($data : any) : Promise<void> => {
                if (!ObjectValidator.IsEmptyOrNull($data)) {
                    await this.FetchUserProfile($data.id);
                } else {
                    await this.FetchUserProfile(null);
                }
            });
        }
        const fileUpload : FileUploadBinding = new FileUploadBinding();
        let imageData : string = "";
        fileUpload.Bind({
            button: $instance.photoUploadButton
        }, {
            onData          : async ($files : FileList) : Promise<void> => {
                if ($files.length === 1) {
                    imageData = (await fileUpload.Read())[0];
                    $instance.profileImg.Source("data:image/png;base64, " + imageData);
                } else {
                    this.ShowMessage(this.localization.onlySingleFileSupported, ToastType.ERROR);
                }
            },
            onEmptySelection: () : void => {
                this.ShowMessage(this.localization.imageMustBeSelected, ToastType.ERROR);
            }
        });
        fileUpload.setFilter(".jpg,.png");
        EventsBinding.SingleClick($instance.updateProfile, async () : Promise<void> => {
            if (StringUtils.Contains($instance.profileName.Value(), ".", "@")) {
                ValidationBinding.IsValidInput($instance.profileName, false);
                this.ShowMessage(this.localization.unsupportedCharactersInName + "\".\", \"@\"", ToastType.ERROR);
            } else {
                const options : IUserProfileOptions = this.getUserProfileData();
                if (!ObjectValidator.IsEmptyOrNull(imageData)) {
                    options.image = imageData;
                }
                await this.auth.setUserProfile(options, this.userId);

                if (!ObjectValidator.IsEmptyOrNull($instance.profilePassNew.Value())) {
                    await this.auth.ChangePassword($instance.profilePassNew.Value(), this.userId);
                    $instance.profilePassNew.Value("");
                }
                this.ShowMessage(this.localization.profileUpdated);
            }
        });
        EventsBinding.SingleClick($instance.resetProfile, async () : Promise<void> => {
            await this.FetchUserProfile(this.userId);
            $instance.profilePassNew.Value("");
        });
        $instance.generatePass.getEvents().setOnClick(() : void => {
            const newPass : string = StringUtils.getSha1(this.getUID());
            $instance.profilePassNew.Value(newPass);
        });

        if ($instance.profileTokens.Enabled()) {
            new DialogBinding().Bind({
                close : [$instance.tokenDialogClose, $instance.tokenDialogCloseButton],
                holder: $instance.tokenDialog,
                submit: $instance.tokenAccessValidate
            });
            EventsBinding.SingleClick($instance.tokenAccessValidate, async () : Promise<void> => {
                if (await this.auth.IsPasswordValid(this.auth.CurrentToken(), $instance.tokenValidatePass.Value())) {
                    this.authenticated = true;
                    $instance.tokenValidatePass.Error(false);
                    $instance.tokenDialog.Visible(false);
                    await this.FetchUserProfile(this.userId);
                } else {
                    $instance.tokenValidatePass.Error(true);
                }
            });
            EventsBinding.SingleClick($instance.addNewToken, async () : Promise<void> => {
                if (!ObjectValidator.IsEmptyOrNull(this.userId)) {
                    await this.auth.GenerateAuthToken("", this.userId);
                    await this.FetchUserProfile(this.userId);
                }
            });
        }
        if (this.context.IsAdmin || this.context.IsSysAdmin) {
            EventsBinding.SingleClick($instance.deactivateProfile, async () : Promise<void> => {
                if (!ObjectValidator.IsEmptyOrNull(this.userId)) {
                    await this.auth.DeactivateUser(this.userId, true);
                    await this.FetchUserProfile(this.userId);
                }
            });
            EventsBinding.SingleClick($instance.activateProfile, async () : Promise<void> => {
                if (!ObjectValidator.IsEmptyOrNull(this.userId)) {
                    await this.auth.DeactivateUser(this.userId, false);
                    await this.FetchUserProfile(this.userId);
                }
            });
            EventsBinding.SingleClick($instance.restoreProfile, async () : Promise<void> => {
                if (!ObjectValidator.IsEmptyOrNull(this.userId)) {
                    await this.auth.RestoreUser(this.userId);
                    await this.FetchUserProfile(this.userId);
                }
            });
            EventsBinding.SingleClick($instance.deleteProfile, async () : Promise<void> => {
                if (!ObjectValidator.IsEmptyOrNull(this.userId)) {
                    await this.auth.DeleteUser(this.userId);
                    await this.FetchUserProfile(this.userId);
                }
            });
            EventsBinding.SingleClick($instance.exportPerms, async () : Promise<void> => {
                const methods : string[] = await this.auth.getAuthorizedMethodsFor(this.userId);
                await new FileFetch().Open(<IFileFetchOptions>{
                    content : Convert.Base64ToBlob(
                        ObjectEncoder.Base64(JSON.stringify({
                            userId: this.userId,
                            methods
                        }, null, 2)),
                        "application/json; charset=utf-8"),
                    fileName: this.userId + "_authMethods.json"
                });
            });
        }
        EventsBinding.SingleClick($instance.faActivate, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull(this.userId) && !ObjectValidator.IsEmptyOrNull(this.faSecret)) {
                ValidationBinding.RestoreValidMark($instance.verifyCode);
                if (ObjectValidator.IsEmptyOrNull($instance.verifyCode.Value())) {
                    ValidationBinding.IsValidInput($instance.verifyCode, false);
                    this.ShowMessage(this.localization.codeMustBeEntered, ToastType.ERROR);
                } else if (await this.auth.Validate2FA($instance.verifyCode.Value(), this.faSecret)) {
                    await this.auth.setUserProfile({twoFA: this.faSecret}, this.userId);
                    $instance.verifyCode.Value("");
                    await this.FetchUserProfile(this.userId);
                } else {
                    ValidationBinding.IsValidInput($instance.verifyCode, false);
                    this.ShowMessage(this.localization.failedToValidateAuthCode, ToastType.ERROR);
                }
            }
        });
        EventsBinding.SingleClick($instance.faDeactivate, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull(this.userId)) {
                await this.auth.setUserProfile({twoFA: null}, this.userId);
                await this.FetchUserProfile(this.userId);
            }
        });
    }

    protected processProfileData($profile : User) : void {
        const instance : Panel = this.getInstanceOwner();
        instance.profileMail.Value("");
        instance.profilePhone.Value("");
        instance.profileName.Value("");
        if (!ObjectValidator.IsEmptyOrNull($profile)) {
            instance.profileMail.Value($profile.Email);
            instance.profilePhone.Value($profile.Phone);
            instance.profileName.Value($profile.UserName);
        }
    }

    protected getUserProfileData() : IUserProfileOptions {
        const instance : Panel = this.getInstanceOwner();
        return {
            email: instance.profileMail.Value(),
            name : instance.profileName.Value(),
            phone: instance.profilePhone.Value()
        };
    }
}

export interface IUsersPanelLocalization {
    permissionsAdd : string;
    noTokensCreated : string;
    failedToGenerate2FACode : string;
    failedToGenerated2FAKey : string;
    onlySingleFileSupported : string;
    imageMustBeSelected : string;
    unsupportedCharactersInName : string;
    profileUpdated : string;
    codeMustBeEntered : string;
    failedToValidateAuthCode : string;
}

// generated-code-start
/* eslint-disable */
export const IUsersPanelLocalization = globalThis.RegisterInterface(["permissionsAdd", "noTokensCreated", "failedToGenerate2FACode", "failedToGenerated2FAKey", "onlySingleFileSupported", "imageMustBeSelected", "unsupportedCharactersInName", "profileUpdated", "codeMustBeEntered", "failedToValidateAuthCode"]);
/* eslint-enable */
// generated-code-end
