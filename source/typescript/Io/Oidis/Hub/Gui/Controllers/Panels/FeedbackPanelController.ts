/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { EnvironmentConnector } from "../../Connectors/EnvironmentConnector.js";
import { EmailValidator } from "../../DAO/Validators/EmailValidator.js";
import { FeedbackType } from "../../Enums/FeedbackType.js";
import { DashboardPage } from "../../Pages/DashboardPage.js";
import { FeedbackPanel, IFeedbackCategory } from "../../Panels/Dashboard/FeedbackPanel.js";
import { BasePanelController } from "../../Primitives/BasePanelController.js";

@Resolver("/feedback", ["/feedback/{category}"])
export class FeedbackPanelController extends BasePanelController {
    protected validationLocalization : IFeedbackPanelValidation;
    protected ignoreMailDomains : string[];
    private env : EnvironmentConnector;

    constructor($instance : FeedbackPanel) {
        super($instance);

        this.env = EnvironmentConnector.getInstanceSingleton();
        this.validationLocalization = {
            emailMissing: "E-mail is required",
            emailFormat : "E-mail must be in correct format",
            emailIgnored: "Specify your e-mail to we should reply",
            uuid        : "Specify user ID, email or name",
            request     : "Specify your request",
            sendError   : "Request send has failed. Please try later again or send email directly to info@oidis.io."
        };
        this.ignoreMailDomains = ["oidis.io", "oidis.org"];
    }

    public async FetchData() : Promise<void> {
        const instance : FeedbackPanel = this.getInstanceOwner();
        if (this.context.IsAuthenticated) {
            instance.RestoreForm();
            instance.email.Value((await this.context.getUserProfile()).Email);
        } else {
            instance.RestoreForm(true);
        }
        if (this.RequestArgs().GET().KeyExists("category")) {
            const selectType : FeedbackType = this.categoryToType(this.RequestArgs().GET().getItem("category"));
            if (!ObjectValidator.IsEmptyOrNull(selectType)) {
                instance.getCategories().forEach(($config : IFeedbackCategory) : void => {
                    if ($config.value === selectType) {
                        instance.SelectCategory($config);
                    }
                });
            }
        }
        if (this.RequestArgs().POST().KeyExists("body")) {
            instance.body.Value(this.RequestArgs().POST().getItem("body"));
        }
    }

    protected categoryToType($value : string) : FeedbackType {
        switch (StringUtils.ToLowerCase($value)) {
        case "agents":
            return FeedbackType.Agents;
        case "email":
            return FeedbackType.Email;
        case "logs":
            return FeedbackType.Logs;
        case "perms":
            return FeedbackType.Permissions;
        case "registry":
            return FeedbackType.Registry;
        default:
            return null;
        }
    }

    protected async resolver($instance : FeedbackPanel) : Promise<void> {
        EventsBinding.SingleClick($instance.send, async () : Promise<void> => {
            ValidationBinding.RestoreValidMark($instance.email);
            ValidationBinding.RestoreValidMark($instance.uuid);
            ValidationBinding.RestoreValidMark($instance.body);
            if (ObjectValidator.IsEmptyOrNull($instance.email.Value())) {
                ValidationBinding.IsValidInput($instance.email, false);
                (<DashboardPage>$instance.Parent()).ShowMessage(this.validationLocalization.emailMissing, ToastType.ERROR);
            } else if (!new EmailValidator().validate($instance.email.Value())) {
                ValidationBinding.IsValidInput($instance.email, false);
                (<DashboardPage>$instance.Parent()).ShowMessage(this.validationLocalization.emailFormat, ToastType.ERROR);
            } else if (StringUtils.ContainsIgnoreCase($instance.email.Value(), ...this.ignoreMailDomains)) {
                ValidationBinding.IsValidInput($instance.email, false);
                (<DashboardPage>$instance.Parent()).ShowMessage(this.validationLocalization.emailIgnored, ToastType.ERROR);
            } else if (ObjectValidator.IsEmptyOrNull($instance.body.Value())) {
                ValidationBinding.IsValidInput($instance.body, false);
                (<DashboardPage>$instance.Parent()).ShowMessage(this.validationLocalization.request, ToastType.ERROR);
            } else if ($instance.catSelector.Value() === FeedbackType.Permissions &&
                ObjectValidator.IsEmptyOrNull($instance.uuid.Value())) {
                ValidationBinding.IsValidInput($instance.uuid, false);
                (<DashboardPage>$instance.Parent()).ShowMessage(this.validationLocalization.uuid, ToastType.ERROR);
            } else if (await this.env.SendFeedback($instance.email.Value(),
                $instance.catSelector.Value(), $instance.body.Value(), $instance.uuid.Value(), !this.context.IsAuthenticated)) {
                $instance.send.Visible(false);
                $instance.success.Visible(true);
                setTimeout(async () : Promise<void> => {
                    await this.FetchData();
                }, 3500);
            } else {
                (<DashboardPage>$instance.Parent()).ShowMessage(this.validationLocalization.sendError, ToastType.ERROR);
            }
        }, 10000);
    }

    protected getInstanceOwner() : FeedbackPanel {
        return <FeedbackPanel>super.getInstanceOwner();
    }
}

export interface IFeedbackPanelValidation {
    emailMissing? : string;
    emailFormat? : string;
    emailIgnored? : string;
    uuid? : string;
    request? : string;
    sendError? : string;
}

// generated-code-start
/* eslint-disable */
export const IFeedbackPanelValidation = globalThis.RegisterInterface(["emailMissing", "emailFormat", "emailIgnored", "uuid", "request", "sendError"]);
/* eslint-enable */
// generated-code-end
