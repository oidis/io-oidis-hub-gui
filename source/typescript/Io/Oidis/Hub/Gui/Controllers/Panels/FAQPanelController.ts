/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { Accordion } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Accordion.js";
import { EnvironmentConnector } from "../../Connectors/EnvironmentConnector.js";
import { IFAQTopic } from "../../DAO/FAQDao.js";
import { FAQPanel } from "../../Panels/Dashboard/FAQPanel.js";
import { BasePanelController } from "../../Primitives/BasePanelController.js";

@Resolver("/faq")
export class FAQPanelController extends BasePanelController {
    protected client : EnvironmentConnector;

    constructor($instance : FAQPanel) {
        super($instance);
        this.client = EnvironmentConnector.getInstanceSingleton();
    }

    public async FetchData() : Promise<void> {
        const instance : FAQPanel = this.getInstanceOwner();
        const accordion : Accordion = new Accordion();
        instance.content.Clear();
        instance.content.AddChild(accordion);
        (await this.client.getFAQTopics()).forEach(($topic : IFAQTopic) : void => {
            accordion.AddItem($topic.what, `<p class="mb-0">${$topic.how}</p>`);
        });
    }

    protected getInstanceOwner() : FAQPanel {
        return <FAQPanel>super.getInstanceOwner();
    }
}
