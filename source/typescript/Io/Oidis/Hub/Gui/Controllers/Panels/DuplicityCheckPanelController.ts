/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { BasePanelController } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanelController.js";
import { AuthManagerConnector } from "../../Connectors/AuthManagerConnector.js";
import { DuplicityCheckPanel } from "../../Panels/Dashboard/DuplicityCheckPanel.js";

@Resolver("/duplicityCheck")
export class DuplicityCheckController extends BasePanelController {
    private auth : AuthManagerConnector;

    constructor($instance : DuplicityCheckPanel) {
        super($instance);
        this.auth = AuthManagerConnector.getClient();
    }

    public async FetchData() : Promise<void> {
        this.getInstanceOwner().reportContent.Content("Report not available yet");
    }

    protected getInstanceOwner() : DuplicityCheckPanel {
        return <DuplicityCheckPanel>super.getInstanceOwner();
    }

    protected async resolver($instance : DuplicityCheckPanel) : Promise<void> {
        $instance.emailCheck.getEvents().setOnClick(async () : Promise<void> => {
            $instance.reportContent.Content("<h2>Result</h2>" +
                JSON.stringify(await this.auth.CheckEmailDuplicity(), null, 2));
        });
    }
}
