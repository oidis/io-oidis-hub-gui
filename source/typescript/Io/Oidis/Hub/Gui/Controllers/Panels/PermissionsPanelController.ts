/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { FileFetch, IFileFetchOptions } from "@io-oidis-services/Io/Oidis/Services/Utils/FileFetch.js";
import { DialogBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/DialogBinding.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { FileUploadBinding } from "../../Bindings/FileUploadBinding.js";
import { UserSearchBinding } from "../../Bindings/UserSearchBinding.js";
import { AuthManagerConnector, IDefaultGroupOwner, IGroupsDump, IPermissionsReport } from "../../Connectors/AuthManagerConnector.js";
import { Group } from "../../Models/Group.js";
import { User } from "../../Models/User.js";
import { PermissionsPanel, PermissionsTabType } from "../../Panels/Dashboard/PermissionsPanel.js";
import { BasePanelController } from "../../Primitives/BasePanelController.js";
import { GroupsTableRecord } from "../../UserControls/GroupsTable.js";
import { PermissionInputGroup } from "../../UserControls/PermissionInputGroup.js";
import { PermissionsItem } from "../../UserControls/PermissionsItem.js";
import { UsersTableRecord } from "../../UserControls/UsersTable.js";
import { Convert } from "../../Utils/Convert.js";

@Resolver("/perms")
export class PermissionsPanelController extends BasePanelController<PermissionsPanel> {
    private auth : AuthManagerConnector;
    private groups : string[];
    private readonly persistence : IPersistenceHandler;
    private report : IPermissionsReport;

    constructor($instance : PermissionsPanel) {
        super($instance);

        this.auth = AuthManagerConnector.getClient();
        this.groups = [];
        this.persistence = PersistenceFactory.getPersistence(this.getClassName());
    }

    public async FetchData() : Promise<void> {
        const instance : PermissionsPanel = this.getInstanceOwner();
        this.report = await this.auth.getPermissionsReport();
        instance.syncWarningHolder.Visible(Object.keys(this.report.sysPermsRelicts).length > 0);
        await this.fetchAllGroups();
        await instance.groupsTable.ShowData(true);
        await instance.usersTable.ShowData(true);
    }

    protected async beforeLoad($instance? : PermissionsPanel) : Promise<void> {
        if (!this.persistence.Exists(PersistenceType.TAB)) {
            $instance.SelectedTab([PermissionsTabType.Users]);
            this.persistence.Variable(PersistenceType.TAB, [PermissionsTabType.Users]);
        } else {
            $instance.SelectedTab(this.persistence.Variable(PersistenceType.TAB));
        }
    }

    protected async resolver($instance : PermissionsPanel) : Promise<void> {
        [$instance.usersTab, $instance.groupsTab].forEach(($element : GuiCommons) : void => {
            $element.getEvents().setOnClick(() : void => {
                this.persistence.Variable(PersistenceType.TAB, $instance.SelectedTab());
            });
        });
        $instance.usersTable.ItemsPerPage(15);
        $instance.usersTable.PagesLimit(5);

        $instance.newUserName.Value("");
        new UserSearchBinding().Bind($instance.searchUser, async () : Promise<void> => {
            await $instance.usersTable.ShowData(true);
        });

        [
            $instance.withDeactivatedUsers, $instance.withDeletedUsers, $instance.withUniqueUsersPerms
        ].forEach(($checkbox : CheckBox) : void => {
            EventsBinding.SingleClick($checkbox, async () : Promise<void> => {
                await $instance.usersTable.ShowData(true);
            });
        });
        EventsBinding.SingleClick($instance.resetCache, async () : Promise<void> => {
            await this.auth.ResetCache();
        });
        EventsBinding.SingleClick($instance.addNewUser, async () : Promise<void> => {
            ValidationBinding.RestoreValidMark($instance.newUserName);
            if (StringUtils.Contains($instance.newUserName.Value(), ".", "@")) {
                ValidationBinding.IsValidInput($instance.newUserName, false);
                this.ShowMessage("User name contains some of unsupported characters: \".\", \"@\"", ToastType.ERROR);
            } else if (await this.auth.Register({
                name          : $instance.newUserName.Value(),
                password      : this.getUID(),
                withActivation: false
            })) {
                const size : number = $instance.usersTable.Size();
                const perPageCount : number = $instance.usersTable.ItemsPerPage();
                $instance.usersTable.SelectPage(Math.floor((size + 1) / perPageCount) * perPageCount);
                await $instance.usersTable.ShowData();
                $instance.newUserName.Value("");
            } else {
                this.ShowMessage("Failed to add new user", ToastType.ERROR);
            }
        });
        $instance.usersTable.BindData(async ($offset : number, $limit : number) : Promise<IModelListResult> => {
            let user : string = "";
            if (!ObjectValidator.IsEmptyOrNull($instance.searchUser.autocomplete.Data())) {
                user = $instance.searchUser.autocomplete.Data().id;
            }
            if (ObjectValidator.IsEmptyOrNull(user)) {
                user = ".*";
            }
            return this.auth.FindUsers(user, {
                offset                : $offset,
                limit                 : $limit,
                onlyActive            : !$instance.withDeactivatedUsers.Value(),
                withDeleted           : $instance.withDeletedUsers.Value(),
                onlyWithOwnPermissions: $instance.withUniqueUsersPerms.Value()
            });
        }, async ($data : User, $item : UsersTableRecord) : Promise<void> => {
            let userName : string = $data.UserName;
            if ($data.Deleted) {
                userName = `<span style="color: red;">${userName}</span>`;
            } else if (!$data.Activated) {
                userName = `<span style="color: gray;">${userName}</span>`;
            }
            $item.name.Text(userName);
            $item.Data($data);
            let info : string = "";
            if (!ObjectValidator.IsEmptyOrNull($data.Firstname)) {
                info += $data.Firstname;
                if (!ObjectValidator.IsEmptyOrNull($data.Lastname)) {
                    info += " " + $data.Lastname;
                }
            }
            if (!ObjectValidator.IsEmptyOrNull($data.Email) && StringUtils.Contains($data.Email, "@")) {
                if (!ObjectValidator.IsEmptyOrNull(info)) {
                    info += ", ";
                }
                info += $data.Email;
            }
            if (ObjectValidator.IsEmptyOrNull(info)) {
                info = " ";
            }
            $item.info.Text(info);

            const allUserMethods : string[] = [];
            $data.Groups.forEach(($group : Group) : void => {
                if (!$group.Deleted && !$group.Hidden) {
                    $group.AuthorizedMethods.forEach(($method : string) : void => {
                        if (!allUserMethods.includes($method)) {
                            allUserMethods.push($method);
                        }
                    });
                    const groupItem : PermissionsItem = new PermissionsItem();
                    groupItem.Text($group.Name);
                    groupItem.Value({user: $data.Id, group: $group.Name});
                    groupItem.Visible(!$group.Deleted);
                    EventsBinding.SingleClick(groupItem, async () : Promise<void> => {
                        await this.auth.RemoveGroup(groupItem.Value().group, groupItem.Value().user);
                        await $instance.usersTable.ShowData();
                    });
                    $item.groups.AddChild(groupItem);
                }
            });
            $data.DefaultGroup.AuthorizedMethods.forEach(($method : string) : void => {
                if (!this.report.allDefaultMethods.includes($method) && !allUserMethods.includes($method)) {
                    const groupItem : PermissionsItem = new PermissionsItem();
                    groupItem.Text($method);
                    groupItem.setStyle("btn-outline-info");
                    if ($method === "*") {
                        groupItem.setStyle("btn-outline-warning");
                    } else if (StringUtils.Contains($method, "*")) {
                        groupItem.setStyle("btn-outline-primary");
                    }
                    groupItem.Value({group: $data.DefaultGroup.Id, methods: $data.DefaultGroup.AuthorizedMethods, method: $method});
                    EventsBinding.SingleClick(groupItem, async () : Promise<void> => {
                        await this.auth.setAuthorizedMethods(groupItem.Value().group,
                            groupItem.Value().methods.filter(($method : string) : boolean => {
                                return $method !== groupItem.Value().method;
                            }), false);
                        await $instance.usersTable.ShowData();
                    });
                    $item.groups.AddChild(groupItem);
                }
            });
            let showGroups : boolean = false;
            const availableGroups : PermissionInputGroup = new PermissionInputGroup();
            availableGroups.Text("Add");
            EventsBinding.SingleClick(availableGroups, async () : Promise<void> => {
                if (!ObjectValidator.IsEmptyOrNull(availableGroups.Data()) || this.groups.includes(availableGroups.Value())) {
                    await this.auth.AddGroup(availableGroups.Data().group, availableGroups.Data().user);
                } else if (!ObjectValidator.IsEmptyOrNull(availableGroups.Value())) {
                    await this.auth.setAuthorizedMethods($data.DefaultGroup.Id,
                        [availableGroups.Value()], true);
                }
                await $instance.usersTable.ShowData();
            });
            this.groups.forEach(($group : string) : void => {
                let groupExit : boolean = false;
                $data.Groups.forEach(($userGroup : Group) : void => {
                    if ($userGroup.Name === $group) {
                        groupExit = true;
                    }
                });
                if (!groupExit) {
                    showGroups = true;
                    availableGroups.AddItem($group, {user: $data.Id, group: $group});
                }
            });
            if (showGroups) {
                $item.groups.AddChild(availableGroups);
            }
        }, async ($item : UsersTableRecord) : Promise<void> => {
            EventsBinding.SingleClick($item.deleteButton, async () : Promise<void> => {
                const user : User = $item.Data();
                if (user.Deleted) {
                    $instance.userDialog.Data(user.Id);
                    $instance.userDialog.Visible(true);
                } else {
                    await this.auth.DeleteUser(user.Id);
                    await $instance.usersTable.ShowData(true);
                }
            });
            EventsBinding.SingleClick($item.profileButton, async () : Promise<void> => {
                await this.getParent().users.FetchUserProfile($item.Data().Id);
                this.getHttpManager().ReloadTo("/users/");
            });
        });
        new DialogBinding().Bind({
            close : [$instance.userDialogClose, $instance.userDialogCloseButton],
            holder: $instance.userDialog,
            submit: $instance.userDeleteAction
        });
        [$instance.userDialogClose, $instance.userDialogCloseButton].forEach(($element : Button) : void => {
            $element.getEvents().setOnClick(() : void => {
                $instance.userDialog.Data(null);
            });
        });
        EventsBinding.SingleClick($instance.userDeleteAction, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull($instance.userDialog.Data())) {
                await this.auth.RemoveUser($instance.userDialog.Data());
                await $instance.usersTable.ShowData(true);
            }
            $instance.userDialog.Visible(false);
        });

        EventsBinding.SingleClick($instance.addNewGroup, async () : Promise<void> => {
            await this.auth.CreateGroup($instance.newGroupName.Value());
            await $instance.groupsTable.ShowData(true);
            await this.fetchAllGroups();
            await $instance.usersTable.ShowData();
            $instance.newGroupName.Value("");
        });
        [
            $instance.withDeletedGroups, $instance.withHiddenGroups, $instance.withEmptyGroups,
            $instance.onlySysGroups, $instance.withAsterix,
            $instance.groupsSearch
        ].forEach(($checkbox : CheckBox | Button) : void => {
            EventsBinding.SingleClick($checkbox, async () : Promise<void> => {
                await $instance.groupsTable.ShowData(true);
            });
        });
        EventsBinding.SingleClick($instance.withDefaultPerms, async () : Promise<void> => {
            await $instance.groupsTable.ShowData();
        });

        EventsBinding.SingleClick($instance.exportPermsReport, async () : Promise<void> => {
            await new FileFetch().Open(<IFileFetchOptions>{
                content : Convert.Base64ToBlob(
                    ObjectEncoder.Base64(JSON.stringify(this.report, null, 2)),
                    "application/json; charset=utf-8"),
                fileName: "PermissionReport_" + this.getEnvironmentArgs().getProjectName() + "_" + new Date().getTime() + ".json"
            });
        });
        EventsBinding.SingleClick($instance.exportGroupsPerms, async () : Promise<void> => {
            await new FileFetch().Open(<IFileFetchOptions>{
                content : Convert.Base64ToBlob(
                    ObjectEncoder.Base64(JSON.stringify(await this.auth.ExportAuthMethods(), null, 2)),
                    "application/json; charset=utf-8"),
                fileName: "AuthMethodsBackup_" + this.getEnvironmentArgs().getProjectName() + "_" + new Date().getTime() + ".json"
            });
        });
        const fileUpload : FileUploadBinding = new FileUploadBinding();
        fileUpload.Bind({
            button: $instance.importGroupsPerms
        }, {
            onData          : async ($files : FileList) : Promise<void> => {
                if ($files.length === 1) {
                    let data : IGroupsDump[] = null;
                    try {
                        data = JSON.parse((await fileUpload.Read({encoded: false}))[0]);
                    } catch (ex) {
                        LogIt.Error("Failed to parse permissions data", ex);
                        this.ShowMessage("Failed to parse permissions data", ToastType.ERROR);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(data)) {
                        if (ObjectValidator.IsArray(data)) {
                            try {
                                await this.auth.ImportAuthMethods(data);
                            } catch (ex) {
                                LogIt.Error("Failed to import permissions data", ex);
                                this.ShowMessage("Failed to import permissions data", ToastType.ERROR);
                            }
                            await this.FetchData();
                        } else {
                            this.ShowMessage("Failed to import permissions data - incompatible format", ToastType.ERROR);
                        }
                    }
                } else {
                    this.ShowMessage("Only single file is supported", ToastType.ERROR);
                }
            },
            onEmptySelection: () : void => {
                this.ShowMessage("File with permissions data must be selected", ToastType.ERROR);
            }
        });
        fileUpload.setFilter(".json");
        [$instance.syncSysGroups, $instance.resetSysGroups].forEach(($button : Button) : void => {
            EventsBinding.SingleClick($button, async () : Promise<void> => {
                await this.auth.SyncSystemGroups();
                await this.FetchData();
            });
        });
        EventsBinding.SingleClick($instance.cleanupGroupsPerms, async () : Promise<void> => {
            await this.auth.ResetDefaultGroups();
            await $instance.groupsTable.ShowData(true);
        });
        EventsBinding.SingleClick($instance.deleteRedundantGroups, async () : Promise<void> => {
            await this.auth.DeleteRedundantGroups();
            await $instance.groupsTable.ShowData(true);
        });
        EventsBinding.SingleClick($instance.cleanupDefaultPerms, async () : Promise<void> => {
            await this.auth.ResetDefaultAuthMethods();
            await $instance.groupsTable.ShowData(true);
        });
        $instance.groupsTable.BindData(async ($offset : number, $limit : number) : Promise<IModelListResult> => {
            let filter : any = $instance.groupsSearchField.Value();
            if (ObjectValidator.IsEmptyOrNull(filter)) {
                filter = ".*";
            } else {
                filter = {
                    authorizedMethods:
                        {$regex: ".*" + StringUtils.Replace(filter, " ", ".*") + ".*", $options: "i"}
                };
            }
            return this.auth.FindGroups(filter, {
                withDeleted         : $instance.withDeletedGroups.Value(),
                withHidden          : $instance.withHiddenGroups.Value(),
                offset              : $offset,
                limit               : $limit,
                withEmptyPermissions: $instance.withEmptyGroups.Value(),
                onlySystem          : $instance.onlySysGroups.Value(),
                onlyWithAsterix     : $instance.withAsterix.Value()
            });
        }, async ($data : Group, $item : GroupsTableRecord) : Promise<void> => {
            let groupName : string = $data.Name;
            if (this.report.sysGroups.includes($data.Id)) {
                groupName += " (system)";
            } else if (this.report.redundantGroups.includes($data.Id)) {
                groupName += " (unused)";
            } else {
                const owner : IDefaultGroupOwner = this.report.defaultGroups.find(($group : IDefaultGroupOwner) : boolean => {
                    return $group.Id === $data.Id;
                });
                if (!ObjectValidator.IsEmptyOrNull(owner)) {
                    groupName += " (" + owner.UserName + ")";
                    (<any>$data).OwnerId = owner.UserId;
                }
            }
            if ($data.Deleted) {
                groupName = `<span style="color: red;">${groupName}</span>`;
            }
            $item.name.Text(groupName);
            $item.Data($data);

            $data.AuthorizedMethods.forEach(($methodName : string) : void => {
                if ($instance.withDefaultPerms.Value() || !this.report.allDefaultMethods.includes($methodName)) {
                    const groupItem : PermissionsItem = new PermissionsItem();
                    groupItem.Text($methodName);
                    groupItem.Value({method: $methodName, group: $data.Name});
                    groupItem.setStyle("btn-outline-info");
                    if (this.report.sysPermsRelicts.hasOwnProperty($data.Name) &&
                        this.report.sysPermsRelicts[$data.Name].includes($methodName)) {
                        groupItem.setStyle("btn-outline-danger");
                    } else if (this.report.allDefaultMethods.includes($methodName)) {
                        groupItem.setStyle("btn-outline-secondary");
                    } else if ($methodName === "*") {
                        groupItem.setStyle("btn-outline-warning");
                    } else if (StringUtils.Contains($methodName, "*") || !StringUtils.Contains($methodName, ".")) {
                        groupItem.setStyle("btn-outline-primary");
                    }
                    EventsBinding.SingleClick(groupItem, async () : Promise<void> => {
                        await this.auth.setAuthorizedMethods(groupItem.Value().group,
                            $data.AuthorizedMethods.filter(($value : string) : boolean => {
                                return $value !== groupItem.Value().method;
                            }));
                        await $instance.groupsTable.ShowData();
                    });
                    $item.permissions.AddChild(groupItem);
                }
            });
            if (!$data.Deleted) {
                const addMethod : PermissionInputGroup = new PermissionInputGroup();
                addMethod.Text("Add");
                this.report.features.forEach(($method : string) : void => {
                    addMethod.AddItem($method);
                });
                this.report.appMethods.forEach(($method : string) : void => {
                    addMethod.AddItem($method);
                });
                EventsBinding.SingleClick(addMethod, async () : Promise<void> => {
                    await this.auth.setAuthorizedMethods($item.Data().Id,
                        [addMethod.Value()], true);
                    await $instance.groupsTable.ShowData();
                });
                $item.permissions.AddChild(addMethod);
                $item.deleteButton.Visible(!this.report.sysGroups.includes($data.Id));
                $item.restoreButton.Visible(false);
            } else {
                $item.restoreButton.Visible(true);
                $item.deleteButton.Visible(this.report.redundantGroups.includes($data.Id));
            }
        }, async ($item : GroupsTableRecord) : Promise<void> => {
            EventsBinding.SingleClick($item.deleteButton, async () : Promise<void> => {
                if ($item.Data().Deleted) {
                    if (this.report.redundantGroups.includes($item.Data().Id)) {
                        $instance.groupRemoveDialog.Data($item.Data().Id);
                        $instance.groupRemoveDialog.Visible(true);
                    } else {
                        this.ShowMessage("Groups not marked as redundant can not be deleted.", ToastType.ERROR);
                    }
                } else {
                    $instance.groupDialog.Data($item.Data().Id);
                    $instance.groupDialog.Visible(true);
                }
            });
            EventsBinding.SingleClick($item.restoreButton, async () : Promise<void> => {
                await this.auth.RestoreGroup($item.Data().Id);
                await $instance.groupsTable.ShowData();
            });
        });
        new DialogBinding().Bind({
            close : [$instance.groupDialogClose, $instance.groupDialogCloseButton],
            holder: $instance.groupDialog,
            submit: $instance.groupDeleteAction
        });
        [$instance.groupDialogClose, $instance.groupDialogCloseButton].forEach(($element : Button) : void => {
            $element.getEvents().setOnClick(() : void => {
                $instance.groupDialog.Data(null);
            });
        });
        EventsBinding.SingleClick($instance.groupDeleteAction, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull($instance.groupDialog.Data())) {
                await this.auth.DeleteGroup($instance.groupDialog.Data());
                this.report = await this.auth.getPermissionsReport();
                await $instance.groupsTable.ShowData(true);
                await this.fetchAllGroups();
                await $instance.usersTable.ShowData();
            }
            $instance.groupDialog.Visible(false);
        });
        new DialogBinding().Bind({
            close : [$instance.groupRemoveDialogClose, $instance.groupRemoveDialogCloseButton],
            holder: $instance.groupRemoveDialog,
            submit: $instance.groupRemoveAction
        });
        [$instance.groupRemoveDialogClose, $instance.groupRemoveDialogCloseButton].forEach(($element : Button) : void => {
            $element.getEvents().setOnClick(() : void => {
                $instance.groupRemoveDialog.Data(null);
            });
        });
        EventsBinding.SingleClick($instance.groupRemoveAction, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull($instance.groupRemoveDialog.Data())) {
                await this.auth.RemoveGroup($instance.groupRemoveDialog.Data());
                await this.FetchData();
            }
            $instance.groupRemoveDialog.Visible(false);
        });
    }

    private async fetchAllGroups() : Promise<void> {
        this.groups = await this.auth.getActiveGroupsList();
        if (ObjectValidator.IsEmptyOrNull(this.groups)) {
            this.groups = ["Public"];
        }
    }
}

class PersistenceType extends BaseEnum {
    public static readonly TAB : string = "tab";
}
