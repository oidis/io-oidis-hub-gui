/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanelController } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanelController.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { ReportsConnector } from "../../Connectors/ReportsConnector.js";
import { ReportsPanel } from "../../Panels/Dashboard/ReportsPanel.js";

@Resolver("/report", ["/report/{appId}"])
export class ReportsPanelController extends BasePanelController {
    private reports : ReportsConnector;

    constructor($instance : ReportsPanel) {
        super($instance);
        this.reports = new ReportsConnector(true, this.getHttpManager().CreateLink("/connector.config.jsonp"));
    }

    public async FetchReports($appId : string) : Promise<void> {
        const instance : ReportsPanel = this.getInstanceOwner();
        instance.appIdSelector.Content($appId);
        const items : string[] = await this.reports.getReports($appId);
        instance.reportIdSelector.Content("report #");
        instance.reportContent.Content("Select report ...");

        instance.reportIdMenu.Clear();
        if (!ObjectValidator.IsEmptyOrNull(items)) {
            for await (const logPath of items) {
                const logName : string = StringUtils.Substring(logPath, StringUtils.IndexOf(logPath, $appId + "/"));
                const item : DropDownListItem = new DropDownListItem();
                item.Text(logName);
                item.Value({name: logName, path: logPath});
                item.getEvents().setOnClick(async ($event : EventArgs) : Promise<void> => {
                    const item : DropDownListItem = $event.Owner();
                    instance.reportIdSelector.Content(item.Value().name);
                    const path : string = item.Value().path;
                    const data : string = await this.reports.getReport(path);
                    let content : string = "";
                    if (StringUtils.EndsWith(path, ".png")) {
                        content = `<img class="img-fluid" src="${data}" alt="${path}">`;
                    } else {
                        content = StringUtils.Replace(data, "INFO:", "\nINFO:");
                        content = StringUtils.Replace(content, "DEBUG:", "\nDEBUG:");
                        content = StringUtils.Replace(content, "ERROR:", "\nERROR:");
                        content = StringUtils.Replace(content, "WARNING:", "\nWARNING:");
                        let index : number = 0;
                        const length : number = StringUtils.Length(content);
                        let wrapped : string = "";
                        let lastBreak : number = 0;
                        while (index < length) {
                            if (content[index] === "\n") {
                                lastBreak = index;
                            }
                            if (index - lastBreak > 140) {
                                wrapped += "\n";
                                lastBreak = index;
                            }
                            wrapped += content[index];
                            index++;
                        }
                        wrapped = StringUtils.Replace(wrapped, "ERROR:", "<span class=\"Error\">ERROR:</span>");
                        wrapped = StringUtils.Replace(wrapped, "WARNING:", "<span class=\"Warning\">WARNING:</span>");
                        content = StringUtils.Replace(wrapped, "\n", StringUtils.NewLine());
                    }

                    instance.reportContent.Content(content);
                });
                instance.reportIdMenu.AddChild(item);
            }
            instance.reportIdSelector.Enabled(true);
        } else {
            instance.reportIdMenu.Content("No reports found");
            instance.reportIdSelector.Enabled(false);
        }
    }

    public async FetchData() : Promise<void> {
        const instance : ReportsPanel = this.getInstanceOwner();
        const items : string[] = await this.reports.getReportingApps();
        if (!ObjectValidator.IsEmptyOrNull(items)) {
            instance.appIdMenu.Clear();
            for (const appName of items) {
                const item : DropDownListItem = new DropDownListItem();
                item.Text(appName);
                item.getEvents().setOnClick(($event : EventArgs) : void => {
                    this.FetchReports($event.Owner().Text());
                });
                instance.appIdMenu.AddChild(item);
            }
        } else {
            instance.appIdMenu.Content("No apps found");
        }
        const appId : string = this.RequestArgs().GET().getItem("appId");
        if (!ObjectValidator.IsEmptyOrNull(appId)) {
            await this.FetchReports(appId);
        }
    }

    protected getInstanceOwner() : ReportsPanel {
        return <ReportsPanel>super.getInstanceOwner();
    }
}
