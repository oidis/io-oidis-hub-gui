/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { IConnectorInfo } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IConnectorInfo.js";
import { DialogBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/DialogBinding.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { EnvironmentConnector } from "../../Connectors/EnvironmentConnector.js";
import { PushNotificationType } from "../../Enums/PushNotificationType.js";
import { IProject } from "../../Interfaces/IProject.js";
import { RuntimeTool } from "../../Panels/Dashboard/RuntimeTool.js";
import { BasePanelController } from "../../Primitives/BasePanelController.js";
import { ConnectorsTableRecord } from "../../UserControls/ConnectorsTable.js";

@Resolver("/appRuntime")
export class RuntimeToolController extends BasePanelController {
    private client : EnvironmentConnector;

    constructor($instance : RuntimeTool) {
        super($instance);
        this.client = EnvironmentConnector.getInstanceSingleton();
    }

    public async FetchData() : Promise<void> {
        const instance : RuntimeTool = this.getInstanceOwner();
        instance.version.Text(this.getEnvironmentArgs().getProjectVersion());
        instance.buildTime.Text(this.getEnvironmentArgs().getBuildTime());
        instance.upTime.Text(Convert.TimeToLongString(await this.client.getUpTime()));
        instance.platform.Text(this.getEnvironmentArgs().getPlatform());
        await instance.connectorsTable.ShowData();
    }

    protected async beforeLoad($instance : RuntimeTool) : Promise<void> {
        $instance.notificationsSelector.Clear();
        this.getPushNotificationsList().forEach(($option : string) : void => {
            const item : DropDownListItem = new DropDownListItem();
            item.Text($option);
            item.getEvents().setOnClick(() : void => {
                $instance.message.Value($option);
            });
            $instance.notificationsSelector.AddChild(item);
        });
    }

    protected getPushNotificationsList() : string[] {
        return [
            "System update will be done today from 22:00 to 23:30.",
            "System is facing instability. We are working on fix the issue. We are sorry for inconvenience, thanks for your understanding."
        ];
    }

    protected async resolver($instance? : RuntimeTool) : Promise<void> {
        new DialogBinding().Bind({
            close : [$instance.close, $instance.closeButton],
            holder: $instance.killDialog,
            submit: $instance.killConfirm
        });
        $instance.kill.getEvents().setOnClick(() : void => {
            $instance.killDialog.Visible(true);
        });
        EventsBinding.SingleClick($instance.killConfirm, async () : Promise<void> => {
            $instance.getEvents().FireAsynchronousMethod(() : void => {
                $instance.killDialog.Visible(false);
            }, 100);
            await this.client.Exit(130);
        });

        EventsBinding.SingleClick($instance.push, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull($instance.message.Value())) {
                await this.client.PushNotification({type: PushNotificationType.HEADER_CHANGE, data: $instance.message.Value()});
            } else {
                await this.client.PushNotification({type: PushNotificationType.HEADER_RESTORE});
            }
        }, 5000);

        EventsBinding.SingleClick($instance.findConnectors, async () : Promise<void> => {
            await $instance.connectorsTable.ShowData(true);
        });
        $instance.connectorsTable.BindData(async ($offset : number, $limit : number) : Promise<IModelListResult> => {
            return this.client.getConnectorsInfo($instance.connectorsFilter.Value(), {limit: $limit, offset: $offset});
        }, async ($data : IConnectorInfo, $item : ConnectorsTableRecord) : Promise<void> => {
            $item.name.Text($data.namespace);
            $item.owner.Text($data.owner);
            $item.requester.Text($data.requester);
            $item.created.Text(Convert.TimeToGMTformat($data.created) +
                "(" + Convert.TimeToLongString(new Date().getTime() - new Date($data.created).getTime()) + ")");
        });

        EventsBinding.SingleClick($instance.loadFEConfig, async () : Promise<void> => {
            $instance.feConfigContent.Visible(false);
            $instance.feConfigContent.Text(`<pre>${JSON.stringify(this.getEnvironmentArgs().getProjectConfig(), null, 2)}</pre>`);
            $instance.feConfigContent.Visible(true);
        });

        EventsBinding.SingleClick($instance.loadBEConfig, async () : Promise<void> => {
            const operations = window.crypto.subtle || (<any>window.crypto).webkitSubtle;
            if (!operations) {
                this.ShowMessage("Web Crypto API not supported. Try different browser.", ToastType.ERROR);
            } else {
                try {
                    const algorithm : string = "AES-GCM";
                    const iv = new TextDecoder("utf-8").decode(window.crypto.getRandomValues(new Uint8Array(12)));
                    const key = await operations.generateKey({
                            length: 256,
                            name  : algorithm
                        }, true, ["encrypt", "decrypt"]
                    );
                    const encrypted : any = await this.client.getProjectConfig(ObjectEncoder.Base64(JSON.stringify({
                        iv,
                        jwk: await operations.exportKey("jwk", key)
                    })));
                    if (!ObjectValidator.IsEmptyOrNull(encrypted)) {
                        const config : IProject = JSON.parse(new TextDecoder("utf-8").decode(await operations.decrypt(
                            {
                                iv  : new TextEncoder().encode(iv),
                                name: algorithm
                            },
                            key,
                            Convert.Base64ToArrayBuffer(encrypted)
                        )));
                        $instance.beConfigContent.Visible(false);
                        $instance.beConfigContent.Text(`<pre>${JSON.stringify(config, null, 2)}</pre>`);
                        $instance.beConfigContent.Visible(true);
                    } else {
                        this.ShowMessage("Failed to receive config data.", ToastType.ERROR);
                    }
                } catch (ex) {
                    LogIt.Error(ex);
                    this.ShowMessage("Failed to process data or E2E encryption.", ToastType.ERROR);
                }
            }
        });
    }

    protected getInstanceOwner() : RuntimeTool {
        return <RuntimeTool>super.getInstanceOwner();
    }
}
