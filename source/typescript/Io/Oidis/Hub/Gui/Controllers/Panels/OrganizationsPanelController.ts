/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import {
    IModelListResult,
    IOrganizationProfileOptions
} from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import {
    AutocompleteBinding,
    IAutocompleteData,
    IAutocompleteUserControls
} from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/AutocompleteBinding.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { FileUploadBinding } from "../../Bindings/FileUploadBinding.js";
import { AuthManagerConnector } from "../../Connectors/AuthManagerConnector.js";
import { Group } from "../../Models/Group.js";
import { Organization } from "../../Models/Organization.js";
import { User } from "../../Models/User.js";
import { OrganizationsPanel } from "../../Panels/Dashboard/OrganizationsPanel.js";
import { BasePanelController } from "../../Primitives/BasePanelController.js";
import { OrgUserItem } from "../../UserControls/OrgUsersTable.js";

@Resolver("/organizations", ["/organizations/{organizationId}"])
export class OrganizationsPanelController extends BasePanelController<OrganizationsPanel> {
    private auth : AuthManagerConnector;
    private profileId : string;
    private persistence : IPersistenceHandler;

    constructor($instance : OrganizationsPanel) {
        super($instance);
        this.auth = AuthManagerConnector.getClient();

        this.profileId = "";
        this.persistence = PersistenceFactory.getPersistence(this.getClassName());
    }

    public async FetchData() : Promise<void> {
        const instance : OrganizationsPanel = this.getInstanceOwner();
        let id : string = instance.search.autocomplete.Data();
        if (ObjectValidator.IsEmptyOrNull(id)) {
            id = instance.search.searchField.Value();
        }
        let avatarSource : string = "resource/graphics/Io/Oidis/Hub/Gui/OrgImage.png";
        instance.orgId.Value("");
        instance.orgName.Value("");
        instance.deactivate.Visible(false);
        instance.activate.Visible(false);
        instance.restore.Visible(false);
        instance.delete.Visible(false);

        this.persistence.Variable("profile", id);

        instance.editContent.Visible(false);
        instance.createContent.Visible(true);

        if (!ObjectValidator.IsEmptyOrNull(id)) {
            const profile : Organization = await this.auth.getOrganizationProfile(id, true);
            if (!ObjectValidator.IsEmptyOrNull(profile)) {
                this.profileId = id;

                instance.orgName.Value(profile.Name);
                instance.orgId.Value(profile.Id);
                instance.createContent.Visible(false);
                instance.editContent.Visible(true);

                if (!ObjectValidator.IsEmptyOrNull(profile.Image)) {
                    avatarSource = <any>profile.Image;
                }
                if (profile.Activated) {
                    instance.deactivate.Visible(true);
                } else if (!profile.Deleted) {
                    instance.activate.Visible(true);
                    instance.delete.Visible(true);
                } else {
                    instance.restore.Visible(true);
                }

                await instance.usersTable.ShowData(true);
            } else {
                this.ShowMessage("Failed to find organization", ToastType.ERROR);
                instance.usersTable.Clear();
            }
        } else {
            instance.usersTable.Clear();
        }
        (<HTMLImageElement>instance.profileImg.InstanceOwner()).src = avatarSource;
    }

    protected async resolver($instance : OrganizationsPanel) : Promise<void> {
        const binding : AutocompleteBinding = new AutocompleteBinding();
        const selectUC : IAutocompleteUserControls = {
            dropdownSelector: $instance.search.autocompleteSelector,
            field           : $instance.search.searchField,
            menu            : $instance.search.autocomplete
        };
        binding.Bind(selectUC, async ($query : any) : Promise<IAutocompleteData[]> => {
            const data : IModelListResult<Organization> = await this.auth.FindOrganizations({
                $or: [
                    {name: $query},
                    {_id: $query}
                ]
            }, {limit: 15, onlyActive: false, withDeleted: true});
            const output : IAutocompleteData[] = [];
            if (data.size > 0) {
                data.data.forEach(($organization : Organization) : void => {
                    output.push({
                        text : $organization.Name,
                        value: $organization.Id
                    });
                });
            }
            return output;
        });
        EventsBinding.SingleClick($instance.search.search, async () : Promise<void> => {
            $instance.search.clear.Visible(!ObjectValidator.IsEmptyOrNull(selectUC.menu.Data()));
            await this.FetchData();
        });
        EventsBinding.SingleClick($instance.search.clear, async () : Promise<void> => {
            selectUC.field.Value("");
            selectUC.field.Readonly(false);
            selectUC.menu.Data(null);
            await this.FetchData();
            $instance.search.clear.Visible(false);
        });
        selectUC.field.getEvents().setOnDoubleClick(async () : Promise<void> => {
            const data : IModelListResult<Organization> = await this.auth.FindOrganizations(".*", {onlyActive: false, withDeleted: true});
            selectUC.field.Value("");
            selectUC.field.Readonly(false);
            selectUC.menu.Data(null);
            selectUC.menu.Clear();
            data.data.sort(($a : Organization, $b : Organization) : number => {
                const nameA : string = $a.Name.toUpperCase();
                const nameB : string = $b.Name.toUpperCase();
                return (nameA < nameB) ? -1 : (nameA > nameB) ? 1 : 0;
            }).forEach(($organization : Organization) : void => {
                const item : DropDownListItem = new DropDownListItem();
                const text : string = $organization.Name;
                if ($organization.Activated) {
                    item.Text(text);
                } else if ($organization.Deleted) {
                    item.Text(`<span class="text-danger">${text}</span>`);
                } else {
                    item.Text(`<span class="text-secondary">${text}</span>`);
                }
                item.Value($organization.Id);
                selectUC.menu.AddChild(item);
                item.getEvents().setOnClick(async () : Promise<void> => {
                    selectUC.menu.Data(item.Value());
                    selectUC.field.Value(text);
                    selectUC.field.Readonly(true);
                    binding.Show(selectUC, false);
                    $instance.search.clear.Visible(true);
                    await this.FetchData();
                });
            });
            binding.Show(selectUC, true);
        });
        const fileUpload : FileUploadBinding = new FileUploadBinding();
        let imageData : string = "";
        fileUpload.Bind({
            button: $instance.photoUploadButton
        }, {
            onData          : async ($files : FileList) : Promise<void> => {
                if ($files.length === 1) {
                    imageData = (await fileUpload.Read())[0];
                    $instance.profileImg.Source("data:image/png;base64, " + imageData);
                } else {
                    this.ShowMessage("Only single file is supported", ToastType.ERROR);
                }
            },
            onEmptySelection: () : void => {
                this.ShowMessage("Image file must be selected", ToastType.ERROR);
            }
        });
        fileUpload.setFilter(".jpg,.png");
        EventsBinding.SingleClick($instance.create, async () : Promise<void> => {
            const options : IOrganizationProfileOptions = {
                name: $instance.orgName.Value()
            };
            if (ObjectValidator.IsEmptyOrNull(options.name)) {
                this.ShowMessage("Organization name must be specified", ToastType.ERROR);
            } else {
                if (!ObjectValidator.IsEmptyOrNull(imageData)) {
                    options.image = imageData;
                }
                selectUC.menu.Data(await this.auth.RegisterOrganization(options));
                this.ShowMessage("Profile created", ToastType.SUCCESS);
                await this.FetchData();
            }
        });
        EventsBinding.SingleClick($instance.update, async () : Promise<void> => {
            const options : IOrganizationProfileOptions = {
                name: $instance.orgName.Value()
            };
            if (!ObjectValidator.IsEmptyOrNull(imageData)) {
                options.image = imageData;
            }
            await this.auth.setOrganizationProfile(options, this.profileId);
            this.ShowMessage("Profile updated");
        });
        EventsBinding.SingleClick($instance.reset, async () : Promise<void> => {
            await this.FetchData();
        });
        EventsBinding.SingleClick($instance.deactivate, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull(this.profileId)) {
                await this.auth.DeactivateOrganization(this.profileId, true);
                await this.FetchData();
            }
        });
        EventsBinding.SingleClick($instance.activate, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull(this.profileId)) {
                await this.auth.DeactivateOrganization(this.profileId, false);
                await this.FetchData();
            }
        });
        EventsBinding.SingleClick($instance.restore, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull(this.profileId)) {
                await this.auth.RestoreOrganization(this.profileId);
                await this.FetchData();
            }
        });
        EventsBinding.SingleClick($instance.delete, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull(this.profileId)) {
                await this.auth.DeleteOrganization(this.profileId);
                selectUC.field.Value("");
                selectUC.field.Readonly(false);
                selectUC.menu.Data(null);
                await this.FetchData();
            }
        });
        if (!ObjectValidator.IsEmptyOrNull(this.RequestArgs().GET().getItem("organizationId"))) {
            selectUC.field.Value(this.RequestArgs().GET().getItem("organizationId"));
        } else if (!ObjectValidator.IsEmptyOrNull(this.persistence.Variable("profile"))) {
            selectUC.field.Value(this.persistence.Variable("profile"));
        }
        if (!ObjectValidator.IsEmptyOrNull(selectUC.field.Value())) {
            selectUC.field.Readonly(true);
            $instance.search.clear.Visible(true);
        }

        $instance.usersTable.ItemsPerPage(15);
        $instance.usersTable.PagesLimit(5);
        new AutocompleteBinding().Bind({
            dropdownSelector: $instance.userAutocompleteSelector,
            field           : $instance.userField,
            menu            : $instance.userAutocomplete
        }, async ($query : any) : Promise<IAutocompleteData[]> => {
            const data : IModelListResult = await this.auth.FindUsers({
                $or   : [
                    {firstname: $query},
                    {lastname: $query},
                    {email: $query}
                ],
                groups: {$nin: [this.context.getGroups().OrganizationOwner.Id]}
            }, {limit: 10});
            const output : IAutocompleteData[] = [];
            if (data.size > 0) {
                data.data.forEach(($user : User) : void => {
                    let text : string = $user.Email;
                    if (!ObjectValidator.IsEmptyOrNull($user.Firstname)) {
                        text = $user.Firstname + " " + $user.Lastname + " (" + text + ")";
                    } else {
                        text = $user.UserName + " (" + text + ")";
                    }
                    output.push({text, value: $user.Id});
                });
            }
            return output;
        });
        EventsBinding.SingleClick($instance.userAdd, async () : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull(this.profileId) &&
                !ObjectValidator.IsEmptyOrNull($instance.userAutocomplete.Data())) {
                if (await this.auth
                    .AddUserToOrganization($instance.userAutocomplete.Data(), this.profileId)) {
                    await this.auth.AddGroup($instance.usersTable.IsEmpty() ?
                        this.context.getGroups().OrganizationOwner.Name :
                        this.context.getGroups().Member.Name, $instance.userAutocomplete.Data());
                    await this.context.Reload();
                    await $instance.usersTable.ShowData(true);
                    this.ShowMessage("User account has been successfully add to organization");
                    $instance.userField.Value("");
                } else {
                    this.ShowMessage("User account not found", ToastType.ERROR);
                }
            } else {
                this.ShowMessage("User account must be specified", ToastType.ERROR);
            }
        });
        EventsBinding.SingleClick($instance.withInactive, async () : Promise<void> => {
            await $instance.usersTable.ShowData(true);
        });
        const roles : any = [
            {name: "Member", value: this.context.getGroups().Member},
            {name: "Owner", value: this.context.getGroups().OrganizationOwner}
        ];
        $instance.usersTable.BindData(async ($offset : number, $limit : number) : Promise<IModelListResult> => {
            const data : IModelListResult<User> = await this.auth.getOrganizationChart(this.profileId, {
                limit : $limit,
                offset: $offset
            });
            const sorted : User[] = [];
            data.data.sort(($a : User, $b : User) : number => {
                const nameA : string = ($a.Firstname + " " + $a.Lastname).toUpperCase();
                const nameB : string = ($b.Firstname + " " + $b.Lastname).toUpperCase();
                return (nameA < nameB) ? -1 : (nameA > nameB) ? 1 : 0;
            }).forEach(($user : User) : void => {
                sorted.push($user);
            });
            data.data = sorted;
            return data;
        }, async ($data : User, $item : OrgUserItem) : Promise<void> => {
            if ($data.Activated || $instance.withInactive.Value()) {
                if (!$data.Activated) {
                    $item.Enabled(false);
                }
                $item.Data($data);
                if (!ObjectValidator.IsEmptyOrNull($data.Firstname)) {
                    $item.name.Text($data.Firstname + " " + $data.Lastname);
                } else {
                    $item.name.Text($data.UserName);
                }
                $item.info.Text($data.Email);

                $item.roleSelector.Data("");
                $item.roleMenu.Clear();
                roles.forEach(($role : any) : void => {
                    let contains : boolean = false;
                    $data.Groups.forEach(($group : Group) : void => {
                        if ($group.Id === $role.value.Id) {
                            contains = true;
                        }
                    });
                    if (contains) {
                        $item.roleSelector.Content($role.name);
                        $item.roleSelector.Data({edited: false, data: $role.value});
                    } else {
                        const item : DropDownListItem = new DropDownListItem();
                        $item.roleMenu.AddChild(item);
                        item.Text($role.name);
                        item.Data($role.value);
                        item.getEvents().setOnClick(async () : Promise<void> => {
                            $item.roleSelector.Content($role.name);
                            $item.roleSelector.Data({edited: true, data: $role.value});
                        });
                    }
                });
                if (!ObjectValidator.IsEmptyOrNull($item.roleSelector.Data()) &&
                    $item.roleSelector.Data().data === this.context.getGroups().OrganizationOwner) {
                    $item.delete.Visible(false);
                }
            }
        }, async ($item : OrgUserItem) : Promise<void> => {
            EventsBinding.SingleClick($item.edit, async () : Promise<void> => {
                await this.getParent().users.FetchUserProfile($item.Data().Id);
                this.getHttpManager().ReloadTo("/users/");
            });
            EventsBinding.SingleClick($item.save, async () : Promise<void> => {
                const role : any = $item.roleSelector.Data();
                if (!ObjectValidator.IsEmptyOrNull(role) && role.edited) {
                    for await (const possibleRole of roles) {
                        for await (const group of $item.Data().Groups) {
                            if (group.Id === possibleRole.value.Id) {
                                await this.auth.RemoveGroup(possibleRole.value.Name, $item.Data().Id);
                            }
                        }
                    }
                    if (await this.auth.AddGroup(role.data.Name, $item.Data().Id)) {
                        this.ShowMessage("User role has been changed", ToastType.SUCCESS);
                    } else {
                        this.ShowMessage("Change of user role has failed", ToastType.ERROR);
                    }
                }
                await $instance.usersTable.ShowData();
            });
            EventsBinding.SingleClick($item.delete, async () : Promise<void> => {
                if (await this.auth.RemoveUserFromOrganization($item.Data().Id, this.profileId)) {
                    this.ShowMessage("User has been removed from organization");
                    await $instance.usersTable.ShowData(true);
                }
            });
        });
    }
}
