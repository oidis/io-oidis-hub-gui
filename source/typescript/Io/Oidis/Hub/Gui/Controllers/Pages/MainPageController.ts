/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { AsyncBasePageController } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/Resolvers/AsyncBasePageController.js";
import { MainPage } from "../../Pages/MainPage.js";
import { InitPageController } from "./InitPageController.js";

@Resolver("/main")
export class MainPageController extends AsyncBasePageController {

    constructor() {
        super();
        this.setInstanceOwner(new MainPage());
        this.setPageTitle("Oidis Hub");
    }

    protected async afterLoad($instance : MainPage) : Promise<void> {
        $instance.hubInfo.Text("Copyright " + StringUtils.YearFrom(2019) + " Oidis");
        $instance.dashboard.getEvents().setOnClick(() : void => {
            this.getHttpManager().ReloadTo("/registry");
        });
    }

    protected async environmentCheckEnabled() : Promise<boolean> {
        return InitPageController.Enabled();
    }
}
