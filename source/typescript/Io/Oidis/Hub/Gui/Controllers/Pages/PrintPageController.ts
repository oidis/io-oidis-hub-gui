/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { AsyncBasePageController } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/Resolvers/AsyncBasePageController.js";
import { IPrintBindingOptions, IPrintCredentials, PrintBinding } from "../../Bindings/PrintBinding.js";
import { BaseModel } from "../../Models/BaseModel.js";
import { BasePageLayout } from "../../Primitives/BasePageLayout.js";

class PrintPageLayout extends BasePageLayout {
    // make it instanceable
}

@Resolver("/print/{user}/{token}")
export class PrintPageController extends AsyncBasePageController {
    private serviceUser : string;
    private headlessToken : string;

    constructor() {
        super();

        this.setInstanceOwner(new PrintPageLayout());
        this.setPageTitle(this.getEnvironmentArgs().getAppName() + " - Print");
    }

    public Process() : void {
        if ((<any>this).processing) {
            this.getHttpManager().Refresh();
        } else {
            super.Process();
        }
    }

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        if ($GET.KeyExists("user")) {
            this.serviceUser = $GET.getItem("user");
        } else {
            this.serviceUser = null;
        }
        if ($GET.KeyExists("token")) {
            this.headlessToken = $GET.getItem("token");
        } else {
            this.headlessToken = null;
        }
        super.argsHandler($GET, $POST);
    }

    protected async afterLoad($instance : PrintPageLayout) : Promise<void> {
        LogIt.Info("version: " + this.getEnvironmentArgs().getProjectVersion());
        LogIt.Info("build: " + this.getEnvironmentArgs().getBuildTime());
        this.getPrintBindingInstance().Bind($instance, JsonUtils.Extend({
            withRESThooks: true,
            onAuthRequest: async () : Promise<IPrintCredentials> => {
                return {
                    userName: this.serviceUser,
                    token   : this.headlessToken
                };
            }
        }, this.getPrintBindingOptions()));
    }

    protected getPrintBindingOptions() : IPrintBindingOptions<GuiCommons, BaseModel> {
        return <any>{};
    }

    protected getPrintBindingInstance() : PrintBinding {
        return new PrintBinding();
    }
}
