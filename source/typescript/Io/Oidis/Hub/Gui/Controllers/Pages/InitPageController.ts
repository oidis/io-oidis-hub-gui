/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { EnvironmentConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/EnvironmentConnector.js";
import { EnvironmentState } from "@io-oidis-services/Io/Oidis/Services/Enums/EnvironmentState.js";
import { AsyncBasePageController } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/Resolvers/AsyncBasePageController.js";
import { InitPage } from "../../Pages/InitPage.js";

@Resolver("/ServerError/NotReady")
export class InitPageController extends AsyncBasePageController {
    private static noInitPageReported : boolean = false;
    private mainPageUrl : string;

    public static async Enabled() : Promise<boolean> {
        if (!await EnvironmentConnector.getInstanceSingleton().InitPageEnabled()) {
            if (!InitPageController.noInitPageReported && !await EnvironmentConnector.getInstanceSingleton().IsEnvironmentLoaded()) {
                InitPageController.noInitPageReported = true;
                LogIt.Warning("Init page has been disabled by target configuration. " +
                    "Please, read all possible logs in case of page load issues");
            }
            return false;
        }
        return true;
    }

    constructor() {
        super();
        this.setInstanceOwner(new InitPage());
        this.mainPageUrl = "/main";
    }

    protected setMainPageUrl($value : string) : void {
        this.mainPageUrl = Property.String(this.mainPageUrl, $value);
    }

    protected async beforeLoad($instance : InitPage) : Promise<void> {
        let reloadTo : string = this.getRefererUrl();
        if (ObjectValidator.IsEmptyOrNull(reloadTo)) {
            reloadTo = this.mainPageUrl;
        }
        const reload : any = () : void => {
            if (this.getHttpManager().getRequest().getUrl() !== reloadTo) {
                this.getHttpManager().ReloadTo(reloadTo);
            } else {
                this.getHttpManager().Reload();
            }
        };
        if (!await EnvironmentConnector.getInstanceSingleton().IsEnvironmentLoaded()) {
            EnvironmentConnector.getInstanceSingleton().AddLoadingListener()
                .OnChange(($status : EnvironmentState) : void => {
                    if ($status === EnvironmentState.ADMIN_INIT) {
                        this.getHttpManager().ReloadTo("/AdminInit", new ArrayList<any>(), true);
                    } else if ($status !== EnvironmentState.ADMIN_ENABLED) {
                        $instance.status.Text(this.stateToTextMapping($status));
                    }
                })
                .OnError(($error : ErrorEventArgs) : void => {
                    LogIt.Error($error.Message());
                    $instance.status.Text("Failed to init environment");
                })
                .Then(() : void => {
                    reload();
                });
        } else {
            reload();
        }
    }

    protected stateToTextMapping($state : EnvironmentState) : string {
        switch ($state) {
        case EnvironmentState.DB_CONNECTING:
            return "Connecting to database";
        case EnvironmentState.DB_CONNECTED:
            return "Connected to database. Started DB initialization.";
        case EnvironmentState.DB_CONNECTION_ERROR:
        case EnvironmentState.DB_NOT_CONNECTED:
            return "Failed to connect to database.";
        case EnvironmentState.DB_PREPARED:
            return "Database ready and initialized.";
        case EnvironmentState.AGENTS_INIT:
            return "Started agents initialization.";
        default:
            return $state + "";
        }
    }
}
