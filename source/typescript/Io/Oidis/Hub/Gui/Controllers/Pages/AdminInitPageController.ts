/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { AsyncBasePageController } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/Resolvers/AsyncBasePageController.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { UserSearchBinding } from "../../Bindings/UserSearchBinding.js";
import { AuthManagerConnector } from "../../Connectors/AuthManagerConnector.js";
import { AdminInitPage } from "../../Pages/AdminInitPage.js";

@Resolver("/AdminInit")
export class InitPageController extends AsyncBasePageController {
    private auth : AuthManagerConnector;
    private userId : string;

    constructor() {
        super();
        this.setInstanceOwner(new AdminInitPage());
        this.auth = AuthManagerConnector.getClient();
        this.userId = null;
    }

    public InstanceOwner() : AdminInitPage {
        return <AdminInitPage>super.InstanceOwner();
    }

    protected async beforeLoad($instance : AdminInitPage) : Promise<void> {
        await this.auth.LogOut();

        new UserSearchBinding().Bind($instance.find, async ($data : any) : Promise<void> => {
            if (!ObjectValidator.IsEmptyOrNull($data)) {
                this.userId = $data.id;
            } else {
                this.userId = null;
            }
        });
        EventsBinding.SingleClick($instance.authenticate, async () : Promise<void> => {
            ValidationBinding.RestoreValidMark($instance.authToken);
            try {
                this.auth.CurrentToken(await this.auth.AuthenticateAdmin($instance.authToken.Value()));
                if (!ObjectValidator.IsEmptyOrNull(this.auth.CurrentToken())) {
                    $instance.authContent.Visible(false);
                    $instance.statusContent.Visible(false);
                    $instance.activateContent.Visible(true);
                } else {
                    this.showError("Authorization has failed");
                }
            } catch (ex) {
                LogIt.Warning(ex.message);
                ValidationBinding.IsValidInput($instance.authToken, false);
                this.showError("Authorization has failed");
            }
        });
        EventsBinding.SingleClick($instance.activate, async () : Promise<void> => {
            $instance.statusContent.Visible(false);
            ValidationBinding.RestoreValidMark($instance.user);
            ValidationBinding.RestoreValidMark($instance.pass);
            ValidationBinding.RestoreValidMark($instance.repass);
            let passed : boolean = true;
            let user : string = null;

            if (ObjectValidator.IsEmptyOrNull($instance.find.autocomplete.Data())) {
                if (ObjectValidator.IsEmptyOrNull($instance.user.Value())) {
                    ValidationBinding.IsValidInput($instance.user, false);
                    passed = false;
                } else {
                    user = $instance.user.Value();
                }
                if (ObjectValidator.IsEmptyOrNull($instance.pass.Value())) {
                    ValidationBinding.IsValidInput($instance.pass, false);
                    passed = false;
                }
                if (ObjectValidator.IsEmptyOrNull($instance.repass.Value())) {
                    ValidationBinding.IsValidInput($instance.repass, false);
                    passed = false;
                }

                if (!passed) {
                    this.showError("Fill-in all required fields");
                } else if (StringUtils.ContainsIgnoreCase($instance.user.Value(), "@", ".")) {
                    ValidationBinding.IsValidInput($instance.user, false);
                    passed = false;
                    this.showError("User name contains unsupported characters \"@\", \".\".");
                } else if ($instance.pass.Value() !== $instance.repass.Value()) {
                    ValidationBinding.IsValidInput($instance.repass, false);
                    passed = false;
                    this.showError("Passwords do not match");
                }
            } else {
                if (ObjectValidator.IsEmptyOrNull(this.userId)) {
                    user = $instance.find.autocomplete.Data().id;
                } else {
                    user = this.userId;
                }
            }
            if (passed) {
                if (ObjectValidator.IsEmptyOrNull(user)) {
                    this.showError("User must be defined or found");
                } else {
                    try {
                        await this.auth.RegisterAdmin(user, $instance.pass.Value());
                        $instance.activateContent.Visible(false);
                        $instance.confirmContent.Visible(true);
                        const countDown : any = ($index : number = 1) : void => {
                            $instance.getEvents().FireAsynchronousMethod(() : void => {
                                if ($index < 10) {
                                    $instance.countDown.Text((10 - $index) + "");
                                    countDown($index + 1);
                                } else {
                                    this.getHttpManager().ReloadTo("/logout", new ArrayList(), true);
                                }
                            }, 1000);
                        };
                        countDown();
                    } catch (ex) {
                        this.showError(ex.message);
                    }
                }
            }
        });
    }

    private showError($message : string) : void {
        this.InstanceOwner().statusContent.Visible(true);
        this.InstanceOwner().status.Text($message);
    }
}
