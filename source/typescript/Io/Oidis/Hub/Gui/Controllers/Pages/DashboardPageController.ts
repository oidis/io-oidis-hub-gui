/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { DashboardPage } from "../../Pages/DashboardPage.js";
import { UsersPanel } from "../../Panels/Dashboard/UsersPanel.js";
import { ITabDescriptor } from "../../Primitives/BasePageLayout.js";
import { MenuPageLayoutController } from "../../Primitives/MenuPageLayoutController.js";
import { UsersPanelController } from "../Panels/UsersPanelController.js";
import { InitPageController } from "./InitPageController.js";

@Resolver("/", [
    "/index", "/index.html", "/web/",
    "/tools", "/tools/{toolId}",
    "/backdoorLogin"
])
export class DashboardPageController extends MenuPageLayoutController<DashboardPage> {
    private toolsRegister : ITabDescriptor[];
    private declare readonly users : UsersPanelController<UsersPanel>;

    constructor() {
        super();

        this.setInstanceOwner(new DashboardPage());
        this.setPageTitle("Oidis Hub Dashboard");
        this.toolsRegister = [];
    }

    protected async accessValidator() : Promise<boolean> {
        await this.user.Reload();
        return this.user.IsAuthenticated;
    }

    protected async environmentCheckEnabled() : Promise<boolean> {
        return InitPageController.Enabled();
    }

    /**
     * @deprecated Use getTabsConfiguration instead
     * @param {ITabDescriptor} $config Specify configuration of tool panel
     * @returns {void}
     */
    protected registerTool($config : ITabDescriptor) : void {
        let extended : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($config.id)) {
            this.toolsRegister.forEach(($tool : ITabDescriptor) : void => {
                if ($tool.id === $config.id) {
                    JsonUtils.Extend($tool, $config);
                    extended = true;
                }
            });
        }
        if (!extended) {
            this.toolsRegister.push(JsonUtils.Extend({enabled: true}, $config));
        }
    }

    protected async beforeLoad($instance : DashboardPage) : Promise<void> {
        this.toolsRegister.forEach(($config : ITabDescriptor) : void => {
            if ($config.enabled) {
                const panel : any = new (<any>$config.content)();
                $instance[panel.Id()] = panel;
                $config.content = panel;
                if (ObjectValidator.IsEmptyOrNull($config.id)) {
                    $config.id = panel.Id();
                }
                if (ObjectValidator.IsEmptyOrNull($config.link)) {
                    $config.link = "/#/tools/" + $config.id;
                }
                $instance.AddTabConfig($config);
            }
        });
    }

    protected reloadToDefaultResolver($default : string = "/home") : void {
        if (this.user.IsAuthenticated) {
            this.getHttpManager().ReloadTo("/registry");
        } else {
            this.getHttpManager().ReloadTo($default);
        }
    }
}
