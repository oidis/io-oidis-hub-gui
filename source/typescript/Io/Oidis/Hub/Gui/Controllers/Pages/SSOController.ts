/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { SSOBinding } from "../../Bindings/SSOBinding.js";
import { SSOPage } from "../../Pages/SSOPage.js";
import { BasePageLayoutController } from "../../Primitives/BasePageLayoutController.js";
import { InitPageController } from "./InitPageController.js";

@Resolver("/login", [
    "/logout", "/signOut",
    "/signIn", "/signUp",
    "/resetPassword", "/resetPassword/{token}",
    "/accountActivation", "/accountActivation/{token}",
    "/invite", "/invite/{token}",
    "/twoFactorChallenge"
])
export class SSOController extends BasePageLayoutController<SSOPage> {
    private actionToken : string;
    private readonly ssoBinding : SSOBinding;

    constructor() {
        super();

        this.setInstanceOwner(new SSOPage());
        this.setPageTitle("Oidis Hub Authorization");
        this.ssoBinding = new SSOBinding();
    }

    protected async environmentCheckEnabled() : Promise<boolean> {
        return InitPageController.Enabled();
    }

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        super.argsHandler($GET, $POST);
        this.actionToken = $GET.getItem("token");
    }

    protected async afterLoad($instance : SSOPage) : Promise<void> {
        await super.afterLoad($instance);
        await this.ssoBinding.Bind({
            username    : $instance.signIn.email,
            password    : $instance.signIn.password,
            submit      : $instance.signIn.signIn,
            panel       : $instance.signIn,
            keepLoggedIn: $instance.signIn.remember
        }, {
            onRequestArgsFetch: () : HttpRequestEventArgs => {
                return this.RequestArgs();
            },
            onSuccess         : async () : Promise<void> => {
                ValidationBinding.RestoreValidMark($instance.signIn.email);
                ValidationBinding.RestoreValidMark($instance.signIn.password);
                await this.getHttpManager().NavigateTo("/main");
            },
            onError           : ($isValidation : boolean) : void => {
                ValidationBinding.RestoreValidMark($instance.signIn.email);
                ValidationBinding.RestoreValidMark($instance.signIn.password);
                if ($isValidation) {
                    ValidationBinding.IsValidInput($instance.signIn.email, false);
                    ValidationBinding.IsValidInput($instance.signIn.password, false);
                    $instance.ShowMessage("Please specify user name and password", ToastType.ERROR);
                } else {
                    $instance.ShowMessage("Login was not successful. Please validate user name and password.", ToastType.ERROR);
                }
            }
        });

        EventsBinding.SingleClick($instance.signUp.signUp, async () : Promise<void> => {
            if (await this.auth.Register({
                email    : $instance.signUp.email.Value(),
                firstname: $instance.signUp.firstName.Value(),
                lastname : $instance.signUp.lastName.Value(),
                name     : $instance.signUp.userName.Value(),
                password : $instance.signUp.password.Value()
            })) {
                $instance.ShowMessage(
                    "Account successfully created. Please, check your e-mail and process with activation.", ToastType.SUCCESS);
                await this.selectTab($instance.signIn);
            } else {
                $instance.ShowMessage("Failed to create user.", ToastType.ERROR);
            }
        });

        EventsBinding.SingleClick($instance.resetPassword.reset, async () : Promise<void> => {
            if (await this.auth.ResetPassword($instance.resetPassword.email.Value())) {
                $instance.ShowMessage("E-mail with reset password process has been send successfully.", ToastType.SUCCESS);
            } else {
                $instance.ShowMessage("Failed to reset password.", ToastType.ERROR);
            }
        });

        EventsBinding.SingleClick($instance.newPassword.update, async () : Promise<void> => {
            if (await this.auth.ResetPasswordConfirm(this.actionToken, $instance.newPassword.password.Value())) {
                this.actionToken = null;
                $instance.ShowMessage("Password has been successfully updated.", ToastType.SUCCESS);
                await this.selectTab($instance.signIn);
            } else {
                $instance.ShowMessage("Failed to update password.", ToastType.ERROR);
            }
        });

        this.ssoBinding.BindAuthCode($instance.twoFactorChallenge.authCodePanel, {
            on2FARequest  : async () : Promise<void> => {
                await this.selectTab($instance.twoFactorChallenge);
            },
            onAuthCodeRequest: async () : Promise<void> => {
                $instance.twoFactorChallenge.ShowOnlyPersonalCode();
            }
        });

        this.ssoBinding.BindTwoFA($instance.twoFactorChallenge, {
            onError  : async ($message : string) : Promise<void> => {
                $instance.ShowMessage($message, ToastType.ERROR);
            },
            onSuccess: async () : Promise<void> => {
                await this.getHttpManager().NavigateTo("/main");
            }
        });
    }

    protected async processRequestPath($path : string, $instance : SSOPage) : Promise<void> {
        if (StringUtils.Contains($path, "/twoFactorChallenge")) {
            await this.ssoBinding.ProcessOneTimeCode();
        } else if (StringUtils.Contains($path, "/logout", "/signOut")) {
            if (await this.auth.LogOut()) {
                try {
                    const response : Response = await fetch("/api/sso/logout");
                    if (!response.ok) {
                        LogIt.Warning("Failed to correctly log out user: " + await response.text());
                    }
                } catch (ex) {
                    LogIt.Warning("Failed to make log out request: " + ex.stack);
                }
                $instance.ShowMessage("You have been successfully logged out");
            }
            await this.selectTab($instance.signIn);
        } else if (StringUtils.Contains($path, "/login", "/signIn")) {
            await this.selectTab($instance.signIn);
        } else if (StringUtils.Contains($path, "/resetPassword")) {
            if (!ObjectValidator.IsEmptyOrNull(this.actionToken)) {
                await this.selectTab($instance.newPassword);
            } else {
                await this.selectTab($instance.resetPassword);
            }
        } else if (StringUtils.Contains($path, "/accountActivation")) {
            if (await this.auth.ActivateUser(this.actionToken)) {
                $instance.ShowMessage("Thanks for activation, now you can sing in", ToastType.SUCCESS);
            } else {
                $instance.ShowMessage("Failed to activate account or already activated", ToastType.ERROR);
            }
        } else {
            await super.processRequestPath($path, $instance);
        }
    }

    protected async onSuccess($instance : SSOPage) : Promise<void> {
        await super.onSuccess($instance);
        $instance.signIn.email.Value("");
        $instance.signIn.password.Value("");
        $instance.resetPassword.email.Value("");
    }
}
