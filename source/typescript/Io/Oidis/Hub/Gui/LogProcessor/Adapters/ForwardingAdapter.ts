/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { BaseAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/BaseAdapter.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { ITraceBackEntry } from "@io-oidis-commons/Io/Oidis/Commons/Utils/TraceBack.js";
import { LoggerClient } from "../../Connectors/LoggerConnector.js";

export class ForwardingAdapter extends BaseAdapter {
    private connector : LoggerClient;
    private buffer : ILoggerTrace[];
    private context : string;

    constructor() {
        super();
        this.connector = LoggerClient.getClient();
        this.buffer = [];
        this.context = "";
    }

    public setContext($value : string) : void {
        this.context = $value;
    }

    protected timeToString($value : number) : string {
        return <any>$value;
    }

    protected traceBackToString($traceback : ITraceBackEntry) : any {
        return $traceback;
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        let prefix : string = "> FE";
        if (!ObjectValidator.IsEmptyOrNull(this.context)) {
            prefix += "[" + this.context + "]";
        }
        $trace.message = prefix + ": " + $trace.message;
        (<any>$trace).isForwarded = true;
        return $trace;
    }

    protected print($trace : ILoggerTrace) {
        if ($trace.level === LogLevel.ERROR || $trace.level === LogLevel.WARNING) {
            this.buffer.push($trace);
            if (this.buffer.length === 1) {
                this.printAsync();
            }
        }
    }

    private printAsync() : void {
        if (this.buffer.length > 0) {
            this.connector.ProcessTrace(this.buffer[0])
                .then(() : void => {
                    this.buffer.shift();
                    this.printAsync();
                })
                .catch((ex) : void => {
                    console.error(ex); // eslint-disable-line no-console
                });
        }
    }
}
