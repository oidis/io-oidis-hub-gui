/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { BaseAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/BaseAdapter.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { ITraceBackEntry, TraceBack } from "@io-oidis-commons/Io/Oidis/Commons/Utils/TraceBack.js";

export class InMemoryAdapter extends BaseAdapter {
    private buffer : ILoggerTrace[];

    constructor() {
        super();
        this.buffer = [];
    }

    public getLogs() : ILoggerTrace[] {
        return this.buffer;
    }

    public Clear() : void {
        this.buffer = [];
    }

    protected traceBackToString($traceback : ITraceBackEntry) : any {
        return $traceback;
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        if ($trace.level === LogLevel.ERROR) {
            $trace.trace = TraceBack.Parse(<any>{stack: $trace.message});
            $trace.message = $trace.message.split("\n")[0];
        } else if (!this.verbose) {
            delete $trace.trace;
        }
        if (!this.verbose) {
            delete $trace.entryPoint;
        }
        return $trace;
    }

    protected print($trace : ILoggerTrace) {
        this.buffer.push($trace);
    }
}
