/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { AutocompleteBinding, IAutocompleteData } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/AutocompleteBinding.js";
import { AuthManagerConnector } from "../Connectors/AuthManagerConnector.js";
import { User } from "../Models/User.js";
import { AutocompleteSearch } from "../UserControls/AutocompleteSearch.js";

export class UserSearchBinding extends BaseObject {
    private auth : AuthManagerConnector;

    constructor() {
        super();

        this.auth = AuthManagerConnector.getClient();
    }

    public Bind($instance : AutocompleteSearch, $onFetch : ($data : any) => Promise<void>) : void {
        const binding : AutocompleteBinding = new AutocompleteBinding();
        binding.Bind({
            dropdownSelector: $instance.autocompleteSelector,
            field           : $instance.searchField,
            menu            : $instance.autocomplete
        }, async ($query : any) : Promise<IAutocompleteData[]> => {
            const data : IModelListResult = await this.auth.FindUsers({
                $or: [
                    {userName: $query},
                    {firstname: $query},
                    {lastname: $query},
                    {email: $query},
                    {_id: $query}
                ]
            }, {limit: 15, onlyActive: false, withDeleted: true});
            const output : IAutocompleteData[] = [];
            if (data.size > 0) {
                data.data.forEach(($user : User) : void => {
                    let text : string = "";
                    if (!ObjectValidator.IsEmptyOrNull($user.Firstname)) {
                        text += $user.Firstname;
                        if (!ObjectValidator.IsEmptyOrNull($user.Lastname)) {
                            text += " " + $user.Lastname;
                            if (!ObjectValidator.IsEmptyOrNull($user.Email)) {
                                if (!(StringUtils.Contains($user.Email, "@oidis.org", "@oidis.io") &&
                                    $user.Email.length >= 50)) {
                                    text += " (" + $user.Email + ")";
                                }
                            }
                        }
                    }
                    if (ObjectValidator.IsEmptyOrNull(text) && !ObjectValidator.IsEmptyOrNull($user.Email)) {
                        text = $user.Email;
                    }
                    if (ObjectValidator.IsEmptyOrNull(text) && !ObjectValidator.IsEmptyOrNull($user.UserName)) {
                        text = $user.UserName;
                    }
                    if (ObjectValidator.IsEmptyOrNull(text)) {
                        text = $user.Id;
                    }
                    output.push({
                        text,
                        value: {
                            id  : $user.Id,
                            type: "User"
                        }
                    });
                });
            }
            return output;
        });
        EventsBinding.SingleClick($instance.search, async () : Promise<void> => {
            $instance.clear.Visible(!ObjectValidator.IsEmptyOrNull($instance.autocomplete.Data()));
            await $onFetch($instance.autocomplete.Data());
        });
        EventsBinding.SingleClick($instance.clear, async () : Promise<void> => {
            $instance.searchField.Value("");
            $instance.searchField.Readonly(false);
            $instance.autocomplete.Data(null);
            await $onFetch(null);
            $instance.clear.Visible(false);
        });
    }
}
