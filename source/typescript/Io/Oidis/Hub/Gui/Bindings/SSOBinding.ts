/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { HttpRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { HttpRequestParser } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpRequestParser.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { KeyMap } from "@io-oidis-gui/Io/Oidis/Gui/Enums/KeyMap.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { PersistenceFactory } from "@io-oidis-gui/Io/Oidis/Gui/PersistenceFactory.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { HttpManager } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/HttpManager.js";
import { IWebServiceClient } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceClient.js";
import { IWebServiceProtocol } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceProtocol.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { AuthManagerConnector } from "../Connectors/AuthManagerConnector.js";
import { Loader } from "../Loader.js";
import { UserContext } from "../Structures/UserContext.js";
import { AuthCodeBinding, IAuthCodeUserControls } from "./AuthCodeBinding.js";

export class SSOBinding extends BaseObject {
    protected authClient : AuthManagerConnector;
    protected httpManager : HttpManager;
    protected requestArgs : HttpRequestEventArgs;
    protected localization : ITwoFactorBindingLocalization;
    protected readonly codeBinding : AuthCodeBinding;
    private readonly persistence : IPersistenceHandler;
    private twoFAUserControls : ITwoFactorBindingUserControls;
    private twoFAHandlers : ITwoFactorBindingHandlers;
    private authCodeHandlers : IAuthCodeBindingHandlers;
    private context : UserContext;

    constructor() {
        super();

        this.authClient = AuthManagerConnector.getClient();
        this.httpManager = Loader.getInstance().getHttpManager();
        this.persistence = PersistenceFactory.getPersistence(SSOBinding.ClassName());
        this.context = Loader.getInstance().getContext();
        this.codeBinding = new AuthCodeBinding();

        this.localization = {
            enterCode            : "Enter code",
            wrongCodeFormat      : "Wrong code format",
            invalidCode          : "Entered code is not valid",
            codeValidationFailure: "Code validation has failed. Please, try it again later or contact our support."
        };
    }

    public LastLoginUrl($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.persistence.Variable("lastUrl", $value);
            } else if ($value === null) {
                this.persistence.Destroy("lastUrl");
            }
        }
        return this.persistence.Variable("lastUrl");
    }

    public TwoFAChecked($value? : boolean) : boolean {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.persistence.Variable("last2FAResult", $value);
        }
        return this.persistence.Variable("last2FAResult");
    }

    /// TODO: add form validations -> using DOA DeepValidate, required fields etc. <- NK
    //        onError will be bind with this or refactored
    public async Bind($userControls : ISSOBindingUserControls, $handlers : ISSOBindingHandlers) : Promise<void> {
        WindowManager.getEvents().setOnKeyPress(($eventArgs : KeyEventArgs) : void => {
            if ($eventArgs.getKeyCode() === KeyMap.ENTER && $userControls.panel.Visible() &&
                !ObjectValidator.IsEmptyOrNull($userControls.panel.getChildList())) {
                this.ProcessLogIn($userControls, $handlers);
            }
        });
        EventsBinding.SingleClick($userControls.submit, async () : Promise<void> => {
            await this.ProcessLogIn($userControls, $handlers);
        });
        this.requestArgs = $handlers.onRequestArgsFetch();
    }

    public async ProcessLogIn($userControls : ISSOLoginUserControls, $handlers : ISSOBindingHandlers) : Promise<void> {
        this.requestArgs = $handlers.onRequestArgsFetch();
        const userName : string = $userControls.username.Value();
        const password : string = $userControls.password.Value();

        if (ObjectValidator.IsEmptyOrNull(userName) || ObjectValidator.IsEmptyOrNull(password)) {
            $handlers.onError(true);
        } else {
            try {
                let noExpire : boolean = false;
                if (!ObjectValidator.IsEmptyOrNull($userControls.keepLoggedIn)) {
                    /// TODO: this can reset expiration by request from different browser/device, but this should be expected behavior
                    //        otherwise it will need to be somehow supported by FE+BE footprints?
                    noExpire = $userControls.keepLoggedIn.Value();
                }
                const token : string = await this.authClient.LogIn(userName, password, noExpire);
                if (!ObjectValidator.IsEmptyOrNull(token)) {
                    this.authClient.CurrentToken(token);
                    this.TwoFAChecked(false);
                    this.handleCredentialPersistence(userName, password);
                    if (await this.reloadAfterLogin()) {
                        $handlers.onSuccess();
                    }
                }
            } catch (ex) {
                if (!StringUtils.ContainsIgnoreCase(ex.message, "User pass not valid", "None or multiple users has been found.")) {
                    LogIt.Error(ex);
                }
                $handlers.onError();
            }
        }
    }

    public async ProcessOneTimeCode() : Promise<void> {
        try {
            if (this.context.IsAuthenticated) {
                await this.authCodeHandlers.on2FARequest();
                this.codeBinding.setCodeSize(this.context.getUserProfile().AuthCode?.Size);
                const result : any = await this.codeBinding.Validate(this.context.getUserProfile().Id);
                if (result.status) {
                    this.context.Need2FACheck = false;
                    this.TwoFAChecked(true);
                    if (!await this.reloadAfterLogin()) {
                        await this.twoFAHandlers.onSuccess();
                    }
                } else {
                    this.context.Need2FACheck = true;
                    this.TwoFAChecked(false);
                    if (ObjectValidator.IsEmptyOrNull(this.context.getUserProfile().TwoFASecret)) {
                        await this.authCodeHandlers.onAuthCodeRequest();
                    }
                    if (!ObjectValidator.IsEmptyOrNull(result.error)) {
                        await this.twoFAHandlers.onError(result.error);
                    }
                }
            } else {
                await this.httpManager.NavigateTo("/signIn");
            }
        } catch (ex) {
            LogIt.Error("Failed to process one time code.", ex);
        }
    }

    public BindTwoFA($userControls : ITwoFactorBindingUserControls, $handlers : ITwoFactorBindingHandlers) : void {
        this.twoFAUserControls = $userControls;
        if (ObjectValidator.IsEmptyOrNull($userControls.holder) && ObjectValidator.IsSet((<any>$userControls).Visible)) {
            this.twoFAUserControls = {
                codeValue: $userControls.codeValue,
                validate : $userControls.validate,
                holder   : <any>$userControls
            };
        }
        this.twoFAHandlers = $handlers;
        WindowManager.getEvents().setOnKeyPress(($eventArgs : KeyEventArgs) : void => {
            if ($eventArgs.getKeyCode() === KeyMap.ENTER && this.twoFAUserControls.holder.Visible() &&
                !ObjectValidator.IsEmptyOrNull(this.twoFAUserControls.holder.getChildList())) {
                this.twoFAUserControls.validate.InstanceOwner().click();
            }
        });
        EventsBinding.SingleClick(this.twoFAUserControls.validate, async () : Promise<void> => {
            try {
                const code : TextField = this.twoFAUserControls.codeValue;
                ValidationBinding.RestoreValidMark(code);
                if (ObjectValidator.IsEmptyOrNull(code.Value())) {
                    ValidationBinding.IsValidInput(code, false);
                    code.InstanceOwner().focus();
                    await $handlers.onError(this.localization.enterCode);
                } else if (StringUtils.Length(code.Value()) !== 6) {
                    ValidationBinding.IsValidInput(code, false);
                    code.InstanceOwner().focus();
                    await $handlers.onError(this.localization.wrongCodeFormat);
                } else if (await this.authClient.Validate2FA(code.Value())) {
                    this.context.Need2FACheck = false;
                    this.TwoFAChecked(true);
                    code.Value("");
                    if (await this.reloadAfterLogin()) {
                        await $handlers.onSuccess();
                    }
                } else {
                    this.context.Need2FACheck = true;
                    this.TwoFAChecked(false);
                    ValidationBinding.IsValidInput(code, false);
                    code.InstanceOwner().focus();
                    await $handlers.onError(this.localization.invalidCode);
                }
            } catch (ex) {
                LogIt.Error(ex);
                await $handlers.onError(this.localization.codeValidationFailure);
            }
        });
    }

    public BindAuthCode($userControls : IAuthCodeUserControls, $handlers : IAuthCodeBindingHandlers) : void {
        this.authCodeHandlers = $handlers;
        this.codeBinding.Bind($userControls,
            async () : Promise<string> => {
                return this.context.getUserProfile().Id;
            },
            async () : Promise<void> => {
                await this.ProcessOneTimeCode();
            },
            async ($message : string) : Promise<void> => {
                await this.twoFAHandlers.onError($message);
            });
    }

    protected handleCredentialPersistence($name : string, $password : string) : void {
        const form : HTMLFormElement = document.createElement("form");
        form.id = "loginForm";

        const username : HTMLInputElement = document.createElement("input");
        username.name = "username";
        username.autocomplete = "username";
        username.hidden = true;
        username.type = "text";
        username.readOnly = true;
        username.value = $name;

        const password : HTMLInputElement = document.createElement("input");
        password.name = "current-password";
        password.autocomplete = "current-password";
        password.type = "password";
        password.hidden = true;
        password.readOnly = true;
        password.value = $password;

        form.appendChild(username);
        form.appendChild(password);
        document.body.appendChild(form);
    }

    protected getRefererUrl() : string {
        const keyName : string = "Referer";
        let referer : string = null;
        if (this.requestArgs.POST().KeyExists(keyName)) {
            const request : HttpRequestParser = this.requestArgs.POST().getItem(keyName);
            if (this.httpManager.getRequest().getUrl() !== request.getUrl()) {
                referer = request.getUrl();
                if (!StringUtils.Contains("#")) {
                    referer = "/#" + referer;
                }
            }
        } else if (this.requestArgs.GET().KeyExists(keyName)) {
            if (this.httpManager.getRequest().getUrl() !== this.requestArgs.GET().getItem(keyName)) {
                referer = this.requestArgs.GET().getItem(keyName);
            }
        } else if (this.httpManager.getRequest().getCookies().KeyExists(keyName)) {
            const url : string = ObjectDecoder.Base64(this.httpManager.getRequest().getCookieOnce(keyName));
            if (this.httpManager.getRequest().getUrl() !== url) {
                referer = url;
            }
        }
        return referer;
    }

    protected async reloadAfterLogin() : Promise<boolean> {
        const url : string = this.getRefererUrl();
        if (!ObjectValidator.IsEmptyOrNull(url)) {
            const token : string = this.authClient.CurrentToken();
            if (this.requestArgs.POST().KeyExists("Protocol")) {
                const protocol : IWebServiceProtocol = this.requestArgs.POST().getItem("Protocol");
                const client : IWebServiceClient = this.requestArgs.POST().getItem("Client");
                if (!ObjectValidator.IsEmptyOrNull(client) && !ObjectValidator.IsEmptyOrNull(protocol)) {
                    protocol.origin = this.httpManager.getRequest().getBaseUrl();
                    protocol.status = HttpStatusType.CONTINUE;
                    protocol.token = token;
                    if (ObjectValidator.IsObject(protocol.data)) {
                        protocol.data = ObjectEncoder.Base64(JSON.stringify(protocol.data));
                    } else {
                        protocol.data = ObjectEncoder.Base64(ObjectDecoder.Base64(
                            ObjectDecoder.Base64(<string>protocol.data)));
                    }
                    client.Send(<any>JSON.stringify(protocol));
                }
            } else {
                try {
                    await fetch("/api/sso/validate", {
                        headers: new Headers({
                            authorization: "Bearer " + token
                        }),
                        method : "GET"
                    });
                } catch (ex) {
                    LogIt.Error(ex);
                }
                this.httpManager.ReloadTo(url, false);
            }
        } else {
            return true;
        }
        return false;
    }
}

export interface ISSOLoginUserControls {
    username : TextField;
    password : TextField;
    keepLoggedIn? : CheckBox;
}

export interface ISSOBindingUserControls extends ISSOLoginUserControls {
    username : TextField;
    password : TextField;
    submit : Button;
    panel : GuiCommons;
}

export interface ISSOBindingHandlers {
    onError : ($isValidate? : boolean) => void;
    onSuccess : () => void;
    onRequestArgsFetch : () => HttpRequestEventArgs;
}

export interface ITwoFactorBindingLocalization {
    enterCode : string;
    wrongCodeFormat : string;
    invalidCode : string;
    codeValidationFailure : string;
}

export interface ITwoFactorBindingUserControls {
    codeValue : TextField;
    validate : Button;
    holder? : GuiCommons;
}

export interface ITwoFactorBindingHandlers {
    onError : ($message : string) => Promise<void>;
    onSuccess : () => Promise<void>;
}

export interface IAuthCodeBindingHandlers {
    on2FARequest : () => Promise<void>;
    onAuthCodeRequest : () => Promise<void>;
}

// generated-code-start
/* eslint-disable */
export const ISSOLoginUserControls = globalThis.RegisterInterface(["username", "password", "keepLoggedIn"]);
export const ISSOBindingUserControls = globalThis.RegisterInterface(["username", "password", "submit", "panel"], <any>ISSOLoginUserControls);
export const ISSOBindingHandlers = globalThis.RegisterInterface(["onError", "onSuccess", "onRequestArgsFetch"]);
export const ITwoFactorBindingLocalization = globalThis.RegisterInterface(["enterCode", "wrongCodeFormat", "invalidCode", "codeValidationFailure"]);
export const ITwoFactorBindingUserControls = globalThis.RegisterInterface(["codeValue", "validate", "holder"]);
export const ITwoFactorBindingHandlers = globalThis.RegisterInterface(["onError", "onSuccess"]);
export const IAuthCodeBindingHandlers = globalThis.RegisterInterface(["on2FARequest", "onAuthCodeRequest"]);
/* eslint-enable */
// generated-code-end
