/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { PersistenceFactory } from "@io-oidis-gui/Io/Oidis/Gui/PersistenceFactory.js";
import { OffCanvasBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/OffCanvasBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Dialog } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Dialog.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { EnvironmentConnector } from "../Connectors/EnvironmentConnector.js";
import { IReleaseNotesConfig } from "../Interfaces/IReleaseNotesConfig.js";
import { IWhatsNewNote, IWhatsNewNoteBlock } from "../Models/WhatsNewDao.js";

export class ReleaseNotesBinding extends BaseObject {
    private environment : IReleaseNotesEnvironment;
    private whatsNewLoaded : boolean;

    constructor($env : IReleaseNotesEnvironment) {
        super();
        this.environment = JsonUtils.Extend({
            persistence: PersistenceFactory.getPersistence(this.getClassName()),
            wasSeen    : async () : Promise<boolean> => {
                return this.environment.persistence.Variable("whatsNew") !== this.environment.currentVersion;
            },
            afterShow  : async () : Promise<void> => {
                this.environment.persistence.Variable("whatsNew", this.environment.currentVersion);
            }
        }, $env);
        this.whatsNewLoaded = false;
    }

    public Open($userControls : IReleaseNotesUserControls) : void {
        $userControls.holder.getEvents().FireAsynchronousMethod(async () : Promise<void> => {
            if (this.environment.onShowRequest() && !this.whatsNewLoaded) {
                /// TODO: add also some support for print and maybe also announcement by email triggered from dialog?
                try {
                    await this.processReleaseNotes($userControls);
                } catch (ex) {
                    LogIt.Warning("Failed to process release notes. Trying again...\n" + ex.stack);
                    $userControls.holder.getEvents().FireAsynchronousMethod(async () : Promise<void> => {
                        try {
                            await this.processReleaseNotes($userControls);
                        } catch (ex) {
                            let elements : Button[] = [];
                            if (!ObjectValidator.IsArray($userControls.open)) {
                                elements.push(<Button>$userControls.open);
                            } else {
                                elements = <Button[]>$userControls.open;
                            }
                            elements.forEach(($element : Button) : void => {
                                $element.Visible(false);
                            });
                            LogIt.Error(ex);
                        }
                    }, 100);
                }
            }
        }, 250);
    }

    private async processReleaseNotes($userControls : IReleaseNotesUserControls) : Promise<void> {
        try {
            const notesToString : any = ($source : IWhatsNewNoteBlock[]) : string => {
                let content : string = "";
                if (!ObjectValidator.IsEmptyOrNull($source)) {
                    $source.forEach(($block : IWhatsNewNoteBlock) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($block.header)) {
                            content += `<h4>${$block.header}</h4>`;
                        }
                        if (!ObjectValidator.IsEmptyOrNull($block.notes)) {
                            content += `<ul>`;
                            $block.notes.forEach(($value : string) : void => {
                                content += `<li>${$value}</li>`;
                            });
                            content += `</ul>`;
                        }
                    });
                }
                return content;
            };
            const fillInNotes : any = ($source : IWhatsNewNote) : void => {
                if (!ObjectValidator.IsEmptyOrNull($source)) {
                    $userControls.date.Text($source.date);
                    $userControls.newsList.Content(notesToString($source.news));
                    if (this.environment.onAdminRequest()) {
                        if (!ObjectValidator.IsEmptyOrNull($source.admins)) {
                            $userControls.adminsList.Content(notesToString($source.admins));
                            $userControls.adminsContent.Visible(true);
                        } else {
                            $userControls.adminsContent.Visible(false);
                        }
                    } else {
                        $userControls.adminsList.Clear();
                        $userControls.adminsContent.Visible(false);
                    }
                    const issues : string = notesToString($source.issues);
                    $userControls.issuesList.Content(issues);
                    $userControls.issuesContent.Visible(!ObjectValidator.IsEmptyOrNull(issues));
                }
            };
            $userControls.versionSelector.Content(this.environment.currentVersion);
            fillInNotes(await this.environment.helper.getReleaseNotesFor(this.environment.currentVersion));
            const config : IReleaseNotesConfig = await this.environment.helper.getReleaseNotesConfig();
            $userControls.versionMenu.Clear();
            config.versions.forEach(($version : string) : void => {
                const versionItem : DropDownListItem = new DropDownListItem();
                EventsBinding.SingleClick(versionItem, async () : Promise<void> => {
                    $userControls.versionSelector.Content($version);
                    fillInNotes(await this.environment.helper.getReleaseNotesFor($version));
                });
                $userControls.versionMenu.AddChild(versionItem);
                versionItem.Text($version);
            });

            if (!await this.environment.wasSeen() || config.forceShow) {
                $userControls.holder.getEvents().FireAsynchronousMethod(async () : Promise<void> => {
                    new OffCanvasBinding().Open($userControls.holder);
                    await this.environment.afterShow();
                }, 500);
            }

            this.whatsNewLoaded = true;
        } catch (ex) {
            LogIt.Error(ex);
        }
    }
}

export interface IReleaseNotesEnvironment {
    persistence? : IPersistenceHandler;
    helper : EnvironmentConnector;
    currentVersion : string;
    onShowRequest : () => boolean;
    onAdminRequest : () => boolean;
    wasSeen? : () => Promise<boolean>;
    afterShow? : () => Promise<void>;
}

export interface IReleaseNotesUserControls {
    open : Button | Button[];
    holder : Dialog;
    versionSelector : Button;
    versionMenu : GuiCommons;
    date : Label;
    newsList : GuiCommons;
    adminsList : GuiCommons;
    issuesList : GuiCommons;
    adminsContent : GuiCommons;
    issuesContent : GuiCommons;
}

// generated-code-start
/* eslint-disable */
export const IReleaseNotesEnvironment = globalThis.RegisterInterface(["persistence", "helper", "currentVersion", "onShowRequest", "onAdminRequest", "wasSeen", "afterShow"]);
export const IReleaseNotesUserControls = globalThis.RegisterInterface(["open", "holder", "versionSelector", "versionMenu", "date", "newsList", "adminsList", "issuesList", "adminsContent", "issuesContent"]);
/* eslint-enable */
// generated-code-end
