/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { PersistenceFactory } from "@io-oidis-gui/Io/Oidis/Gui/PersistenceFactory.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { BarCodeEngine } from "../Utils/BarCodeEngine.js";
import { FileUploadBinding } from "./FileUploadBinding.js";

export class ScannerBinding extends BaseObject {
    private reader : BarCodeEngine;
    private readonly persistence : IPersistenceHandler;
    private userControls : IScannerUserControls;
    private onFind : ($id : string, $packageIndex : number, $data : string) => Promise<void>;

    constructor() {
        super();
        this.reader = new BarCodeEngine();
        this.persistence = PersistenceFactory.getPersistence(this.getClassName());
    }

    public Bind($userControls : IScannerUserControls,
                $onFind : ($id : string, $packageIndex : number, $data : string) => Promise<void>) : void {
        this.userControls = $userControls;
        this.onFind = $onFind;

        $userControls.start.forEach(($button : Button) : void => {
            EventsBinding.SingleClick($button, async () : Promise<void> => {
                try {
                    $userControls.missingPermissions.Visible(false);
                    $userControls.container.Visible(true);
                    $userControls.deviceMenu.Clear();
                    let deviceId : string = null;
                    if (this.persistence.Exists("readerId")) {
                        deviceId = this.persistence.Variable("readerId");
                    }
                    const devices : any[] = await this.reader.Init($userControls.cameraView.InstanceOwner());
                    let deviceFound : any = null;
                    devices.forEach(($device : any) : void => {
                        const item : DropDownListItem = new DropDownListItem();
                        item.Text($device.label);
                        item.Value($device.id);
                        if (!ObjectValidator.IsEmptyOrNull(deviceId) && deviceId === $device.id) {
                            deviceFound = $device;
                        }
                        EventsBinding.SingleClick(item, async () : Promise<void> => {
                            await this.selectScanDevice(item.Text(), item.Value());
                        });
                        $userControls.deviceMenu.AddChild(item);
                    });
                    if (!ObjectValidator.IsEmptyOrNull(deviceFound)) {
                        await this.selectScanDevice(deviceFound.label, deviceFound.id);
                    } else {
                        await this.selectScanDevice();
                    }
                } catch (ex) {
                    if (StringUtils.ContainsIgnoreCase(ex.message, "Requested device not found")) {
                        this.persistence.Destroy("readerId");
                        await this.selectScanDevice();
                    } else if (StringUtils.ContainsIgnoreCase(ex.message,
                        "Permission denied", "Permission dismissed", "Could not start video source", "denied permission")) {
                        $userControls.missingPermissions.Visible(true);
                    } else {
                        LogIt.Error(ex);
                    }
                }
            });
        });

        [$userControls.title, $userControls.close].forEach(($button : Button) : void => {
            EventsBinding.SingleClick($button, async () : Promise<void> => {
                this.userControls.container.Visible(false);
                await this.Stop();
            });
        });
        const fileUpload : FileUploadBinding = new FileUploadBinding();
        fileUpload.Bind({
            button: $userControls.fileInput
        }, {
            onData: async ($files : FileList) : Promise<void> => {
                try {
                    const data : string = await this.reader.ScanFile($files[0]);
                    await this.onDetect(data);
                } catch (ex) {
                    this.handleDetectError(ex);
                }
            }
        });
        fileUpload.setFilter("image/*");
        EventsBinding.SingleClick($userControls.find, async () : Promise<void> => {
            await this.onDetect($userControls.decoded.Value());
        });
    }

    public async Stop() : Promise<void> {
        try {
            await this.reader.Stop();
        } catch (ex) {
            LogIt.Error(ex);
        }
    }

    public async setState($value : boolean, $data? : string) : Promise<void> {
        if ($value) {
            await this.Stop();
            this.userControls.container.Visible(false);
        } else {
            this.userControls.close.Visible(false);
            this.userControls.decodedHolder.Visible(true);
            this.userControls.decoded.Value($data);
            ValidationBinding.IsValidInput(this.userControls.decoded, false);
        }
    }

    private async selectScanDevice($text? : string, $id? : string) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull($id)) {
            this.userControls.deviceSelector.Content($text);
            this.userControls.deviceSelector.Value($id);
            this.persistence.Variable("readerId", $id);
        }
        this.userControls.error.Visible(false);
        this.userControls.decoded.Value("");
        ValidationBinding.RestoreValidMark(this.userControls.decoded);
        this.userControls.decodedHolder.Visible(false);
        this.userControls.close.Visible(true);
        try {
            await this.reader.Stop();
            await this.reader.Start(($data : string) : void => {
                    this.onDetect($data).catch(($error : Error) : void => {
                        LogIt.Error($error);
                    });
                },
                $id);
        } catch (ex) {
            this.handleDetectError(ex);
        }
    }

    private async onDetect($data : string) : Promise<void> {
        if (ObjectValidator.IsInteger(StringUtils.Remove($data, " ", "/"))) {
            const parts : string[] = StringUtils.Split($data, " ");
            if (parts.length >= 1) {
                const id : string = parts[0];
                let packageIndex : number = 1;
                if (!ObjectValidator.IsEmptyOrNull(parts[1])) {
                    packageIndex = StringUtils.ToInteger(StringUtils.Split(parts[1], "/")[0]);
                }
                await this.onFind(id, packageIndex, $data);
            }
        }
    }

    private handleDetectError($error : Error) : void {
        if (StringUtils.ContainsIgnoreCase($error.message, "No MultiFormat Readers were able to detect the code")) {
            this.userControls.close.Visible(false);
            this.userControls.decodedHolder.Visible(true);
            this.userControls.decoded.Value("");
            this.userControls.error.Visible(true);
            this.userControls.container.getEvents().FireAsynchronousMethod(() : void => {
                this.userControls.error.Visible(false);
            }, 3500);
        } else {
            throw $error;
        }
    }
}

export interface IScannerUserControls {
    deviceSelector : Button;
    deviceMenu : GuiCommons;
    decoded : TextField;
    decodedHolder : GuiCommons;
    start : Button[];
    close : Button;
    find : Button;
    cameraView : GuiCommons;
    fileInput : Button;
    container : GuiCommons;
    title : GuiCommons;
    error : GuiCommons;
    missingPermissions : GuiCommons;
}

// generated-code-start
/* eslint-disable */
export const IScannerUserControls = globalThis.RegisterInterface(["deviceSelector", "deviceMenu", "decoded", "decodedHolder", "start", "close", "find", "cameraView", "fileInput", "container", "title", "error", "missingPermissions"]);
/* eslint-enable */
// generated-code-end
