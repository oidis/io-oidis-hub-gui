/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BrowserType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/BrowserType.js";
import { FileHandlerEventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/FileHandlerEventType.js";
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ProgressEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ProgressEventArgs.js";
import { FileHandler } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/Handlers/FileHandler.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { EventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/EventsManager.js";
import { IFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { ProgressBar } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/ProgressBar.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { FileInputConnector } from "../Connectors/FileInputConnector.js";
import { Loader } from "../Loader.js";

export class FileUploadBinding extends BaseObject {
    private connector : FileInputConnector;
    private files : FileList;
    private userControls : IFileUploadUserControls;
    private inputField : TextField;
    private options : IFileUploadOptions;

    constructor($options? : IFileUploadOptions) {
        super();
        this.connector = FileInputConnector.getClient();
        this.files = null;
        this.inputField = null;
        this.options = JsonUtils.Extend(<IFileUploadOptions>{
            maxChunkSize: 1024 * (Loader.getInstance().getHttpManager().getRequest().getBrowserType() === BrowserType.SAFARI ? 250 : 500)
        }, $options);
    }

    public Bind($userControls : IFileUploadUserControls, $handlers : IFileUploadHandlers) : void {
        this.userControls = $userControls;
        const isNativeInput : boolean = !ObjectValidator.IsEmptyOrNull($userControls.field) &&
            $userControls.field.InstanceOwner().type === "file";
        if (isNativeInput) {
            this.inputField = $userControls.field;
        } else {
            const inputElement : HTMLInputElement = document.createElement("input");
            inputElement.type = "file";
            inputElement.classList.add("HiddenElement");
            this.inputField = new TextField();
            document.body.appendChild(inputElement);
            this.inputField.InstanceOwner(inputElement);
        }
        $handlers = JsonUtils.Extend({
            onEmptySelection: () : void => {
                // do nothing
            },
            onProgressChange: ($status : boolean) : void => {
                // do nothing
            }
        }, $handlers);
        $userControls.button.getEvents().setOnClick(() : void => {
            this.inputField.InstanceOwner().click();
        });
        this.inputField.InstanceOwner().onchange = async () : Promise<void> => {
            const input : HTMLInputElement = this.inputField.InstanceOwner();
            if (!ObjectValidator.IsEmptyOrNull($userControls.field) && !isNativeInput) {
                $userControls.field.Value(input.value);
            }
            try {
                this.files = null;
                if (input.files.length !== 0) {
                    this.files = input.files;
                }
                if (!ObjectValidator.IsEmptyOrNull(this.files)) {
                    $handlers.onProgressChange(true);
                    await $handlers.onData(this.files);
                    $handlers.onProgressChange(false);
                } else {
                    $handlers.onEmptySelection();
                }
            } catch (ex) {
                LogIt.Error(ex);
            }
            this.files = null;
            input.value = "";
            input.files = null;
        };

        if (!ObjectValidator.IsEmptyOrNull($userControls.dropZone)) {
            if (WindowManager.IsDropSupported()) {
                WindowManager.getEvents().setOnDragOver(($eventArgs : EventArgs) : void => {
                    if ($userControls.dropZone.Enabled()) {
                        $eventArgs.PreventDefault();
                    }
                });
                WindowManager.getEvents().setOnDrop(($eventArgs : EventArgs) : void => {
                    if ($userControls.dropZone.Enabled()) {
                        $eventArgs.PreventDefault();
                    }
                });
                $userControls.dropZone.getEvents().setEvent(EventType.ON_DRAG_OVER, ($eventArgs : EventArgs) : void => {
                    if ($userControls.dropZone.Enabled()) {
                        const eventArgs : DragEvent = <DragEvent>$eventArgs.NativeEventArgs();
                        if (ObjectValidator.IsSet(eventArgs.stopPropagation)) {
                            eventArgs.stopPropagation();
                        }
                        $eventArgs.PreventDefault();
                        eventArgs.dataTransfer.dropEffect = "copy";
                    }
                });

                $userControls.dropZone.getEvents().setEvent(EventType.ON_DROP, ($eventArgs : EventArgs) : void => {
                    if ($userControls.dropZone.Enabled()) {
                        const eventArgs : DragEvent = <DragEvent>$eventArgs.NativeEventArgs();
                        if (ObjectValidator.IsSet(eventArgs.stopPropagation)) {
                            eventArgs.stopPropagation();
                        }
                        $eventArgs.PreventDefault();
                        if (!ObjectValidator.IsEmptyOrNull($userControls.field) && !isNativeInput) {
                            $userControls.field.Value("");
                        }
                        this.files = null;
                        if (ObjectValidator.IsSet(eventArgs.dataTransfer.files)) {
                            this.files = eventArgs.dataTransfer.files;
                        }
                        $userControls.dropZone.getEvents().FireAsynchronousMethod(async () : Promise<void> => {
                            if (!ObjectValidator.IsEmptyOrNull(this.files)) {
                                $handlers.onProgressChange(true);
                                await $handlers.onData(this.files);
                                $handlers.onProgressChange(false);
                            } else {
                                $handlers.onEmptySelection();
                            }
                        });
                    }
                });
            }
        }
    }

    public setFilter($value : string) : void {
        this.inputField.InstanceOwner().accept = $value;
    }

    public setMultipleSelectEnabled($value : boolean) : void {
        this.inputField.InstanceOwner().multiple = $value;
    }

    public async Read($options? : IFileUploadReadOptions, $onProgress? : ($value : number) => void) : Promise<string[]> {
        const data : string[] = [];
        if (!ObjectValidator.IsEmptyOrNull(this.files)) {
            $options = JsonUtils.Extend(<IFileUploadReadOptions>{
                encoded: true,
                end    : null,
                start  : null,
                urlSafe: false
            }, $options);
            if (ObjectValidator.IsSet($onProgress) && ObjectValidator.IsEmptyOrNull(this.userControls)) {
                this.userControls = <any>{
                    progress: {
                        Value: $onProgress
                    }
                };
            }
            for await (const file of <any>this.files) {
                await new Promise<void>(($resolve) : void => {
                    const handler : FileHandler = new FileHandler(file);
                    EventsManager.getInstanceSingleton().setEvent(FileHandler.ClassName(), FileHandlerEventType.ON_START, () : void => {
                        if (!ObjectValidator.IsEmptyOrNull(this.userControls) &&
                            !ObjectValidator.IsEmptyOrNull(this.userControls.progress)) {
                            this.userControls.progress.Value(0);
                        }
                    });
                    EventsManager.getInstanceSingleton().setEvent(FileHandler.ClassName(), FileHandlerEventType.ON_CHANGE,
                        ($eventArgs : ProgressEventArgs) : void => {
                            if (!ObjectValidator.IsEmptyOrNull(this.userControls) &&
                                !ObjectValidator.IsEmptyOrNull(this.userControls.progress)) {
                                this.userControls.progress.Value(100 / $eventArgs.RangeEnd() * $eventArgs.CurrentValue());
                            }
                        });
                    EventsManager.getInstanceSingleton().setEvent(FileHandler.ClassName(), FileHandlerEventType.ON_COMPLETE, () : void => {
                        data.push(handler.Data($options.encoded, $options.urlSafe));
                        $resolve();
                    });
                    handler.Load($options.start, $options.end);
                });
            }
        }
        return data;
    }

    public Write($data : FileList | Blob[], $onProgress? : ($value : number) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull($data)) {
            this.files = <any>[];
            for (const file of <any>$data) {
                if (ObjectValidator.IsEmptyOrNull(file.lastModifiedDate)) {
                    file.lastModifiedDate = new Date();
                }
                if (ObjectValidator.IsEmptyOrNull(file.name)) {
                    file.name = "";
                }
                (<any>this.files).push(file);
            }
            if (ObjectValidator.IsSet($onProgress) && ObjectValidator.IsEmptyOrNull(this.userControls)) {
                this.userControls = <any>{
                    progress: {
                        Value: $onProgress
                    }
                };
            }
        }
    }

    public async Upload($dataName? : string) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull(this.files)) {
            await this.uploadFile(this.files, ($chunk : IFileTransferProtocol) : void => {
                if (!ObjectValidator.IsEmptyOrNull(this.userControls) &&
                    !ObjectValidator.IsEmptyOrNull(this.userControls.progress)) {
                    this.userControls.progress.Value(100 / $chunk.size * $chunk.end);
                }
                $chunk.path = $dataName;
            });
        } else {
            throw new Error("Failed to upload file data. No files have been selected or written.");
        }
    }

    private async uploadFile($files : any, $onUpdateHandler : ($data : IFileTransferProtocol) => void) : Promise<void> {
        const handlers : ArrayList<FileHandler> = new ArrayList<FileHandler>();
        for (const file of $files) {
            const reader : FileHandler = new FileHandler(file);
            handlers.Add(reader);
        }
        const filesCount : number = handlers.Length();
        let fileIndex : number = 0;
        const chunk : IFileTransferProtocol = <IFileTransferProtocol>{
            end  : 0,
            id   : "",
            index: 0,
            size : 0,
            start: 0
        };
        const readFileChunk : ($fileIndex : number) => void = ($fileIndex : number) : void => {
            const handler : FileHandler = handlers.getItem($fileIndex);
            if (!ObjectValidator.IsEmptyOrNull(handler)) {
                if (chunk.index === 0) {
                    fileIndex = $fileIndex;
                    chunk.id = StringUtils.getSha1(new Date().getTime() + handler.getName());
                    chunk.path = handler.getName();
                    chunk.size = handler.getSize();
                    chunk.start = 0;
                    chunk.end = 0;
                }
                if (chunk.size > 0) {
                    chunk.end = chunk.start + this.options.maxChunkSize;
                    if (chunk.end > chunk.size) {
                        chunk.end = chunk.size;
                    }
                    $onUpdateHandler(chunk);
                    handler.Load(chunk.start, chunk.end);
                } else {
                    readFileChunk($fileIndex + 1);
                }
            } else {
                LogIt.Error("Failed to find FileHandler for file #" + $fileIndex);
            }
        };

        return new Promise<void>(($resolve : () => void, $reject : any) : void => {
            EventsManager.getInstanceSingleton().setEvent(FileHandler.ClassName(), FileHandlerEventType.ON_COMPLETE,
                async ($eventArgs : EventArgs) : Promise<void> => {
                    try {
                        const handler : FileHandler = <FileHandler>$eventArgs.Owner();
                        if (await this.connector.Upload({
                            data : handler.Data(true),
                            end  : chunk.end,
                            id   : chunk.id,
                            index: chunk.index,
                            path : chunk.path,
                            size : chunk.size,
                            start: chunk.start
                        })) {
                            if (chunk.end < chunk.size) {
                                chunk.start = chunk.end;
                                chunk.index = chunk.index + 1;
                                readFileChunk(fileIndex);
                            } else if (fileIndex + 1 < filesCount) {
                                chunk.index = 0;
                                readFileChunk(fileIndex + 1);
                            } else {
                                $resolve();
                            }
                        }
                    } catch (ex) {
                        $reject(ex);
                    }
                });
            readFileChunk(0);
        });
    }
}

export interface IFileUploadOptions {
    maxChunkSize? : number;
}

export interface IFileUploadUserControls {
    button : Button;
    field? : TextField;
    progress? : ProgressBar;
    dropZone? : GuiCommons;
}

export interface IFileUploadHandlers {
    onData : ($files : FileList) => Promise<void>;
    onEmptySelection? : () => void;
    onProgressChange? : ($status : boolean) => void;
}

export interface IFileUploadReadOptions {
    encoded? : boolean;
    urlSafe? : boolean;
    start? : number;
    end? : number;
}

// generated-code-start
export const IFileUploadOptions = globalThis.RegisterInterface(["maxChunkSize"]);
export const IFileUploadUserControls = globalThis.RegisterInterface(["button", "field", "progress", "dropZone"]);
export const IFileUploadHandlers = globalThis.RegisterInterface(["onData", "onEmptySelection", "onProgressChange"]);
export const IFileUploadReadOptions = globalThis.RegisterInterface(["encoded", "urlSafe", "start", "end"]);
// generated-code-end
