/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";

export class LoadMoreBinding extends BaseObject {
    private fetchEnabled : boolean;
    private controls : ILoadMoreUserControls;

    constructor() {
        super();
        this.fetchEnabled = true;
        this.controls = null;
    }

    public async Bind($controls : ILoadMoreUserControls, $onFetch : () => Promise<IModelListResult>) : Promise<void> {
        this.controls = $controls;
        const loadMore : any = async () : Promise<void> => {
            this.fetchEnabled = false;
            this.Validate(await $onFetch());
            this.fetchEnabled = true;
        };
        let lastScroll : number = 0;
        const holder : HTMLElement = ObjectValidator.IsEmptyOrNull($controls.holder) ? document.body : $controls.holder.InstanceOwner();
        (ObjectValidator.IsEmptyOrNull($controls.holder) ? WindowManager : $controls.holder).getEvents()
            .setEvent(EventType.ON_SCROLL, () : void => {
                const scrollY : number = ObjectValidator.IsEmptyOrNull($controls.holder) ? window.scrollY : holder.scrollTop;
                if ($controls.container.Visible() && this.fetchEnabled && (lastScroll < scrollY)) {
                    $controls.container.getEvents().FireAsynchronousMethod(async () : Promise<void> => {
                        if (holder.scrollHeight - Math.abs(scrollY) - 2 <= holder.clientHeight &&
                            this.fetchEnabled) {
                            $controls.button.Visible(false);
                            await loadMore();
                        }
                    }, true);
                }
                lastScroll = window.scrollY;
            });
        EventsBinding.SingleClick($controls.button, async () : Promise<void> => {
            await loadMore();
        });
        await loadMore();
    }

    public Validate($data : IModelListResult) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.controls)) {
            if ($data.data.length < $data.size) {
                if ($data.data.length > 0 && document.body.scrollHeight - 2 <= document.body.clientHeight) {
                    this.controls.button.Visible(true);
                }
            } else {
                this.controls.button.Visible(false);
            }
        }
    }
}

export interface ILoadMoreUserControls {
    container : GuiCommons;
    button : Button;
    holder? : GuiCommons;
}

// generated-code-start
export const ILoadMoreUserControls = globalThis.RegisterInterface(["container", "button", "holder"]);
// generated-code-end
