/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { KeyEventHandler } from "@io-oidis-gui/Io/Oidis/Gui/Utils/KeyEventHandler.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { AuthCodeConnector, IAuthCodeRequestResult, IAuthCodeValidateResult } from "../Connectors/AuthCodeConnector.js";
import { EmailValidator } from "../DAO/Validators/EmailValidator.js";

export class AuthCodeBinding extends BaseObject {
    protected localization : IAuthCodeBindingLocalization;
    private authClient : AuthCodeConnector;
    private userControls : IAuthCodeUserControls;

    constructor() {
        super();
        this.authClient = AuthCodeConnector.getClient();
        this.localization = {
            specifyEmailOrPhone: "Enter e-mail or phone",
            wrongEmailFormat   : "E-mail must be in correct format",
            missingEmail       : "Enter e-mail",
            sendError          : "Failed to send e-mail. Please try later."
        };
    }

    public Bind($userControls : IAuthCodeUserControls,
                $onModelIdRequest : () => Promise<string>,
                $onMatch : () => Promise<void>,
                $onError : ($message : string) => Promise<void>) : void {
        this.userControls = $userControls;
        if (ObjectValidator.IsEmptyOrNull(this.userControls.holder) && ObjectValidator.IsSet((<any>$userControls).Visible)) {
            this.userControls = {
                code1           : $userControls.code1,
                code2           : $userControls.code2,
                code3           : $userControls.code3,
                code4           : $userControls.code4,
                sendNewCode     : $userControls.sendNewCode,
                emailValid      : $userControls.emailValid,
                phoneValid      : $userControls.phoneValid,
                newCodeRecipient: $userControls.newCodeRecipient,
                sendToRecipient : $userControls.sendToRecipient,
                codeSendSuccess : $userControls.codeSendSuccess,
                validationError : $userControls.validationError,
                emailTemplate   : $userControls.emailTemplate,
                orContent       : $userControls.orContent,
                phoneTemplate   : $userControls.phoneTemplate,
                codeError       : $userControls.codeError,
                holder          : <any>$userControls
            };
        }
        [$userControls.code1, $userControls.code2, $userControls.code3, $userControls.code4].forEach(($input : TextField) : void => {
            ValidationBinding.setOnlyNumbersAllowed($input);
            $input.getEvents().setEvent(EventType.ON_KEY_PRESS, ($eventArgs : KeyEventArgs) : void => {
                if ($input.Value().length !== 0) {
                    $eventArgs.PreventDefault();
                }
            });
            $input.getEvents().setEvent(EventType.ON_KEY_UP, async ($eventArgs : KeyEventArgs) : Promise<void> => {
                const value : string = $input.Value();
                if (value.length === 4) {
                    if (ObjectValidator.IsInteger(value[0]) &&
                        ObjectValidator.IsInteger(value[1]) &&
                        ObjectValidator.IsInteger(value[2]) &&
                        ObjectValidator.IsInteger(value[3])) {
                        $userControls.code1.Value(value[0]);
                        $userControls.code2.Value(value[1]);
                        $userControls.code3.Value(value[2]);
                        $userControls.code4.Value(value[3]);
                        $userControls.code1.getEvents().FireAsynchronousMethod(async () : Promise<void> => {
                            await $onMatch();
                        }, 250);
                    } else {
                        $input.Value("");
                    }
                } else if (!KeyEventHandler.IsEdit($eventArgs.NativeEventArgs()) &&
                    !KeyEventHandler.IsNavigate($eventArgs.NativeEventArgs()) &&
                    value.length !== 0) {
                    if (!ObjectValidator.IsInteger(value)) {
                        $input.Value("");
                    }
                    if ($input === $userControls.code1) {
                        $userControls.code2.InstanceOwner().focus();
                    } else if ($input === $userControls.code2) {
                        $userControls.code3.InstanceOwner().focus();
                    } else if ($input === $userControls.code3) {
                        $userControls.code4.InstanceOwner().focus();
                    } else if ($input === $userControls.code4) {
                        $userControls.code1.InstanceOwner().focus();
                    }
                    if (this.getCode().length === 4) {
                        await $onMatch();
                    }
                }
            });
        });
        EventsBinding.SingleClick($userControls.sendNewCode, async () : Promise<void> => {
            ValidationBinding.RestoreValidMark($userControls.emailValid);
            ValidationBinding.RestoreValidMark($userControls.phoneValid);
            ValidationBinding.RestoreValidMark($userControls.newCodeRecipient);
            if (ObjectValidator.IsEmptyOrNull($userControls.emailValid.Value()) &&
                ObjectValidator.IsEmptyOrNull($userControls.phoneValid.Value())) {
                ValidationBinding.IsValidInput($userControls.emailValid, false);
                ValidationBinding.IsValidInput($userControls.phoneValid, false);
                await $onError(this.localization.specifyEmailOrPhone);
            } else if (!ObjectValidator.IsEmptyOrNull($userControls.emailValid.Value()) &&
                !new EmailValidator().validate($userControls.emailValid.Value())) {
                ValidationBinding.IsValidInput($userControls.emailValid, false);
                await $onError(this.localization.wrongEmailFormat);
            } else if (!$userControls.sendToRecipient.Value() &&
                ObjectValidator.IsEmptyOrNull($userControls.newCodeRecipient.Value())) {
                ValidationBinding.IsValidInput($userControls.newCodeRecipient, false);
                await $onError(this.localization.missingEmail);
            } else if (!$userControls.sendToRecipient.Value() &&
                !ObjectValidator.IsEmptyOrNull($userControls.newCodeRecipient.Value()) &&
                !new EmailValidator().validate($userControls.newCodeRecipient.Value())) {
                ValidationBinding.IsValidInput($userControls.newCodeRecipient, false);
                await $onError(this.localization.wrongEmailFormat);
            } else {
                try {
                    const result : IAuthCodeRequestResult = await this.authClient.RequestAuthCode(await $onModelIdRequest(),
                        $userControls.newCodeRecipient.Value(), $userControls.emailValid.Value(), $userControls.phoneValid.Value());
                    if (result.status) {
                        $userControls.sendNewCode.Visible(false);
                        $userControls.codeSendSuccess.Visible(true);
                        $userControls.code1.getEvents().FireAsynchronousMethod(() : void => {
                            $userControls.emailValid.Value("");
                            $userControls.phoneValid.Value("");
                            $userControls.newCodeRecipient.Value("");
                            $userControls.codeSendSuccess.Visible(false);
                            $userControls.sendNewCode.Visible(true);
                        }, 2500);
                    } else {
                        if (ObjectValidator.IsEmptyOrNull(result.message)) {
                            $userControls.sendNewCode.Visible(false);
                            $userControls.validationError.Visible(true);
                            $userControls.code1.getEvents().FireAsynchronousMethod(() : void => {
                                $userControls.validationError.Visible(false);
                                $userControls.sendNewCode.Visible(true);
                            }, 3500);
                        } else {
                            await $onError(result.message);
                        }
                    }
                } catch (ex) {
                    LogIt.Error("Failed to send new code.", ex);
                    await $onError(this.localization.sendError);
                }
            }
        });
    }

    public setCodeSize($value : number = 4) : void {
        /// TODO: enable variable code size
    }

    public async Validate($id : string) : Promise<any> {
        let currentCode : string = this.getCode();
        if (currentCode.length === 4) {
            currentCode = StringUtils.getSha1(currentCode);
        } else {
            currentCode = null;
        }
        const data : IAuthCodeValidateResult = await this.authClient.Validate($id, currentCode);
        if (data.codeEnabled) {
            this.userControls.emailTemplate.Text(data.emailTemplate);
            if (!ObjectValidator.IsEmptyOrNull(data.phoneTemplate)) {
                this.userControls.orContent.Visible(true);
                this.userControls.phoneTemplate.Text(data.phoneTemplate);
            } else {
                this.userControls.orContent.Visible(false);
            }
            if (!ObjectValidator.IsEmptyOrNull(currentCode)) {
                this.userControls.code1.Value("");
                this.userControls.code2.Value("");
                this.userControls.code3.Value("");
                this.userControls.code4.Value("");
                if (data.codePassed) {
                    this.userControls.holder.Visible(false);
                    this.userControls.codeError.Visible(false);
                } else if (!ObjectValidator.IsEmptyOrNull(data.error)) {
                    this.userControls.holder.Visible(false);
                } else {
                    this.userControls.holder.Visible(true);
                    this.userControls.codeError.Visible(true);
                }
            } else if (data.codePassed) {
                this.userControls.holder.Visible(false);
            } else if (!ObjectValidator.IsEmptyOrNull(data.error)) {
                this.userControls.holder.Visible(false);
            } else {
                this.userControls.holder.Visible(true);
            }
        } else {
            this.userControls.holder.Visible(false);
        }
        if (!ObjectValidator.IsEmptyOrNull(data.error)) {
            return {status: false, error: data.error};
        }
        return {status: data.codeEnabled && data.codePassed};
    }

    public getCode() : string {
        return this.userControls.code1.Value() +
            this.userControls.code2.Value() +
            this.userControls.code3.Value() +
            this.userControls.code4.Value();
    }
}

export interface IAuthCodeUserControls {
    code1 : TextField;
    code2 : TextField;
    code3 : TextField;
    code4 : TextField;
    sendNewCode : Button;
    emailValid : TextField;
    phoneValid : TextField;
    newCodeRecipient : TextField;
    sendToRecipient : CheckBox;
    codeSendSuccess : GuiCommons;
    validationError : GuiCommons;
    emailTemplate : Label;
    orContent : GuiCommons;
    phoneTemplate : Label;
    codeError : GuiCommons;
    holder? : GuiCommons;
}

export interface IAuthCodeBindingLocalization {
    specifyEmailOrPhone : string;
    wrongEmailFormat : string;
    missingEmail : string;
    sendError : string;
}

// generated-code-start
/* eslint-disable */
export const IAuthCodeUserControls = globalThis.RegisterInterface(["code1", "code2", "code3", "code4", "sendNewCode", "emailValid", "phoneValid", "newCodeRecipient", "sendToRecipient", "codeSendSuccess", "validationError", "emailTemplate", "orContent", "phoneTemplate", "codeError", "holder"]);
export const IAuthCodeBindingLocalization = globalThis.RegisterInterface(["specifyEmailOrPhone", "wrongEmailFormat", "missingEmail", "sendError"]);
/* eslint-enable */
// generated-code-end
