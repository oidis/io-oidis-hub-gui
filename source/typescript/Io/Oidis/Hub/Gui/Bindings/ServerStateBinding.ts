/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { EnvironmentConnector } from "../Connectors/EnvironmentConnector.js";
import { Loader } from "../Loader.js";

export class ServerStateBinding extends BaseObject {
    private isOnline : boolean;
    private initCountDownLimit : number = 15; // s
    private postponeLimit : number = 60; // s
    private environmentHelper : EnvironmentConnector;
    private onRestoreHandler : () => Promise<void>;

    constructor($environmentClient : EnvironmentConnector) {
        super();
        this.isOnline = false;
        this.environmentHelper = $environmentClient;
        this.onRestoreHandler = async () : Promise<void> => {
            // default handler
        };
    }

    public IsOnline() : boolean {
        return this.isOnline;
    }

    public Bind($userControls : IServerStateUserControls, $restoreHandler? : () => Promise<void>) : void {
        if (!ObjectValidator.IsEmptyOrNull($restoreHandler)) {
            this.onRestoreHandler = $restoreHandler;
        }
        let countDownThread : any = null;
        this.environmentHelper.getEvents().OnClose(() : void => {
            this.isOnline = false;
            $userControls.onlineAlert.Visible(false);
            $userControls.reloadAlert.Visible(false);
            $userControls.offlineAlert.Visible(true);
            if (!ObjectValidator.IsEmptyOrNull(countDownThread)) {
                clearTimeout(countDownThread);
                countDownThread = null;
            }
        });

        let countDown : number = this.initCountDownLimit;
        $userControls.reloadSuspend.getEvents().setOnClick(($args : EventArgs) : void => {
            $args.PreventDefault();
            countDown = this.postponeLimit;
        });
        const autoReload : any = () : void => {
            try {
                if (!ObjectValidator.IsEmptyOrNull(countDownThread)) {
                    clearTimeout(countDownThread);
                    countDownThread = null;
                }
                if (countDown >= 1) {
                    $userControls.reloadCountDown.Text(countDown + "");
                    countDownThread = setTimeout(() : void => {
                        countDown--;
                        autoReload();
                    }, 1000);
                } else {
                    location.reload();
                }
            } catch (ex) {
                LogIt.Error("Auto-reload failure.", ex);
            }
        };
        this.environmentHelper.getEvents().OnStart(async () : Promise<void> => {
            this.isOnline = true;
            if (Loader.getInstance().getAppConfiguration().build.timestamp !== await this.environmentHelper.getBuildTime()) {
                countDown = 15;
                autoReload();
                $userControls.reloadAlert.Visible(true);
            }
            if ($userControls.offlineAlert.Visible()) {
                $userControls.offlineAlert.Visible(false);
                $userControls.onlineAlert.Visible(true);
                $userControls.onlineAlert.getEvents().FireAsynchronousMethod(() : void => {
                    $userControls.onlineAlert.Visible(false);
                }, 2000);
                await this.onRestoreHandler();
            }
        });
    }
}

export interface IServerStateUserControls {
    reloadAlert : GuiCommons;
    offlineAlert : GuiCommons;
    onlineAlert : GuiCommons;
    reloadCountDown : Label;
    reloadSuspend : Button;
}

// generated-code-start
/* eslint-disable */
export const IServerStateUserControls = globalThis.RegisterInterface(["reloadAlert", "offlineAlert", "onlineAlert", "reloadCountDown", "reloadSuspend"]);
/* eslint-enable */
// generated-code-end
