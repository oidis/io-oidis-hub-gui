/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/EventsManager.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { FileFetch } from "@io-oidis-services/Io/Oidis/Services/Utils/FileFetch.js";
import { AuthManagerConnector } from "../Connectors/AuthManagerConnector.js";
import { PrintConnector } from "../Connectors/PrintConnector.js";
import { PrintType } from "../Enums/PrintType.js";
import { IExportStatus } from "../Interfaces/IExportStatus.js";
import { Loader } from "../Loader.js";
import { BaseModel } from "../Models/BaseModel.js";
import { Print } from "../Models/Print.js";
import { User } from "../Models/User.js";
import { PrintPage } from "../Primitives/PrintPage.js";
import { UserContext } from "../Structures/UserContext.js";
import { PrintContainer } from "../UserControls/PrintContainer.js";
import { PrintRow } from "../UserControls/PrintRow.js";
import { Convert } from "../Utils/Convert.js";

export class PrintBinding<PrintItem = GuiCommons, Data = BaseModel> extends BaseObject {
    protected localization : IPrintBindingLocalization;
    private fetchUtils : FileFetch;
    private auth : AuthManagerConnector;
    private options : any;
    private printSection : PrintContainer;
    private screenSection : GuiCommons;
    private content : GuiCommons;
    private manager : PrintConnector;

    constructor() {
        super();

        this.fetchUtils = new FileFetch();
        this.auth = AuthManagerConnector.getClient();
        this.manager = this.getPrintConnector();
        this.options = {
            withRESThooks    : false,
            onAuthRequest    : async () : Promise<IPrintCredentials> => {
                return {
                    userName: "",
                    token   : ""
                };
            },
            onDataRequest    : async ($ids : string[]) : Promise<Data[]> => {
                return [];
            },
            onMessage        : ($message : string, $level : LogLevel) : void => {
                if ($level === LogLevel.ERROR) {
                    LogIt.Error($message);
                } else if ($level === LogLevel.WARNING) {
                    LogIt.Warning($message);
                } else {
                    LogIt.Info($message);
                }
            },
            sort             : ($data : Data[]) : Data[] => {
                return $data;
            },
            onPrintItemCreate: () : PrintItem => {
                return <any>new GuiCommons();
            },
            onItemPrint      : ($item : PrintItem, $data : Data) : void => {
                // Do nothing
            },
            onFileNameRequest: ($prefix : string = "Print") : string => {
                const now : Date = new Date();
                return $prefix + "_" + StringUtils.Remove(
                    Convert.DateToCsFormat(now) + Convert.TimeToLocalFormat(now), ".", ":") + ".pdf";
            }
        };
        this.localization = {
            notAuthorized          : "Not authorized print request.",
            printIdMissing         : "Print Id must be specified.",
            printOptimizationFailed: "Print optimization has failed.",
            printDataNotFound      : "Print data not found.",
            printFailed            : "Print generic failure."
        };
    }

    public Bind($userControls : IPrintBindingUserControls, $handlers : IPrintBindingOptions<PrintItem, Data>) : void {
        this.screenSection = $userControls.screenSection;
        this.printSection = $userControls.printSection;
        this.content = this.printSection.body;

        JsonUtils.Extend(this.options, $handlers);

        if (this.options.withRESThooks && ObjectValidator.IsEmptyOrNull(document.getElementById("printStart"))) {
            const printIdInput : HTMLInputElement = document.createElement("input");
            printIdInput.id = "printId";
            printIdInput.value = "";
            printIdInput.hidden = true;
            document.body.appendChild(printIdInput);

            const start : HTMLElement = document.createElement("span");
            start.id = "printStart";
            start.onclick = async () : Promise<void> => {
                const printId : string = printIdInput.value;
                LogIt.Debug("> new print request from: " + printId);
                this.screenSection.Visible(false);
                this.printSection.Visible(true);
                const completed : HTMLElement = document.getElementById("printComplete");
                if (!ObjectValidator.IsEmptyOrNull(completed)) {
                    document.body.removeChild(completed);
                }
                const credentials : IPrintCredentials = await this.options.onAuthRequest();
                const context : UserContext = Loader.getInstance().getContext();
                await context.Load();
                const profile : User = context.getUserProfile();
                if (ObjectValidator.IsEmptyOrNull(profile) ||
                    !ObjectValidator.IsEmptyOrNull(profile) && profile.UserName !== credentials.userName) {
                    this.auth.CurrentToken(await this.auth.LogIn(credentials.userName, credentials.token));
                    await context.Reload();
                }
                if (await this.auth.IsAuthenticated(this.auth.CurrentToken())) {
                    await this.printFromRequest(printId);
                    const completed : HTMLElement = document.createElement("span");
                    completed.id = "printComplete";
                    document.body.appendChild(completed);
                    setTimeout(() : void => {
                        document.body.removeChild(completed);
                    }, 100);
                } else {
                    LogIt.Warning("Not authorized token: {0}", this.auth.CurrentToken());
                    this.options.onMessage(this.localization.notAuthorized, LogLevel.WARNING);
                }
            };
            document.body.appendChild(start);
        }
    }

    public async PrintPreview($ids : string[], $settings : Print) : Promise<void> {
        const style : HTMLStyleElement = document.createElement("style");
        style.type = "text/css";
        style.appendChild(document.createTextNode(`
@media print {
    @page {
        size: ${$settings.Width}mm ${$settings.Height}mm !important;
    }
}`));
        document.body.appendChild(style);

        await this.printItems($ids, $settings);
        this.printSection.Visible(true);
        this.printSection.InstanceOwner().classList.remove("HiddenElement");

        this.printSection.warning.Visible(false);
    }

    public async PrintToPDF($ids : string[], $settings : Print) : Promise<void> {
        await this.printItems($ids, $settings);
        const name : string = $settings.SaveAsFile ? this.options.onFileNameRequest() : "";
        const status : IExportStatus = await this.manager.GeneratePDF(this.content.InstanceOwner().innerHTML, {
            height: $settings.Height + "mm",
            width : $settings.Width + "mm"
        });
        await this.fetchUtils.Open({
            content : Convert.Base64ToBlob(<string>status.data.data,
                "application/" + status.data.format + "; charset=" + status.data.encoding),
            fileName: name
        });
        this.screenSection.Visible(true);
        this.content.Visible(false);
        this.content.Clear();
        this.printSection.warning.Visible(true);
    }

    protected getPrintConnector() : PrintConnector {
        return PrintConnector.getClient();
    }

    private async printFromRequest($printId : string) : Promise<void> {
        if (ObjectValidator.IsEmptyOrNull($printId)) {
            LogIt.Warning("Print Id must be specified.");
            this.options.onMessage(this.localization.printIdMissing, LogLevel.ERROR);
        } else {
            this.screenSection.Visible(false);
            this.printSection.Visible(true);
            this.printSection.InstanceOwner().classList.remove("HiddenElement");
            this.printSection.warning.Visible(false);
            const options : IPrintAPI = await this.manager.getPrintOptions($printId);
            if (!ObjectValidator.IsEmptyOrNull(options)) {
                LogIt.Debug("Started processing of print for: {0}", JSON.stringify(options, null, 2));
                const settings : Print = new Print();
                settings.Format = options.settings.format;
                if (!ObjectValidator.IsEmptyOrNull(options.settings.width)) {
                    settings.Width = options.settings.width;
                }
                if (!ObjectValidator.IsEmptyOrNull(options.settings.height)) {
                    settings.Height = options.settings.height;
                }
                if (!ObjectValidator.IsEmptyOrNull(options.settings.rows)) {
                    settings.Rows = options.settings.rows;
                }
                if (!ObjectValidator.IsEmptyOrNull(options.settings.columns)) {
                    settings.Columns = options.settings.columns;
                }
                if (!ObjectValidator.IsEmptyOrNull(options.settings.limit)) {
                    settings.Limit = options.settings.limit;
                }
                if (!ObjectValidator.IsEmptyOrNull(options.settings.offset)) {
                    settings.Offset = options.settings.offset;
                }
                if (!ObjectValidator.IsEmptyOrNull(options.settings.padding)) {
                    settings.Padding = options.settings.padding;
                }
                settings.Grid = options.settings.grid;
                await this.printItems(options.itemsIds, settings);
            } else {
                LogIt.Error("Failed to find print options for id: {0}", $printId);
            }
        }
    }

    private async printItems($ids : string[], $options : Print) : Promise<void> {
        this.content.Clear();
        this.content.Visible(true);

        try {
            LogIt.Debug("Print items for: " + $ids.join());
            const data : any[] = await this.options.onDataRequest($ids);
            if (!ObjectValidator.IsEmptyOrNull(data)) {
                try {
                    await this.generatePrintContent(this.options.sort(data), $options);
                } catch (ex) {
                    LogIt.Error(ex);
                    this.options.onMessage(this.localization.printOptimizationFailed, LogLevel.ERROR);
                }
            } else {
                LogIt.Error("Print data not found");
                this.options.onMessage(this.localization.printDataNotFound, LogLevel.ERROR);
            }
        } catch (ex) {
            LogIt.Error(ex);
            this.options.onMessage(this.localization.printFailed, LogLevel.ERROR);
        }
    }

    private async generatePrintContent($data : any[], $options : Print) : Promise<void> {
        const children : GuiCommons[] = [];
        const padding : number = $options.Padding;
        const itemsLimit : number = $options.Rows * $options.Columns;
        const itemsPerRow : number = $options.Columns;
        let offset : number = $options.Offset;
        if (offset < 0) {
            offset = 0;
        }
        if (offset > itemsLimit) {
            offset = itemsLimit;
        }
        let itemPerPage : number = 0;
        let itemIndex : number = 0;
        let page : PrintPage = new PrintPage();
        page.Width($options.Width);
        page.Height($options.Height);
        page.Padding(padding / 2);
        const rowHeight : number = Convert.ToFixed((page.Height() - page.Padding() * 2) / $options.Rows, 2);
        this.content.AddChild(page);
        let row : PrintRow;
        if (offset > 0) {
            let offsetCount : number = 0;
            for (let index : number = 0; index < offset; index++) {
                row = new PrintRow(itemsPerRow);
                row.Padding(page.Padding());
                row.Height(rowHeight);
                row.WithGrid($options.Grid);
                page.AddChild(row);
                index++;
                offsetCount += itemsPerRow;
                itemPerPage += itemsPerRow;
            }
            itemIndex = offsetCount - offset;
        }

        $data.forEach(($record : any) : void => {
            if (itemIndex === 0 || itemsPerRow === 1) {
                row = new PrintRow(itemsPerRow);
                row.Padding(page.Padding());
                row.Height(rowHeight);
                row.WithGrid($options.Grid);
                if (itemPerPage === itemsLimit) {
                    page = new PrintPage();
                    page.Width($options.Width);
                    page.Height($options.Height);
                    page.Padding(padding / 2);
                    this.content.AddChild(page);
                    itemPerPage = 0;
                }
                page.AddChild(row);
                itemPerPage += itemsPerRow;
            }
            const printItem : GuiCommons = this.options.onPrintItemCreate();
            children.push(printItem);
            row.columns[itemIndex].AddChild(printItem);
            if (itemIndex < itemsPerRow - 1) {
                itemIndex++;
            } else {
                itemIndex = 0;
            }
            this.options.onItemPrint(<any>printItem, $record);
        });
        return new Promise<void>(($resolve, $reject) : void => {
            EventsManager.getInstanceSingleton().FireAsynchronousMethod(async () : Promise<void> => {
                try {
                    const maxHeight : number = Math.floor((rowHeight - padding) * this.getMillimeterInPixels());
                    children.forEach(($printItem : GuiCommons) : void => {
                        try {
                            if ($printItem.InstanceOwner().scrollHeight > maxHeight) {
                                let elementWidth : number = $printItem.InstanceOwner().offsetWidth;
                                if (elementWidth < $printItem.InstanceOwner().scrollWidth) {
                                    elementWidth = $printItem.InstanceOwner().scrollWidth;
                                }
                                ElementManager.Scale($printItem.InstanceOwner(), elementWidth, maxHeight);
                            }
                        } catch (ex) {
                            LogIt.Error(ex);
                        }
                    });
                    $resolve();
                } catch (ex) {
                    $reject(ex);
                }
            }, true, 10);
        });
    }

    private getMillimeterInPixels() : number {
        const ruler : GuiCommons = this.printSection.ruler;
        ruler.Visible(true);
        const pixels : number = ruler.InstanceOwner().offsetHeight / 100;
        ruler.Visible(false);
        this.getMillimeterInPixels = () : number => {
            return pixels;
        };
        return pixels;
    }
}

export interface IPrintBindingUserControls {
    printSection : PrintContainer;
    screenSection : GuiCommons;
}

export interface IPrintBindingOptions<PrintItem = GuiCommons, Data = BaseModel> {
    withRESThooks? : boolean;
    onMessage? : ($message : string, $level : LogLevel) => void;
    onAuthRequest? : () => Promise<IPrintCredentials>;
    onDataRequest : ($ids : string[]) => Promise<Data[]>;
    sort? : ($data : Data[]) => Data[];
    onPrintItemCreate : () => PrintItem;
    onItemPrint : ($item : PrintItem, $data : Data) => void;
    onFileNameRequest? : ($prefix : string) => string;
}

export interface IPrintCredentials {
    userName : string;
    token : string;
}

export interface IPrintBindingLocalization {
    notAuthorized : string;
    printIdMissing : string;
    printOptimizationFailed : string;
    printDataNotFound : string;
    printFailed : string;
}

export interface IPrintAPI {
    itemsIds : string[];
    version : string;
    fileName? : string;
    printType? : string;
    settings? : IPrintSettingsAPI;
    withCache? : boolean;
}

export interface IPrintSettingsAPI {
    offset? : number;
    limit? : number; /// TODO: limit should be removed and replaced by columns and rows or from limit should be calculated rows and cols
    columns? : number;
    rows? : number;
    width? : number;
    height? : number;
    format? : PrintType;
    padding? : number;
    grid? : boolean;
}

// generated-code-start
/* eslint-disable */
export const IPrintBindingUserControls = globalThis.RegisterInterface(["printSection", "screenSection"]);
export const IPrintBindingOptions = globalThis.RegisterInterface(["withRESThooks", "onMessage", "onAuthRequest", "onDataRequest", "sort", "onPrintItemCreate", "onItemPrint", "onFileNameRequest"]);
export const IPrintCredentials = globalThis.RegisterInterface(["userName", "token"]);
export const IPrintBindingLocalization = globalThis.RegisterInterface(["notAuthorized", "printIdMissing", "printOptimizationFailed", "printDataNotFound", "printFailed"]);
export const IPrintAPI = globalThis.RegisterInterface(["itemsIds", "version", "fileName", "printType", "settings", "withCache"]);
export const IPrintSettingsAPI = globalThis.RegisterInterface(["offset", "limit", "columns", "rows", "width", "height", "format", "padding", "grid"]);
/* eslint-enable */
// generated-code-end
