/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { BaseModel } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { Entity, Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";

@Entity("testModel")
export class TestModel extends BaseModel {
    @Property({type: String})
    public declare TestString : string;
}

export class ModelTest extends RuntimeTestRunner {

    public async SaveAndLoadTest() : Promise<void> {
        const records : TestModel[] = await TestModel.Find({});
        Echo.Printf(records.length);
        records.forEach(($model : TestModel) : void => {
            Echo.Printf($model.Id + ": " + $model.TestString);
        });
        if (records.length > 0) {
            const model : TestModel = await TestModel.FindById(records[0].Id);
            Echo.Printf(model.ToInterface());
        } else {
            const model : TestModel = new TestModel();
            model.TestString = "test";
            await model.Save();
            const model2 : TestModel = new TestModel();
            model2.TestString = "test model 2";
            await model2.Save();

            const model3 : TestModel = await TestModel.FindById(model.Id);
            Echo.Printf(model.ToInterface());
            Echo.Printf(model3.ToInterface());
            model3.TestString = "updated string " + this.getUID();
            await model3.Save();
            Echo.Printf(model3.ToInterface());

            await model3.Delete();

            await model2.Remove();
        }
    }
}

/* dev:end */
