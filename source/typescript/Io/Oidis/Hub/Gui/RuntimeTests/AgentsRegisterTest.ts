/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { AgentsRegisterConnectorAsync, IAgentInfo } from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";
import { AuthManagerConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { FileSystemHandlerConnectorAsync } from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";

class MockFSHandler extends FileSystemHandlerConnectorAsync {

    public async NotExist() : Promise<void> {
        return this.asyncInvoke("Some.Method.Somewhere");
    }

    public async NotAuthorized() : Promise<void> {
        return this.asyncInvoke("DeleteMe");
    }
}

export class AgentsRegisterTest extends RuntimeTestRunner {
    private auth : AuthManagerConnector;
    private client : AgentsRegisterConnectorAsync;
    private agent : MockFSHandler;

    constructor() {
        super();

        // this.setMethodFilter("");
    }

    public async testInitUser() : Promise<void> {
        Echo.Printf("To pass this test root:root has to be configured in private.conf defaultUser section");
        this.auth.CurrentToken(await this.auth.LogIn("root", "root"));

        this.addButton("login root", async () : Promise<void> => {
            this.auth.CurrentToken(await this.auth.LogIn("root", "root"));
            Echo.Printf("Logged in as root");
        });
        this.addButton("login test", async () : Promise<void> => {
            const userName : string = "agent-register-test-user";
            const groupName : string = "agent-register-test-group";
            const userPass : string = "test";
            if ((await this.auth.FindUsers(userName)).size === 0) {
                await this.auth.Register({name: userName, activated: true, password: userPass});
                await this.auth.CreateGroup(groupName, [], true);
                await this.auth.AddGroup(groupName, userName);
            }
            await this.auth.CreateGroup(groupName, [
                "Io.Oidis.Hub.Utils.AuthManager.*",
                "Io.Oidis.Hub.Utils.ConnectorHub.ForwardRequest",
                "Io.Oidis.Hub.Primitives.AgentsRegister.getAgentsList"
            ], true);
            this.auth.CurrentToken(await this.auth.LogIn(userName, userPass));
            Echo.Printf("Logged in as " + userName);
        });
    }

    public async testRegister() : Promise<void> {
        Echo.Printf("" +
            "<button id='genToken' type='button'>Generate Token</button>" +
            "<input id='generatedToken' type='text' readonly width='350' onClick='this.setSelectionRange(0, this.value.length)'/>" +
            "<br>" +
            "<textarea id=\"agents_info\" readonly cols=\"100\" rows=\"7\"/>");
        const agentsInfo : HTMLTextAreaElement = <HTMLTextAreaElement>document.getElementById("agents_info");
        const generatedToken : HTMLInputElement = <HTMLInputElement>document.getElementById("generatedToken");
        const genToken : HTMLButtonElement = <HTMLButtonElement>document.getElementById("genToken");

        genToken.onclick = async () : Promise<void> => {
            generatedToken.value = (await this.auth.GenerateAuthToken("", "test-agent")).value;
        };

        const getInfo : any = async ($onRequest : () => Promise<string>) : Promise<void> => {
            const list : IAgentInfo[] = await this.client.getAgentsList();
            agentsInfo.value = "";
            for await (const info of list) {
                this.agent.setAgent(info.id, this.getRequest().getHostUrl() + "connector.config.jsonp");
                this.agent.setForwardingMethod("Io.Oidis.Hub.Utils.ConnectorHub.ForwardRequest");
                try {
                    agentsInfo.value = info.domain + "-" + info.platform + " [" + info.id + "] - " + "\n" + agentsInfo.value + " " +
                        await $onRequest();
                } catch (ex) {
                    Echo.Printf(ex.message);
                }
            }
        };
        const loadInfo : any = () : void => {
            setTimeout(async () : Promise<void> => {
                await getInfo(() : Promise<string> => {
                    return this.agent.getAppDataPath();
                });
                loadInfo();
            }, 1000);
        };
        this.addButton("start loop", () : void => {
            loadInfo();
        });
        this.addButton("invoke method", async () : Promise<void> => {
            await getInfo(() : Promise<string> => {
                return this.agent.getAppDataPath();
            });
        });
        this.addButton("invoke not existing", async () : Promise<void> => {
            await getInfo(async () : Promise<string> => {
                await this.agent.NotExist();
                return "";
            });
        });
    }

    protected before() : void {
        this.auth = new AuthManagerConnector(true, this.getRequest().getHostUrl() + "connector.config.jsonp");
        this.client = new AgentsRegisterConnectorAsync(true, this.getRequest().getHostUrl() + "connector.config.jsonp");
        this.agent = new MockFSHandler(true, this.getRequest().getHostUrl() + "connector.config.jsonp",
            WebServiceClientType.FORWARD_CLIENT);
        this.auth.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
        this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
        this.agent.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
    }

    // protected async tearDown() : Promise<void> {
    //     /// TODO: remove test user and test methods
    // }
}

/* dev:end */
