/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { IRuntimeTestPromise } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { IWebServiceClient } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceClient.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";
import { WebServiceClientFactory } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/WebServiceClientFactory.js";

export class Snippets extends RuntimeTestRunner {

    private client : IWebServiceClient;

    constructor() {
        super();

        this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
            this.getRequest().getHostUrl() + "connector.config.jsonp");
        this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
    }

    public __IgnoretestSendMail() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, "Io.Oidis.Hub.Utils.SendMail", "Send",
                "kuba@oidis.io",
                "Report from Oidis Hub",
                "<h1>New TEST mail</h1>" +
                "This is test message from Oidis Mail sender<br>" +
                "<p>Regards, Oidis Reporter</p>" +
                "<img src='/resource/graphics/Io/Oidis/Commons/WUILogo.png'>",
                ["/resource/graphics/Io/Oidis/Commons/WUILogo.png"],
                "Some important test mail from Oidis Reporter",
                "",
                "" // noreply@oidis.io
            ).Then(($status : boolean) : void => {
                this.assertEquals($status, true);
                $done();
            });
        };
    }
}
/* dev:end */
