/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { SSOBinding } from "../Bindings/SSOBinding.js";
import { AuthManagerConnector, ISystemGroups } from "../Connectors/AuthManagerConnector.js";
import { Loader } from "../Loader.js";
import { Group } from "../Models/Group.js";
import { User } from "../Models/User.js";

export class UserContext extends BaseObject {
    public IsAuthenticated : boolean;
    public Need2FACheck : boolean;
    public IsAdmin : boolean;
    public IsSysAdmin : boolean;
    public IsActive : boolean;
    public IsMobile : boolean;
    public IsIOS : boolean;
    private auth : AuthManagerConnector;
    private user : User;
    private systemGroups : ISystemGroups;

    constructor() {
        super();

        this.Restore();
    }

    public async Load() : Promise<void> {
        if (ObjectValidator.IsEmptyOrNull(this.user)) {
            this.IsMobile = Loader.getInstance().getHttpManager().getRequest().IsMobileDevice();
            this.IsIOS = Loader.getInstance().getHttpManager().getRequest().IsIOS();
            this.auth = this.initAuthClient();
            this.IsAuthenticated = await this.auth.IsAuthenticated();
            this.IsAdmin = false;
            this.IsSysAdmin = false;
            this.IsActive = false;
            this.Need2FACheck = false;
            const sso : SSOBinding = new SSOBinding();
            if (this.IsAuthenticated) {
                this.systemGroups = await this.auth.getSystemGroups();
                this.IsAdmin = await this.auth.HasGroup(this.systemGroups.Admin);
                this.IsSysAdmin = await this.auth.HasGroup(this.systemGroups.SysAdmin);
                this.user = await this.auth.getUserProfile(false);
                Loader.getInstance().setLogsContext(this.user.Id);
                this.IsActive = this.user.Activated;
                this.Need2FACheck = await this.auth.Has2FAActivated();
                if (this.Need2FACheck) {
                    this.Need2FACheck = !sso.TwoFAChecked();
                }

                try {
                    await fetch("/api/sso/validate", {
                        headers: new Headers({
                            authorization: "Bearer " + this.auth.CurrentToken()
                        }),
                        method : "GET"
                    });
                } catch (ex) {
                    LogIt.Error(ex);
                }
            } else {
                sso.TwoFAChecked(false);
            }
        }
    }

    public async SwitchToProfile($group : Group) : Promise<void> {
        const groups : ISystemGroups = this.getGroups();
        if (ObjectValidator.IsEmptyOrNull($group)) {
            this.IsAdmin = false;
            this.IsSysAdmin = false;
        } else if (await this.getAuth().HasGroup(groups.Admin)) {
            switch ($group.Id) {
            case groups.SysAdmin.Id:
                this.IsAdmin = false;
                this.IsSysAdmin = true;
                break;
            case groups.Admin.Id:
                this.IsAdmin = true;
                this.IsSysAdmin = false;
                break;
            default:
                LogIt.Warning("Unrecognized or unsupported FE profile: " + $group.Name);
                break;
            }
        } else {
            LogIt.Error("User profile switch is enabled only for admins.");
        }
    }

    public Restore() : void {
        this.user = null;
        this.IsAuthenticated = false;
        this.IsAdmin = false;
        this.IsSysAdmin = false;
        this.IsActive = false;
        this.IsMobile = false;
        this.IsIOS = false;
        this.Need2FACheck = false;
    }

    public async Reload() : Promise<void> {
        /// TODO: this should be invoked from BE push notification
        this.Restore();
        await this.Load();
    }

    public getUserProfile() : User {
        return this.user;
    }

    public getGroups() : ISystemGroups {
        return this.systemGroups;
    }

    public getCurrentToken() : string {
        return this.auth.CurrentToken();
    }

    protected initAuthClient() : AuthManagerConnector {
        return AuthManagerConnector.getClient();
    }

    protected getAuth() : AuthManagerConnector {
        return this.auth;
    }
}
