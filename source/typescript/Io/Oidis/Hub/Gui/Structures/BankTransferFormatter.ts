/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";

export class BankTransferFormatter extends BaseObject {
    public Iban : string;
    public Message : string;
    public Recipient : string;
    public Amount : number;
    public VS : string;
    public Currency : string = "CZK";

    public ToString() : string {
        // message format based on https://qr-platba.cz/

        let msg : string = this.Message;
        if (!ObjectValidator.IsEmptyOrNull(msg)) {
            msg = "*MSG:" + msg;
        }
        let rn : string = this.Recipient;
        if (!ObjectValidator.IsEmptyOrNull(rn)) {
            rn = "*RN:" + rn;
        }
        let vs : string = this.VS;
        if (!ObjectValidator.IsEmptyOrNull(vs)) {
            vs = "*X-VS:" + vs;
        }
        return "SPD*1.0*ACC:" + this.Iban + "*AM:" + this.Amount + "*CC:" + this.Currency + msg + rn + vs;
    }
}
