/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Dialog } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Dialog.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";

export class ScannerDialog extends Dialog {
    public readonly title : GuiCommons;
    public readonly cameraView : GuiCommons;
    public readonly close : Button;
    public readonly decodedContent : GuiCommons;
    public readonly decoded : TextField;
    public readonly deviceSelector : Button;
    public readonly deviceMenu : GuiCommons;
    public readonly find : Button;
    public readonly decodedError : GuiCommons;
    public readonly permissionError : GuiCommons;
    public readonly uploadButton : Button;
    protected localization : IScannerDialogLocalization;

    constructor() {
        super();

        this.title = new GuiCommons();
        this.cameraView = new GuiCommons();
        this.close = new Button();
        this.decodedContent = new GuiCommons();
        this.decoded = new TextField();
        this.deviceSelector = new Button();
        this.deviceMenu = new GuiCommons();
        this.find = new Button();
        this.uploadButton = new Button();
        this.decodedError = new GuiCommons();
        this.permissionError = new GuiCommons();
        this.localization = {
            chooseDevice      : "Choose device",
            uploadImage       : "Upload image",
            decodingError     : "Failed to decode data.",
            missingPermissions: "Scanning is not possible, due to missing permission for camera access.",
            close             : "Close",
            result            : "Decoded",
            search            : "Search"
        };
    }

    protected innerHtml() : string {
        return `
<div class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable modal-fullscreen-md-down" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="dropdown" style="margin-left: 15px;width: calc(100% - 60px);"><button class="btn btn-outline-primary dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" type="button" data-oidis-bind="${this.deviceSelector}" style="width: 100%;"><svg class="bi bi-camera" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-bottom: 3px;margin-right: 10px;">
                            <path d="M15 12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h1.172a3 3 0 0 0 2.12-.879l.83-.828A1 1 0 0 1 6.827 3h2.344a1 1 0 0 1 .707.293l.828.828A3 3 0 0 0 12.828 5H14a1 1 0 0 1 1 1v6zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2z"></path>
                            <path d="M8 11a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5zm0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7zM3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"></path>
                        </svg>${this.localization.chooseDevice}</button>
                    <div class="dropdown-menu" data-oidis-bind="${this.deviceMenu}" style="width: 100%;"></div>
                </div><button class="btn btn-primary" data-bs-toggle="tooltip" data-bs-placement="bottom" type="button" style="margin-left: 15px;margin-right: 15px;" title="${this.localization.uploadImage}" data-oidis-bind="${this.uploadButton}"><svg class="bi bi-image" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-bottom: 1px;">
                        <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"></path>
                        <path d="M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z"></path>
                    </svg></button><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal" data-oidis-bind="${this.title}"></button>
            </div>
            <div class="modal-body" style="height: calc(100% - 300px);padding: 0;overflow: hidden;">
                <div class="row d-xl-flex" style="min-height: 300px;margin: 0;">
                    <div class="col text-center align-self-center" style="padding: 0;position: relative;">
                        <div class="alert alert-danger ${GeneralCSS.DNONE}" role="alert" style="position: absolute;width: calc(100% - 40px);z-index: 1;margin-left: 20px;" data-oidis-bind="${this.decodedError}"><span><strong>${this.localization.decodingError}</strong></span></div>
                        <div class="alert alert-danger ${GeneralCSS.DNONE}" role="alert" style="position: absolute;width: calc(100% - 40px);z-index: 1;margin-left: 20px;" data-oidis-bind="${this.permissionError}"><span><strong>${this.localization.missingPermissions}</strong></span></div>
                        <div class="row" style="margin: 0;">
                            <div class="col ScannerBox" data-oidis-bind="${this.cameraView}" style="padding: 0;"><span class="spinner-grow" role="status"></span><span class="spinner-grow" role="status"></span><span class="spinner-grow" role="status"></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal" data-oidis-bind="${this.close}">${this.localization.close}</button>
                <div class="input-group ${GeneralCSS.DNONE}" data-oidis-bind="${this.decodedContent}"><span class="input-group-text">${this.localization.result}</span><input class="form-control" type="text" data-oidis-bind="${this.decoded}" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.find}">${this.localization.search}</button></div>
            </div>
        </div>
    </div>
</div>`;
    }
}

export interface IScannerDialogLocalization {
    chooseDevice : string;
    uploadImage : string;
    decodingError : string;
    missingPermissions : string;
    close : string;
    result : string;
    search : string;
}

// generated-code-start
/* eslint-disable */
export const IScannerDialogLocalization = globalThis.RegisterInterface(["chooseDevice", "uploadImage", "decodingError", "missingPermissions", "close", "result", "search"]);
/* eslint-enable */
// generated-code-end
