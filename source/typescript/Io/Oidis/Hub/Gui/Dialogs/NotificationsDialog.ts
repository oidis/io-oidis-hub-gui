/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Dialog } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Dialog.js";

export class NotificationsDialog extends Dialog {
    public readonly body : GuiCommons;
    public readonly close : Button;
    public readonly clear : Button;
    protected localization : INotificationsDialogLocalization;

    constructor() {
        super();

        this.body = new GuiCommons();
        this.close = new Button();
        this.clear = new Button();
        this.localization = {
            header  : "Notifications",
            loading : "loading ...",
            clearAll: "Clear all"
        };
    }

    protected innerHtml() : string {
        return `
<div class="offcanvas offcanvas-end ${this.getClassNameWithoutNamespace()}" data-bs-backdrop="false" tabindex="-1">
    <div class="offcanvas-header" data-oidis-bind="${this.close}">
        <h5 class="offcanvas-title">${this.localization.header}</h5><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="offcanvas"></button>
    </div>
    <div class="offcanvas-body text-center d-flex flex-column" data-oidis-bind="${this.body}">
        <div class="row">
            <div class="col">
                <div class="table-responsive text-start">
                    <table class="table table-striped">
                        <tbody data-oidis-bind="${this.body}">
                            <tr>
                                <td>${this.localization.loading}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row mt-auto">
            <div class="col"><button class="btn btn-primary" type="button" data-bs-dismiss="offcanvas" data-oidis-bind="${this.clear}">${this.localization.clearAll}</button></div>
        </div>
    </div>
</div>`;
    }
}

export interface INotificationsDialogLocalization {
    header : string;
    loading : string;
    clearAll : string;
}

// generated-code-start
export const INotificationsDialogLocalization = globalThis.RegisterInterface(["header", "loading", "clearAll"]);
// generated-code-end
