/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Dialog } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Dialog.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";

export class BackdoorLoginDialog extends Dialog {
    public readonly userName : TextField;
    public readonly password : TextField;
    public readonly login : Button;
    public readonly cancel : Button;
    public readonly profilesMenu : GuiCommons;
    private readonly adminForm : GuiCommons;
    private readonly loginForm : GuiCommons;
    private readonly switchToLogin : Button;

    constructor() {
        super();
        this.userName = new TextField();
        this.password = new TextField();
        this.login = new Button();
        this.cancel = new Button();
        this.profilesMenu = new GuiCommons();
        this.adminForm = new GuiCommons();
        this.loginForm = new GuiCommons();
        this.switchToLogin = new Button();
    }

    public IsForAdmin($value : boolean) : void {
        this.loginForm.Visible(!$value);
        this.adminForm.Visible($value);
    }

    protected innerCode() : string {
        this.switchToLogin.getEvents().setOnClick(() : void => {
            this.loginForm.Visible(true);
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<div class="modal fade ${this.getClassNameWithoutNamespace()}" role="dialog" tabindex="-1" style="background-color: rgba(49,49,49,0.35);">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body mt-4 mx-5">
                <section data-oidis-bind="${this.adminForm}">
                    <div class="card" style="margin-bottom: 20px;">
                        <div class="card-body">
                            <h4 class="card-title">Backdoor login</h4>
                            <h6 class="text-muted card-subtitle mb-2">User login dialog for administrators</h6>
                            <p class="card-text">Choose one of predefined user profiles, accounts or log by any of registered account either by valid password or by auth token.</p>
                            <p class="text-secondary" style="font-size: 12px;"><svg class="bi bi-info-circle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 4px;margin-bottom: 2px;">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"></path>
                                    <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0"></path>
                                </svg>Profiles are designed to switch only front-end configuration and simulate group UX.</p>
                            <p class="text-secondary" style="font-size: 12px;"><svg class="bi bi-info-circle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 4px;margin-bottom: 2px;">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"></path>
                                    <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0"></path>
                                </svg>Accounts provide regular logout and login under selected system account with specific group permissions.</p>
                        </div>
                    </div>
                    <div class="dropdown"><button class="btn btn-primary" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;margin-bottom: 20px;">Choose profile</button>
                        <div class="dropdown-menu" style="width: 100%;">
                            <div data-oidis-bind="${this.profilesMenu}"><button class="btn dropdown-item" type="button">loading ...</button></div><button class="btn dropdown-item" type="button" data-oidis-bind="${this.switchToLogin}">Custom login</button>
                        </div>
                    </div>
                </section>
                <form class="${GeneralCSS.DNONE}" data-oidis-bind="${this.loginForm}">
                    <div class="input-form-group d-flex flex-column mb-4"><label class="form-label d-none" for="email">Email:</label><input id="email" class="form-control text-center" type="email" placeholder="user name" data-oidis-bind="${this.userName}" name="email" /></div>
                    <div class="input-form-group d-flex flex-column mb-4"><label class="form-label d-none" for="passwords">Password:</label><input id="password" class="form-control text-center" type="password" placeholder="**********" data-oidis-bind="${this.password}" name="current-password" autocomplete="on" /></div>
                    <div class="text-center mb-3"><button class="btn btn-outline-secondary" type="button" data-oidis-bind="${this.cancel}" style="margin-right: 30px;">Cancel</button><button class="btn btn-primary" type="button" data-oidis-bind="${this.login}">Login</button></div>
                </form>
            </div>
        </div>
    </div>
</div>`;
    }
}
