/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Dialog } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Dialog.js";
import { WhatsNewPanel } from "../Panels/Dashboard/WhatsNewPanel.js";

export class ReleaseNotesDialog extends Dialog {
    public readonly body : WhatsNewPanel;
    public readonly versionSelector : Button;
    public readonly versionMenu : GuiCommons;
    public readonly close : Button;
    protected localization : IReleaseNotesDialogLocalization;

    constructor() {
        super();

        this.body = new WhatsNewPanel();
        this.versionSelector = new Button();
        this.versionMenu = new GuiCommons();
        this.close = new Button();
        this.localization = {
            header     : "What is new in version",
            loading    : "loading ...",
            initVersion: "xxxx.x.x"
        };
    }

    protected innerHtml() : string {
        return `
<div class="offcanvas offcanvas-end ${this.getClassNameWithoutNamespace()}" tabindex="-1" style="width: 80%;">
    <div class="offcanvas-header" style="border-bottom: 1px solid #dddddd;">
        <div class="d-md-flex align-items-md-center" style="width: 100%;">
            <h3 style="margin-top: 6px;width: 260px;font-size: 25px;">${this.localization.header} </h3>
            <div class="dropdown" style="width: 115px;"><button class="btn btn-outline-primary dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" type="button" data-oidis-bind="${this.versionSelector}" style="width: 115px;text-align: left;">${this.localization.initVersion}</button>
                <div class="dropdown-menu" data-oidis-bind="${this.versionMenu}"><a class="dropdown-item" href="#">${this.localization.loading}</a></div>
            </div>
        </div><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="offcanvas" data-oidis-bind="${this.close}"></button>
    </div>
    <div class="offcanvas-body" data-oidis-bind="${this.body}"></div>
</div>`;
    }
}

export interface IReleaseNotesDialogLocalization {
    header : string;
    loading : string;
    initVersion? : string;
}

// generated-code-start
export const IReleaseNotesDialogLocalization = globalThis.RegisterInterface(["header", "loading", "initVersion"]);
// generated-code-end
