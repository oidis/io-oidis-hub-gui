/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";

export class ReportsPanel extends BasePanel {
    public readonly appIdSelector : Button;
    public readonly appIdMenu : GuiCommons;
    public readonly reportIdSelector : Button;
    public readonly reportIdMenu : GuiCommons;
    public readonly reportContent : GuiCommons;

    constructor() {
        super();

        this.appIdSelector = new Button();
        this.appIdMenu = new GuiCommons();
        this.reportIdSelector = new Button();
        this.reportIdMenu = new GuiCommons();
        this.reportContent = new GuiCommons();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row">
        <div class="col">
            <div class="dropdown"><button class="btn btn-outline-primary" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;margin-bottom: 10px;" data-oidis-bind="${this.appIdSelector}">application #</button>
                <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.appIdMenu}"><a class="dropdown-item" href="#">loading ...</a></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="dropdown"><button class="btn btn-outline-primary disabled text-center" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;" data-oidis-bind="${this.reportIdSelector}">report #</button>
                <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.reportIdMenu}"><a class="dropdown-item" href="#">loading ...</a></div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <p class="card-text ReportLog" data-oidis-bind="${this.reportContent}">Report content will be available only for selected application and report #</p>
                </div>
            </div>
        </div>
    </div>
</section>
`;
    }
}
