/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Dialog } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Dialog.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { ConnectorsTable } from "../../UserControls/ConnectorsTable.js";

export class RuntimeTool extends BasePanel {
    public readonly version : Label;
    public readonly buildTime : Label;
    public readonly upTime : Label;
    public readonly platform : Label;
    public readonly kill : Button;
    public readonly message : TextField;
    public readonly push : Button;
    public readonly killDialog : Dialog;
    public readonly close : Button;
    public readonly closeButton : Button;
    public readonly killConfirm : Button;
    public readonly notificationsSelector : GuiCommons;
    public readonly connectorsFilter : TextField;
    public readonly findConnectors : Button;
    public readonly connectorsTable : ConnectorsTable;
    public readonly loadFEConfig : Button;
    public readonly feConfigContent : Label;
    public readonly loadBEConfig : Button;
    public readonly beConfigContent : Label;

    constructor() {
        super();

        this.version = new Label();
        this.buildTime = new Label();
        this.upTime = new Label();
        this.platform = new Label();
        this.kill = new Button();
        this.killDialog = new Dialog();
        this.close = new Button();
        this.closeButton = new Button();
        this.killConfirm = new Button();
        this.message = new TextField();
        this.push = new Button();
        this.notificationsSelector = new GuiCommons();
        this.connectorsFilter = new TextField();
        this.findConnectors = new Button();
        this.connectorsTable = new ConnectorsTable();
        this.loadFEConfig = new Button();
        this.feConfigContent = new Label();
        this.loadBEConfig = new Button();
        this.beConfigContent = new Label();
    }

    protected innerHtml() : string {
        return `
<section data-oidis-bind="${this}">
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col">
                    <h4>Runtime info</h4>
                    <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 160px;">Version</span><span class="input-group-text" style="width: calc(100% - 160px);" data-oidis-bind="${this.version}"></span></div>
                    <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 160px;">Build time</span><span class="input-group-text" style="width: calc(100% - 160px);" data-oidis-bind="${this.buildTime}"></span></div>
                    <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 160px;">Up time</span><span class="input-group-text" style="width: calc(100% - 160px);" data-oidis-bind="${this.upTime}"></span></div>
                    <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 160px;">Platform</span><span class="input-group-text" style="width: calc(100% - 160px);" data-oidis-bind="${this.platform}"></span></div>
                    <div id="connectorsAccordion" class="accordion" role="tablist">
                        <div class="accordion-item">
                            <h2 class="accordion-header" role="tab"><button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectorsAccordion .item-1" aria-expanded="false" aria-controls="connectorsAccordion .item-1">Active connectors</button></h2>
                            <div class="accordion-collapse collapse item-1" role="tabpanel" data-bs-parent="#connectorsAccordion">
                                <div class="accordion-body">
                                    <div class="input-group" style="margin-bottom: 5px;"><input class="form-control" type="text" data-oidis-bind="${this.connectorsFilter}" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.findConnectors}"><svg class="bi bi-search" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 4px;margin-bottom: 2px;">
                                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0"></path>
                                            </svg>Search</button></div>
                                    <div data-oidis-bind="${this.connectorsTable}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="text-align: center;">
                <div class="col"><button class="btn btn-danger" type="button" data-oidis-bind="${this.kill}" style="margin-top: 10px;"><svg class="bi bi-power" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 2px;">
                            <path d="M7.5 1v7h1V1z"></path>
                            <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812"></path>
                        </svg>Stop application</button></div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col">
            <div class="row">
                <div class="col">
                    <h4>Environment config</h4>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div id="accordion-1" class="accordion" role="tablist">
                        <div class="accordion-item">
                            <h2 class="accordion-header" role="tab"><button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-1 .item-1" aria-expanded="false" aria-controls="accordion-1 .item-1">Front-end</button></h2>
                            <div class="accordion-collapse collapse item-1" role="tabpanel" data-bs-parent="#accordion-1">
                                <div class="accordion-body">
                                    <div class="row">
                                        <div class="col text-center"><button class="btn btn-primary" type="button" data-oidis-bind="${this.loadFEConfig}"><svg class="bi bi-braces-asterisk" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 1px;">
                                                    <path fill-rule="evenodd" d="M1.114 8.063V7.9c1.005-.102 1.497-.615 1.497-1.6V4.503c0-1.094.39-1.538 1.354-1.538h.273V2h-.376C2.25 2 1.49 2.759 1.49 4.352v1.524c0 1.094-.376 1.456-1.49 1.456v1.299c1.114 0 1.49.362 1.49 1.456v1.524c0 1.593.759 2.352 2.372 2.352h.376v-.964h-.273c-.964 0-1.354-.444-1.354-1.538V9.663c0-.984-.492-1.497-1.497-1.6ZM14.886 7.9v.164c-1.005.103-1.497.616-1.497 1.6v1.798c0 1.094-.39 1.538-1.354 1.538h-.273v.964h.376c1.613 0 2.372-.759 2.372-2.352v-1.524c0-1.094.376-1.456 1.49-1.456v-1.3c-1.114 0-1.49-.362-1.49-1.456V4.352C14.51 2.759 13.75 2 12.138 2h-.376v.964h.273c.964 0 1.354.444 1.354 1.538V6.3c0 .984.492 1.497 1.497 1.6M7.5 11.5V9.207l-1.621 1.621-.707-.707L6.792 8.5H4.5v-1h2.293L5.172 5.879l.707-.707L7.5 6.792V4.5h1v2.293l1.621-1.621.707.707L9.208 7.5H11.5v1H9.207l1.621 1.621-.707.707L8.5 9.208V11.5z"></path>
                                                </svg>Load config</button></div>
                                    </div>
                                    <p class="mb-0 ${GeneralCSS.DNONE}" data-oidis-bind="${this.feConfigContent}">Loading ...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" role="tab"><button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-1 .item-2" aria-expanded="false" aria-controls="accordion-1 .item-2">Back-end</button></h2>
                            <div class="accordion-collapse collapse item-2" role="tabpanel" data-bs-parent="#accordion-1">
                                <div class="accordion-body">
                                    <div class="row">
                                        <div class="col text-center"><button class="btn btn-primary" type="button" data-oidis-bind="${this.loadBEConfig}"><svg class="bi bi-braces-asterisk" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 1px;">
                                                    <path fill-rule="evenodd" d="M1.114 8.063V7.9c1.005-.102 1.497-.615 1.497-1.6V4.503c0-1.094.39-1.538 1.354-1.538h.273V2h-.376C2.25 2 1.49 2.759 1.49 4.352v1.524c0 1.094-.376 1.456-1.49 1.456v1.299c1.114 0 1.49.362 1.49 1.456v1.524c0 1.593.759 2.352 2.372 2.352h.376v-.964h-.273c-.964 0-1.354-.444-1.354-1.538V9.663c0-.984-.492-1.497-1.497-1.6ZM14.886 7.9v.164c-1.005.103-1.497.616-1.497 1.6v1.798c0 1.094-.39 1.538-1.354 1.538h-.273v.964h.376c1.613 0 2.372-.759 2.372-2.352v-1.524c0-1.094.376-1.456 1.49-1.456v-1.3c-1.114 0-1.49-.362-1.49-1.456V4.352C14.51 2.759 13.75 2 12.138 2h-.376v.964h.273c.964 0 1.354.444 1.354 1.538V6.3c0 .984.492 1.497 1.497 1.6M7.5 11.5V9.207l-1.621 1.621-.707-.707L6.792 8.5H4.5v-1h2.293L5.172 5.879l.707-.707L7.5 6.792V4.5h1v2.293l1.621-1.621.707.707L9.208 7.5H11.5v1H9.207l1.621 1.621-.707.707L8.5 9.208V11.5z"></path>
                                                </svg>Load config</button></div>
                                    </div>
                                    <p class="mb-0 ${GeneralCSS.DNONE}" data-oidis-bind="${this.beConfigContent}">Loading ...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col">
            <div class="row">
                <div class="col">
                    <h4>Push notifications</h4>
                    <div class="input-group" style="margin-bottom: 10px;"><span class="input-group-text" style="width: 160px;">Header message</span><span class="input-group-text" style="width: 35px;"></span>
                        <div class="dropdown" style="width: calc(100% - 175px);position: absolute;left: 160px;"><button class="btn btn-primary dropdown-toggle rounded-0" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 35px;"></button>
                            <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.notificationsSelector}"><button class="btn rounded-0 dropdown-item" type="button">loading ...</button></div>
                        </div><input class="form-control" type="text" style="position: relative;z-index: 1;" data-oidis-bind="${this.message}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col text-center"><button class="btn btn-primary" type="button" style="width: 100px;" data-oidis-bind="${this.push}"><svg class="bi bi-bell" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 1px;">
                            <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6"></path>
                        </svg>Push</button></div>
            </div>
        </div>
    </div>
    <div id="modal-1" class="modal fade" role="dialog" tabindex="-1" data-oidis-bind="${this.killDialog}">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" data-oidis-bind="${this.close}">
                    <h4 class="modal-title">Confirm application stop</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure to kill current application process? Automatic restart may depend on host settings.</p>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal" data-oidis-bind="${this.closeButton}">Close</button><button class="btn btn-danger" type="button" data-oidis-bind="${this.killConfirm}">Confirm kill</button></div>
            </div>
        </div>
    </div>
</section>`;
    }
}
