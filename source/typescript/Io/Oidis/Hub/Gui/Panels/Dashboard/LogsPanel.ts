/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { LogsTable } from "../../UserControls/LogsTable.js";
import { MonitorTable } from "../../UserControls/MonitorTable.js";

export class LogsPanel extends BasePanel {
    public readonly source : TextField;
    public readonly sourceSelector : Button;
    public readonly sourceMenu : GuiCommons;
    public readonly load : Button;
    public readonly date : TextField;
    public readonly ascending : CheckBox;
    public readonly info : CheckBox;
    public readonly infoCount : Label;
    public readonly warning : CheckBox;
    public readonly warningCount : Label;
    public readonly error : CheckBox;
    public readonly errorCount : Label;
    public readonly debug : CheckBox;
    public readonly debugCount : Label;
    public readonly searchText : TextField;
    public readonly searchButton : Button;
    public readonly dropArea : GuiCommons;
    public readonly logsContent : GuiCommons;
    public readonly monitorContent : GuiCommons;
    public readonly logsTable : LogsTable;
    public readonly monitoringTable : MonitorTable;
    public readonly clearMonitor : Button;
    public readonly generateLogs : Button;

    constructor() {
        super();

        this.source = new TextField();
        this.sourceSelector = new Button();
        this.sourceMenu = new GuiCommons();
        this.load = new Button();
        this.date = new TextField();
        this.ascending = new CheckBox();
        this.info = new CheckBox();
        this.infoCount = new Label();
        this.warning = new CheckBox();
        this.warningCount = new Label();
        this.error = new CheckBox();
        this.errorCount = new Label();
        this.debug = new CheckBox();
        this.debugCount = new Label();
        this.searchText = new TextField();
        this.searchButton = new Button();
        this.dropArea = new GuiCommons();
        this.logsContent = new GuiCommons();
        this.monitorContent = new GuiCommons();
        this.logsTable = new LogsTable();
        this.monitoringTable = new MonitorTable();
        this.clearMonitor = new Button();
        this.generateLogs = new Button();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row">
        <div class="col" style="margin-bottom: 15px;">
            <div class="input-group"><span class="input-group-text">Source</span>
                <div class="dropdown"><button class="btn btn-primary dropdown-toggle rounded-0" aria-expanded="false" data-bs-toggle="dropdown" type="button" data-oidis-bind="${this.sourceSelector}" style="text-align: left;width: 135px;">Type</button>
                    <div class="dropdown-menu" data-oidis-bind="${this.sourceMenu}"><a class="dropdown-item disabled">Loading ...</a></div>
                </div><input class="form-control" type="text" data-oidis-bind="${this.source}" readonly /><button class="btn btn-primary" type="button" data-oidis-bind="${this.load}">Load</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col"><input type="date" data-oidis-bind="${this.date}" style="margin-right: 10px;" readonly />
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.ascending}" checked /><label class="form-check-label" for="formCheck-5">Ascending</label></div>
        </div>
        <div class="col text-end">
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" checked data-oidis-bind="${this.info}" /><label class="form-check-label" for="formCheck-1">Info<span class="badge rounded-pill bg-danger" style="position: relative;top: -10px;left: 3px;" data-oidis-bind="${this.infoCount}"></span></label></div>
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" checked data-oidis-bind="${this.warning}" /><label class="form-check-label" for="formCheck-2">Warning<span class="badge rounded-pill bg-danger" style="position: relative;top: -10px;left: 3px;" data-oidis-bind="${this.warningCount}"></span></label></div>
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" checked data-oidis-bind="${this.error}" /><label class="form-check-label" for="formCheck-3">Error<span class="badge rounded-pill bg-danger" style="position: relative;top: -10px;left: 3px;" data-oidis-bind="${this.errorCount}"></span></label></div>
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" checked data-oidis-bind="${this.debug}" /><label class="form-check-label" for="formCheck-4">Debug<span class="badge rounded-pill bg-danger" style="position: relative;top: -10px;left: 3px;" data-oidis-bind="${this.debugCount}"></span></label></div>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="input-group input-group-sm" style="margin-bottom: 8px;"><input class="form-control" type="text" data-oidis-bind="${this.searchText}" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.searchButton}">Search</button></div>
                    <div class="row ${GeneralCSS.DNONE}" data-oidis-bind="${this.logsContent}">
                        <div class="col">
                            <div class="row" data-oidis-bind="${this.logsTable}"></div>
                        </div>
                    </div>
                    <div class="row ${GeneralCSS.DNONE}" data-oidis-bind="${this.monitorContent}">
                        <div class="col">
                            <div class="row">
                                <div class="col" style="text-align: center;"><button class="btn btn-danger" type="button" style="margin-bottom: 10px;" data-oidis-bind="${this.clearMonitor}"><svg class="bi bi-eraser" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 4px;">
                                            <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm2.121.707a1 1 0 0 0-1.414 0L4.16 7.547l5.293 5.293 4.633-4.633a1 1 0 0 0 0-1.414l-3.879-3.879zM8.746 13.547 3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"></path>
                                        </svg>Clean up monitoring data</button></div>
                            </div>
                            <div class="row" data-oidis-bind="${this.monitoringTable}"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col">
            <h3>Error reports validation</h3>
            <div class="card" style="margin-bottom: 10px;">
                <div class="card-body">
                    <p class="card-text"><svg class="bi bi-info-circle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"></path>
                            <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0"></path>
                        </svg>This validation can be used as integration test for all enabled underlying reporting systems: LogIt, SystemLogs, Crash reports emails, Sentry, etc.</p>
                </div>
            </div><button class="btn btn-primary" type="button" data-oidis-bind="${this.generateLogs}">Generate test logs</button>
        </div>
    </div>
    <div class="row g-0 justify-content-center align-items-center d-none" style="position: fixed;top: 0;left: 0;width: 100%;height: 100%;z-index: 10;background: rgba(164,164,164,0.49);" data-oidis-bind="${this.dropArea}">
        <div class="col">
            <div class="card" style="margin: 30px;">
                <div class="card-body text-center">
                    <h4 class="card-title" style="margin-bottom: 25px;">Save and view</h4><i class="fa fa-save" style="font-size: 70px;color: rgb(127,130,133);margin-bottom: 15px;"></i><i class="fa fa-plus" style="margin-right: 30px;margin-left: 30px;font-size: 40px;color: rgb(127,130,133);position: relative;top: -5px;"></i><i class="fa fa-list" style="font-size: 70px;color: rgb(127,130,133);"></i>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card" style="margin: 30px;">
                <div class="card-body text-center">
                    <h4 class="card-title" style="margin-bottom: 25px;">Just view</h4><i class="fa fa-list" style="font-size: 70px;color: rgb(127,130,133);margin-bottom: 16px;"></i>
                </div>
            </div>
        </div>
    </div>
</section>
`;
    }
}
