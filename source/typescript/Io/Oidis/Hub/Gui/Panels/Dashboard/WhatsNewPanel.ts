/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";

export class WhatsNewPanel extends BasePanel {
    public readonly date : Label;
    public readonly newsList : GuiCommons;
    public readonly adminsContent : GuiCommons;
    public readonly adminsList : GuiCommons;
    public readonly issuesList : GuiCommons;
    public readonly issuesContent : GuiCommons;

    constructor() {
        super();
        this.date = new Label();
        this.newsList = new GuiCommons();
        this.adminsContent = new GuiCommons();
        this.adminsList = new GuiCommons();
        this.issuesList = new GuiCommons();
        this.issuesContent = new GuiCommons();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row">
        <div class="col">
            <p><svg class="bi bi-calendar3" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 2px;">
                    <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"></path>
                    <path d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                </svg><span data-oidis-bind="${this.date}">xx.xx.xxxx</span></p>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h3 class="card-title"><svg class="bi bi-star" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 15px;margin-bottom: 3px;">
                    <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z"></path>
                </svg>News</h3>
            <h6 class="text-muted card-subtitle mb-2">Here you can find list of new features and bug fixes for select version. If you are looking for historical records you can choose specific version from menu at header of this window.</h6>
            <p class="card-text" data-oidis-bind="${this.newsList}">Loading ...</p>
        </div>
    </div>
    <div class="card ${GeneralCSS.DNONE}" data-oidis-bind="${this.adminsContent}" style="border-color: var(--bs-blue);margin-top: 15px;">
        <div class="card-body">
            <h3 class="card-title"><svg class="bi bi-award" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 15px;margin-bottom: 3px;">
                    <path d="M9.669.864 8 0 6.331.864l-1.858.282-.842 1.68-1.337 1.32L2.6 6l-.306 1.854 1.337 1.32.842 1.68 1.858.282L8 12l1.669-.864 1.858-.282.842-1.68 1.337-1.32L13.4 6l.306-1.854-1.337-1.32-.842-1.68L9.669.864zm1.196 1.193.684 1.365 1.086 1.072L12.387 6l.248 1.506-1.086 1.072-.684 1.365-1.51.229L8 10.874l-1.355-.702-1.51-.229-.684-1.365-1.086-1.072L3.614 6l-.25-1.506 1.087-1.072.684-1.365 1.51-.229L8 1.126l1.356.702 1.509.229z"></path>
                    <path d="M4 11.794V16l4-1 4 1v-4.206l-2.018.306L8 13.126 6.018 12.1 4 11.794z"></path>
                </svg>Updates for admins</h3>
            <h6 class="text-muted card-subtitle mb-2">Here you can find list of new features and bug fixes which are available only for users with admin rights.</h6>
            <p class="card-text" data-oidis-bind="${this.adminsList}">Loading ...</p>
        </div>
    </div>
    <div class="card ${GeneralCSS.DNONE}" style="border-color: var(--bs-orange);margin-top: 15px;" data-oidis-bind="${this.issuesContent}">
        <div class="card-body">
            <h3 class="card-title"><svg class="bi bi-exclamation-triangle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 15px;margin-bottom: 3px;">
                    <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"></path>
                    <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"></path>
                </svg>Bugs</h3>
            <h6 class="text-muted card-subtitle mb-2">Here you can find list of known bugs and system limitations. You can find also workarounds or plans for mitigation.</h6>
            <p class="card-text" data-oidis-bind="${this.issuesList}">Loading ...</p>
        </div>
    </div>
    <div class="row" style="height: 25px;"></div>
</section>`;
    }
}
