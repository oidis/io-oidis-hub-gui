/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { EventLoopTable } from "../../UserControls/EventLoopTable.js";

export class EmailPanel extends BasePanel {
    public readonly serviceType : TextField;
    public readonly user : TextField;
    public readonly pass : TextField;
    public readonly location : TextField;
    public readonly port : TextField;
    public readonly secure : CheckBox;
    public readonly from : TextField;
    public readonly to : TextField;
    public readonly subject : TextField;
    public readonly body : TextField;
    public readonly send : Button;
    public readonly adminsList : TextField;
    public readonly blackList : TextField;
    public readonly fetchTick : TextField;
    public readonly inactiveTable : EventLoopTable;

    constructor() {
        super();

        this.serviceType = new TextField();
        this.user = new TextField();
        this.pass = new TextField();
        this.location = new TextField();
        this.port = new TextField();
        this.secure = new CheckBox();
        this.from = new TextField();
        this.to = new TextField();
        this.subject = new TextField();
        this.body = new TextField();
        this.send = new Button();
        this.adminsList = new TextField();
        this.blackList = new TextField();
        this.fetchTick = new TextField();
        this.inactiveTable = new EventLoopTable();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row">
        <div class="col">
            <h3>Configuration</h3>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">Service Type</span><input class="form-control" type="text" data-oidis-bind="${this.serviceType}" /></div>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">User name</span><input class="form-control" type="text" data-oidis-bind="${this.user}" /></div>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">Password</span><input class="form-control" type="password" data-oidis-bind="${this.pass}" autocomplete="off" /></div>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">Location</span><input class="form-control" type="text" data-oidis-bind="${this.location}" /></div>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">Port</span><input class="form-control" type="text" data-oidis-bind="${this.port}" /></div>
            <div class="form-check form-switch"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.secure}" /><label class="form-check-label">Secure</label></div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3 style="margin-top: 15px;">Send email</h3>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">From</span><input class="form-control" type="text" data-oidis-bind="${this.from}" /></div>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">To</span><input class="form-control" type="text" data-oidis-bind="${this.to}" /></div>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">Subject</span><input class="form-control" type="text" data-oidis-bind="${this.subject}" /></div>
            <p style="margin-bottom: 6px;">Body</p><textarea class="form-control" style="width: 100%;margin-bottom: 5px;" data-oidis-bind="${this.body}"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col" style="text-align: center;margin-top: 10px;"><button class="btn btn-primary" type="button" data-oidis-bind="${this.send}">Send</button></div>
    </div>
    <div class="row">
        <div class="col">
            <h3 style="margin-top: 15px;">Other settings</h3>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">Admins list</span><input class="form-control" type="text" data-oidis-bind="${this.adminsList}" readonly disabled /></div>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">Black list</span><input class="form-control" type="text" data-oidis-bind="${this.blackList}" readonly disabled /></div>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">Loop tick</span><input class="form-control" type="text" data-oidis-bind="${this.fetchTick}" readonly disabled /><span class="input-group-text">ms</span></div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3 style="margin-top: 15px;">Inactive records</h3>
            <div data-oidis-bind="${this.inactiveTable}"></div>
        </div>
    </div>
</section>`;
    }
}
