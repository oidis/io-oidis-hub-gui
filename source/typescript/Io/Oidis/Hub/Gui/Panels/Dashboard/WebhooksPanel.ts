/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { CountDown } from "../../UserControls/CountDown.js";
import { EventLoopTable } from "../../UserControls/EventLoopTable.js";
import { WebhookAdapterTable } from "../../UserControls/WebhookAdapterTable.js";

export class WebhooksPanel extends BasePanel {
    public readonly fetchTick : TextField;
    public readonly bucketSize : TextField;
    public readonly serviceType : TextField;
    public readonly location : TextField;
    public readonly stop : Button;
    public readonly start : Button;
    public readonly restart : Button;
    public readonly status : Button;
    public readonly hooks : EventLoopTable;
    public readonly adapters : WebhookAdapterTable;
    public readonly masterStop : Button;
    public readonly disableMasterStop : Button;
    public readonly reloadTables : Button;
    public readonly removeOldRecords : Button;
    public readonly refresh : CheckBox;
    public readonly refreshRate : TextField;
    public readonly countDown : CountDown;

    constructor() {
        super();

        this.fetchTick = new TextField();
        this.bucketSize = new TextField();
        this.serviceType = new TextField();
        this.location = new TextField();
        this.stop = new Button();
        this.start = new Button();
        this.restart = new Button();
        this.status = new Button();
        this.hooks = new EventLoopTable();
        this.adapters = new WebhookAdapterTable();
        this.masterStop = new Button();
        this.disableMasterStop = new Button();
        this.reloadTables = new Button();
        this.removeOldRecords = new Button();
        this.refresh = new CheckBox();
        this.refreshRate = new TextField();
        this.countDown = new CountDown();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row">
        <div class="col">
            <h3>Configuration</h3>
            <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 120px;">Bucket size</span><input class="form-control" type="text" data-oidis-bind="${this.bucketSize}" readonly /></div>
            <div class="input-group" style="margin-bottom: 10px;"><span class="input-group-text" style="width: 120px;">Loop tick</span><input class="form-control" type="text" style="text-align: right;" data-oidis-bind="${this.fetchTick}" /><span class="input-group-text">ms</span></div>
            <div class="input-group" style="margin-bottom: 10px;"><span class="input-group-text" style="width: 120px;">Status</span><button class="btn btn-warning disabled" type="button" style="width: calc(100% - 120px);" data-oidis-bind="${this.status}" disabled>IDLE</button></div>
        </div>
    </div>
    <div class="row">
        <div class="col text-center"><button class="btn btn-danger" type="button" style="width: 100px;" data-oidis-bind="${this.stop}">Stop</button><button class="btn btn-success ${GeneralCSS.DNONE}" type="button" style="width: 100px;" data-oidis-bind="${this.start}">Start</button><button class="btn btn-warning" type="button" style="margin-left: 10px;width: 100px;" data-oidis-bind="${this.restart}">Restart</button></div>
    </div>
    <div class="row">
        <div class="col text-center" style="margin-top: 10px;"><button class="btn btn-danger" type="button" data-oidis-bind="${this.masterStop}">Master stop</button><button class="btn btn-primary ${GeneralCSS.DNONE}" type="button" style="margin-left: 10px;" data-oidis-bind="${this.disableMasterStop}">Disable master stop</button></div>
    </div>
    <div class="row" style="margin-top: 10px;">
        <div class="col"><button class="btn btn-primary" type="button" style="margin-right: 10px;" data-oidis-bind="${this.reloadTables}">Reload tables</button><button class="btn btn-primary" type="button" data-oidis-bind="${this.removeOldRecords}">Remove all inactive older than 3 mounts</button></div>
        <div class="col-xl-2 align-self-center" style="text-align: right;">
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" checked data-oidis-bind="${this.refresh}" /><label class="form-check-label" for="formCheck-1">Auto refresh</label></div>
        </div>
        <div class="col-xl-1 text-start align-self-center">
            <div class="row">
                <div class="col-auto" style="padding: 0;">
                    <div data-oidis-bind="${this.countDown}"></div>
                </div>
                <div class="col">
                    <div class="input-group input-group-sm"><input class="form-control" type="number" data-oidis-bind="${this.refreshRate}" min="1" max="3600" step="5" value="10" /><span class="input-group-text">s</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3 style="margin-top: 15px;">Webhook Adapters</h3>
            <div data-oidis-bind="${this.adapters}"></div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3 style="margin-top: 15px;">Webhooks</h3>
            <div data-oidis-bind="${this.hooks}"></div>
        </div>
    </div>
</section>`;
    }
}
