/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";

export class FooterPanel extends BasePanel {
    public readonly hubInfo : Label;

    constructor() {
        super();

        this.hubInfo = new Label();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row mt-auto" style="background: rgb(180,181,181);">
        <div class="col" style="color: var(--bs-gray-dark);">
            <p class="text-center" style="margin-top: 9px;" data-oidis-bind="${this.hubInfo}">Copyright ${StringUtils.YearFrom(2019)} Oidis</p>
        </div>
    </div>
</section>`;
    }
}
