/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";

export class DuplicityCheckPanel extends BasePanel {
    public readonly emailCheck : Button;
    public readonly reportContent : GuiCommons;

    constructor() {
        super();

        this.emailCheck = new Button();
        this.reportContent = new GuiCommons();
    }

    protected innerHtml() : string {
        return `
<section data-oidis-bind="${this}"><button class="btn btn-primary" type="button" data-oidis-bind="${this.emailCheck}" style="margin-bottom: 10px;">Check emails duplicity</button>
    <div class="card">
        <div class="card-body">
            <p class="card-text ReportLog" data-oidis-bind="${this.reportContent}">Report not available yet</p>
        </div>
    </div>
</section>
`;
    }
}
