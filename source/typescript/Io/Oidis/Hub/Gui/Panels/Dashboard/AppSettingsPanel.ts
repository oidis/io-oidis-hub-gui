/*! ******************************************************************************************************** *
 *
 * Copyright 2023 NXP
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";

export class AppSettingsPanel extends BasePanel {
    public readonly mailEngine : CheckBox;
    public readonly adminsList : TextField;
    public readonly blackList : TextField;
    public readonly accessMethods : TextField;
    public readonly groupAccess : TextField;
    public readonly saveSettings : Button;
    public readonly resetSettings : Button;

    constructor() {
        super();

        this.mailEngine = new CheckBox();
        this.adminsList = new TextField();
        this.blackList = new TextField();
        this.accessMethods = new TextField();
        this.groupAccess = new TextField();
        this.saveSettings = new Button();
        this.resetSettings = new Button();
    }

    protected innerHtml() : string {
        return `
<section data-oidis-bind="${this}">
    <div class="row">
        <div class="col">
            <h3>Features</h3>
            ${this.getFeatures()}
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col">
                    <h3>Settings</h3>
                    ${this.getSettings()}
                </div>
            </div>
            <div class="row">
                <div class="col" style="text-align: center;margin-top: 10px;">
                    <button class="btn btn-danger" type="button" style="margin-right: 15px;" data-oidis-bind="${this.resetSettings}">Reload from server config</button>
                    <button class="btn btn-primary" type="button" data-oidis-bind="${this.saveSettings}">Save settings</button>
                </div>
            </div>
        </div>
    </div>
</section>`;
    }

    protected getFeatures() : string {
        return `<div class="form-check form-switch"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.mailEngine}" /><label class="form-check-label">Mail Engine</label></div>`;
    }

    protected getSettings() : string {
        return `
        <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 200px;">Admins mail list</span><input class="form-control" type="text" data-oidis-bind="${this.adminsList}" /></div>
        <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 200px;">Mail blacklist</span><input class="form-control" type="text" data-oidis-bind="${this.blackList}" /></div>
        <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 200px;">Default access methods</span><input class="form-control" type="text" data-oidis-bind="${this.accessMethods}" /></div>
        <div class="input-group" style="margin-bottom: 5px;"><span class="input-group-text" style="width: 200px;">Default group access</span><input class="form-control" type="text" data-oidis-bind="${this.groupAccess}" /></div>`;
    }
}
