/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { FeedbackType } from "../../Enums/FeedbackType.js";

export class FeedbackPanel extends BasePanel {
    public readonly catSelector : Button;
    public readonly email : TextField;
    public readonly uuid : TextField;
    public readonly body : TextField;
    public readonly send : Button;
    public readonly success : GuiCommons;
    public readonly uuidContent : GuiCommons;
    protected localization : IFeedbackPanelLocalization;
    protected readonly internalNote : GuiCommons;
    protected readonly catMenu : GuiCommons;
    private readonly categories : IFeedbackCategory[];

    constructor() {
        super();

        this.catSelector = new Button();
        this.catMenu = new GuiCommons();
        this.email = new TextField();
        this.uuid = new TextField();
        this.body = new TextField();
        this.send = new Button();
        this.success = new GuiCommons();
        this.uuidContent = new GuiCommons();
        this.internalNote = new GuiCommons();

        this.categories = [];
        this.getCategoriesConfiguration().forEach(($config : IFeedbackCategory) : void => {
            this.categories.push(JsonUtils.Extend(<IFeedbackCategory>{
                value       : FeedbackType.General,
                enabled     : true,
                public      : true,
                default     : false,
                uuidRequired: true,
                onSelect    : null,
                instance    : null
            }, $config));
        });
        this.localization = {
            afterSend: "Request send successfully. We will contact you as soon as possible. Thanks.",
            bodyHint : "Please specify user ID, if the request is about user permissions it will speed up our response. Thanks",
            uuidHint : "Specify unique identificator connected with your question (package name, User ID, log date, ...)"
        };
    }

    public RestoreForm($forPublic : boolean = false) : void {
        this.catSelector.Content("Choose category");
        this.catSelector.Value(null);
        this.email.Value("");
        this.uuid.Value("");
        this.body.Value("");
        this.success.Visible(false);
        this.send.Visible(true);
        ValidationBinding.RestoreValidMark(this.email);
        ValidationBinding.RestoreValidMark(this.body);
        this.uuidContent.Visible(false);
        this.internalNote.Visible(!$forPublic);
        this.getCategories().forEach(($config : IFeedbackCategory) : void => {
            if (!ObjectValidator.IsEmptyOrNull($config.instance)) {
                $config.instance.Visible($config.public || !$config.public && !$forPublic);
            }
            if ($config.default) {
                this.SelectCategory($config);
            }
        });
    }

    public getCategories() : IFeedbackCategory[] {
        return this.categories;
    }

    public SelectCategory($config : IFeedbackCategory) : void {
        this.catSelector.Content($config.text);
        this.catSelector.Value($config.value);
        this.uuidContent.Visible($config.uuidRequired);
        if (!ObjectValidator.IsEmptyOrNull($config.onSelect)) {
            $config.onSelect();
        }
    }

    protected getCategoriesConfiguration() : IFeedbackCategory[] {
        return [
            {
                text   : "Registry",
                value  : FeedbackType.Registry,
                default: true
            },
            {
                text : "Agents",
                value: FeedbackType.Agents
            },
            {
                text  : "Logs and reports",
                value : FeedbackType.Logs,
                public: false
            },
            {
                text  : "Emails",
                value : FeedbackType.Email,
                public: false
            },
            {
                text  : "Permissions",
                value : FeedbackType.Permissions,
                public: false
            },
            {
                text        : "Other or general question",
                value       : FeedbackType.General,
                uuidRequired: false
            }
        ];
    }

    protected innerCode() : string {
        this.getEvents().setOnLoad(() : void => {
            this.catMenu.Clear();
            this.getCategories().forEach(($config : IFeedbackCategory) : void => {
                if ($config.enabled) {
                    const item : DropDownListItem = new DropDownListItem();
                    item.Text($config.text);
                    item.Value($config.value);
                    item.getEvents().setOnClick(() : void => {
                        this.SelectCategory($config);
                    });
                    $config.instance = item;
                    this.catMenu.AddChild(item);
                }
            });
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<section>
    <h1>Feedback form</h1>
    <div class="card" style="margin-bottom: 15px;">
        <div class="card-body">
            <p class="card-text">This form is the fastest way how to resolve your requests. <span class="${GeneralCSS.DNONE}" data-oidis-bind="${this.internalNote}">Please read <a href="#/faq">frequently asked questions</a> section before sending your request.</span></p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="input-group" style="margin-bottom: 10px;"><span class="input-group-text" style="width: 100px;">Category</span>
                <div class="dropdown" style="width: calc(100% - 100px);"><button class="btn btn-outline-primary dropdown-toggle rounded-0 rounded-end" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;" data-oidis-bind="${this.catSelector}">Choose category</button>
                    <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.catMenu}"><span style="margin-left: 10px;">Loading ...</span></div>
                </div>
            </div>
            <div class="input-group" style="margin-bottom: 10px;"><span class="input-group-text" style="width: 100px;">E-mail</span><input class="form-control" type="text" data-oidis-bind="${this.email}" placeholder="Specify email to which we should reply" /></div>
            <div class="input-group ${GeneralCSS.DNONE}" style="margin-bottom: 10px;" data-oidis-bind="${this.uuidContent}"><span class="input-group-text" style="width: 100px;">UUID</span><input class="form-control" type="text" data-oidis-bind="${this.uuid}" placeholder="${this.localization.uuidHint}" /></div>
        </div>
    </div>
    <p style="text-decoration: underline;">Request</p><textarea style="width: 100%;min-height: 150px;border-radius: 4px;border: 1px solid rgb(206,212,218);margin-bottom: 15px;" data-oidis-bind="${this.body}" placeholder="${this.localization.bodyHint}"></textarea>
    <div class="row">
        <div class="col" style="text-align: center;"><button class="btn btn-primary" type="button" data-oidis-bind="${this.send}"><svg class="bi bi-send" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                    <path fill-rule="evenodd" d="M15.854.146a.5.5 0 0 1 .11.54l-5.819 14.547a.75.75 0 0 1-1.329.124l-3.178-4.995L.643 7.184a.75.75 0 0 1 .124-1.33L15.314.037a.5.5 0 0 1 .54.11ZM6.636 10.07l2.761 4.338L14.13 2.576 6.636 10.07Zm6.787-8.201L1.591 6.602l4.339 2.76 7.494-7.493Z"></path>
                </svg>Send</button></div>
    </div>
    <div class="alert alert-success ${GeneralCSS.DNONE}" role="alert" style="height: 38px;padding: 6px;text-align: center;" data-oidis-bind="${this.success}"><span>${this.localization.afterSend}</span><span><br /></span></div>
</section>`;
    }
}

export interface IFeedbackCategory {
    text : string;
    value? : number;
    public? : boolean;
    default? : boolean;
    enabled? : boolean;
    onSelect? : () => void;
    instance? : DropDownListItem;
    uuidRequired? : boolean;
}

export interface IFeedbackPanelLocalization {
    uuidHint? : string;
    bodyHint? : string;
    afterSend? : string;
}

// generated-code-start
/* eslint-disable */
export const IFeedbackCategory = globalThis.RegisterInterface(["text", "value", "public", "default", "enabled", "onSelect", "instance", "uuidRequired"]);
export const IFeedbackPanelLocalization = globalThis.RegisterInterface(["uuidHint", "bodyHint", "afterSend"]);
/* eslint-enable */
// generated-code-end
