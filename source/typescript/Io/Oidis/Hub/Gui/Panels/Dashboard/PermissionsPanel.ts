/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { Dialog } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Dialog.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { AutocompleteSearch } from "../../UserControls/AutocompleteSearch.js";
import { GroupsTable } from "../../UserControls/GroupsTable.js";
import { UsersTable } from "../../UserControls/UsersTable.js";

export class PermissionsPanel extends BasePanel {
    public readonly searchUser : AutocompleteSearch;
    public readonly usersTable : UsersTable;
    public readonly groupsTable : GroupsTable;
    public readonly newUserName : TextField;
    public readonly addNewUser : Button;
    public readonly newGroupName : TextField;
    public readonly addNewGroup : Button;
    public readonly userDialog : Dialog;
    public readonly userDialogClose : Button;
    public readonly userDialogCloseButton : Button;
    public readonly userDeleteAction : Button;
    public readonly groupDialog : Dialog;
    public readonly groupDialogClose : Button;
    public readonly groupDialogCloseButton : Button;
    public readonly groupDeleteAction : Button;
    public readonly withDeactivatedUsers : CheckBox;
    public readonly withDeletedUsers : CheckBox;
    public readonly withDeletedGroups : CheckBox;
    public readonly withHiddenGroups : CheckBox;
    public readonly resetCache : Button;
    public readonly usersTab : GuiCommons;
    public readonly groupsTab : GuiCommons;
    public readonly syncWarningHolder : GuiCommons;
    public readonly syncSysGroups : Button;
    public readonly withUniqueUsersPerms : CheckBox;
    public readonly withEmptyGroups : CheckBox;
    public readonly withDefaultPerms : CheckBox;
    public readonly resetSysGroups : Button;
    public readonly cleanupGroupsPerms : Button;
    public readonly exportPermsReport : Button;
    public readonly exportGroupsPerms : Button;
    public readonly importGroupsPerms : Button;
    public readonly deleteRedundantGroups : Button;
    public readonly cleanupDefaultPerms : Button;
    public readonly groupRemoveDialog : Dialog;
    public readonly groupRemoveDialogClose : Button;
    public readonly groupRemoveDialogCloseButton : Button;
    public readonly groupRemoveAction : Button;
    public readonly onlySysGroups : CheckBox;
    public readonly withAsterix : CheckBox;
    public readonly groupsSearchField : TextField;
    public readonly groupsSearch : Button;

    constructor() {
        super();

        this.searchUser = new AutocompleteSearch();
        this.usersTable = new UsersTable();
        this.groupsTable = new GroupsTable();
        this.newUserName = new TextField();
        this.addNewUser = new Button();
        this.newGroupName = new TextField();
        this.addNewGroup = new Button();
        this.userDialog = new Dialog();
        this.userDialogClose = new Button();
        this.userDialogCloseButton = new Button();
        this.userDeleteAction = new Button();
        this.groupDialog = new Dialog();
        this.groupDialogClose = new Button();
        this.groupDialogCloseButton = new Button();
        this.groupDeleteAction = new Button();
        this.withDeactivatedUsers = new CheckBox();
        this.withDeletedUsers = new CheckBox();
        this.withDeletedGroups = new CheckBox();
        this.withHiddenGroups = new CheckBox();
        this.resetCache = new Button();
        this.usersTab = new GuiCommons();
        this.groupsTab = new GuiCommons();
        this.syncWarningHolder = new GuiCommons();
        this.syncSysGroups = new Button();
        this.withUniqueUsersPerms = new CheckBox();
        this.withEmptyGroups = new CheckBox();
        this.withDefaultPerms = new CheckBox();
        this.resetSysGroups = new Button();
        this.cleanupGroupsPerms = new Button();
        this.exportPermsReport = new Button();
        this.exportGroupsPerms = new Button();
        this.importGroupsPerms = new Button();
        this.deleteRedundantGroups = new Button();
        this.cleanupDefaultPerms = new Button();
        this.groupRemoveDialog = new Dialog();
        this.groupRemoveDialogClose = new Button();
        this.groupRemoveDialogCloseButton = new Button();
        this.groupRemoveAction = new Button();
        this.onlySysGroups = new CheckBox();
        this.withAsterix = new CheckBox();
        this.groupsSearchField = new TextField();
        this.groupsSearch = new Button();
    }

    public SelectedTab($types? : PermissionsTabType[]) : PermissionsTabType[] {
        if (ObjectValidator.IsSet($types)) {
            try {
                if (!(<any>this).completed) {
                    this.getEvents().setOnComplete(() : void => {
                        this.SelectedTab($types);
                    });
                } else {
                    $types.forEach(($type : PermissionsTabType) : void => {
                        switch ($type) {
                        case PermissionsTabType.Users:
                            this.usersTab.InstanceOwner().click();
                            break;
                        case PermissionsTabType.Groups:
                            this.groupsTab.InstanceOwner().click();
                            break;
                        default:
                            LogIt.Warning("Instance {0} has not been detected as permissions tab.", PermissionsTabType[$type]);
                            break;
                        }
                    });
                }
            } catch (ex) {
                LogIt.Error(ex);
            }
        }
        const selected : PermissionsTabType[] = [];
        const attribute : string = "aria-expanded";
        if (StringUtils.ToBoolean(this.usersTab.InstanceOwner().getAttribute(attribute))) {
            selected.push(PermissionsTabType.Users);
        }
        if (StringUtils.ToBoolean(this.groupsTab.InstanceOwner().getAttribute(attribute))) {
            selected.push(PermissionsTabType.Groups);
        }
        return selected;
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row g-0 ${GeneralCSS.DNONE}" style="margin-bottom: 10px;background: var(--bs-yellow);border-radius: 3px;margin-left: auto;margin-right: auto;border: 1px solid var(--bs-orange);" data-oidis-bind="${this.syncWarningHolder}">
        <div class="col align-self-center"><span class="text-danger"><svg class="bi bi-exclamation-triangle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 15px;margin-left: 16px;margin-bottom: 4px;">
                    <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"></path>
                    <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"></path>
                </svg>Not all of system groups are synced with database</span></div>
        <div class="col-xl-3" style="text-align: right;"><button class="btn btn-outline-dark" type="button" style="margin-right: 15px;margin-top: 5px;margin-bottom: 5px;" data-oidis-bind="${this.syncSysGroups}">Sync now</button></div>
    </div>
    <div class="row">
        <div class="col">
            <div id="accordion-2" class="accordion" role="tablist">
                <div class="accordion-item">
                    <h2 class="accordion-header" role="tab"><button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-2 .item-1" aria-expanded="false" aria-controls="accordion-2 .item-1" data-oidis-bind="${this.usersTab}">Users</button></h2>
                    <div class="accordion-collapse collapse item-1" role="tabpanel">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col" data-oidis-bind="${this.searchUser}"></div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.withUniqueUsersPerms}" /><label class="form-check-label">Unique user&#39;s permissions</label></div>
                                    <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.withDeactivatedUsers}" /><label class="form-check-label">With deactivated</label></div>
                                    <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.withDeletedUsers}" /><label class="form-check-label">With deleted</label></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col text-end"><button class="btn btn-danger" type="button" data-oidis-bind="${this.resetCache}" style="width: 235px;"><svg class="bi bi-eraser" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                                            <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm2.121.707a1 1 0 0 0-1.414 0L4.16 7.547l5.293 5.293 4.633-4.633a1 1 0 0 0 0-1.414l-3.879-3.879zM8.746 13.547 3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"></path>
                                        </svg>Reset permissions cache</button></div>
                            </div>
                            <div data-oidis-bind="${this.usersTable}"></div>
                            <div class="input-group"><span class="input-group-text">User name</span><input class="form-control" type="text" data-oidis-bind="${this.newUserName}" autocomplete="off" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.addNewUser}"><svg class="bi bi-plus-circle-fill" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"></path>
                                    </svg></button></div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" role="tab"><button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-2 .item-2" aria-expanded="false" aria-controls="accordion-2 .item-2" data-oidis-bind="${this.groupsTab}">Groups</button></h2>
                    <div class="accordion-collapse collapse item-2" role="tabpanel">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col">
                                    <div class="input-group"><input class="form-control" type="text" placeholder="Authorized methods search ..." data-oidis-bind="${this.groupsSearchField}" autocomplete="off" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.groupsSearch}">Search <svg class="bi bi-search" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-left: 5px;">
                                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
                                            </svg></button></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.onlySysGroups}" /><label class="form-check-label">Only System groups</label></div>
                                    <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.withAsterix}" /><label class="form-check-label">Only with &quot;*&quot;</label></div>
                                    <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.withHiddenGroups}" /><label class="form-check-label">With hidden</label></div>
                                    <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.withDeletedGroups}" /><label class="form-check-label">With deleted</label></div>
                                    <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.withEmptyGroups}" /><label class="form-check-label">With empty</label></div>
                                    <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.withDefaultPerms}" /><label class="form-check-label">Show default methods</label></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col text-end">
                                    <div class="dropdown"><button class="btn btn-outline-primary dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="text-align: left;width: 170px;"><svg class="bi bi-database-up" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                                                <path d="M12.5 16a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7Zm.354-5.854 1.5 1.5a.5.5 0 0 1-.708.708L13 11.707V14.5a.5.5 0 0 1-1 0v-2.793l-.646.647a.5.5 0 0 1-.708-.708l1.5-1.5a.5.5 0 0 1 .708 0Z"></path>
                                                <path d="M12.096 6.223A4.92 4.92 0 0 0 13 5.698V7c0 .289-.213.654-.753 1.007a4.493 4.493 0 0 1 1.753.25V4c0-1.007-.875-1.755-1.904-2.223C11.022 1.289 9.573 1 8 1s-3.022.289-4.096.777C2.875 2.245 2 2.993 2 4v9c0 1.007.875 1.755 1.904 2.223C4.978 15.71 6.427 16 8 16c.536 0 1.058-.034 1.555-.097a4.525 4.525 0 0 1-.813-.927C8.5 14.992 8.252 15 8 15c-1.464 0-2.766-.27-3.682-.687C3.356 13.875 3 13.373 3 13v-1.302c.271.202.58.378.904.525C4.978 12.71 6.427 13 8 13h.027a4.552 4.552 0 0 1 0-1H8c-1.464 0-2.766-.27-3.682-.687C3.356 10.875 3 10.373 3 10V8.698c.271.202.58.378.904.525C4.978 9.71 6.427 10 8 10c.262 0 .52-.008.774-.024a4.525 4.525 0 0 1 1.102-1.132C9.298 8.944 8.666 9 8 9c-1.464 0-2.766-.27-3.682-.687C3.356 7.875 3 7.373 3 7V5.698c.271.202.58.378.904.525C4.978 6.711 6.427 7 8 7s3.022-.289 4.096-.777ZM3 4c0-.374.356-.875 1.318-1.313C5.234 2.271 6.536 2 8 2s2.766.27 3.682.687C12.644 3.125 13 3.627 13 4c0 .374-.356.875-1.318 1.313C10.766 5.729 9.464 6 8 6s-2.766-.27-3.682-.687C3.356 4.875 3 4.373 3 4Z"></path>
                                            </svg>Import/Export</button>
                                        <div class="dropdown-menu"><button class="btn dropdown-item" type="button" data-oidis-bind="${this.exportPermsReport}">Export permissions report</button><button class="btn dropdown-item" type="button" data-oidis-bind="${this.exportGroupsPerms}">Export all methods</button><button class="btn dropdown-item" type="button" data-oidis-bind="${this.importGroupsPerms}">Import methods</button></div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="dropdown"><button class="btn btn-danger dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 235px;text-align: left;"><svg class="bi bi-eraser" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                                                <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm2.121.707a1 1 0 0 0-1.414 0L4.16 7.547l5.293 5.293 4.633-4.633a1 1 0 0 0 0-1.414l-3.879-3.879zM8.746 13.547 3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"></path>
                                            </svg>Clean up</button>
                                        <div class="dropdown-menu"><button class="btn dropdown-item" type="button" data-oidis-bind="${this.resetSysGroups}">Synchronize system groups</button><button class="btn dropdown-item" type="button" data-oidis-bind="${this.cleanupGroupsPerms}">Clean up all default groups</button><button class="btn dropdown-item" type="button" data-oidis-bind="${this.cleanupDefaultPerms}">Clean up all default methods</button><button class="btn dropdown-item" type="button" data-oidis-bind="${this.deleteRedundantGroups}">Delete redundant groups</button></div>
                                    </div>
                                </div>
                            </div>
                            <div data-oidis-bind="${this.groupsTable}"></div>
                            <div class="input-group"><span class="input-group-text">Group name</span><input class="form-control" type="text" data-oidis-bind="${this.newGroupName}" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.addNewGroup}"><svg class="bi bi-plus-circle-fill" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"></path>
                                    </svg></button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="modal fade" role="dialog" tabindex="-1" data-oidis-bind="${this.userDialog}">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header" data-oidis-bind="${this.userDialogClose}">
                            <h4 class="modal-title">User delete confirmation</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <p>The user will be permanently deleted from DB. Are you sure?</p>
                        </div>
                        <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal" data-oidis-bind="${this.userDialogCloseButton}">Storno</button><button class="btn btn-danger" type="button" data-oidis-bind="${this.userDeleteAction}">Delete</button></div>
                    </div>
                </div>
            </div>
            <div class="modal fade" role="dialog" tabindex="-1" data-oidis-bind="${this.groupDialog}">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header" data-oidis-bind="${this.groupDialogClose}">
                            <h4 class="modal-title">Group delete confirmation</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <p>The group will be marked as Deleted in DB. Are you sure?</p>
                        </div>
                        <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal" data-oidis-bind="${this.groupDialogCloseButton}">Storno</button><button class="btn btn-danger" type="button" data-oidis-bind="${this.groupDeleteAction}">Delete</button></div>
                    </div>
                </div>
            </div>
            <div class="modal fade" role="dialog" tabindex="-1" data-oidis-bind="${this.groupRemoveDialog}">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header" data-oidis-bind="${this.groupRemoveDialogClose}">
                            <h4 class="modal-title">Group remove confirmation</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <p>The group will be permanently removed from DB! This action can not be undone and can lead to system malfunction.<br /><br />Are you sure, did you do all check in the code?<br /><br />NOTE: <br />This group has been marked as redundant, so it should be safe to remove, but all checks are highly recommended.</p>
                        </div>
                        <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal" data-oidis-bind="${this.groupRemoveDialogCloseButton}">Storno</button><button class="btn btn-danger" type="button" data-oidis-bind="${this.groupRemoveAction}">Delete</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
`;
    }
}

export enum PermissionsTabType {
    Users,
    Groups
}
