/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { MenuItem } from "../../UserControls/MenuItem.js";

export class MenuPanel extends BasePanel {
    public readonly declare releaseNotes : Button;
    public readonly declare notifications : Button;
    public readonly show : Button;
    public readonly hide : Button;
    public readonly mobileOnly : GuiCommons;
    public readonly sysAdminOnly : GuiCommons;
    public readonly adminOnly : GuiCommons;
    private readonly forAll : GuiCommons;
    private readonly forMobile : GuiCommons;
    private readonly forAdmins : GuiCommons;
    private readonly forSysAdmins : GuiCommons;

    constructor() {
        super();
        this.show = new Button();
        this.hide = new Button();
        this.mobileOnly = new GuiCommons();
        this.sysAdminOnly = new GuiCommons();
        this.adminOnly = new GuiCommons();
        this.forAll = new GuiCommons();
        this.forMobile = new GuiCommons();
        this.forAdmins = new GuiCommons();
        this.forSysAdmins = new GuiCommons();
    }

    public AddItem($name : string, $link : string, $icon : string, $scope? : MenuItemScope) : MenuItem {
        const item : MenuItem = new MenuItem();
        item.Name($name);
        item.Link($link);
        item.Icon($icon);
        switch ($scope) {
        case MenuItemScope.HIDDEN:
            // do not process hidden items
            break;
        case MenuItemScope.MOBILE_ONLY:
            this.forMobile.AddChild(item);
            break;
        case MenuItemScope.ADMINS:
            this.forAdmins.AddChild(item);
            break;
        case MenuItemScope.SYS_ADMINS:
            this.forSysAdmins.AddChild(item);
            break;
        default:
            this.forAll.AddChild(item);
            break;
        }
        return item;
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row g-0 h-100">
        <div class="col" style="display: flex;flex-direction: column;">
            <div class="row g-0 flex-fill Items">
                <div class="col" style="overflow: hidden;">
                    <div class="row">
                        <div class="col" data-oidis-bind="${this.forAll}"></div>
                    </div>
                    <div class="row MobileOnly" data-oidis-bind="${this.mobileOnly}">
                        <div class="col" data-oidis-bind="${this.forMobile}">
                            <hr />
                        </div>
                    </div>
                    <div class="row ${GeneralCSS.DNONE}" data-oidis-bind="${this.sysAdminOnly}">
                        <div class="col" data-oidis-bind="${this.forSysAdmins}">
                            <hr />
                        </div>
                    </div>
                    <div class="row ${GeneralCSS.DNONE}" data-oidis-bind="${this.adminOnly}">
                        <div class="col" data-oidis-bind="${this.forAdmins}">
                            <hr />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row g-0 Footer">
                <div class="col text-end"><button class="btn btn-outline-light ShowItems" type="button" data-oidis-bind="${this.show}"><svg class="bi bi-chevron-double-right" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 2px;">
                            <path fill-rule="evenodd" d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z"></path>
                            <path fill-rule="evenodd" d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z"></path>
                        </svg></button><button class="btn btn-outline-light HideItems" type="button" data-oidis-bind="${this.hide}"><svg class="bi bi-chevron-double-left" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M8.354 1.646a.5.5 0 0 1 0 .708L2.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"></path>
                            <path fill-rule="evenodd" d="M12.354 1.646a.5.5 0 0 1 0 .708L6.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"></path>
                        </svg></button></div>
            </div>
        </div>
    </div>
</section>`;
    }
}

export class MenuItemScope extends BaseEnum {
    public static readonly FOR_ALL : string = "all";
    public static readonly MOBILE_ONLY : string = "mobile";
    public static readonly ADMINS : string = "admins";
    public static readonly SYS_ADMINS : string = "sysadmins";
    public static readonly HIDDEN : string = "hidden";
}
