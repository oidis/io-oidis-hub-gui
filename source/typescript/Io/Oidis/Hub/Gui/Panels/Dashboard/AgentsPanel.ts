/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { Dialog } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Dialog.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { AgentsTable } from "../../UserControls/AgentsTable.js";
import { CountDown } from "../../UserControls/CountDown.js";

export class AgentsPanel extends BasePanel {
    public readonly searchValue : TextField;
    public readonly searchButton : Button;
    public readonly agentsCount : Label;
    public readonly idle : CheckBox;
    public readonly idleCount : Label;
    public readonly running : CheckBox;
    public readonly runningCount : Label;
    public readonly stopped : CheckBox;
    public readonly stoppedCount : Label;
    public readonly refresh : CheckBox;
    public readonly refreshRate : TextField;
    public readonly agentsTable : AgentsTable;
    public readonly searchByText : Button;
    public readonly searchByPlatform : Button;
    public readonly searchByPool : Button;
    public readonly searchByTag : Button;
    public readonly searchByVersion : Button;
    public readonly autocompleteSelector : Button;
    public readonly searchAutocomplete : GuiCommons;
    public readonly clearFilter : Button;
    public readonly killDialog : Dialog;
    public readonly closeKill : Button;
    public readonly cancelKill : Button;
    public readonly killConfirm : Button;
    public readonly statusConfirmDialog : Dialog;
    public readonly closeStatus : Button;
    public readonly cancelStatus : Button;
    public readonly statusConfirm : Button;
    public readonly countDown : CountDown;
    public readonly metadataDialog : Dialog;
    public readonly agentMetadata : TextField;
    public readonly closeMetadata : Button;
    public readonly cancelMetadata : Button;

    constructor() {
        super();

        this.searchValue = new TextField();
        this.searchButton = new Button();
        this.agentsCount = new Label();
        this.idle = new CheckBox();
        this.idleCount = new Label();
        this.running = new CheckBox();
        this.runningCount = new Label();
        this.stopped = new CheckBox();
        this.stoppedCount = new Label();
        this.refresh = new CheckBox();
        this.refreshRate = new TextField();
        this.agentsTable = new AgentsTable();
        this.searchByText = new Button();
        this.searchByPlatform = new Button();
        this.searchByPool = new Button();
        this.searchByTag = new Button();
        this.searchByVersion = new Button();
        this.autocompleteSelector = new Button();
        this.searchAutocomplete = new GuiCommons();
        this.clearFilter = new Button();
        this.killDialog = new Dialog();
        this.closeKill = new Button();
        this.cancelKill = new Button();
        this.killConfirm = new Button();
        this.statusConfirmDialog = new Dialog();
        this.closeStatus = new Button();
        this.cancelStatus = new Button();
        this.statusConfirm = new Button();
        this.countDown = new CountDown();
        this.metadataDialog = new Dialog();
        this.agentMetadata = new TextField();
        this.closeMetadata = new Button();
        this.cancelMetadata = new Button();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row">
        <div class="col">
            <div class="input-group" style="margin-bottom: 25px;">
                <div class="dropdown" style="width: 140px;"><button class="btn btn-primary dropdown-toggle rounded-0 rounded-start" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 140px;padding-left: 0px;">Filter by</button>
                    <div class="dropdown-menu"><button class="btn rounded-0 dropdown-item" type="button" data-oidis-bind="${this.searchByText}">Search query</button><button class="btn rounded-0 dropdown-item" type="button" data-oidis-bind="${this.searchByPlatform}">Platforms</button><button class="btn rounded-0 dropdown-item" type="button" data-oidis-bind="${this.searchByPool}">Pools</button><button class="btn rounded-0 dropdown-item" type="button" data-oidis-bind="${this.searchByTag}">Tags</button><button class="btn rounded-0 dropdown-item" type="button" data-oidis-bind="${this.searchByVersion}">Versions</button></div>
                </div>
                <div class="dropdown" style="width: calc(100% - 210px);position: absolute;left: 140px;"><button class="btn btn-primary rounded-0" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;z-index: 0;" data-oidis-bind="${this.autocompleteSelector}">Dropdown </button>
                    <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.searchAutocomplete}"></div>
                </div><input class="form-control" type="text" data-oidis-bind="${this.searchValue}" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.searchButton}" style="width: 70px;border-radius: 4px;border-top-left-radius: 0px;border-bottom-left-radius: 0px;border-top-right-radius: 4px;border-bottom-right-radius: 4px;"><svg class="bi bi-search" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0"></path>
                    </svg></button><span class="input-group-text" style="margin: 0;padding: 0;width: 0;position: relative;border-width: 0px;"><button class="btn btn-secondary ${GeneralCSS.DNONE}" type="button" style="position: absolute;left: -110px;background-color: transparent;border-width: 0px;z-index: 100;border-radius: 0;" data-oidis-bind="${this.clearFilter}"><svg class="bi bi-x-circle text-primary" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"></path>
                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708"></path>
                        </svg></button></span>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 10px;">
        <div class="col-xl-2"><span><strong>Agents count:</strong></span><span style="margin-left: 15px;" data-oidis-bind="${this.agentsCount}">0</span></div>
        <div class="col text-center align-self-center">
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" checked data-oidis-bind="${this.idle}" /><label class="form-check-label" for="formCheck-2">IDLE<span class="badge rounded-pill bg-danger" style="position: relative;top: -10px;left: 3px;" data-oidis-bind="${this.idleCount}"></span></label></div>
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" checked data-oidis-bind="${this.running}" /><label class="form-check-label" for="formCheck-3">Running<span class="badge rounded-pill bg-danger" style="position: relative;top: -10px;left: 3px;" data-oidis-bind="${this.runningCount}"></span></label></div>
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" checked data-oidis-bind="${this.stopped}" /><label class="form-check-label" for="formCheck-4">Stopped<span class="badge rounded-pill bg-danger" style="position: relative;top: -10px;left: 3px;" data-oidis-bind="${this.stoppedCount}"></span></label></div>
        </div>
        <div class="col-xl-2 align-self-center" style="text-align: right;">
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" checked data-oidis-bind="${this.refresh}" /><label class="form-check-label" for="formCheck-1">Auto refresh</label></div>
        </div>
        <div class="col-xl-1 text-start align-self-center">
            <div class="row">
                <div class="col-auto" style="padding: 0;">
                    <div data-oidis-bind="${this.countDown}"></div>
                </div>
                <div class="col">
                    <div class="input-group input-group-sm"><input class="form-control" type="number" data-oidis-bind="${this.refreshRate}" min="1" max="3600" step="5" value="15" /><span class="input-group-text">s</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col">
            <div data-oidis-bind="${this.agentsTable}"></div>
        </div>
    </div>
    <div id="modal-1" class="modal fade" role="dialog" tabindex="-1" data-oidis-bind="${this.killDialog}">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" data-oidis-bind="${this.closeKill}">
                    <h4 class="modal-title">Confirm agent stop</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure to kill selected agent? Automatic restart may depend on host settings.</p>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal" data-oidis-bind="${this.cancelKill}">Cancel</button><button class="btn btn-danger" type="button" data-oidis-bind="${this.killConfirm}">Confirm kill</button></div>
            </div>
        </div>
    </div>
    <div id="modal-2" class="modal fade" role="dialog" tabindex="-1" data-oidis-bind="${this.statusConfirmDialog}">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" data-oidis-bind="${this.closeStatus}">
                    <h4 class="modal-title">Confirm agent status change</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure with agent status change? It can cause application malfunction.</p>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal" data-oidis-bind="${this.cancelStatus}">Cancel</button><button class="btn btn-danger" type="button" data-oidis-bind="${this.statusConfirm}">Confirm</button></div>
            </div>
        </div>
    </div>
    <div id="modal-3" class="modal fade" role="dialog" tabindex="-1" data-oidis-bind="${this.metadataDialog}">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header" data-oidis-bind="${this.closeMetadata}">
                    <h4 class="modal-title">Agent metadata</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div style="width: 100%;height: 100%;white-space: pre;overflow: auto;" data-oidis-bind="${this.agentMetadata}"></div>
                </div>
                <div class="modal-footer"><button class="btn btn-primary" type="button" data-bs-dismiss="modal" data-oidis-bind="${this.cancelMetadata}">Close</button></div>
            </div>
        </div>
    </div>
</section>`;
    }
}
