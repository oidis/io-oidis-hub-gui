/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { Image } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Image.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { AutocompleteSearch } from "../../UserControls/AutocompleteSearch.js";
import { OrgUsersTable } from "../../UserControls/OrgUsersTable.js";

export class OrganizationsPanel extends BasePanel {
    public readonly search : AutocompleteSearch;
    public readonly profileImg : Image;
    public readonly orgId : TextField;
    public readonly orgName : TextField;
    public readonly create : Button;
    public readonly update : Button;
    public readonly reset : Button;
    public readonly deactivate : Button;
    public readonly activate : Button;
    public readonly restore : Button;
    public readonly delete : Button;
    public readonly photoUploadButton : Button;
    public readonly createContent : GuiCommons;
    public readonly editContent : GuiCommons;
    public readonly usersTable : OrgUsersTable;
    public readonly userField : TextField;
    public readonly userAutocompleteSelector : Button;
    public readonly userAutocomplete : GuiCommons;
    public readonly userAdd : Button;
    public readonly withInactive : CheckBox;

    constructor() {
        super();

        this.search = new AutocompleteSearch();
        this.profileImg = new Image();
        this.orgId = new TextField();
        this.orgName = new TextField();
        this.create = new Button();
        this.update = new Button();
        this.reset = new Button();
        this.deactivate = new Button();
        this.activate = new Button();
        this.restore = new Button();
        this.delete = new Button();
        this.photoUploadButton = new Button();
        this.createContent = new GuiCommons();
        this.editContent = new GuiCommons();
        this.usersTable = new OrgUsersTable();
        this.userField = new TextField();
        this.userAutocompleteSelector = new Button();
        this.userAutocomplete = new GuiCommons();
        this.userAdd = new Button();
        this.withInactive = new CheckBox();
    }

    protected innerCode() : string {
        this.profileImg.getEvents().setOnMouseOver(() : void => {
            ElementManager.Show(this.photoUploadButton.InstanceOwner());
        });
        this.profileImg.getEvents().setOnMouseOut(() : void => {
            this.getEvents().FireAsynchronousMethod(() : void => {
                ElementManager.Hide(this.photoUploadButton.InstanceOwner());
            }, 250);
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<section>
    <form>
        <div class="row">
            <div class="col" data-oidis-bind="${this.search}"></div>
        </div>
        <div class="row" style="margin-top: 30px;">
            <div class="col-xxl-3 text-center">
                <div style="position: relative;cursor: pointer;">
                    <div style="width: 100px;margin: auto;margin-top: 36px;position: absolute;margin-left: -51px;left: 50%;display: none;" data-oidis-bind="${this.photoUploadButton}"><svg class="bi bi-camera-fill" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="font-size: 51px;color: var(--bs-gray-dark);">
                            <path d="M10.5 8.5a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                            <path d="M2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2zm.5 2a.5.5 0 1 1 0-1 .5.5 0 0 1 0 1zm9 2.5a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0z"></path>
                        </svg><span>Upload photo</span></div>
                </div><img class="rounded-circle img-fluid" src="resource/graphics/Io/Oidis/Hub/Gui/OrgImage.png" style="width: 150px;border-style: solid;border-top: 2px solid var(--bs-blue);border-right: 2px solid var(--bs-blue);border-bottom: 2px solid var(--bs-blue);border-left: 2px solid var(--bs-blue);margin-bottom: 25px;height: 150px;" data-oidis-bind="${this.profileImg}" />
            </div>
            <div class="col-xxl-6 offset-xxl-0 text-center">
                <div class="row">
                    <div class="col">
                        <div class="input-group"><span class="input-group-text" style="width: 20%;">Id</span><input class="form-control" type="text" readonly data-oidis-bind="${this.orgId}" /></div>
                        <div class="input-group" style="margin-top: 5px;"><span class="input-group-text" style="width: 20%;">Name</span><input class="form-control" type="text" data-oidis-bind="${this.orgName}" /></div>
                    </div>
                </div>
                <div class="row" style="margin-top: 15px;" data-oidis-bind="${this.createContent}">
                    <div class="col text-center"><button class="btn btn-success" type="button" data-oidis-bind="${this.create}" style="width: 100%;margin-top: 10px;">Create new</button></div>
                </div>
                <div class="row ${GeneralCSS.DNONE}" style="margin-top: 15px;" data-oidis-bind="${this.editContent}">
                    <div class="col-12 col-md-10 text-center text-md-end"><button class="btn btn-primary" type="button" data-oidis-bind="${this.update}" style="width: 100%;margin-top: 10px;">Update profile</button></div>
                    <div class="col-12 col-md-2 text-center text-md-start"><button class="btn btn-outline-secondary" type="button" data-oidis-bind="${this.reset}" style="margin-top: 10px;width: 100%;">Cancel</button></div>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col"><button class="btn btn-warning ${GeneralCSS.DNONE}" type="button" data-oidis-bind="${this.deactivate}" style="margin-top: 10px;width: 110px;margin-right: 5px;margin-left: 5px;">Deactivate</button><button class="btn btn-success ${GeneralCSS.DNONE}" type="button" data-oidis-bind="${this.activate}" style="margin-top: 10px;width: 110px;margin-right: 5px;margin-left: 5px;">Activate</button><button class="btn btn-success ${GeneralCSS.DNONE}" type="button" data-oidis-bind="${this.restore}" style="margin-top: 10px;margin-right: 5px;margin-left: 5px;width: 110px;">Restore</button><button class="btn btn-danger ${GeneralCSS.DNONE}" type="button" data-oidis-bind="${this.delete}" style="margin-top: 10px;margin-right: 5px;margin-left: 5px;width: 110px;">Delete</button></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card" style="margin-top: 30px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h4><svg class="bi bi-people" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 5px;">
                                        <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"></path>
                                    </svg>Organization users</h4>
                                <h6 class="text-muted mb-2">List of users assigned to current profile</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-check"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.withInactive}" /><label class="form-check-label">With inactive users</label></div>
                                <div data-oidis-bind="${this.usersTable}"></div>
                                <div class="input-group"><span class="input-group-text" style="width: 140px;">Add user</span>
                                    <div class="dropup" style="width: calc(100% - 210px);position: absolute;left: 140px;"><button class="btn btn-primary rounded-0" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;z-index: 0;" data-oidis-bind="${this.userAutocompleteSelector}">Dropdown </button>
                                        <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.userAutocomplete}"></div>
                                    </div><input class="form-control" type="text" data-oidis-bind="${this.userField}" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.userAdd}" style="width: 70px;">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
`;
    }
}
