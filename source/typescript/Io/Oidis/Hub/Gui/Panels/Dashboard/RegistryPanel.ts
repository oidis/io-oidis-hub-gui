/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { ProgressBar } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/ProgressBar.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { RegistryItemsTable } from "../../UserControls/RegistryItemsTable.js";

export class RegistryPanel extends BasePanel {
    public readonly projectName : Label;
    public readonly projectNameMenu : GuiCommons;
    public readonly version : Label;
    public readonly versionMenu : GuiCommons;
    public readonly platform : Label;
    public readonly platformMenu : GuiCommons;
    public readonly searchFilter : TextField;
    public readonly clear : Button;
    public readonly configsTable : GuiCommons;
    public readonly updatesTable : GuiCommons;
    public readonly downloadsTable : GuiCommons;
    public readonly ascending : CheckBox;
    public readonly refresh : Button;
    public readonly uploadGroup : GuiCommons;
    public readonly fileSource : TextField;
    public readonly upload : Button;
    public readonly progressEnvelop : GuiCommons; // TODO: envelop should be part of ProgressBar as visible will not work as expected
    public readonly uploadProgress : ProgressBar;
    public readonly configsTab : GuiCommons;
    public readonly updatesTab : GuiCommons;
    public readonly downloadsTab : GuiCommons;
    public readonly reindexHolder : GuiCommons;
    public readonly reindex : Button;
    public readonly resetConfigs : Button;
    public readonly fileEntries : RegistryItemsTable;
    public readonly filesTab : GuiCommons;

    constructor() {
        super();

        this.projectName = new Label();
        this.projectNameMenu = new GuiCommons();
        this.version = new Label();
        this.versionMenu = new GuiCommons();
        this.platform = new Label();
        this.platformMenu = new GuiCommons();
        this.searchFilter = new TextField();
        this.clear = new Button();
        this.configsTable = new GuiCommons();
        this.updatesTable = new GuiCommons();
        this.downloadsTable = new GuiCommons();
        this.ascending = new CheckBox();
        this.refresh = new Button();
        this.uploadGroup = new GuiCommons();
        this.fileSource = new TextField();
        this.upload = new Button();
        this.progressEnvelop = new GuiCommons();
        this.uploadProgress = new ProgressBar();
        this.configsTab = new GuiCommons();
        this.updatesTab = new GuiCommons();
        this.downloadsTab = new GuiCommons();
        this.reindexHolder = new GuiCommons();
        this.reindex = new Button();
        this.fileSource.HtmlTagsEnabled(true);
        this.resetConfigs = new Button();
        this.fileEntries = new RegistryItemsTable();
        this.filesTab = new GuiCommons();
    }

    public SelectedTab($type? : RegistryTabType) : RegistryTabType {
        if (!ObjectValidator.IsEmptyOrNull($type)) {
            try {
                if (!(<any>this).completed) {
                    this.getEvents().setOnComplete(() : void => {
                        this.SelectedTab($type);
                    });
                } else {
                    switch ($type) {
                    case RegistryTabType.Configs:
                        this.configsTab.InstanceOwner().click();
                        break;
                    case RegistryTabType.Updates:
                        this.updatesTab.InstanceOwner().click();
                        break;
                    case RegistryTabType.Downloads:
                        this.downloadsTab.InstanceOwner().click();
                        break;
                    case RegistryTabType.Files:
                        this.filesTab.InstanceOwner().click();
                        break;
                    default:
                        LogIt.Warning("Instance {0} has not been detected as registry tab.", RegistryTabType[$type]);
                        break;
                    }
                }
            } catch (ex) {
                LogIt.Error(ex);
            }
        }
        const attribute : string = "aria-expanded";
        if (StringUtils.ToBoolean(this.configsTab.InstanceOwner().getAttribute(attribute))) {
            return RegistryTabType.Configs;
        } else if (StringUtils.ToBoolean(this.updatesTab.InstanceOwner().getAttribute(attribute))) {
            return RegistryTabType.Updates;
        } else if (StringUtils.ToBoolean(this.downloadsTab.InstanceOwner().getAttribute(attribute))) {
            return RegistryTabType.Downloads;
        } else if (StringUtils.ToBoolean(this.filesTab.InstanceOwner().getAttribute(attribute))) {
            return RegistryTabType.Files;
        }
        return null;
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row g-0 ${GeneralCSS.DNONE}" style="margin-bottom: 10px;background: var(--bs-yellow);border-radius: 3px;margin-left: auto;margin-right: auto;border: 1px solid var(--bs-orange);" data-oidis-bind="${this.reindexHolder}">
        <div class="col align-self-center"><span class="text-danger"><svg class="bi bi-exclamation-triangle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 15px;margin-left: 16px;margin-bottom: 4px;">
                    <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"></path>
                    <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z"></path>
                </svg>Not all of registry items are indexed by database</span></div>
        <div class="col-xl-3" style="text-align: right;"><button class="btn btn-outline-dark" type="button" style="margin-right: 15px;margin-top: 5px;margin-bottom: 5px;" data-oidis-bind="${this.reindex}">Reindex</button></div>
    </div>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col"><label class="col-form-label float-start">Project name:</label></div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="dropdown"><button class="btn btn-outline-primary dropdown-toggle text-start" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;" data-oidis-bind="${this.projectName}">all</button>
                        <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.projectNameMenu}"><a class="dropdown-item">loading ...</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-2">
            <div class="row">
                <div class="col"><label class="col-form-label float-start">Version:</label></div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="dropdown"><button class="btn btn-outline-primary dropdown-toggle text-start" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;" data-oidis-bind="${this.version}">all</button>
                        <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.versionMenu}"><a class="dropdown-item">loading ...</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-2">
            <div class="row">
                <div class="col-xl-2"><label class="col-form-label float-start">Platform:</label></div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="dropdown"><button class="btn btn-outline-primary dropdown-toggle text-start" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;" data-oidis-bind="${this.platform}">all</button>
                        <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.platformMenu}"><a class="dropdown-item">loading ...</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                <div class="col"><label class="col-form-label float-start">Search:</label></div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-9"><input class="form-control" type="text" data-oidis-bind="${this.searchFilter}" autocomplete="off" /></div>
                        <div class="col-3 text-end"><button class="btn btn-primary" type="button" data-oidis-bind="${this.clear}">Clear</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col" style="margin-top: 10px;">
            <div class="form-check form-switch form-check-inline"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.ascending}" /><label class="form-check-label" for="formCheck-1">Ascending<br /></label></div><button class="btn btn-primary" type="button" data-oidis-bind="${this.refresh}">Refresh</button>
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col">
            <div id="accordion-1" class="accordion" role="tablist">
                <div class="accordion-item">
                    <h2 class="accordion-header" role="tab"><button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-1 .item-1" aria-expanded="false" aria-controls="accordion-1 .item-1" data-oidis-bind="${this.configsTab}"><svg class="bi bi-gear" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                                <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"></path>
                                <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"></path>
                            </svg>Configs</button></h2>
                    <div class="accordion-collapse collapse item-1" role="tabpanel" data-bs-parent="#accordion-1">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col text-center"><button class="btn btn-danger" type="button" style="margin-bottom: 10px;" data-oidis-bind="${this.resetConfigs}"><svg class="bi bi-eraser" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 5px;margin-bottom: 3px;">
                                            <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm2.121.707a1 1 0 0 0-1.414 0L4.16 7.547l5.293 5.293 4.633-4.633a1 1 0 0 0 0-1.414l-3.879-3.879zM8.746 13.547 3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"></path>
                                        </svg>Reset configs cache</button>
                                    <div class="card" style="margin-bottom: 10px;">
                                        <div class="card-body">
                                            <p class="card-text" style="font-size: 13px;"><svg class="bi bi-info-circle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-bottom: 2px;margin-right: 10px;">
                                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"></path>
                                                    <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0"></path>
                                                </svg>Reset configs cache is required in case of configs deployment at Hub runtime, otherwise UI will keep data from application start or last cache reset. Uploaded data can be validated by click on links listed below before cache reset action.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-sm">
                                    <tbody data-oidis-bind="${this.configsTable}">
                                        <tr>
                                            <td>no data</td>
                                        </tr>
                                        <tr></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" role="tab"><button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-1 .item-2" aria-expanded="false" aria-controls="accordion-1 .item-2" data-oidis-bind="${this.updatesTab}"><svg class="bi bi-tags" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                                <path d="M3 2v4.586l7 7L14.586 9l-7-7zM2 2a1 1 0 0 1 1-1h4.586a1 1 0 0 1 .707.293l7 7a1 1 0 0 1 0 1.414l-4.586 4.586a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 2 6.586z"></path>
                                <path d="M5.5 5a.5.5 0 1 1 0-1 .5.5 0 0 1 0 1m0 1a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3M1 7.086a1 1 0 0 0 .293.707L8.75 15.25l-.043.043a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 0 7.586V3a1 1 0 0 1 1-1z"></path>
                            </svg>Updates</button></h2>
                    <div class="accordion-collapse collapse item-2" role="tabpanel" data-bs-parent="#accordion-1">
                        <div class="accordion-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-sm">
                                    <tbody data-oidis-bind="${this.updatesTable}">
                                        <tr>
                                            <td>no data</td>
                                        </tr>
                                        <tr></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" role="tab"><button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-1 .item-3" aria-expanded="false" aria-controls="accordion-1 .item-3" data-oidis-bind="${this.downloadsTab}"><svg class="bi bi-file-zip" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                                <path d="M6.5 7.5a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v.938l.4 1.599a1 1 0 0 1-.416 1.074l-.93.62a1 1 0 0 1-1.109 0l-.93-.62a1 1 0 0 1-.415-1.074l.4-1.599V7.5zm2 0h-1v.938a1 1 0 0 1-.03.243l-.4 1.598.93.62.93-.62-.4-1.598a1 1 0 0 1-.03-.243z"></path>
                                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2zm5.5-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9v1H8v1h1v1H8v1h1v1H7.5V5h-1V4h1V3h-1V2h1z"></path>
                            </svg>Downloads</button></h2>
                    <div class="accordion-collapse collapse item-3" role="tabpanel" data-bs-parent="#accordion-1">
                        <div class="accordion-body">
                            <div class="input-group" data-oidis-bind="${this.uploadGroup}"><input class="form-control" type="file" data-oidis-bind="${this.fileSource}" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.upload}">Upload</button></div>
                            <div class="progress ${GeneralCSS.DNONE}" style="height: 38px;" data-oidis-bind="${this.progressEnvelop}">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" data-oidis-bind="${this.uploadProgress}" style="height: 38px;">0%</div>
                            </div>
                            <div class="table-responsive" style="margin-top: 20px;">
                                <table class="table table-striped table-sm">
                                    <tbody data-oidis-bind="${this.downloadsTable}">
                                        <tr>
                                            <td>no data</td>
                                        </tr>
                                        <tr></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" role="tab"><button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-1 .item-4" aria-expanded="false" aria-controls="accordion-1 .item-4" data-oidis-bind="${this.filesTab}"><svg class="bi bi-files" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                                <path d="M13 0H6a2 2 0 0 0-2 2 2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2 2 2 0 0 0 2-2V2a2 2 0 0 0-2-2m0 13V4a2 2 0 0 0-2-2H5a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1M3 4a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1z"></path>
                            </svg>Files</button></h2>
                    <div class="accordion-collapse collapse item-4" role="tabpanel" data-bs-parent="#accordion-1">
                        <div class="accordion-body">
                            <div class="row" data-oidis-bind="${this.fileEntries}"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>`;
    }
}

export enum RegistryTabType {
    Configs,
    Updates,
    Downloads,
    Files
}
