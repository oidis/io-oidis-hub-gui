/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Dialog } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Dialog.js";
import { Image } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Image.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { AutocompleteSearch } from "../../UserControls/AutocompleteSearch.js";

export class UsersPanel extends BasePanel {
    public readonly profileSearch : AutocompleteSearch;
    public readonly profileImg : Image;
    public readonly profileId : TextField;
    public readonly profileMail : TextField;
    public readonly profileName : TextField;
    public readonly profilePhone : TextField;
    public readonly lastLogin : TextField;
    public readonly profilePassNew : TextField;
    public readonly generatePass : Button;
    public readonly addNewToken : Button;
    public readonly profileTokens : GuiCommons;
    public readonly tokenDialog : Dialog;
    public readonly tokenDialogClose : Button;
    public readonly tokenDialogCloseButton : Button;
    public readonly tokenValidatePass : TextField;
    public readonly tokenAccessValidate : Button;
    public readonly photoUploadButton : Button;
    public readonly updateProfile : Button;
    public readonly resetProfile : Button;
    public readonly groups : GuiCommons;
    public readonly deactivateProfile : Button;
    public readonly activateProfile : Button;
    public readonly restoreProfile : Button;
    public readonly deleteProfile : Button;
    public readonly exportPerms : Button;
    public readonly faIsInactive : GuiCommons;
    public readonly faIsActive : GuiCommons;
    public readonly qrCode : GuiCommons;
    public readonly faCodeValue : Label;
    public readonly verifyCode : TextField;
    public readonly faActivate : Button;
    public readonly faDeactivate : Button;
    public readonly faQRCodeContent : GuiCommons;
    public readonly faCodeContent : GuiCommons;
    protected readonly enterCode : Button;
    protected readonly scanQRCode : Button;

    constructor() {
        super();

        this.profileSearch = new AutocompleteSearch();
        this.profileImg = new Image();
        this.profileId = new TextField();
        this.profileMail = new TextField();
        this.profileName = new TextField();
        this.profilePhone = new TextField();
        this.lastLogin = new TextField();
        this.profilePassNew = new TextField();
        this.generatePass = new Button();
        this.addNewToken = new Button();
        this.profileTokens = new GuiCommons();
        this.tokenDialog = new Dialog();
        this.tokenDialogClose = new Button();
        this.tokenDialogCloseButton = new Button();
        this.tokenValidatePass = new TextField();
        this.tokenAccessValidate = new Button();
        this.photoUploadButton = new Button();
        this.updateProfile = new Button();
        this.resetProfile = new Button();
        this.groups = new GuiCommons();
        this.deactivateProfile = new Button();
        this.activateProfile = new Button();
        this.restoreProfile = new Button();
        this.deleteProfile = new Button();
        this.exportPerms = new Button();
        this.faIsInactive = new GuiCommons();
        this.faIsActive = new GuiCommons();
        this.qrCode = new GuiCommons();
        this.faCodeValue = new Label();
        this.verifyCode = new TextField();
        this.faActivate = new Button();
        this.faDeactivate = new Button();
        this.faQRCodeContent = new GuiCommons();
        this.faCodeContent = new GuiCommons();
        this.enterCode = new Button();
        this.scanQRCode = new Button();
    }

    protected innerCode() : string {
        this.profileImg.getEvents().setOnMouseOver(() : void => {
            ElementManager.Show(this.photoUploadButton.InstanceOwner());
        });
        this.profileImg.getEvents().setOnMouseOut(() : void => {
            this.getEvents().FireAsynchronousMethod(() : void => {
                ElementManager.Hide(this.photoUploadButton.InstanceOwner());
            }, 250);
        });
        this.enterCode.getEvents().setOnClick(($args : EventArgs) : void => {
            $args.PreventDefault();
            this.faQRCodeContent.Visible(false);
            this.faCodeContent.Visible(true);
        });
        this.scanQRCode.getEvents().setOnClick(($args : EventArgs) : void => {
            $args.PreventDefault();
            this.faCodeContent.Visible(false);
            this.faQRCodeContent.Visible(true);
        });
        this.getEvents().setOnComplete(() : void => {
            ValidationBinding.setOnlyNumbersAllowed(this.verifyCode);
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<section>
    <form>
        <div class="row">
            <div class="col" data-oidis-bind="${this.profileSearch}"></div>
        </div>
        <div class="row" style="margin-top: 30px;">
            <div class="col-xxl-3 text-center">
                <div style="position: relative;cursor: pointer;">
                    <div style="width: 100px;margin: auto;margin-top: 36px;position: absolute;margin-left: -51px;left: 50%;display: none;" data-oidis-bind="${this.photoUploadButton}"><svg class="bi bi-camera-fill" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="font-size: 51px;color: var(--bs-gray-dark);">
                            <path d="M10.5 8.5a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0"></path>
                            <path d="M2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4zm.5 2a.5.5 0 1 1 0-1 .5.5 0 0 1 0 1m9 2.5a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0"></path>
                        </svg><span>Upload photo</span></div>
                </div><img class="rounded-circle" src="resource/graphics/Io/Oidis/Hub/Gui/UserImage.png" style="width: 150px;border-style: solid;border-top: 2px solid var(--bs-blue);border-right: 2px solid var(--bs-blue);border-bottom: 2px solid var(--bs-blue);border-left: 2px solid var(--bs-blue);margin-bottom: 25px;height: 150px;object-fit: cover;" data-oidis-bind="${this.profileImg}" />
            </div>
            <div class="col-xxl-6 offset-xxl-0 text-center">
                <div class="row">
                    <div class="col">
                        <div class="input-group"><span class="input-group-text" style="width: 20%;">Id</span><input class="form-control" type="text" readonly data-oidis-bind="${this.profileId}" /></div>
                        <div>${this.getProfile()}</div>
                        <div class="input-group" style="margin-top: 5px;"><span class="input-group-text" style="width: 20%;">Last login</span><input class="form-control" type="text" data-oidis-bind="${this.lastLogin}" readonly /></div>
                        <div class="input-group" style="margin-top: 5px;"><span class="input-group-text" style="width: 20%;">Password</span><input class="form-control" type="password" data-oidis-bind="${this.profilePassNew}" autocomplete="off" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.generatePass}">Generate</button></div>
                    </div>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-12 col-md-10 text-center text-md-end"><button class="btn btn-primary" type="button" data-oidis-bind="${this.updateProfile}" style="width: 100%;margin-top: 10px;">Update profile</button></div>
                    <div class="col-12 col-md-2 text-center text-md-start"><button class="btn btn-outline-secondary" type="button" data-oidis-bind="${this.resetProfile}" style="margin-top: 10px;width: 100%;">Cancel</button></div>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col"><button class="btn btn-warning ${GeneralCSS.DNONE}" type="button" data-oidis-bind="${this.deactivateProfile}" style="margin-top: 10px;width: 110px;margin-right: 5px;margin-left: 5px;">Deactivate</button><button class="btn btn-success ${GeneralCSS.DNONE}" type="button" data-oidis-bind="${this.activateProfile}" style="margin-top: 10px;width: 110px;margin-right: 5px;margin-left: 5px;">Activate</button><button class="btn btn-success ${GeneralCSS.DNONE}" type="button" data-oidis-bind="${this.restoreProfile}" style="margin-top: 10px;margin-right: 5px;margin-left: 5px;width: 110px;">Restore</button><button class="btn btn-danger ${GeneralCSS.DNONE}" type="button" data-oidis-bind="${this.deleteProfile}" style="margin-top: 10px;margin-right: 5px;margin-left: 5px;width: 110px;">Delete</button></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card" style="margin-top: 30px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h4><svg class="bi bi-person-check" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 5px;">
                                        <path d="M12.5 16a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7m1.679-4.493-1.335 2.226a.75.75 0 0 1-1.174.144l-.774-.773a.5.5 0 0 1 .708-.708l.547.548 1.17-1.951a.5.5 0 1 1 .858.514ZM11 5a3 3 0 1 1-6 0 3 3 0 0 1 6 0M8 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4"></path>
                                        <path d="M8.256 14a4.474 4.474 0 0 1-.229-1.004H3c.001-.246.154-.986.832-1.664C4.484 10.68 5.711 10 8 10c.26 0 .507.009.74.025.226-.341.496-.65.804-.918C9.077 9.038 8.564 9 8 9c-5 0-6 3-6 4s1 1 1 1z"></path>
                                    </svg>Permissions</h4>
                                <h6 class="text-muted mb-2">Groups of permissions for user profile</h6>
                            </div>
                            <div class="col-sm-3 col-xxl-3" style="text-align: right;"><button class="btn btn-primary" type="button" data-oidis-bind="${this.exportPerms}" style="border-radius: 25px;width: 100px;"><svg class="bi bi-list-columns-reverse" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                                        <path fill-rule="evenodd" d="M0 .5A.5.5 0 0 1 .5 0h2a.5.5 0 0 1 0 1h-2A.5.5 0 0 1 0 .5m4 0a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1h-10A.5.5 0 0 1 4 .5m-4 2A.5.5 0 0 1 .5 2h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5m4 0a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5m-4 2A.5.5 0 0 1 .5 4h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5m4 0a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5m-4 2A.5.5 0 0 1 .5 6h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5m4 0a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 0 1h-8a.5.5 0 0 1-.5-.5m-4 2A.5.5 0 0 1 .5 8h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5m4 0a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 0 1h-8a.5.5 0 0 1-.5-.5m-4 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5m4 0a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1h-10a.5.5 0 0 1-.5-.5m-4 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5m4 0a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5m-4 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5m4 0a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5"></path>
                                    </svg>Export</button></div>
                        </div>
                        <div class="row">
                            <div class="col Permissions" data-oidis-bind="${this.groups}">
                                <p>Loading ...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card" style="margin-top: 30px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-9">
                                <h4><svg class="bi bi-key-fill" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 5px;">
                                        <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2"></path>
                                    </svg>Tokens</h4>
                                <h6 class="text-muted mb-2">Authorization tokens for head-less authentification</h6>
                            </div>
                            <div class="col-sm-3 col-xxl-3 align-self-start"><button class="btn btn-success float-end" type="button" style="border-radius: 25px;width: 100px;" data-oidis-bind="${this.addNewToken}"><svg class="bi bi-plus-circle-fill" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 3px;">
                                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3z"></path>
                                    </svg>New</button></div>
                        </div>
                        <div class="row">
                            <div class="col" data-oidis-bind="${this.profileTokens}">
                                <p>Loading ...</p>
                            </div>
                        </div>
                        <div class="modal fade" role="dialog" tabindex="-1" data-oidis-bind="${this.tokenDialog}">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" data-oidis-bind="${this.tokenDialogClose}">
                                        <h4 class="modal-title">Enter current user password</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="input-group"><span class="input-group-text">Password</span><input class="form-control" type="password" data-oidis-bind="${this.tokenValidatePass}" autocomplete="off" /></div>
                                    </div>
                                    <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal" data-oidis-bind="${this.tokenDialogCloseButton}">Close</button><button class="btn btn-primary" type="button" data-oidis-bind="${this.tokenAccessValidate}">Validate</button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card" style="margin-top: 30px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-9">
                                <h4><svg class="bi bi-shield-lock" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 5px;">
                                        <path d="M5.338 1.59a61.44 61.44 0 0 0-2.837.856.481.481 0 0 0-.328.39c-.554 4.157.726 7.19 2.253 9.188a10.725 10.725 0 0 0 2.287 2.233c.346.244.652.42.893.533.12.057.218.095.293.118a.55.55 0 0 0 .101.025.615.615 0 0 0 .1-.025c.076-.023.174-.061.294-.118.24-.113.547-.29.893-.533a10.726 10.726 0 0 0 2.287-2.233c1.527-1.997 2.807-5.031 2.253-9.188a.48.48 0 0 0-.328-.39c-.651-.213-1.75-.56-2.837-.855C9.552 1.29 8.531 1.067 8 1.067c-.53 0-1.552.223-2.662.524zM5.072.56C6.157.265 7.31 0 8 0s1.843.265 2.928.56c1.11.3 2.229.655 2.887.87a1.54 1.54 0 0 1 1.044 1.262c.596 4.477-.787 7.795-2.465 9.99a11.775 11.775 0 0 1-2.517 2.453 7.159 7.159 0 0 1-1.048.625c-.28.132-.581.24-.829.24s-.548-.108-.829-.24a7.158 7.158 0 0 1-1.048-.625 11.777 11.777 0 0 1-2.517-2.453C1.928 10.487.545 7.169 1.141 2.692A1.54 1.54 0 0 1 2.185 1.43 62.456 62.456 0 0 1 5.072.56"></path>
                                        <path d="M9.5 6.5a1.5 1.5 0 0 1-1 1.415l.385 1.99a.5.5 0 0 1-.491.595h-.788a.5.5 0 0 1-.49-.595l.384-1.99a1.5 1.5 0 1 1 2-1.415z"></path>
                                    </svg>Two Factor Authentication</h4>
                                <h6 class="text-muted mb-2">Time based one time passwords for higher security</h6>
                            </div>
                            <div class="col-sm-3 col-xxl-3 align-self-start"></div>
                        </div>
                        <div class="row ${GeneralCSS.DNONE}" data-oidis-bind="${this.faIsInactive}">
                            <div class="col text-center">
                                <p>Install authenticator app to your mobile device and scan QRCode below. You can use for example <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank">Google Authenticator</a> or <a href="https://play.google.com/store/apps/details?id=com.azure.authenticator" target="_blank">Microsoft Authenticator</a>.</p>
                                <div class="row ${GeneralCSS.DNONE}" data-oidis-bind="${this.faQRCodeContent}">
                                    <div class="col"><canvas data-oidis-bind="${this.qrCode}" style="width: 250px;height: 250px;"></canvas>
                                        <p><a href="#/enterCode" data-oidis-bind="${this.enterCode}">Unable to scan?</a></p>
                                    </div>
                                </div>
                                <div class="row ${GeneralCSS.DNONE}" data-oidis-bind="${this.faCodeContent}">
                                    <div class="col">
                                        <h1 data-oidis-bind="${this.faCodeValue}">Auth code</h1>
                                        <p>Use this app configuration if available</p>
                                        <div class="row justify-content-center">
                                            <div class="col-auto">
                                                <ul style="width: 380px;text-align: left;">
                                                    <li>Type: TOTP (Time-based One-Time Password)</li>
                                                    <li>Algorithm: SHA1</li>
                                                    <li>Number of digits: 6</li>
                                                    <li>Interval: 30</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <p><a href="#/scanQRCode" data-oidis-bind="${this.scanQRCode}">Back to QR code</a></p>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-auto">
                                        <div class="input-group" style="width: 500px;"><span class="input-group-text">One time code</span><input class="form-control" type="text" placeholder="enter 6 digit code from app" data-oidis-bind="${this.verifyCode}" /><button class="btn btn-success" type="button" data-oidis-bind="${this.faActivate}">Verify and active</button></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ${GeneralCSS.DNONE}" data-oidis-bind="${this.faIsActive}">
                            <div class="col text-center">
                                <h5>Two factor authentication is already active for this account.</h5><button class="btn btn-danger" type="button" data-oidis-bind="${this.faDeactivate}">Deactive</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>${this.getOther()}</div>
    </form>
</section>
`;
    }

    protected getProfile() : string {
        return `
        <div class="input-group" style="margin-top: 5px;"><span class="input-group-text" style="width: 20%;">@</span><input class="form-control" type="email" data-oidis-bind="${this.profileMail}" /></div>
        <div class="input-group" style="margin-top: 5px;"><span class="input-group-text" style="width: 20%;">Name</span><input class="form-control" type="text" data-oidis-bind="${this.profileName}" /></div>
        <div class="input-group" style="margin-top: 5px;"><span class="input-group-text" style="width: 20%;">Phone</span><input class="form-control" type="text" data-oidis-bind="${this.profilePhone}" /></div>`;
    }

    protected getOther() : string {
        return "";
    }
}
