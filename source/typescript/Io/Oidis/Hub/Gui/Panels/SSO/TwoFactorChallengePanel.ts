/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";
import { AuthCodePanel } from "./AuthCodePanel.js";

export class TwoFactorChallengePanel extends BasePanel {
    public readonly twoFAContent : GuiCommons;
    public readonly authCodeContent : GuiCommons;
    public readonly codeValue : TextField;
    public readonly validate : Button;
    public readonly authCodePanel : AuthCodePanel;
    protected localization : ITwoFactorChallengePanelLocalization;
    private readonly goToPersonalCode : Button;
    private readonly backTo2FACode : Button;

    constructor() {
        super();

        this.twoFAContent = new GuiCommons();
        this.authCodeContent = new GuiCommons();
        this.codeValue = new TextField();
        this.validate = new Button();
        this.authCodePanel = new AuthCodePanel();
        this.goToPersonalCode = new Button();
        this.backTo2FACode = new Button();

        this.localization = {
            header          : "Two Factor Authentication",
            codeLocation    : "Please entry your Time based one time passwords from authenticator application",
            code            : "Code",
            validate        : "Validate",
            or              : "or",
            goToPersonalCode: "Enter personal one time code",
            backTo2FACode   : "Enter time based one time passwords"
        };
    }

    public ShowOnlyPersonalCode() : void {
        this.twoFAContent.Visible(false);
        this.authCodeContent.Visible(true);
        this.authCodePanel.Visible(true);
        this.backTo2FACode.Visible(false);
    }

    protected innerCode() : string {
        this.goToPersonalCode.getEvents().setOnClick(($args : EventArgs) : void => {
            $args.PreventDefault();
            this.twoFAContent.Visible(false);
            this.authCodeContent.Visible(true);
        });
        this.backTo2FACode.getEvents().setOnClick(($args : EventArgs) : void => {
            $args.PreventDefault();
            this.authCodeContent.Visible(false);
            this.twoFAContent.Visible(true);
        });
        this.getEvents().setOnComplete(() : void => {
            ValidationBinding.setOnlyNumbersAllowed(this.codeValue);
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<section>
    <form style="margin: auto;">
        <div class="row" data-oidis-bind="${this.twoFAContent}">
            <div class="col">
                <h4>${this.localization.header}</h4>
                <h6 class="text-muted card-subtitle mb-2">${this.localization.codeLocation}.</h6>
                <div class="row justify-content-center">
                    <div class="col-12 col-md-6">
                        <div class="input-group"><span class="input-group-text" style="width: 80px;">${this.localization.code}</span><input class="form-control" type="text" data-oidis-bind="${this.codeValue}" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.validate}">${this.localization.validate}</button></div>
                    </div>
                </div>
                <div class="row justify-content-center" style="margin-top: 20px;margin-bottom: 20px;">
                    <div class="col-3 col-md-2">
                        <hr style="margin-top: 23px;" />
                    </div>
                    <div class="col-3 col-md-1">
                        <p style="text-align: center;margin-top: 10px;color: #c8c9ca;">${this.localization.or}</p>
                    </div>
                    <div class="col-3 col-md-2">
                        <hr style="margin-top: 23px;" />
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center" style="margin-top: 10px;"><a href="#/goToPersonalCode" data-oidis-bind="${this.goToPersonalCode}">${this.localization.goToPersonalCode}</a></div>
                </div>
            </div>
        </div>
        <div class="row ${GeneralCSS.DNONE}" data-oidis-bind="${this.authCodeContent}">
            <div class="col">
                <div class="row" data-oidis-bind="${this.authCodePanel}"></div>
                <div class="row">
                    <div class="col text-center" style="margin-top: 10px;"><a href="#/backTo2FACode" data-oidis-bind="${this.backTo2FACode}">${this.localization.backTo2FACode}</a></div>
                </div>
            </div>
        </div>
    </form>
</section>`;
    }
}

export interface ITwoFactorChallengePanelLocalization {
    header : string;
    codeLocation : string;
    code : string;
    validate : string;
    or : string;
    goToPersonalCode : string;
    backTo2FACode : string;
}

// generated-code-start
/* eslint-disable */
export const ITwoFactorChallengePanelLocalization = globalThis.RegisterInterface(["header", "codeLocation", "code", "validate", "or", "goToPersonalCode", "backTo2FACode"]);
/* eslint-enable */
// generated-code-end
