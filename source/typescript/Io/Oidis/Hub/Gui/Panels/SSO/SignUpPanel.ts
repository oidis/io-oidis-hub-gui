/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";

export class SignUpPanel extends BasePanel {
    public readonly firstName : TextField;
    public readonly lastName : TextField;
    public readonly userName : TextField;
    public readonly email : TextField;
    public readonly password : TextField;
    public readonly signUp : Button;

    constructor() {
        super();

        this.firstName = new TextField();
        this.lastName = new TextField();
        this.userName = new TextField();
        this.email = new TextField();
        this.password = new TextField();
        this.signUp = new Button();
    }

    protected innerHtml() : string {
        return `
<section>
    <form style="margin: auto;">
        <div class="row" style="margin-bottom: 35px;">
            <div class="col">
                <div class="row">
                    <div class="col"><input type="text" class="form-control" placeholder="First name" style="margin-bottom: 20px;" data-oidis-bind="${this.firstName}" autocomplete="on" /></div>
                    <div class="col"><input type="text" class="form-control" placeholder="Last name" style="margin-bottom: 20px;" data-oidis-bind="${this.lastName}" autocomplete="on" /></div>
                </div><input type="text" class="form-control" name="username" placeholder="Username" style="margin-bottom: 20px;" data-oidis-bind="${this.userName}" autocomplete="on" /><input type="text" class="form-control" name="email" placeholder="E-mail" style="margin-bottom: 20px;" data-oidis-bind="${this.email}" autocomplete="on" inputmode="email" /><input type="password" class="form-control" name="current-password" placeholder="Password" style="margin-bottom: 0px;" data-oidis-bind="${this.password}" autocomplete="on" /><span style="font-size: 12px;">Minimum length is 8 characters.</span>
            </div>
        </div>
        <div class="row">
            <div class="col"><button class="btn btn-primary" type="button" style="width: 100%;" data-oidis-bind="${this.signUp}">Register</button></div>
        </div>
        <div class="row">
            <div class="col" style="margin-top: 5px;">
                <p style="font-size: 12px;">By clicking Register, I agree that I have read and accepted the Oidis <a href="#/terms">Terms of Use and Privacy Policy</a></p>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col text-center"><span>Already have login and password?</span><a href="#/signIn" style="margin-left: 10px;">Sign in</a></div>
        </div>
    </form>
</section>`;
    }
}
