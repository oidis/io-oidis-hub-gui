/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";

export class SignInPanel extends BasePanel {
    public readonly email : TextField;
    public readonly password : TextField;
    public readonly remember : CheckBox;
    public readonly signIn : Button;

    constructor() {
        super();

        this.email = new TextField();
        this.password = new TextField();
        this.remember = new CheckBox();
        this.signIn = new Button();
    }

    protected innerHtml() : string {
        return `
<section>
    <form style="margin: auto;">
        <div class="row" style="margin-bottom: 35px;">
            <div class="col"><input type="text" class="form-control" name="email" placeholder="E-mail" style="margin-bottom: 20px;" data-oidis-bind="${this.email}" autocomplete="on" inputmode="email" /><input type="password" class="form-control" name="current-password" placeholder="Password" style="margin-bottom: 20px;" data-oidis-bind="${this.password}" autocomplete="on" />
                <div class="row">
                    <div class="col">
                        <div class="form-check"><input type="checkbox" class="form-check-input" data-oidis-bind="${this.remember}" /><label class="form-check-label" for="formCheck-1">Remember me</label></div>
                    </div>
                    <div class="col" style="text-align: right;"><a href="#/resetPassword" style="margin-right: 10px;">Forgot your password?</a></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col"><button class="btn btn-primary" type="button" style="width: 100%;" data-oidis-bind="${this.signIn}">Sign in</button></div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col text-center"><span>Don&#39;t have an account yet?</span><a href="#/signUp" style="margin-left: 10px;">Register now</a></div>
        </div>
    </form>
</section>`;
    }
}
