/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";

export class ResetPasswordPanel extends BasePanel {
    public readonly email : TextField;
    public readonly reset : Button;

    constructor() {
        super();

        this.email = new TextField();
        this.reset = new Button();
    }

    protected innerHtml() : string {
        return `
<section>
    <form style="margin: auto;">
        <div class="row" style="margin-bottom: 35px;">
            <div class="col"><input type="text" class="form-control" name="email" placeholder="E-mail" style="margin-bottom: 5px;" data-oidis-bind="${this.email}" autocomplete="on" /><span style="font-size: 12px;">Requires your primary Oidis email address.</span></div>
        </div>
        <div class="row">
            <div class="col"><button class="btn btn-primary" type="button" style="width: 100%;" data-oidis-bind="${this.reset}">Reset password</button></div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col text-center"><span>Already have login and password?</span><a href="#/signIn" style="margin-left: 10px;">Sign in</a></div>
        </div>
    </form>
</section>`;
    }
}
