/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ValidationBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/ValidationBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { CheckBox } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/CheckBox.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";

export class AuthCodePanel extends BasePanel {
    public readonly code1 : TextField;
    public readonly code2 : TextField;
    public readonly code3 : TextField;
    public readonly code4 : TextField;
    public readonly codeError : GuiCommons;
    public readonly emailTemplate : Label;
    public readonly emailValid : TextField;
    public readonly phoneTemplate : Label;
    public readonly phoneValid : TextField;
    public readonly newCodeRecipient : TextField;
    public readonly sendNewCode : Button;
    public readonly validationError : GuiCommons;
    public readonly codeSendSuccess : GuiCommons;
    public readonly sendToRecipient : CheckBox;
    public readonly orContent : GuiCommons;
    protected localization : IAuthCodePanelLocalization;
    private readonly getNewCode : GuiCommons;
    private readonly codeEntryContent : GuiCommons;
    private readonly newCodeContent : GuiCommons;
    private readonly backToEntry : GuiCommons;
    private readonly newRecipientContent : GuiCommons;

    constructor() {
        super();

        this.code1 = new TextField();
        this.code2 = new TextField();
        this.code3 = new TextField();
        this.code4 = new TextField();
        this.codeError = new GuiCommons();
        this.emailTemplate = new Label();
        this.emailValid = new TextField();
        this.phoneTemplate = new Label();
        this.phoneValid = new TextField();
        this.newCodeRecipient = new TextField();
        this.sendNewCode = new Button();
        this.getNewCode = new GuiCommons();
        this.codeEntryContent = new GuiCommons();
        this.newCodeContent = new GuiCommons();
        this.backToEntry = new GuiCommons();
        this.sendToRecipient = new CheckBox();
        this.newRecipientContent = new GuiCommons();
        this.validationError = new GuiCommons();
        this.codeSendSuccess = new GuiCommons();
        this.orContent = new GuiCommons();
        this.localization = {
            header         : "Personal authentication code",
            codeLocation   : "Please entry your personal authentication code received in email with subject " +
                "&quot;Oidis: personal authentication code&quot; or create request for",
            getNewCode     : "send of new code",
            codeError      : "Incorrect code",
            newCodeInfo    : "Send of new personal authentication code is protected by validation of owner information. " +
                "Validation is done by entering of correct info based on provided pattern below.",
            email          : "E-mail",
            or             : "or",
            phone          : "Phone",
            recipientHeader: "E-mail where new code should be send:",
            sendToRecipient: "Send to owner's e-mail",
            sendNewCode    : "Send",
            validationError: "Verification failed",
            codeSendSuccess: "New code has been successfully send",
            backToEntry    : "Back to code"
        };
    }

    protected innerCode() : string {
        this.getNewCode.getEvents().setOnClick(($args : EventArgs) : void => {
            $args.PreventDefault();
            this.codeEntryContent.Visible(false);
            this.newCodeContent.Visible(true);
        });
        this.backToEntry.getEvents().setOnClick(($args : EventArgs) : void => {
            $args.PreventDefault();
            this.newCodeContent.Visible(false);
            this.codeEntryContent.Visible(true);
        });
        this.sendToRecipient.getEvents().setOnClick(() : void => {
            this.newRecipientContent.Visible(!this.sendToRecipient.Value());
        });
        this.getEvents().setOnComplete(() : void => {
            ValidationBinding.setOnlyPhoneFormat(this.phoneValid);
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="row">
        <div class="col">
            <h4>${this.localization.header}</h4>
            <h6 class="text-muted card-subtitle mb-2">${this.localization.codeLocation} <a href="#/newCode" data-oidis-bind="${this.getNewCode}">${this.localization.getNewCode}</a>.</h6>
            <div class="row" data-oidis-bind="${this.codeEntryContent}">
                <div class="col">
                    <div class="row Code" style="margin: auto;margin-bottom: 15px;margin-top: 25px;">
                        <div class="col" style="text-align: center;"><input type="text" style="width: 50px;height: 50px;border-radius: 6px;border: 1px solid var(--bs-gray);text-align: center;font-size: 30px;" data-oidis-bind="${this.code1}" placeholder="#" /></div>
                        <div class="col" style="text-align: center;"><input type="text" style="width: 50px;height: 50px;border-radius: 6px;border: 1px solid var(--bs-gray);text-align: center;font-size: 30px;" data-oidis-bind="${this.code2}" placeholder="#" /></div>
                        <div class="col" style="text-align: center;"><input type="text" style="width: 50px;height: 50px;border-radius: 6px;border: 1px solid var(--bs-gray);text-align: center;font-size: 30px;" data-oidis-bind="${this.code3}" placeholder="#" /></div>
                        <div class="col" style="text-align: center;"><input type="text" style="width: 50px;height: 50px;border-radius: 6px;border: 1px solid var(--bs-gray);text-align: center;font-size: 30px;" data-oidis-bind="${this.code4}" placeholder="#" /></div>
                    </div>
                    <div class="alert alert-danger ${GeneralCSS.DNONE}" role="alert" style="text-align: center;font-weight: bold;font-size: 22px;padding: 10px;" data-oidis-bind="${this.codeError}"><span>${this.localization.codeError}</span></div>
                </div>
            </div>
            <div class="row ${GeneralCSS.DNONE}" data-oidis-bind="${this.newCodeContent}">
                <div class="col">
                    <div class="card" style="margin-bottom: 15px;">
                        <div class="card-body">
                            <p class="card-text" style="font-size: 13px;"><svg class="bi bi-info-circle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-bottom: 2px;margin-right: 10px;">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"></path>
                                    <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0"></path>
                                </svg>${this.localization.newCodeInfo}</p>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6" style="text-align: center;"><span data-oidis-bind="${this.emailTemplate}" style="font-weight: bold;font-size: 20px;"></span>
                            <div class="input-group" style="margin-top: 5px;"><span class="input-group-text" style="width: 80px;">${this.localization.email}</span><input class="form-control" type="text" data-oidis-bind="${this.emailValid}" /></div>
                        </div>
                    </div>
                    <div class="row" data-oidis-bind="${this.orContent}">
                        <div class="col">
                            <div class="row justify-content-center" style="margin-top: 20px;margin-bottom: 20px;">
                                <div class="col-3 col-md-2">
                                    <hr style="margin-top: 23px;" />
                                </div>
                                <div class="col-3 col-md-1">
                                    <p style="text-align: center;margin-top: 10px;color: #c8c9ca;">${this.localization.or}</p>
                                </div>
                                <div class="col-3 col-md-2">
                                    <hr style="margin-top: 23px;" />
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-6" style="text-align: center;"><span data-oidis-bind="${this.phoneTemplate}" style="font-weight: bold;font-size: 20px;"></span>
                                    <div class="input-group" style="margin-top: 5px;"><span class="input-group-text" style="width: 80px;">${this.localization.phone}</span><input class="form-control" type="text" data-oidis-bind="${this.phoneValid}" /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center" style="margin-top: 30px;">
                        <div class="col-12 col-md-6">
                            <p style="text-decoration: underline;">${this.localization.recipientHeader}</p>
                            <div class="form-check"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.sendToRecipient}" checked /><label class="form-check-label">${this.localization.sendToRecipient}</label></div>
                            <div class="input-group ${GeneralCSS.DNONE}" data-oidis-bind="${this.newRecipientContent}"><span class="input-group-text" style="width: 80px;">${this.localization.email}</span><input class="form-control" type="text" data-oidis-bind="${this.newCodeRecipient}" /></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-center" style="margin-top: 10px;"><button class="btn btn-primary" type="button" data-oidis-bind="${this.sendNewCode}"><svg class="bi bi-send" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;">
                                    <path d="M15.854.146a.5.5 0 0 1 .11.54l-5.819 14.547a.75.75 0 0 1-1.329.124l-3.178-4.995L.643 7.184a.75.75 0 0 1 .124-1.33L15.314.037a.5.5 0 0 1 .54.11ZM6.636 10.07l2.761 4.338L14.13 2.576zm6.787-8.201L1.591 6.602l4.339 2.76 7.494-7.493Z"></path>
                                </svg>${this.localization.sendNewCode}</button>
                            <div class="alert alert-danger ${GeneralCSS.DNONE}" role="alert" style="text-align: center;font-size: 22px;padding: 0px;height: 38px;margin: 0px;" data-oidis-bind="${this.validationError}"><span style="font-size: 16px;">${this.localization.validationError}</span></div>
                            <div class="alert alert-success ${GeneralCSS.DNONE}" role="alert" style="text-align: center;font-size: 22px;padding: 0px;height: 38px;margin: 0px;" data-oidis-bind="${this.codeSendSuccess}"><span style="font-size: 16px;">${this.localization.codeSendSuccess}</span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-center" style="margin-top: 10px;"><a href="#/backToCode" data-oidis-bind="${this.backToEntry}">${this.localization.backToEntry}</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>`;
    }
}

export interface IAuthCodePanelLocalization {
    header : string;
    codeLocation : string;
    getNewCode : string;
    codeError : string;
    newCodeInfo : string;
    email : string;
    or : string;
    phone : string;
    recipientHeader : string;
    sendToRecipient : string;
    sendNewCode : string;
    validationError : string;
    codeSendSuccess : string;
    backToEntry : string;
}

// generated-code-start
/* eslint-disable */
export const IAuthCodePanelLocalization = globalThis.RegisterInterface(["header", "codeLocation", "getNewCode", "codeError", "newCodeInfo", "email", "or", "phone", "recipientHeader", "sendToRecipient", "sendNewCode", "validationError", "codeSendSuccess", "backToEntry"]);
/* eslint-enable */
// generated-code-end
