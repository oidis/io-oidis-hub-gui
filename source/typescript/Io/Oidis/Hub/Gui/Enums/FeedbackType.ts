/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export abstract class FeedbackType extends BaseEnum {
    public static readonly General : number = 0;
    public static readonly Registry : number = 1;
    public static readonly Agents : number = 2;
    public static readonly Logs : number = 3;
    public static readonly Email : number = 4;
    public static readonly Permissions : number = 5;
}
