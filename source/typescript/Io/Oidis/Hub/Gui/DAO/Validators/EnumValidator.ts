/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { RegexValidator } from "./RegexValidator.js";

export class EnumValidator extends RegexValidator {

    private static convertEnumToRegex($enum : any) : RegExp {
        return new RegExp(Object.values($enum).map(($value) => "(" + $value + ")").join("|"), "g");
    }

    constructor($enum : any) {
        super(EnumValidator.convertEnumToRegex($enum));
    }
}
