/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { PropertyValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/PropertyValidator.js";

export class RangeValidator<T> extends PropertyValidator {
    private readonly allowEmpty : boolean;
    private readonly min : T;
    private readonly max : T;

    constructor($min : T, $max : T, $allowEmpty? : boolean) {
        super();
        this.min = $min;
        this.max = $max;
        if (this.min >= this.max) {
            throw new Error("Range validator limits not matching condition: " + this.min + " < " + this.max);
        }
        this.allowEmpty = true;
        if (!ObjectValidator.IsEmptyOrNull($allowEmpty)) {
            this.allowEmpty = $allowEmpty;
        }
    }

    public validate($value : T) : boolean {
        if (ObjectValidator.IsEmptyOrNull($value) && this.allowEmpty) {
            return true;
        }
        return this.min <= $value && $value <= this.max;
    }
}
