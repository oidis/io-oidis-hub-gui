/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { RegexValidator } from "./RegexValidator.js";

export class PhoneValidator extends RegexValidator {
    constructor() {
        super(/^[+]?[(]?\d{3}[)]?[-\s.]?\d{3}[-\s.]?\d{3,6}$/i);
    }

    public validate($value : string) : boolean {
        let value : string = "";
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            value = $value.replace(/[\s-]+/gm, "").trim();
        }
        return super.validate(value);
    }
}
