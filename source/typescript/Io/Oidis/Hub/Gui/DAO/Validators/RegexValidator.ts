/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IValidator } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IValidator.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";

export class RegexValidator extends BaseObject implements IValidator {
    private readonly regex : RegExp;
    private readonly allowEmpty : boolean;

    constructor($regex : string | RegExp, $allowEmpty? : boolean) {
        super();

        this.regex = new RegExp($regex);
        this.allowEmpty = true;
        if (!ObjectValidator.IsEmptyOrNull($allowEmpty)) {
            this.allowEmpty = $allowEmpty;
        }
    }

    public validate($value : string) : boolean {
        this.regex.lastIndex = 0;
        if (ObjectValidator.IsEmptyOrNull(this.regex)) {
            throw new Error("Regex validator miss regex expression.");
        }
        if (this.allowEmpty && ObjectValidator.IsEmptyOrNull($value)) {
            return true;
        }
        return this.regex.test($value);
    }
}
