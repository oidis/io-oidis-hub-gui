/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { RegexValidator } from "./RegexValidator.js";

export class PasswordValidator extends RegexValidator {
    constructor() {
        // upper, lower, number, special character, at least 8 characters, max 30 characters
        super("^(?=.*[0-9])(?=.*[- ?!@#$%^&*\\/\\\\])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9- ?!@#$%^&*\\/\\\\]{8,30}$");
    }
}
