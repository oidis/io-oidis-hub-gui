/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseDAO } from "@io-oidis-services/Io/Oidis/Services/DAO/BaseDAO.js";
import { IBaseConfiguration } from "@io-oidis-services/Io/Oidis/Services/Interfaces/DAO/IBaseConfiguration.js";

export class FAQDao extends BaseDAO {
    public static defaultConfigurationPath : string = "resource/data/Io/Oidis/Hub/Localization/FAQ.jsonp";

    public getStaticConfiguration() : IFAQDao {
        return <IFAQDao>super.getStaticConfiguration();
    }
}

export interface IFAQDao extends IBaseConfiguration {
    topics : IFAQTopic[];
}

export interface IFAQTopic {
    what : string;
    how : string;
}

// generated-code-start
export const IFAQDao = globalThis.RegisterInterface(["topics"], <any>IBaseConfiguration);
export const IFAQTopic = globalThis.RegisterInterface(["what", "how"]);
// generated-code-end
