/*! ******************************************************************************************************** *
 *
 * Copyright 2023 NXP
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Entity, Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";
import { Sendmail } from "./Sendmail.js";

@Entity("appSettings")
export class AppSettings extends BaseModel {
    @Property({type: String})
    public declare Version : string;

    @Property({type: Sendmail})
    public declare Sendmail : Sendmail;

    @Property({type: [String], default: []})
    public declare DefaultAccessibleMethods : string[];

    @Property({type: [String], default: []})
    public declare DefaultGroupAccess : string[];
}
