/*! ******************************************************************************************************** *
 *
 * Copyright 2023 NXP
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseModel as Parent } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";

/**
 * @deprecated Use BaseModel from Commons namespace
 */
export class BaseModel extends Parent {
    // Fallback mapping
}
