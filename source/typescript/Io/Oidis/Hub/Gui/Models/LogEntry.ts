/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";

export class LogEntry extends BaseModel {

    @Property({type: String})
    public declare User : string;

    @Property({type: String})
    public declare TraceBack : string;

    @Property({type: String})
    public declare OnBehalf : string;

    @Property({type: String})
    public declare Message : string;

    @Property({type: [String]})
    public declare Tags : string[];
}
