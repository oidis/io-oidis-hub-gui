/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseDAO } from "@io-oidis-services/Io/Oidis/Services/DAO/BaseDAO.js";
import { IBaseConfiguration } from "@io-oidis-services/Io/Oidis/Services/Interfaces/DAO/IBaseConfiguration.js";

export class WhatsNewDao extends BaseDAO {
    public static defaultConfigurationPath : string = "resource/data/Io/Oidis/Hub/Localization/ReleaseNotes.jsonp";

    public getStaticConfiguration() : IWhatsNewDao {
        return <IWhatsNewDao>super.getStaticConfiguration();
    }
}

export interface IWhatsNewDao extends IBaseConfiguration {
    forceShow : boolean;
    notes : IWhatsNewNote[];
}

export interface IWhatsNewNote {
    date : string;
    news : IWhatsNewNoteBlock[];
    admins : IWhatsNewNoteBlock[];
    issues : IWhatsNewNoteBlock[];
}

export interface IWhatsNewNoteBlock {
    header? : string;
    notes : string[];
}

// generated-code-start
export const IWhatsNewDao = globalThis.RegisterInterface(["forceShow", "notes"], <any>IBaseConfiguration);
export const IWhatsNewNote = globalThis.RegisterInterface(["date", "news", "admins", "issues"]);
export const IWhatsNewNoteBlock = globalThis.RegisterInterface(["header", "notes"]);
// generated-code-end
