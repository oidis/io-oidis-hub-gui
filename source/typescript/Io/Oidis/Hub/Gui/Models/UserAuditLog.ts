/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";

export class UserAuditLog extends BaseModel {
    @Property({type: Date})
    public declare LastActivationNotice : Date;

    @Property({type: Number})
    public declare ActivationNoticeCounter : number;

    @Property({type: Date})
    public declare LastResetAction : Date;

    @Property({type: Date})
    public declare LastLogIn : Date;

    @Property({type: Number})
    public declare LogInAttempts : number;
}
