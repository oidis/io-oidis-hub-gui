/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";
import { Group } from "./Group.js";

export class Organization extends BaseModel {
    @Property({type: String})
    public declare Name : string;

    @Property({type: [Group], default: []})
    public declare Groups : Group[];

    @Property({type: Buffer})
    public declare Image : Buffer;

    @Property({type: Boolean, default: true})
    public declare Activated : boolean;
}
