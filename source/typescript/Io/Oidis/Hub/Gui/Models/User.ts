/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { InclusionConverter } from "@io-oidis-commons/Io/Oidis/Commons/DAO/Converters/InclusionConverter.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { EmailValidator } from "../DAO/Validators/EmailValidator.js";
import { PhoneValidator } from "../DAO/Validators/PhoneValidator.js";
import { AuthCode } from "./AuthCode.js";
import { AuthToken } from "./AuthToken.js";
import { BaseModel } from "./BaseModel.js";
import { Group } from "./Group.js";
import { SSOTokens } from "./SSOTokens.js";
import { UserAuditLog } from "./UserAuditLog.js";

export class User extends BaseModel {
    @Property({type: String})
    public declare UserName : string;

    @Property({type: String, required: false})
    public declare Firstname : string;

    @Property({type: String, required: false})
    public declare Lastname : string;

    @Property({type: String, required: false, validator: new EmailValidator()})
    public declare Email : string;

    @Property({type: String, required: false, validator: new PhoneValidator()})
    public declare Phone : string;

    @Property({type: [String], default: []})
    public declare AltEmails : string[];

    @Property({type: String})
    public declare Password : string;

    @Property({type: [Group], default: []})
    public declare Groups : Group[];

    @Property({type: Group})
    public declare DefaultGroup : Group;

    @Property({type: [AuthToken], default: []})
    public declare AuthTokens : AuthToken[];

    @Property({type: Buffer, required: false})
    public declare Image : Buffer;

    @Property({type: SSOTokens, symbol: "ssoTokens"})
    public declare SSOTokens : SSOTokens;

    @Property({type: UserAuditLog})
    public declare AuditLog : UserAuditLog;

    @Property({type: Boolean, required: false, default: true})
    public declare Activated : boolean;

    @Property({type: String, required: false})
    public declare TwoFASecret : string;

    @Property({type: AuthCode, converter: new InclusionConverter(AuthCode), required: false})
    public declare AuthCode : AuthCode;

    @Property({type: String, required: false})
    public declare WhatsNewSeen : string;
}
