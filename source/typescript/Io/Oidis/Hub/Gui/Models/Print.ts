/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { PrintType } from "../Enums/PrintType.js";
import { BaseModel } from "./BaseModel.js";

export class Print extends BaseModel {
    @Property({type: Number, default: 210})
    public declare Width : number;

    @Property({type: Number, default: 297})
    public declare Height : number;

    @Property({type: Number, default: 10})
    public declare Padding : number;

    @Property({type: Number})
    public declare Limit : number;

    @Property({type: Number, default: 0})
    public declare Offset : number;

    @Property({type: Number, default: 2})
    public declare Columns : number;

    @Property({type: Number, default: 4})
    public declare Rows : number;

    @Property({type: Boolean, default: true})
    public declare SaveAsFile : boolean;

    @Property({type: Number, default: PrintType.A4})
    public declare Format : number;

    @Property({type: Boolean, default: false})
    public declare Grid : boolean;

    constructor() {
        super();

        this.Limit = this.Rows * this.Columns;
    }
}
