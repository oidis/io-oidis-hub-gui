/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";

export class AuthToken extends BaseModel {
    @Property({type: String})
    public declare Name : string;

    @Property({type: String})
    public declare Token : string;

    @Property({type: Number})
    public declare Date : number;

    @Property({type: Number})
    public declare Expire : number;
}
