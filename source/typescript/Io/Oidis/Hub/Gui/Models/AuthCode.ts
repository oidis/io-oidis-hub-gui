/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseModel } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { Entity, Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";

@Entity("authCode")
export class AuthCode extends BaseModel<AuthCode> {
    @Property({type: Boolean, default: false})
    public declare Send : boolean;

    @Property({type: String, required: false})
    public declare Code : string;

    @Property({type: Number, default: 0})
    public declare ValidationAttempts : number;

    @Property({type: Date, required: false})
    public declare LastValidation : Date;

    @Property({type: Number, default: 0})
    public declare RequestAttempts : number;

    @Property({type: Date, required: false})
    public declare LastRequest : Date;

    @Property({type: Number, default: 4})
    public declare Size : number;
}
