/*! ******************************************************************************************************** *
 *
 * Copyright 2023 NXP
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";

export class Sendmail extends BaseModel {
    @Property({type: Boolean})
    public declare Enabled : boolean;

    @Property({type: [String], default: []})
    public declare AdminsList : string[];

    @Property({type: [String], default: []})
    public declare BlackList : string[];
}
