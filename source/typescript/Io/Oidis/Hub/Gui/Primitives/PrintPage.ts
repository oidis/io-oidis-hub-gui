/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class PrintPage extends GuiCommons {
    private height : number;
    private width : number;
    private padding : number;

    constructor() {
        super();
        this.height = 297;
        this.width = 210;
        this.padding = 5;
    }

    public Height($value? : number) : number {
        return this.height = Property.PositiveInteger(this.height, $value);
    }

    public Width($value? : number) : number {
        return this.width = Property.PositiveInteger(this.width, $value);
    }

    public Padding($value? : number) : number {
        return this.padding = Property.PositiveInteger(this.padding, $value);
    }

    protected innerHtml() : string {
        return `
<section class="${this.getClassNameWithoutNamespace()}" style="height: calc(${this.height}mm - 2px); width: ${this.width}mm; padding: 0; margin: 0; overflow: hidden;">
    <div data-oidis-bind="${this}" style="height: 100%; width: 100%; padding: ${this.padding}mm;"></div>
    <div style="page-break-after: always; height: 0; margin: 0;"></div>
</section>`;
    }
}
