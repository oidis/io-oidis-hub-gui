/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { User } from "../Models/User.js";
import { MenuItemScope } from "../Panels/Dashboard/MenuPanel.js";
import { MenuItem } from "../UserControls/MenuItem.js";
import { ITabDescriptor, PageTabIds } from "./BasePageLayout.js";
import { BasePageLayoutController } from "./BasePageLayoutController.js";
import { MenuPageLayout } from "./MenuPageLayout.js";

export abstract class MenuPageLayoutController<Page extends MenuPageLayout> extends BasePageLayoutController<Page> {

    protected async afterLoad($instance : Page) : Promise<void> {
        await super.afterLoad($instance);

        $instance.footerPanel.hubInfo.Text("" +
            "version: " + this.currentVersion +
            ", build: " + this.getEnvironmentArgs().getBuildTime() +
            ", " + this.getEnvironmentArgs().getProjectConfig().target.copyright);

        $instance.headerPanel.help.Visible(false);

        $instance.headerPanel.logo.getEvents().setOnClick(() : void => {
            this.reloadToDefaultResolver();
        });
        $instance.headerPanel.login.getEvents().setOnClick(() : void => {
            this.getHttpManager().ReloadTo("/signIn");
        });
        $instance.headerPanel.signOut.getEvents().setOnClick(() : void => {
            this.getHttpManager().ReloadTo("/signOut", null, true);
        });

        $instance.menuPanel.show.getEvents().setOnClick(() : void => {
            this.persistence.Variable("MenuShow", true);
        });
        $instance.menuPanel.hide.getEvents().setOnClick(() : void => {
            this.persistence.Variable("MenuShow", false);
        });
        $instance.menuPanel.getEvents().setOnClick(() : void => {
            if (this.withMobileMenu()) {
                $instance.ShowMenu(false);
            }
        });
    }

    protected async afterTabsRegister($instance : Page) : Promise<void> {
        let withMobileOnlyItems : boolean = false;
        let withSysAdminItems : boolean = false;
        let withAdminItems : boolean = false;
        for await (const tab of $instance.getTabsList()) {
            if (tab.enabled) {
                if (tab.scope === MenuItemScope.MOBILE_ONLY) {
                    withMobileOnlyItems = true;
                }
                if (tab.scope === MenuItemScope.SYS_ADMINS) {
                    withSysAdminItems = true;
                }
                if (tab.scope === MenuItemScope.ADMINS) {
                    withAdminItems = true;
                }
            }
        }
        if (!withMobileOnlyItems) {
            ElementManager.setCssProperty(this.InstanceOwner().menuPanel.mobileOnly.InstanceOwner(), "display", "none");
        }
        if (this.user.IsAuthenticated) {
            $instance.menuPanel.sysAdminOnly.Visible((this.user.IsAdmin || this.user.IsSysAdmin) && withSysAdminItems);
            $instance.menuPanel.adminOnly.Visible(this.user.IsAdmin && withAdminItems);
        }
    }

    protected async beforeTabLoad($instance : Page) : Promise<void> {
        await super.beforeTabLoad($instance);

        if (this.user.IsAuthenticated) {
            $instance.headerPanel.user.Visible(true);
            $instance.headerPanel.login.Visible(false);
            const user : User = this.user.getUserProfile();
            $instance.headerPanel.settings.Content(ObjectValidator.IsEmptyOrNull(user.Email) ? user.UserName : user.Email);
            $instance.headerPanel.userImage.Visible(false);
            $instance.headerPanel.userAvatar.Visible(true);
            if (!ObjectValidator.IsEmptyOrNull(user.Image)) {
                $instance.headerPanel.userAvatar.Visible(false);
                $instance.headerPanel.userImage.Source(<any>user.Image);
                $instance.headerPanel.userImage.Visible(true);
            }

            if (this.user.IsActive) {
                if (!ObjectValidator.IsEmptyOrNull($instance.getTabConfig(PageTabIds.FAQ)) &&
                    $instance.getTabConfig(PageTabIds.FAQ).enabled) {
                    $instance.headerPanel.help.Visible(true);
                }
            }
        } else {
            $instance.headerPanel.help.Visible(false);
            $instance.headerPanel.user.Visible(false);
            $instance.headerPanel.login.Visible(true);
            $instance.headerPanel.settings.Content("user@domain");
        }
    }

    protected getReleaseNotesOpenElements() : Button[] {
        const instance : Page = this.InstanceOwner();
        return [instance.headerPanel.releaseNotes, instance.menuPanel.releaseNotes];
    }

    protected getNotificationsOpenElements() : Button[] {
        const instance : Page = this.InstanceOwner();
        return [instance.headerPanel.notifications, instance.menuPanel.notifications];
    }

    protected notificationsCounterHandler($count : number) : void {
        const instance : Page = this.InstanceOwner();
        if ($count > 0) {
            instance.headerPanel.notificationsCount.Text($count + "");
            instance.headerPanel.notificationsCount.Visible(true);
            instance.headerPanel.notificationsAlert.Visible(true);
        } else {
            instance.headerPanel.notificationsCount.Visible(false);
            instance.headerPanel.notificationsAlert.Visible(false);
        }
    }

    protected getTabMenuInstance($tab : ITabDescriptor) : any {
        const instance : Page = this.InstanceOwner();
        const menuItem : MenuItem = instance.menuPanel.AddItem($tab.name, $tab.link, $tab.icon, $tab.scope);
        instance.menuPanel[!ObjectValidator.IsEmptyOrNull($tab.declaredBy) ? $tab.declaredBy : $tab.id] = menuItem;
        return menuItem;
    }

    protected async selectTab($instance : BasePanel) : Promise<void> {
        const instance : Page = this.InstanceOwner();
        await super.selectTab($instance);

        if (this.user.IsAuthenticated && this.user.IsActive) {
            instance.menuContent.Visible(true);
        } else {
            instance.menuContent.Visible(false);
        }
        instance.menuContent.Visible(true);
        instance.headerPanel.menuToggler.Visible(instance.menuContent.Visible());
        if (instance.menuContent.Visible()) {
            instance.pageContent.InstanceOwner().classList.remove("col-xl-10");

            if (this.persistence.Variable("MenuShow") === true && !this.withMobileMenu()) {
                instance.ShowMenu(true);
            }
        } else {
            instance.pageContent.InstanceOwner().classList.add("col-xl-10");
        }
    }

    private withMobileMenu() : boolean {
        return window.getComputedStyle(this.InstanceOwner().headerPanel.menuToggler.InstanceOwner()).display !== "none";
    }
}
