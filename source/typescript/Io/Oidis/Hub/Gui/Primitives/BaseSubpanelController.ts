/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { Loader } from "../Loader.js";
import { UserContext } from "../Structures/UserContext.js";
import { BasePanelController } from "./BasePanelController.js";

export class BaseSubpanelController<Panel extends BasePanel, Parent extends BasePanel, Owner extends BasePanelController>
    extends BaseObject {
    protected readonly instance : Panel;
    protected readonly parent : Parent;
    protected readonly owner : Owner;
    protected readonly context : UserContext;

    constructor($instance : Panel, $owner : Owner) {
        super();
        this.context = Loader.getInstance().getContext();
        this.instance = $instance;
        this.parent = <Parent>$instance.Parent();
        this.owner = $owner;
    }
}
