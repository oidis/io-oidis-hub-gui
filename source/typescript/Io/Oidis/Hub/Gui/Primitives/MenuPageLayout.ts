/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { FooterPanel } from "../Panels/Dashboard/FooterPanel.js";
import { HeaderPanel } from "../Panels/Dashboard/HeaderPanel.js";
import { MenuPanel } from "../Panels/Dashboard/MenuPanel.js";
import { BasePageLayout } from "./BasePageLayout.js";

export abstract class MenuPageLayout extends BasePageLayout {
    public readonly headerPanel : HeaderPanel;
    public readonly footerPanel : FooterPanel;
    public readonly menuContent : GuiCommons;
    public readonly menuPanel : MenuPanel;
    public readonly pageContent : GuiCommons;
    public readonly scrollBody : GuiCommons;
    protected readonly headerHeight : number;
    private menuShown : boolean;

    constructor() {
        super();

        this.headerPanel = new HeaderPanel();
        this.footerPanel = new FooterPanel();
        this.menuContent = new GuiCommons();
        this.menuPanel = new MenuPanel();
        this.pageContent = new GuiCommons();
        this.scrollBody = new GuiCommons();
        this.headerHeight = 70;
        this.menuShown = false;
    }

    public ShowMenu($status : boolean) : void {
        if ($status) {
            this.menuContent.InstanceOwner().classList.add("Show");
            this.headerPanel.menuToggler.InstanceOwner().classList.add("Close");
            this.menuShown = true;
        } else {
            this.menuContent.InstanceOwner().classList.remove("Show");
            this.headerPanel.menuToggler.InstanceOwner().classList.remove("Close");
            this.menuShown = false;
        }
    }

    protected innerCode() : string {
        this.headerPanel.menuToggler.getEvents().setOnClick(() : void => {
            this.ShowMenu(!this.menuShown);
        });
        this.menuPanel.show.getEvents().setOnClick(() : void => {
            this.ShowMenu(true);
        });
        this.menuPanel.hide.getEvents().setOnClick(() : void => {
            this.ShowMenu(false);
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<section class="ScreenSection" data-oidis-bind="${this.screenSection}">
    <div style="display: flex;flex-direction: column;height: 100%;">
        <div class="HeaderContainer" style="display: flex;flex-direction: column;height: ${this.headerHeight}px;">
            <div class="row" data-oidis-bind="${this.headerPanel}" style="margin: 0;"></div>
        </div>
        <div style="display: flex;height: calc(100% - ${this.headerHeight}px);">
            <div class="MenuContainer ${GeneralCSS.DNONE}" data-oidis-bind="${this.menuContent}">
                <div style="position: absolute;height: calc(100% - ${this.headerHeight}px);" data-oidis-bind="${this.menuPanel}"></div>
            </div>
            <div style="display: flex;flex: 1;flex-direction: column;overflow: auto;">
                <div class="row text-dark justify-content-xl-end ${GeneralCSS.DNONE}" data-oidis-bind="${this.headerStrip}" style="background: var(--bs-yellow);margin: 0;">
                    <div class="col text-center"><span> </span></div>
                </div>
                <div style="display: flex;flex: 1;overflow: auto;" data-oidis-bind="${this.scrollBody}">
                    <div style="width: 100%;display: flex;flex-direction: column;">
                        <div class="row flex-fill" style="margin: 0;">
                            <div class="col" style="height: 100%;display: flex;flex-direction: column;">
                                <div class="row justify-content-center" style="min-height: 500px;" data-oidis-bind="${this.tabsContent}">
                                    <div class="col-xl-10 col" style="margin-bottom: 20px;margin-top: 15px;" data-oidis-bind="${this.pageContent}">
                                        <div class="row" data-oidis-bind="${this.systemState}"></div>
                                        <div class="row" data-oidis-bind="${this.panelsContainer}">${this.getTabContent()}</div>
                                    </div>
                                </div>
                                <div class="row" data-oidis-bind="${this.skeletonLoader}"></div>
                            </div>
                        </div>
                        <div class="row" data-oidis-bind="${this.footerPanel}" style="background: #0E2333;margin: 0;"></div>
                        <div class="toast-container position-fixed bottom-0 end-0 p-3" style="z-index: 11;" data-oidis-bind="${this.toasts}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-oidis-bind="${this.releaseNotesDialog}"></div>
    <div data-oidis-bind="${this.notificationsDialog}"></div>
    <div data-oidis-bind="${this.backdoorLoginDialog}"></div>
</section>
<section class="HiddenElement PrintSection" data-oidis-bind="${this.printSection}"></section>`;
    }
}
