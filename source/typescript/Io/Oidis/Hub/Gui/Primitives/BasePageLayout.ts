/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { EventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/EventsManager.js";
import { Dialog } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Dialog.js";
import { Toast, ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { BackdoorLoginDialog } from "../Dialogs/BackdoorLoginDialog.js";
import { NotificationsDialog } from "../Dialogs/NotificationsDialog.js";
import { ReleaseNotesDialog } from "../Dialogs/ReleaseNotesDialog.js";
import { MenuItemScope } from "../Panels/Dashboard/MenuPanel.js";
import { MenuItem } from "../UserControls/MenuItem.js";
import { PrintContainer } from "../UserControls/PrintContainer.js";
import { SkeletonLoader } from "../UserControls/SkeletonLoader.js";
import { SystemStateNotification } from "../UserControls/SystemStateNotification.js";
import { BasePanelController } from "./BasePanelController.js";

export abstract class BasePageLayout extends BasePanel {
    public readonly skeletonLoader : SkeletonLoader;
    public readonly tabsContent : GuiCommons;
    public readonly headerStrip : GuiCommons;
    public readonly systemState : SystemStateNotification;
    public readonly releaseNotesDialog : ReleaseNotesDialog;
    public readonly notificationsDialog : NotificationsDialog;
    public readonly backdoorLoginDialog : BackdoorLoginDialog;
    public readonly panelsContainer : GuiCommons;
    public readonly screenSection : GuiCommons;
    public readonly printSection : PrintContainer;
    protected readonly toasts : GuiCommons;
    private readonly tabs : ITabDescriptor[];
    private notificationsHandler : ($description : INotificationDescriptor) => void;

    constructor() {
        super();

        this.skeletonLoader = new SkeletonLoader();
        this.tabsContent = new GuiCommons();
        this.headerStrip = new GuiCommons();
        this.systemState = new SystemStateNotification();
        this.releaseNotesDialog = new ReleaseNotesDialog();
        this.notificationsDialog = new NotificationsDialog();
        this.backdoorLoginDialog = new BackdoorLoginDialog();
        this.panelsContainer = new GuiCommons();
        this.toasts = new GuiCommons();
        this.screenSection = new GuiCommons();
        /// TODO: print section should be conditional and bound with puppeteer config somehow,
        //        css definition must be general in that case or mixin
        this.printSection = new PrintContainer();

        this.tabs = [];
        const config : any = this.getTabsConfiguration();
        for (const name in config) {
            if (config.hasOwnProperty(name)) {
                config[name].id = name;
                this.AddTabConfig(config[name]);
            }
        }

        this.notificationsHandler = ($description : INotificationDescriptor) : void => {
            // do nothing
        };
    }

    public ShowLoader($value : boolean) : void {
        this.tabsContent.Visible(!$value);
        this.skeletonLoader.Visible($value);
    }

    public ShowMessage($text : string, $type? : ToastType, $withNotification? : boolean) : void;
    public ShowMessage($content : INotificationContent, $withNotification? : boolean) : void;

    public ShowMessage($content : INotificationContent | string, $typeOrSwitch? : ToastType | boolean,
                       $withNotification? : boolean) : void {
        const content : INotificationContent = {
            body  : "",
            header: "",
            icon  : "",
            type  : null
        };
        if (ObjectValidator.IsString($content)) {
            content.body = <string>$content;
        } else {
            JsonUtils.Extend(content, $content);
        }
        if (!ObjectValidator.IsEmptyOrNull($typeOrSwitch)) {
            if (ObjectValidator.IsBoolean($typeOrSwitch)) {
                $withNotification = <boolean>$typeOrSwitch;
            } else {
                content.type = <ToastType>$typeOrSwitch;
            }
        }
        const message : Toast = this.getNewToast(content);
        this.toasts.AddChild(message);
        message.Visible(true);
        EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
            message.Visible(false);
        }, false, content.type === ToastType.ERROR ? 5000 : 2500);
        if (ObjectValidator.IsEmptyOrNull($withNotification) && !ObjectValidator.IsEmptyOrNull(content.type)) {
            $withNotification = this.getNotificationFilter(content.type);
        }
        if ($withNotification) {
            this.notificationsHandler({viewed: false, content, time: new Date().getTime()});
        }
    }

    public getTabsList() : ITabDescriptor[] {
        return this.tabs;
    }

    public getTabConfig($id : PageTabIds) : ITabDescriptor {
        let config : ITabDescriptor = null;
        this.tabs.forEach(($tab : ITabDescriptor) : void => {
            if ($tab.id === $id) {
                config = $tab;
            }
        });
        return config;
    }

    public AddTabConfig($config : ITabDescriptor) : void {
        if (ObjectValidator.IsEmptyOrNull($config.name)) {
            $config.name = "Tab " + this.tabs.length + 1;
        }
        const newConfig : ITabDescriptor = JsonUtils.Extend({
            id             : this.getUID(),
            enabled        : true,
            isDefault      : false,
            autoReload     : false,
            reloadOnRestart: false,
            lazyLoad       : true,
            altLinks       : [],
            icon           : `<div style="width: 16px;height: 23px;float: left;font-weight: bold;border: 1px solid;border-radius: 5px;text-align: center;line-height: 20px;text-transform: uppercase;">
                ${$config.name[0]}</div>`
        }, $config);
        if (!ObjectValidator.IsEmptyOrNull(newConfig.controller)) {
            if (ObjectValidator.IsEmptyOrNull(newConfig.link) && ObjectValidator.IsSet((<any>newConfig.controller).getLink)) {
                newConfig.link = "/#" + this.normalizeLink((<any>newConfig.controller).getLink());
                newConfig.altLinks = (<any>newConfig.controller).getAltLinks();
                if (!ObjectValidator.IsEmptyOrNull(newConfig.altLinks)) {
                    newConfig.altLinks = newConfig.altLinks.map(($value : string) : string => {
                        return this.normalizeLink($value);
                    });
                }
            }
        }
        this.tabs.push(newConfig);
    }

    public setNotificationsHandler($handler : ($description : INotificationDescriptor) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull($handler)) {
            this.notificationsHandler = $handler;
        }
    }

    protected getTabsConfiguration() : ITabDescriptor[] {
        return <any>{};
    }

    protected innerHtml() : string {
        return `
<section class="ScreenSection" data-oidis-bind="${this.screenSection}">
    <div class="container-fluid d-flex h-100 flex-column">
        <div class="row text-dark ${GeneralCSS.DNONE}" data-oidis-bind="${this.headerStrip}" style="background: var(--bs-yellow);">
            <div class="col text-center"><span> </span></div>
        </div>
        <div class="row"><div class="col" data-oidis-bind="${this.systemState}" style="padding: 0;"></div></div>
        <div class="row justify-content-center flex-fill" style="min-height: 500px;" data-oidis-bind="${this.tabsContent}">
            <div class="col">
                <div class="row h-100" data-oidis-bind="${this.panelsContainer}">${this.getTabContent()}</div>
            </div>
        </div>
        <div class="row justify-content-center flex-fill" data-oidis-bind="${this.skeletonLoader}"></div>
    </div>
    <div class="toast-container position-fixed bottom-0 end-0 p-3" style="z-index: 11;" data-oidis-bind="${this.toasts}"></div>
    <div data-oidis-bind="${this.releaseNotesDialog}"></div>
    <div data-oidis-bind="${this.notificationsDialog}"></div>
    <div data-oidis-bind="${this.backdoorLoginDialog}"></div>
</section>
<section class="HiddenElement PrintSection" data-oidis-bind="${this.printSection}"></section>`;
    }

    protected getTabContent() : string {
        let content : string = "";
        this.getTabsList().forEach(($tab : ITabDescriptor) : void => {
            if ($tab.enabled && ObjectValidator.IsSet($tab.content)) {
                if (!ObjectValidator.IsSet((<BasePanel>$tab.content).getClassName)) {
                    const contentClass : any = $tab.content;
                    $tab.content = new contentClass();
                }
                this[ObjectValidator.IsEmptyOrNull($tab.declaredBy) ? $tab.id : $tab.declaredBy] = $tab.content;
                (<BasePanel>$tab.content).WithLazyLoad($tab.lazyLoad);
                if (!$tab.lazyLoad) {
                    content += `<div class="${(<BasePanel>$tab.content).IsMemberOf(Dialog) ? "" : GeneralCSS.DNONE}" data-oidis-bind="${$tab.content}"></div>`;
                }
            } else {
                $tab.content = null;
            }
        });
        return content;
    }

    protected getNewToast($content : INotificationContent) : Toast {
        return new Toast($content.body, $content.type);
    }

    protected getNotificationFilter($type : ToastType) : boolean {
        return false;
    }

    private normalizeLink($value : string) : string {
        if (ObjectValidator.IsEmptyOrNull($value)) {
            return $value;
        }
        if (!StringUtils.StartsWith($value, "/")) {
            $value = "/" + $value;
        }
        return StringUtils.Replace($value, "/#/", "/");
    }
}

export class PageTabIds extends BaseEnum {
    public static readonly FAQ : string = "faq";
    public static readonly FEEDBACK : string = "feedback";
    public static readonly RELEASE_NOTES : string = "releaseNotes";
    public static readonly NOTIFICATIONS : string = "notifications";
}

export interface ITabDescriptor {
    name? : string;
    id? : string;
    declaredBy? : string;
    link? : string;
    altLinks? : string[];
    icon? : string;
    scope? : MenuItemScope;
    autoReload? : boolean;
    reloadOnRestart? : boolean;
    menuItem? : MenuItem;
    content? : BasePanel | IClassName;
    isDefault? : boolean;
    enabled? : boolean;
    controller? : BasePanelController | IClassName;
    lazyLoad? : boolean;
}

export interface INotificationDescriptor {
    viewed : boolean;
    content : INotificationContent;
    time : number;
}

export interface INotificationContent {
    header? : string;
    body : string;
    icon? : any;
    type? : ToastType;
}

// generated-code-start
/* eslint-disable */
export const ITabDescriptor = globalThis.RegisterInterface(["name", "id", "declaredBy", "link", "altLinks", "icon", "scope", "autoReload", "reloadOnRestart", "menuItem", "content", "isDefault", "enabled", "controller", "lazyLoad"]);
export const INotificationDescriptor = globalThis.RegisterInterface(["viewed", "content", "time"]);
export const INotificationContent = globalThis.RegisterInterface(["header", "body", "icon", "type"]);
/* eslint-enable */
// generated-code-end
