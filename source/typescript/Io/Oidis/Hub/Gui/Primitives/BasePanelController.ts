/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { BasePanelController as Parent } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanelController.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { Loader } from "../Loader.js";
import { UserContext } from "../Structures/UserContext.js";
import { INotificationContent } from "./BasePageLayout.js";
import { BasePageLayoutController } from "./BasePageLayoutController.js";

export class BasePanelController<Panel = BasePanel> extends Parent<Panel> {
    protected readonly context : UserContext;

    public static getLink() : string {
        return null;
    }

    public static getAltLinks() : string[] {
        return [];
    }

    constructor($instance : Panel) {
        super($instance);
        this.context = Loader.getInstance().getContext();
    }

    public ShowMessage($text : string, $type? : ToastType) : void;
    public ShowMessage($content : INotificationContent) : void;
    public ShowMessage($content : INotificationContent | string, $type? : ToastType) : void {
        if (ObjectValidator.IsString($content)) {
            BasePageLayoutController.ShowMessage({body: <string>$content, type: $type});
        } else {
            BasePageLayoutController.ShowMessage(<INotificationContent>$content);
        }
    }
}
