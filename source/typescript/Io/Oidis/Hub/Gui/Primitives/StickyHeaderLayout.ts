/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { FooterPanel } from "../Panels/Dashboard/FooterPanel.js";
import { HeaderPanel } from "../Panels/Dashboard/HeaderPanel.js";
import { BasePageLayout } from "./BasePageLayout.js";

export abstract class StickyHeaderLayout extends BasePageLayout {
    public readonly headerPanel : BasePanel;
    public readonly footerPanel : FooterPanel;
    public readonly pageContent : GuiCommons;
    public readonly scrollBody : GuiCommons;
    protected readonly headerHeight : number;

    constructor() {
        super();
        this.headerPanel = new HeaderPanel();
        this.footerPanel = new FooterPanel();
        this.pageContent = new GuiCommons();
        this.scrollBody = new GuiCommons();
        this.headerHeight = 70;
    }

    protected innerHtml() : string {
        return `
<section class="ScreenSection" data-oidis-bind="${this.screenSection}">
    <div style="display: flex;flex-direction: column;height: 100%;">
        <div style="display: flex;flex-direction: column;height: ${this.headerHeight}px;z-index: 100;">
            <div class="row g-0" data-oidis-bind="${this.headerPanel}"></div>
        </div>
        <div style="display: flex;height: calc(100% - ${this.headerHeight}px);">
            <div style="display: flex;flex: 1;flex-direction: column;overflow: auto;">
                <div class="row text-dark justify-content-xl-end ${GeneralCSS.DNONE}" data-oidis-bind="${this.headerStrip}" style="background: var(--bs-yellow);margin: 0;padding-top: 6px;">
                    <div class="col text-center"><span> </span></div>
                </div>
                <div style="display: flex;flex: 1;overflow: auto;" data-oidis-bind="${this.scrollBody}">
                    <div style="width: 100%;display: flex;flex-direction: column;">
                        <div class="row flex-fill" style="margin: 0;">
                            <div class="col" style="height: 100%;display: flex;flex-direction: column;">
                                <div class="row justify-content-center" style="min-height: 500px;" data-oidis-bind="${this.tabsContent}">
                                    <div class="col-xl-10 col" style="margin-bottom: 15px;margin-top: 30px;" data-oidis-bind="${this.pageContent}">
                                        <div class="row" data-oidis-bind="${this.systemState}"></div>
                                        <div class="row" data-oidis-bind="${this.panelsContainer}">${this.getTabContent()}</div>
                                    </div>
                                </div>
                                <div class="row justify-content-center h-100" data-oidis-bind="${this.skeletonLoader}"></div>
                            </div>
                        </div>
                        <div class="row g-0" data-oidis-bind="${this.footerPanel}"></div>
                        <div class="toast-container position-fixed end-0 p-3" style="z-index: 11;" data-oidis-bind="${this.toasts}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-oidis-bind="${this.releaseNotesDialog}"></div>
    <div data-oidis-bind="${this.notificationsDialog}"></div>
    <div data-oidis-bind="${this.backdoorLoginDialog}"></div>
</section>
<section class="HiddenElement PrintSection" data-oidis-bind="${this.printSection}"></section>`;
    }
}
