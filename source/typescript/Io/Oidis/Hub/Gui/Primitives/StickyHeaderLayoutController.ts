/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePageLayoutController } from "./BasePageLayoutController.js";
import { StickyHeaderLayout } from "./StickyHeaderLayout.js";

export abstract class StickyHeaderController<Page extends StickyHeaderLayout> extends BasePageLayoutController<Page> {
    // Override just for ability to specify required Page type
}
