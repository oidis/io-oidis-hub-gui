/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { PersistenceFactory } from "@io-oidis-gui/Io/Oidis/Gui/PersistenceFactory.js";
import { BaseViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { EnvironmentType } from "@io-oidis-services/Io/Oidis/Services/Enums/EnvironmentType.js";
import { AsyncBasePageController } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/Resolvers/AsyncBasePageController.js";
import { DialogBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/DialogBinding.js";
import { OffCanvasBinding } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bindings/OffCanvasBinding.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { ToastType } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Toast.js";
import { ReleaseNotesBinding } from "../Bindings/ReleaseNotesBinding.js";
import { ServerStateBinding } from "../Bindings/ServerStateBinding.js";
import { SSOBinding } from "../Bindings/SSOBinding.js";
import { AuthManagerConnector, IServiceAccounts } from "../Connectors/AuthManagerConnector.js";
import { EnvironmentConnector, IPushNotificationEvent } from "../Connectors/EnvironmentConnector.js";
import { PushNotificationType } from "../Enums/PushNotificationType.js";
import { IHeaderStripType } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";
import { User } from "../Models/User.js";
import { UserContext } from "../Structures/UserContext.js";
import { NotificationItem } from "../UserControls/NotificationItem.js";
import { BasePageLayout, INotificationContent, INotificationDescriptor, ITabDescriptor, PageTabIds } from "./BasePageLayout.js";
import { BasePanelController } from "./BasePanelController.js";

declare const __SENTRY__ : any;

export abstract class BasePageLayoutController<Page extends BasePageLayout> extends AsyncBasePageController {
    protected static statusHandlers : Array<($content : INotificationContent, $withNotification : boolean) => void> = [];
    private static versionInfoPrinted : boolean = false;
    private static initStatus : { content : INotificationContent, withNotification : boolean };
    protected user : UserContext;
    protected readonly envHelper : EnvironmentConnector;
    protected readonly autoReloadTabs : GuiCommons[];
    protected readonly autoRefreshTabs : string[];
    protected readonly persistence : IPersistenceHandler;
    protected readonly auth : AuthManagerConnector;
    protected readonly currentVersion : string;
    private tabLoaded : GuiCommons[];
    private loaderTimeout : number;
    private releaseNotes : ReleaseNotesBinding;
    private notifications : INotificationDescriptor[];

    public static ShowMessage($text : string, $type? : ToastType, $withNotification? : boolean) : void;
    public static ShowMessage($content : INotificationContent, $withNotification? : boolean) : void;

    public static ShowMessage($content : INotificationContent | string, $typeOrSwitch? : ToastType | boolean,
                              $withNotification? : boolean) : void {
        const content : INotificationContent = {
            body  : "",
            header: "",
            icon  : "",
            type  : null
        };
        if (ObjectValidator.IsString($content)) {
            content.body = <string>$content;
        } else {
            JsonUtils.Extend(content, $content);
        }
        if (!ObjectValidator.IsEmptyOrNull($typeOrSwitch)) {
            if (ObjectValidator.IsBoolean($typeOrSwitch)) {
                $withNotification = <boolean>$typeOrSwitch;
            } else {
                content.type = <ToastType>$typeOrSwitch;
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(this.statusHandlers)) {
            this.statusHandlers.forEach(($handler : any) : void => {
                $handler(content, $withNotification);
            });
        } else {
            this.initStatus = {content, withNotification: $withNotification};
        }
    }

    constructor() {
        super();

        this.tabLoaded = [];
        this.autoReloadTabs = [];
        this.autoRefreshTabs = [];
        this.loaderTimeout = null;
        this.notifications = [];

        this.persistence = PersistenceFactory.getPersistence(this.getClassName());
        const notifications : any[] = this.persistence.Variable("notifications");
        if (!ObjectValidator.IsEmptyOrNull(notifications)) {
            this.notifications = notifications.map(($notification : any) : any => {
                if (ObjectValidator.IsSet($notification.text)) {
                    return {
                        content: <INotificationContent>{
                            body: $notification.text,
                            type: $notification.status
                        },
                        time   : $notification.time,
                        viewed : $notification.viewed
                    };
                }
                return $notification;
            });
        }

        this.user = Loader.getInstance().getContext();
        this.auth = AuthManagerConnector.getClient();
        BasePageLayoutController.statusHandlers.push(($content : INotificationContent, $withNotification? : boolean) : void => {
            const instance : Page = this.InstanceOwner();
            if (!ObjectValidator.IsEmptyOrNull(instance) && instance.Visible()) {
                instance.ShowMessage($content, $withNotification);

                if (ObjectValidator.IsEmptyOrNull($withNotification) && !ObjectValidator.IsEmptyOrNull($content.type)) {
                    $withNotification = (<any>instance).getNotificationFilter($content.type);
                }
                if ($withNotification) {
                    this.notifications.push({viewed: false, content: $content, time: new Date().getTime()});
                    this.showNotifications();
                    this.persistence.Variable("notifications", this.notifications);
                }
            }
        });
        this.envHelper = EnvironmentConnector.getInstanceSingleton();
        this.currentVersion = this.getEnvironmentArgs().getProjectVersion();
    }

    public InstanceOwner() : Page {
        return <Page>super.InstanceOwner();
    }

    public Process() : void {
        if ((<any>this).processing) {
            this.getHttpManager().Refresh();
        } else {
            super.Process();
        }
    }

    protected async beforeLoad($instance? : BasePanel, $args? : BaseViewerArgs, $dao? : any) : Promise<void> {
        if ("__SENTRY__" in globalThis && !ObjectValidator.IsEmptyOrNull(__SENTRY__.hub)) {
            __SENTRY__.hub.getClient().getOptions().environment = await this.getEnvironmentType();
        }
        await super.beforeLoad($instance, $args, $dao);
    }

    protected async afterLoad($instance : Page) : Promise<void> {
        if (!BasePageLayoutController.versionInfoPrinted) {
            BasePageLayoutController.versionInfoPrinted = true;
            LogIt.Info("version: " + this.currentVersion);
            LogIt.Info("build: " + this.getEnvironmentArgs().getBuildTime());
        }

        await this.user.Load();
        await this.showHeaderStrip();
        this.envHelper.AttachToPushNotifications().OnChange(async ($event : IPushNotificationEvent) : Promise<void> => {
            await this.pushNotificationHandler($event);
        });

        for await (const tab of $instance.getTabsList()) {
            tab.menuItem = this.getTabMenuInstance(tab);
            const controllerClass : any = ObjectValidator.IsEmptyOrNull(tab.controller) ? this.getTabController(tab) : tab.controller;
            if (!ObjectValidator.IsEmptyOrNull(controllerClass)) {
                tab.controller = new controllerClass(tab.content);
                if (tab.enabled) {
                    this[!ObjectValidator.IsEmptyOrNull(tab.declaredBy) ? tab.declaredBy : tab.id] = <Page>tab.controller;
                }
            } else {
                tab.controller = null;
            }
            await this.onTabRegister(tab);
            if (!ObjectValidator.IsEmptyOrNull(tab.content)) {
                if (tab.enabled && tab.autoReload && !this.autoReloadTabs.includes(<BasePanel>tab.content)) {
                    this.autoReloadTabs.push(<BasePanel>tab.content);
                    if (!ObjectValidator.IsEmptyOrNull(tab.menuItem)) {
                        EventsBinding.SingleClick(tab.menuItem, async () : Promise<void> => {
                            if ((<BasePanel>tab.content).Visible()) {
                                await this.onTabLoad($instance);
                            }
                        });
                    }
                }
                if (tab.enabled && tab.reloadOnRestart && !this.autoRefreshTabs.includes(tab.id)) {
                    this.autoRefreshTabs.push(tab.id);
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(tab.menuItem)) {
                tab.menuItem.Visible(tab.enabled);
            }
        }
        await this.afterTabsRegister($instance);

        new OffCanvasBinding().Bind({
            close : $instance.releaseNotesDialog.close,
            holder: $instance.releaseNotesDialog,
            open  : this.getReleaseNotesOpenElements()
        });

        new ServerStateBinding(this.envHelper).Bind({
            offlineAlert   : $instance.systemState.offlineAlert,
            onlineAlert    : $instance.systemState.onlineAlert,
            reloadAlert    : $instance.systemState.reloadAlert,
            reloadCountDown: $instance.systemState.reloadCountDown,
            reloadSuspend  : $instance.systemState.reloadSuspend
        }, async () : Promise<void> => {
            for await (const id of this.autoRefreshTabs) {
                const tab : ITabDescriptor = $instance.getTabConfig(id);
                if (tab.enabled) {
                    const tabInstance : BasePanel = <BasePanel>tab.content;
                    if (tabInstance.Visible()) {
                        const controller : BasePanelController = <BasePanelController>tab.controller;
                        if (!ObjectValidator.IsEmptyOrNull(controller) && ObjectValidator.IsSet(controller.FetchData)) {
                            await controller.FetchData();
                        }
                    } else if (this.tabLoaded.includes(tabInstance)) {
                        const index : number = this.tabLoaded.indexOf(tabInstance);
                        if (index !== -1) {
                            this.tabLoaded.splice(index, 1);
                        }
                    }
                }
            }
        });

        $instance.setNotificationsHandler(($description : INotificationDescriptor) : void => {
            this.notifications.push($description);
            this.showNotifications();
            this.persistence.Variable("notifications", this.notifications);
        });

        const notifications : OffCanvasBinding = new OffCanvasBinding();
        notifications.Bind({
            close : $instance.notificationsDialog.close,
            holder: $instance.notificationsDialog,
            open  : this.getNotificationsOpenElements()
        });
        $instance.notificationsDialog.clear.getEvents().setOnClick(() : void => {
            this.notifications = [];
            this.showNotifications();
            notifications.Close($instance.notificationsDialog);
            this.persistence.Variable("notifications", this.notifications);
        });

        new DialogBinding().Bind({
            close : $instance.backdoorLoginDialog.cancel,
            holder: $instance.backdoorLoginDialog,
            submit: $instance.backdoorLoginDialog.login
        });
        EventsBinding.SingleClick($instance.backdoorLoginDialog.login, async () : Promise<void> => {
            try {
                this.auth.CurrentToken(await this.auth.LogIn(
                    $instance.backdoorLoginDialog.userName.Value(),
                    $instance.backdoorLoginDialog.password.Value()));
                await this.user.Reload();
            } catch (ex) {
                $instance.ShowMessage(ex.message, ToastType.ERROR);
            }
            $instance.backdoorLoginDialog.Visible(false);
            this.reloadToDefaultResolver();
        });
    }

    protected async onSuccess($instance : Page) : Promise<void> {
        this.loaderTimeout = this.getEventsManager().FireAsynchronousMethod(() : void => {
            $instance.ShowLoader(true);
        }, true, 150);
        await this.user.Load();
        if (!ObjectValidator.IsEmptyOrNull(BasePageLayoutController.initStatus)) {
            BasePageLayoutController.ShowMessage(BasePageLayoutController.initStatus.content,
                BasePageLayoutController.initStatus.withNotification);
            BasePageLayoutController.initStatus = null;
        }

        await this.beforeTabLoad($instance);
        await this.processRequestPath(this.RequestArgs().Url(), $instance);
        await this.onTabLoad($instance);

        if (!ObjectValidator.IsEmptyOrNull(this.loaderTimeout)) {
            clearTimeout(this.loaderTimeout);
        }
        $instance.ShowLoader(false);

        if (ObjectValidator.IsEmptyOrNull(this.releaseNotes)) {
            this.releaseNotes = new ReleaseNotesBinding({
                helper        : EnvironmentConnector.getInstanceSingleton(),
                currentVersion: this.currentVersion,
                onShowRequest : () : boolean => {
                    return this.user.IsAuthenticated && this.user.IsActive;
                },
                onAdminRequest: () : boolean => {
                    return this.user.IsAdmin;
                },
                wasSeen       : async () : Promise<boolean> => {
                    if (this.persistence.Variable("whatsNew") !== this.currentVersion) {
                        return this.user.IsAuthenticated && this.user.getUserProfile().WhatsNewSeen === this.currentVersion;
                    }
                    return true;
                },
                afterShow     : async () : Promise<void> => {
                    this.persistence.Variable("whatsNew", this.currentVersion);
                    if (this.user.IsAuthenticated) {
                        try {
                            await this.auth.setUserProfile({
                                whatsNewSeen: this.currentVersion
                            });
                        } catch (ex) {
                            LogIt.Debug("Failed to save whats new state\n" + ex.stack);
                        }
                    }
                }
            });
        }
        if (!ObjectValidator.IsEmptyOrNull($instance.getTabConfig(PageTabIds.RELEASE_NOTES)) &&
            $instance.getTabConfig(PageTabIds.RELEASE_NOTES).enabled) {
            this.releaseNotes.Open({
                open           : this.getReleaseNotesOpenElements(),
                holder         : $instance.releaseNotesDialog,
                versionMenu    : $instance.releaseNotesDialog.versionMenu,
                versionSelector: $instance.releaseNotesDialog.versionSelector,
                adminsContent  : $instance.releaseNotesDialog.body.adminsContent,
                date           : $instance.releaseNotesDialog.body.date,
                newsList       : $instance.releaseNotesDialog.body.newsList,
                adminsList     : $instance.releaseNotesDialog.body.adminsList,
                issuesList     : $instance.releaseNotesDialog.body.issuesList,
                issuesContent  : $instance.releaseNotesDialog.body.issuesContent
            });
        }
        if (!ObjectValidator.IsEmptyOrNull($instance.getTabConfig(PageTabIds.NOTIFICATIONS)) &&
            $instance.getTabConfig(PageTabIds.NOTIFICATIONS).enabled) {
            this.showNotifications();
        }
    }

    protected async processTabContent($instance : GuiCommons, $handler : () => Promise<void>) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull($instance) && $instance.Visible() && !this.tabLoaded.includes($instance)) {
            await $handler();
            if (!this.autoReloadTabs.includes($instance)) {
                this.tabLoaded.push($instance);
            }
        }
    }

    protected async processRequestPath($path : string, $instance : Page) : Promise<void> {
        if (StringUtils.Contains($path, "/backdoorLogin")) {
            $instance.backdoorLoginDialog.profilesMenu.Clear();
            if (this.user.IsAdmin) {
                await this.generateUserProfilesOptions($instance);
            }
            $instance.backdoorLoginDialog.IsForAdmin(this.user.IsAdmin);
            $instance.backdoorLoginDialog.Visible(true);
            return;
        } else if (this.user.IsAuthenticated && this.user.Need2FACheck) {
            (<any>this).processing = false;
            this.reloadTo2FAChallenge();
            return;
        }
        let selected : boolean = false;
        let defaultTab : GuiCommons = null;
        let authorized : boolean = true;
        for await (const $tab of $instance.getTabsList()) {
            if ($tab.enabled && !ObjectValidator.IsEmptyOrNull($tab.content)) {
                if (!selected && !ObjectValidator.IsEmptyOrNull($tab.link)) {
                    for await (let $link of [$tab.link].concat($tab.altLinks)) {
                        if (!selected) {
                            $link = StringUtils.Replace($link, "/#/", "/");
                            $link = $link.replace(/{\w+}/g, "*");
                            if (StringUtils.PatternMatched($link, $path)) {
                                if (ObjectValidator.IsEmptyOrNull($tab.controller) ||
                                    !ObjectValidator.IsEmptyOrNull($tab.controller) &&
                                    await (<BasePanelController>$tab.controller).AccessValidator()) {
                                    await this.selectTab(<BasePanel>$tab.content);
                                    selected = true;
                                } else {
                                    authorized = false;
                                }
                            }
                        }
                    }
                }
                if ($tab.isDefault) {
                    defaultTab = <BasePanel>$tab.content;
                }
            }
        }
        if (!authorized) {
            if (!this.user.IsAuthenticated) {
                new SSOBinding().LastLoginUrl($path);
            }
            this.reloadToNotAuthorized();
        } else if (!selected) {
            if (!this.user.IsActive && this.user.IsAuthenticated) {
                this.reloadToInactive();
            } else if (!ObjectValidator.IsEmptyOrNull(defaultTab)) {
                await this.selectTab(defaultTab);
            } else {
                LogIt.Debug("Failed to display any of tab: non of required conditions has been met");
            }
        }
    }

    protected async onTabRegister($tab : ITabDescriptor) : Promise<void> {
        // override if needed
    }

    protected async afterTabsRegister($instance : Page) : Promise<void> {
        // override if needed
    }

    protected getTabMenuInstance($tab : ITabDescriptor) : any {
        return null;
    }

    protected getTabController($tab : ITabDescriptor) : any {
        return null;
    }

    protected async beforeTabLoad($instance : Page) : Promise<void> {
        if (this.user.IsAuthenticated) {
            if (this.user.IsAdmin) {
                $instance.releaseNotesDialog.body.adminsContent.Visible(
                    !ObjectValidator.IsEmptyOrNull($instance.releaseNotesDialog.body.adminsList.InstanceOwner().innerText));
            } else {
                $instance.releaseNotesDialog.body.adminsContent.Visible(false);
                $instance.releaseNotesDialog.body.adminsList.Clear();
            }
        } else {
            Loader.getInstance().setLogsContext("");
        }
    }

    protected async onTabLoad($instance : Page) : Promise<void> {
        for await (const tab of $instance.getTabsList()) {
            if (!tab.lazyLoad) {
                await this.processTabContent(<BasePanel>tab.content, () : Promise<void> => {
                    if (!ObjectValidator.IsEmptyOrNull(tab.controller)) {
                        (<BasePanelController>tab.controller).RequestArgs(this.RequestArgs());
                        return (<BasePanelController>tab.controller).FetchData();
                    }
                    return;
                });
            }
        }
    }

    protected async selectTab($instance : BasePanel) : Promise<void> {
        const instance : Page = this.InstanceOwner();

        for await (const tab of instance.getTabsList()) {
            if (!ObjectValidator.IsEmptyOrNull(tab.content)) {
                if (!ObjectValidator.IsEmptyOrNull(tab.menuItem)) {
                    (tab.menuItem).Active($instance === tab.content);
                }
                if (tab.lazyLoad) {
                    if ($instance === tab.content) {
                        tab.lazyLoad = false;
                        instance.panelsContainer.AddChild(<BasePanel>tab.content);
                        (<BasePanel>tab.content).Parent(instance);
                        if (!ObjectValidator.IsEmptyOrNull(tab.controller)) {
                            (<BasePanelController>tab.controller).RequestArgs(this.RequestArgs());
                            await (<any>tab.controller).beforeLoad(<BasePanel>tab.content);
                        }
                        (<BasePanel>tab.content).Visible(true);
                    }
                } else {
                    (<BasePanel>tab.content).Visible($instance === tab.content);
                }
            }
        }
    }

    protected reloadToDefaultResolver($default : string = "/home") : void {
        this.getHttpManager().ReloadTo($default);
    }

    protected async showHeaderStrip($message? : string) : Promise<void> {
        const instance : Page = this.InstanceOwner();
        let message : string = "";
        if (!ObjectValidator.IsEmptyOrNull($message)) {
            message = $message;
        } else {
            const config : IHeaderStripType = Loader.getInstance().getAppConfiguration().target.headerStrip;
            const environmentType : EnvironmentType = await this.getEnvironmentType();
            if (environmentType !== EnvironmentType.NONE) {
                switch (environmentType) {
                case EnvironmentType.PROD:
                    message = config.prod.message;
                    if (!ObjectValidator.IsEmptyOrNull(message)) {
                        this.setPageTitle(config.prod.title);
                    }
                    break;
                case EnvironmentType.DEV:
                    message = config.dev.message;
                    this.setPageTitle(config.dev.title);
                    break;
                case EnvironmentType.EAP:
                    message = config.eap.message;
                    this.setPageTitle(config.eap.title);
                    break;
                case EnvironmentType.LOCALHOST:
                    message = config.localhost.message;
                    this.setPageTitle(config.localhost.title);
                    break;
                default:
                    LogIt.Debug("Unrecognized header warning type: {0}", environmentType);
                    break;
                }
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(message)) {
            instance.headerStrip.Content(`<div class="col text-center"><span>${message}</span></div>`);
            instance.headerStrip.Visible(true);
        }
    }

    protected async generateUserProfilesOptions($instance : Page) : Promise<void> {
        {
            const item : DropDownListItem = new DropDownListItem();
            EventsBinding.SingleClick(item, async () : Promise<void> => {
                await this.user.SwitchToProfile(null);
                $instance.backdoorLoginDialog.Visible(false);
                this.reloadToDefaultResolver();
            });
            $instance.backdoorLoginDialog.profilesMenu.AddChild(item);
            item.Text("Normal user profile");
        }
        {
            const item : DropDownListItem = new DropDownListItem();
            EventsBinding.SingleClick(item, async () : Promise<void> => {
                await this.user.SwitchToProfile(this.user.getGroups().SysAdmin);
                $instance.backdoorLoginDialog.Visible(false);
                this.reloadToDefaultResolver();
            });
            $instance.backdoorLoginDialog.profilesMenu.AddChild(item);
            item.Text("Contributor (SysAdmin) profile");
        }
        {
            const item : DropDownListItem = new DropDownListItem();
            EventsBinding.SingleClick(item, async () : Promise<void> => {
                await this.user.SwitchToProfile(this.user.getGroups().Admin);
                $instance.backdoorLoginDialog.Visible(false);
                this.reloadToDefaultResolver();
            });
            $instance.backdoorLoginDialog.profilesMenu.AddChild(item);
            item.Text("Admin profile");
        }
        const accounts : IServiceAccounts = await this.auth.getServiceAccounts();
        for (const name in accounts) {
            if (accounts.hasOwnProperty(name)) {
                const account : User = accounts[name];
                const item : DropDownListItem = new DropDownListItem();
                EventsBinding.SingleClick(item, async () : Promise<void> => {
                    try {
                        const token : string = await this.auth.SwitchToAccount(account);
                        if (!ObjectValidator.IsEmptyOrNull(token)) {
                            this.auth.CurrentToken(token);
                            await this.user.Reload();
                            $instance.backdoorLoginDialog.Visible(false);
                            this.reloadToDefaultResolver();
                        } else {
                            $instance.ShowMessage("Failed to switch to requested account: " + account.UserName, ToastType.ERROR);
                        }
                    } catch (ex) {
                        LogIt.Error(ex);
                        $instance.ShowMessage("Failed to switch to requested account: " + account.UserName, ToastType.ERROR);
                    }
                });
                $instance.backdoorLoginDialog.profilesMenu.AddChild(item);
                const groupName : string = ObjectValidator.IsEmptyOrNull(account.Groups) ? "default permissions" : account.Groups[0].Name;
                item.Text(account.UserName + " [" + groupName + "] account");
            }
        }
    }

    protected showNotifications() : void {
        const instance : Page = this.InstanceOwner();
        instance.notificationsDialog.body.Clear();
        let counter : number = 0;
        if (this.notifications.length > 0) {
            this.notifications.forEach(($item : INotificationDescriptor) : void => {
                const notification : NotificationItem = new NotificationItem();
                notification.Date(new Date($item.time));
                notification.Text($item.content.body);
                notification.Viewed($item.viewed);
                notification.getEvents().setOnClick(() : void => {
                    if (!$item.viewed) {
                        $item.viewed = true;
                        this.showNotifications();
                    }
                });
                instance.notificationsDialog.body.AddChild(notification);
                if (!notification.Viewed()) {
                    counter++;
                }
            });
        } else {
            instance.notificationsDialog.body.Content(`<tr><td>No notifications yet</td></tr>`);
        }
        this.notificationsCounterHandler(counter);
    }

    protected notificationsCounterHandler($count : number) : void {
        // override this method if notifications count should be displayed somewhere e.g. badge
    }

    protected async pushNotificationHandler($event : IPushNotificationEvent) : Promise<void> {
        switch ($event.type) {
        case PushNotificationType.HEADER_CHANGE:
            await this.showHeaderStrip($event.data);
            break;
        case PushNotificationType.HEADER_RESTORE:
            await this.showHeaderStrip();
            break;
        default:
            LogIt.Debug("Ignoring notification: {0}\n{1}", $event.type, JSON.stringify($event.data));
            break;
        }
    }

    protected getReleaseNotesOpenElements() : Button[] {
        return [];
    }

    protected getNotificationsOpenElements() : Button[] {
        return [];
    }

    protected reloadToInactive() : void {
        const postData : ArrayList<any> = new ArrayList<any>();
        postData.Add(this.getRequest(), "Referer");
        this.getHttpManager().ReloadTo("/ServerError/Http/InActive", postData, true);
    }

    protected reloadTo2FAChallenge() : void {
        const postData : ArrayList<any> = new ArrayList<any>();
        postData.Add(this.getRequest(), "Referer");
        this.getHttpManager().ReloadTo("/twoFactorChallenge", postData, true);
    }

    private async getEnvironmentType() : Promise<EnvironmentType> {
        const environmentType : EnvironmentType = await this.envHelper.getEnvType();
        this.getEnvironmentType = async () : Promise<EnvironmentType> => {
            return environmentType;
        };
        return environmentType;
    }
}
