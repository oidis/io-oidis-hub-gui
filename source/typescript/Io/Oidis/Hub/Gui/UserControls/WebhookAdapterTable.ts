/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { ITableColumn, Table, TableItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Table.js";

export class WebhookAdapterTable extends Table<WebhookAdapterTableRecord> {
    constructor() {
        super();

        this.localization = {
            noData: "No adapter found"
        };
        this.setNoDataStyle("success");
    }

    protected getHeaderConfig() : ITableColumn[] {
        return [{text: "Type"}, {text: "Status"}, {text: "Action", width: "100px", modifiers: ["text-end"]}];
    }

    protected getNewItemInstance() : WebhookAdapterTableRecord {
        return new WebhookAdapterTableRecord();
    }
}

export class WebhookAdapterTableRecord extends TableItem {
    public activate : Button;
    public deactivate : Button;
    public reactivateRecords : Button;
    public type : Label;
    public status : Label;

    constructor() {
        super();
        this.activate = new Button();
        this.deactivate = new Button();
        this.reactivateRecords = new Button();
        this.type = new Label();
        this.status = new Label();
    }

    public Restore() {
        super.Restore();
        this.type.Text(" ");
        this.status.Text(" ");
    }

    protected innerHtml() : string {
        return `
<tr data-oidis-bind="${this}">
    <td><span data-oidis-bind="${this.type}">type</span></td>
    <td><span data-oidis-bind="${this.status}">status</span></td>
    <td class="text-end"><svg class="bi bi-lightbulb" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" data-bs-toggle="tooltip" data-bss-tooltip style="margin-right: 10px;color: var(--bs-success);" title="Activate" data-oidis-bind="${this.activate}">
            <path d="M2 6a6 6 0 1 1 10.174 4.31c-.203.196-.359.4-.453.619l-.762 1.769A.5.5 0 0 1 10.5 13a.5.5 0 0 1 0 1 .5.5 0 0 1 0 1l-.224.447a1 1 0 0 1-.894.553H6.618a1 1 0 0 1-.894-.553L5.5 15a.5.5 0 0 1 0-1 .5.5 0 0 1 0-1 .5.5 0 0 1-.46-.302l-.761-1.77a1.964 1.964 0 0 0-.453-.618A5.984 5.984 0 0 1 2 6m6-5a5 5 0 0 0-3.479 8.592c.263.254.514.564.676.941L5.83 12h4.342l.632-1.467c.162-.377.413-.687.676-.941A5 5 0 0 0 8 1"></path>
        </svg><svg class="bi bi-lightbulb-off" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" data-bs-toggle="tooltip" data-bss-tooltip style="margin-right: 10px;color: var(--bs-red);" title="Deactivate" data-oidis-bind="${this.deactivate}">
            <path fill-rule="evenodd" d="M2.23 4.35A6.004 6.004 0 0 0 2 6c0 1.691.7 3.22 1.826 4.31.203.196.359.4.453.619l.762 1.769A.5.5 0 0 0 5.5 13a.5.5 0 0 0 0 1 .5.5 0 0 0 0 1l.224.447a1 1 0 0 0 .894.553h2.764a1 1 0 0 0 .894-.553L10.5 15a.5.5 0 0 0 0-1 .5.5 0 0 0 0-1 .5.5 0 0 0 .288-.091L9.878 12H5.83l-.632-1.467a2.954 2.954 0 0 0-.676-.941 4.984 4.984 0 0 1-1.455-4.405l-.837-.836zm1.588-2.653.708.707a5 5 0 0 1 7.07 7.07l.707.707a6 6 0 0 0-8.484-8.484zm-2.172-.051a.5.5 0 0 1 .708 0l12 12a.5.5 0 0 1-.708.708l-12-12a.5.5 0 0 1 0-.708z"></path>
        </svg><svg class="bi bi-lightning" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" data-bs-toggle="tooltip" data-bss-tooltip style="margin-right: 10px;color: var(--bs-orange);" title="Reactivate all records" data-oidis-bind="${this.reactivateRecords}">
            <path d="M5.52.359A.5.5 0 0 1 6 0h4a.5.5 0 0 1 .474.658L8.694 6H12.5a.5.5 0 0 1 .395.807l-7 9a.5.5 0 0 1-.873-.454L6.823 9.5H3.5a.5.5 0 0 1-.48-.641zM6.374 1 4.168 8.5H7.5a.5.5 0 0 1 .478.647L6.78 13.04 11.478 7H8a.5.5 0 0 1-.474-.658L9.306 1z"></path>
        </svg></td>
</tr>`;
    }
}
