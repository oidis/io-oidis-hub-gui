/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class SkeletonLoader extends GuiCommons {

    protected innerHtml() : string {
        return `
<section>
    <div class="row justify-content-center align-items-center h-100 SkeletonLoader">
        <div class="col-11 col-xl-6">
            <div style="background: #d4d3d8;height: 15px;margin-bottom: 10px;border-radius: 5px;width: 50%;"></div>
            <div style="background: #dddddd;height: 15px;margin-bottom: 10px;border-radius: 5px;width: 30%;"></div>
            <div style="margin-bottom: 10px;border-radius: 5px;min-height: 200px;border: 2px solid #dddddd;"></div>
        </div>
    </div>
</section>`;
    }
}
