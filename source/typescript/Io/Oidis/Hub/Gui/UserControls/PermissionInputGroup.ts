/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";
import { DropDownSearch } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownSearch.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";

export class PermissionInputGroup extends Label {
    private readonly field : TextField;
    private readonly menu : DropDownSearch;

    constructor() {
        super();
        this.field = new TextField();
        this.menu = new DropDownSearch();
        this.menu.Text(" ");
        this.menu.FillInParent(false);
        (<any>this.menu).selectorCss = " btn-primary dropdown-toggle rounded-0 btn-sm";
    }

    public Value($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            this.Data(null);
        }
        return this.field.Value($value);
    }

    public Clear() : void {
        this.menu.Clear();
        this.Data(null);
    }

    public AddItem($text : string, $value? : any) : DropDownListItem {
        const item : DropDownListItem = this.menu.AddItem($text);
        item.Text($text);
        item.Data($value);
        item.getEvents().setOnClick(() : void => {
            this.Value($text);
            this.Data($value);
        });
        return item;
    }

    protected innerHtml() : string {
        return `
            <div class="input-group input-group-sm float-start" style="width: 20%;">
                <input data-oidis-bind="${this.field}" type="text" class="form-control" style="border-top-left-radius: 25px;border-bottom-left-radius: 25px;" />
                <div data-oidis-bind="${this.menu}"></div>
                <button data-oidis-bind="${this}" class="btn btn-primary" type="button" style="border-top-right-radius: 25px;border-bottom-right-radius: 25px;">${this.Text()}</button>
            </div>`;
    }
}
