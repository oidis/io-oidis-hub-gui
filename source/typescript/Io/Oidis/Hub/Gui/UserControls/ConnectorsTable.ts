/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { ITableColumn, Table, TableItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Table.js";

export class ConnectorsTable extends Table<ConnectorsTableRecord> {

    constructor() {
        super();

        this.localization = {
            noData: "No connectors found"
        };
    }

    protected getHeaderConfig() : ITableColumn[] {
        return [
            {text: "Namespace"}, {text: "Owner ID"}, {text: "Requester"}, {text: "Created"}
        ];
    }

    protected getNewItemInstance() : ConnectorsTableRecord {
        return new ConnectorsTableRecord();
    }
}

export class ConnectorsTableRecord extends TableItem {
    public name : Label;
    public owner : Label;
    public requester : Label;
    public created : Label;

    constructor() {
        super();
        this.name = new Label();
        this.owner = new Label();
        this.requester = new Label();
        this.created = new Label();
    }

    public Restore() {
        super.Restore();
        this.name.Text(" ");
        this.owner.Text(" ");
        this.requester.Text(" ");
        this.created.Text(" ");
    }

    protected innerHtml() : string {
        return `
<tr data-oidis-bind="${this}" class="${this.styleClasses.join(" ")}">
    <td><span data-oidis-bind="${this.name}"></span></td>
    <td><span data-oidis-bind="${this.owner}"></span></td>
    <td><span data-oidis-bind="${this.requester}"></span></td>
    <td><span data-oidis-bind="${this.created}"></span></td>
</tr>`;
    }
}
