/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";

export class SystemStateNotification extends BasePanel {
    public readonly offlineAlert : GuiCommons;
    public readonly onlineAlert : GuiCommons;
    public readonly reloadAlert : GuiCommons;
    public readonly reloadSuspend : Button;
    public readonly reloadCountDown : Label;
    protected localization : ISystemStateLocalization;

    constructor() {
        super();

        this.offlineAlert = new GuiCommons();
        this.onlineAlert = new GuiCommons();
        this.reloadAlert = new GuiCommons();
        this.reloadSuspend = new Button();
        this.reloadCountDown = new Label();
        this.localization = {
            online : `You are back online`,
            offline: `<strong>Off-line mode.</strong> Connection with server has been lost.`,
            update : `<strong>Oidis Hub has been updated and page reload is required. </strong>System can be unstable so please do not postpone this action too much. Automatic reload in`,
            suspend: `Postpone`
        };
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="alert alert-success ${GeneralCSS.DNONE}" role="alert" data-oidis-bind="${this.onlineAlert}"><svg class="bi bi-cloud-check" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 3px;font-size: 22px;">
            <path fill-rule="evenodd" d="M10.354 6.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7 8.793l2.646-2.647a.5.5 0 0 1 .708 0z"></path>
            <path d="M4.406 3.342A5.53 5.53 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773 16 11.569 14.502 13 12.687 13H3.781C1.708 13 0 11.366 0 9.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383zm.653.757c-.757.653-1.153 1.44-1.153 2.056v.448l-.445.049C2.064 6.805 1 7.952 1 9.318 1 10.785 2.23 12 3.781 12h8.906C13.98 12 15 10.988 15 9.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 4.825 10.328 3 8 3a4.53 4.53 0 0 0-2.941 1.1z"></path>
        </svg><span>${this.localization.online}</span></div>
    <div class="alert alert-warning ${GeneralCSS.DNONE}" role="alert" data-oidis-bind="${this.offlineAlert}"><svg class="bi bi-cloud-slash" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 3px;font-size: 22px;">
            <path fill-rule="evenodd" d="M3.112 5.112a3.125 3.125 0 0 0-.17.613C1.266 6.095 0 7.555 0 9.318 0 11.366 1.708 13 3.781 13H11l-1-1H3.781C2.231 12 1 10.785 1 9.318c0-1.365 1.064-2.513 2.46-2.666l.446-.05v-.447c0-.075.006-.152.018-.231l-.812-.812zm2.55-1.45-.725-.725A5.512 5.512 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773a3.2 3.2 0 0 1-1.516 2.711l-.733-.733C14.498 11.378 15 10.626 15 9.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 4.825 10.328 3 8 3c-.875 0-1.678.26-2.339.661z"></path>
            <path d="m13.646 14.354-12-12 .708-.708 12 12-.707.707z"></path>
        </svg><span>${this.localization.offline}</span></div>
    <div class="alert alert-info ${GeneralCSS.DNONE}" role="alert" data-oidis-bind="${this.reloadAlert}"><svg class="bi bi-clock-history" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;margin-bottom: 3px;font-size: 22px;">
            <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"></path>
            <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"></path>
            <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"></path>
        </svg><span>${this.localization.update} <span data-oidis-bind="${this.reloadCountDown}">15</span>s. <a class="alert-link" href="#/suspend" data-oidis-bind="${this.reloadSuspend}">${this.localization.suspend}.</a></span></div>
</section>`;
    }
}

export interface ISystemStateLocalization {
    online : string;
    offline : string;
    update : string;
    suspend : string;
}

// generated-code-start
export const ISystemStateLocalization = globalThis.RegisterInterface(["online", "offline", "update", "suspend"]);
// generated-code-end
