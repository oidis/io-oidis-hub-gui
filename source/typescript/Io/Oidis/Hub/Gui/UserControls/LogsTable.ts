/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { bootstrap } from "@io-oidis-gui/Io/Oidis/Gui/Utils/EnvironmentHelper.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { ITableColumn, Table, TableItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Table.js";

export class LogsTable extends Table<LogsTableItem> {

    constructor() {
        super();

        this.localization = {
            noData: "No logs for selected criteria"
        };
    }

    protected getHeaderConfig() : ITableColumn[] {
        return [{text: "Message"}, {text: "Time", width: "260px"}];
    }

    protected getNewItemInstance() : LogsTableItem {
        return new LogsTableItem();
    }
}

export class LogsTableItem extends TableItem {
    public readonly time : Label;
    public readonly message : Label;
    public readonly trace : Label;
    public readonly entryPoint : Label;
    public readonly entryPointToggle : GuiCommons;
    public readonly traceToggle : GuiCommons;
    private isError : boolean;
    private isWarning : boolean;
    private isDebug : boolean;
    private isVerbose : boolean;

    constructor() {
        super();
        this.time = new Label();
        this.message = new Label();
        this.trace = new Label();
        this.entryPoint = new Label();
        this.entryPointToggle = new GuiCommons();
        this.traceToggle = new GuiCommons();
        this.isError = false;
        this.isWarning = false;
        this.isDebug = false;
        this.isVerbose = false;
    }

    public setError($value : boolean) : void {
        this.isError = Property.Boolean(this.isError, $value);
        if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
            this.InstanceOwner().classList.add("table-danger");
        }
    }

    public setWarning($value : boolean) : void {
        this.isWarning = Property.Boolean(this.isWarning, $value);
        if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
            this.InstanceOwner().classList.add("table-warning");
        }
    }

    public setDebug($value : boolean) : void {
        this.isDebug = Property.Boolean(this.isDebug, $value);
        if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
            this.InstanceOwner().classList.add("table-secondary");
        }
    }

    public setVerbose($value : boolean) : void {
        this.isVerbose = Property.Boolean(this.isVerbose, $value);
        if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
            this.InstanceOwner().classList.add("table-info");
        }
    }

    public Restore() {
        super.Restore();
        this.isError = false;
        this.isWarning = false;
        this.isDebug = false;
        this.isVerbose = false;
        this.getRowModifiers();
        this.time.Text(" ");
        this.message.Text(" ");
        this.entryPoint.Text(" ");
        this.trace.Text(" ");
        this.entryPointToggle.Visible(false);
        this.entryPointToggle.Visible(false);
        (new bootstrap.Collapse(this.entryPoint.InstanceOwner())).hide();
        (new bootstrap.Collapse(this.trace.InstanceOwner())).hide();
    }

    protected innerCode() : string {
        this.entryPointToggle.getEvents().setOnClick(() : void => {
            (new bootstrap.Collapse(this.entryPoint.InstanceOwner())).toggle();
        });
        this.traceToggle.getEvents().setOnClick(() : void => {
            (new bootstrap.Collapse(this.trace.InstanceOwner())).toggle();
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        const modifiers : string = this.getRowModifiers().join(" ");
        return `
<tr data-oidis-bind="${this}" class="${modifiers}">
    <td>
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col"><i class="fa fa-map-signs d-none" style="margin-right: 4px;" data-oidis-bind="${this.entryPointToggle}"></i><i class="fa fa-bug d-none" style="margin-right: 4px;" data-oidis-bind="${this.traceToggle}"></i><span style="word-break: break-all;" data-oidis-bind="${this.message}">Loading ...</span></div>
                </div>
                <div class="row collapse multi-collapse" data-oidis-bind="${this.entryPoint}" style="font-size: 10px;padding-left: 20px;"></div>
                <div class="row collapse multi-collapse" data-oidis-bind="${this.trace}" style="font-size: 10px;padding-left: 20px;"></div>
            </div>
        </div>
    </td>
    <td><span data-oidis-bind="${this.time}"></span></td>
</tr>`;
    }

    private getRowModifiers() : string[] {
        const classes : string[] = [];
        if (this.isError) {
            classes.push("table-danger");
        } else if (this.isWarning) {
            classes.push("table-warning");
        } else if (this.isDebug) {
            classes.push("table-secondary");
        } else if (this.isVerbose) {
            classes.push("table-info");
        }
        const element : HTMLElement = this.InstanceOwner();
        if ((<any>this).completed && !ObjectValidator.IsEmptyOrNull(element)) {
            element.classList.remove("table-danger", "table-warning", "table-secondary", "table-info");
            if (this.isError) {
                element.classList.add("table-danger");
            }
            if (this.isWarning) {
                element.classList.add("table-warning");
            }
            if (this.isDebug) {
                element.classList.add("table-secondary");
            }
            if (this.isVerbose) {
                element.classList.add("table-info");
            }
        }
        return classes;
    }
}
