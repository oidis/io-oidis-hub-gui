/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";

export class AutocompleteSearch extends GuiCommons {
    public readonly autocompleteSelector : Button;
    public readonly autocomplete : GuiCommons;
    public readonly searchField : TextField;
    public readonly search : Button;
    public readonly clear : Button;

    constructor() {
        super();

        this.autocompleteSelector = new Button();
        this.autocomplete = new GuiCommons();
        this.searchField = new TextField();
        this.search = new Button();
        this.clear = new Button();
    }

    protected innerHtml() : string {
        return `
<div class="input-group">
    <div class="dropdown" style="width: calc(100% - 100px);position: absolute;left: 0px;"><button class="btn btn-primary rounded-0 rounded-start" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;z-index: 0;" data-oidis-bind="${this.autocompleteSelector}">Dropdown </button>
        <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.autocomplete}"></div>
    </div><input class="form-control" type="text" data-oidis-bind="${this.searchField}" style="border-top-left-radius: 4px;border-bottom-left-radius: 4px;" /><button class="btn btn-primary" type="button" data-oidis-bind="${this.search}" style="width: 105px;border-radius: 4px;border-top-left-radius: 0px;border-bottom-left-radius: 0px;border-top-right-radius: 4px;border-bottom-right-radius: 4px;">Search<svg class="bi bi-search" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-left: 10px;">
            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
        </svg></button><span class="input-group-text" style="margin: 0;padding: 0;width: 0;position: relative;border-width: 0px;"><button class="btn btn-secondary ${GeneralCSS.DNONE}" type="button" style="position: absolute;left: -140px;background-color: transparent;border-width: 0px;z-index: 100;border-radius: 0;" data-oidis-bind="${this.clear}"><svg class="bi bi-x-circle text-primary" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
            </svg></button></span>
</div>`;
    }
}
