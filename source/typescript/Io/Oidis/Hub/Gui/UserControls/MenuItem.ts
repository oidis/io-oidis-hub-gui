/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class MenuItem extends GuiCommons {
    private name : string;
    private link : string;
    private icon : string;

    constructor() {
        super();
        this.name = this.Id();
        this.link = "";
        this.icon = "";
    }

    public Name($value? : string) : string {
        return this.name = Property.String(this.name, $value);
    }

    public Link($value? : string) : string {
        return this.link = Property.String(this.link, $value);
    }

    public Icon($value? : string) : string {
        return this.icon = Property.String(this.icon, $value);
    }

    protected innerHtml() : string {
        if (ObjectValidator.IsEmptyOrNull(this.link)) {
            return `
            <div class="row">
                <div class="col"><button class="btn btn-outline-light" type="button" data-oidis-bind="${this}">${this.icon}<span>${this.name}</span></button></div>
            </div>`;
        }
        return `
            <div class="row">
                <div class="col"><a class="btn btn-outline-light" href="${this.link}" data-oidis-bind="${this}">${this.icon}<span>${this.name}</span></a></div>
            </div>`;
    }
}
