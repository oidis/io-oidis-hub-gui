/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { ITableColumn, Table, TableItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Table.js";

export class RegistryItemsTable extends Table<RegistryItem> {

    constructor() {
        super();

        this.localization = {
            noData: "No data found"
        };
        this.setNoDataStyle("warning");
    }

    protected getHeaderConfig() : ITableColumn[] {
        return [{text: "Type"}, {text: "Info"}, {text: ""}];
    }

    protected getNewItemInstance() : RegistryItem {
        return new RegistryItem();
    }
}

export class RegistryItem extends TableItem {
    public readonly type : Label;
    public readonly info : Label;
    public readonly convert : Button;
    public readonly delete : Button;

    constructor() {
        super();
        this.type = new Label();
        this.info = new Label();
        this.convert = new Button();
        this.delete = new Button();
    }

    public Restore() : void {
        super.Restore();
        this.Enabled(true);
        this.type.Text(" ");
        this.info.Text(" ");
        this.delete.Visible(false);
    }

    protected innerHtml() : string {
        return `
<tr data-oidis-bind="${this}">
    <td><span data-oidis-bind="${this.type}"></span></td>
    <td><span data-oidis-bind="${this.info}"></span></td>
    <td class="text-end">
        <span data-oidis-bind="${this.convert}">
            <svg class="bi bi-arrow-left-right" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" data-bs-toggle="tooltip" title="Convert to FS/DB" style="cursor: pointer;">
                <path fill-rule="evenodd" d="M1 11.5a.5.5 0 0 0 .5.5h11.793l-3.147 3.146a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 11H1.5a.5.5 0 0 0-.5.5m14-7a.5.5 0 0 1-.5.5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H14.5a.5.5 0 0 1 .5.5"></path>
            </svg>
        </span>
        <span class="${GeneralCSS.DNONE}" data-oidis-bind="${this.delete}">
            <svg class="bi bi-trash" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" data-bs-toggle="tooltip" title="Delete" style="cursor: pointer;">
                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"></path>
            </svg>
        </span>
    </td>
</tr>`;
    }
}
