/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { ITableColumn, Table, TableItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Table.js";

export class UsersTable extends Table<UsersTableRecord> {

    constructor() {
        super();

        this.localization = {
            noData: "No users found"
        };
    }

    protected getHeaderConfig() : ITableColumn[] {
        return [{text: "Name"}, {text: "Info"}, {text: "Groups/Methods"}, {text: ""}];
    }

    protected getNewItemInstance() : UsersTableRecord {
        return new UsersTableRecord();
    }
}

export class UsersTableRecord extends TableItem {
    public name : Label;
    public info : Label;
    public groups : GuiCommons;
    public deleteButton : Button;
    public profileButton : Button;

    constructor() {
        super();
        this.name = new Label();
        this.info = new Label();
        this.groups = new GuiCommons();
        this.deleteButton = new Button();
        this.profileButton = new Button();
    }

    public Restore() {
        super.Restore();
        this.name.Text(" ");
        this.info.Text(" ");
        this.groups.Clear();
    }

    protected innerHtml() : string {
        return `
<tr data-oidis-bind="${this}">
    <td><span data-oidis-bind="${this.name}"></span></td>
    <td><span data-oidis-bind="${this.info}"></span></td>
    <td data-oidis-bind="${this.groups}"></td>
    <td>
        <svg data-oidis-bind="${this.deleteButton}" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-trash float-end" style="margin-top: 5px;margin-right: 5px;" data-bs-toggle="tooltip" title="Delete">
            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"></path>
        </svg>
        <svg data-oidis-bind="${this.profileButton}" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-person float-end" style="margin-top: 5px;margin-right: 5px;" data-bs-toggle="tooltip" title="User profile">
            <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"></path>
        </svg>
    </td>
</tr>`;
    }
}
