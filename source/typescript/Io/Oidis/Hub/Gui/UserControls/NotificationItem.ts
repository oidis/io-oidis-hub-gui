/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 NXP
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class NotificationItem extends GuiCommons {
    private date : Date;
    private message : string;
    private viewed : boolean;

    constructor() {
        super();
        this.date = new Date();
        this.message = "";
        this.viewed = false;
    }

    public Date($value? : Date) : Date {
        if (ObjectValidator.IsSet($value)) {
            this.date = $value;
        }
        return this.date;
    }

    public Text($value? : string) : string {
        return this.message = Property.String(this.message, $value);
    }

    public Viewed($value? : boolean) : boolean {
        return this.viewed = Property.Boolean(this.viewed, $value);
    }

    protected innerHtml() : string {
        return `
<tr data-oidis-bind="${this}" style="cursor: pointer">
    <td>${this.message}</td>
    <td style="text-align: right;">${this.date.toDateString()}</td>
    <td class="text-end"><i class="far fa-eye${(!this.viewed ? "d-none" : "")}" style="font-size: 20px;"></i></td>
</tr>`;
    }
}
