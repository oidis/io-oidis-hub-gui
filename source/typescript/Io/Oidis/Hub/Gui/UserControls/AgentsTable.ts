/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { IAgentStatus } from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { ITableColumn, Table, TableItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Table.js";

export class AgentsTable extends Table<AgentsRecord> {

    constructor() {
        super();

        this.localization = {
            noData: "No agents found"
        };
    }

    protected getHeaderConfig() : ITableColumn[] {
        return [
            {text: "Name"}, {text: "Version"}, {text: "Platform"}, {text: "Location"}, {text: "Start time"}, {text: "Status"},
            {text: "Action", width: "100px", modifiers: ["text-end"]}
        ];
    }

    protected getNewItemInstance() : AgentsRecord {
        return new AgentsRecord();
    }
}

export class AgentsRecord extends TableItem {
    public name : Label;
    public version : Label;
    public platform : Label;
    public location : Label;
    public startTime : Label;
    public statusMenu : GuiCommons;
    public statusSelector : Button;
    public stop : Button;
    public openMetadata : Button;
    public agentId : string;

    constructor() {
        super();
        this.name = new Label();
        this.version = new Label();
        this.platform = new Label();
        this.location = new Label();
        this.startTime = new Label();
        this.statusMenu = new GuiCommons();
        this.statusSelector = new Button();
        this.stop = new Button();
        this.openMetadata = new Button();
    }

    public Restore() {
        super.Restore();
        this.setStatus(null);
        this.name.Text(" ");
        this.version.Text(" ");
        this.platform.Text(" ");
        this.location.Text(" ");
        this.startTime.Text(" ");
    }

    public setStatus($value : IAgentStatus) : void {
        this.styleClasses = [];

        switch ($value) {
        case IAgentStatus.Idle:
            this.styleClasses.push("table-warning");
            break;
        case IAgentStatus.Running:
            this.styleClasses.push("table-success");
            break;
        case IAgentStatus.Stopped:
            this.styleClasses.push("table-danger");
            break;
        default:
            // default value already initiated
            break;
        }
        const element : HTMLElement = this.InstanceOwner();
        if ((<any>this).completed && !ObjectValidator.IsEmptyOrNull(element)) {
            element.className = "";
            element.classList.add(...this.styleClasses);
        }
    }

    protected innerHtml() : string {
        const styleClassesStr : string = this.styleClasses.join(" ");
        return `
<tr class="${styleClassesStr}" data-oidis-bind="${this}">
    <td><span data-oidis-bind="${this.name}"></span></td>
    <td><span data-oidis-bind="${this.version}"></span></td>
    <td><span data-oidis-bind="${this.platform}"></span></td>
    <td><span data-oidis-bind="${this.location}"></span></td>
    <td><span data-oidis-bind="${this.startTime}"></span></td>
    <td>
        <div class="dropdown"><button class="btn btn-outline-primary dropdown-toggle rounded-end" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;" data-oidis-bind="${this.statusSelector}"></button>
            <div class="dropdown-menu" data-oidis-bind="${this.statusMenu}"><span>Loading ...</span></div>
        </div>
    </td>
    <td class="text-end"><svg class="bi bi-info-circle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" data-oidis-bind="${this.openMetadata}" style="margin-right: 10px;" data-bs-toggle="tooltip" title="Metadata">
            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"></path>
            <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0"></path>
        </svg><svg class="bi bi-power" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" data-oidis-bind="${this.stop}" style="margin-right: 10px;" data-bs-toggle="tooltip" title="Stop">
            <path d="M7.5 1v7h1V1z"></path>
            <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812"></path>
        </svg></td>
</tr>`;
    }
}
