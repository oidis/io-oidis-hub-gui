/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { ITableColumn, Table, TableItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Table.js";

export class OrgUsersTable extends Table<OrgUserItem> {

    constructor() {
        super();

        this.localization = {
            noData: "This organization does not have any users yet"
        };
        this.setNoDataStyle("warning");
    }

    protected getHeaderConfig() : ITableColumn[] {
        return [{text: "Name"}, {text: "Info"}, {text: "Role"}, {text: ""}];
    }

    protected getNewItemInstance() : OrgUserItem {
        return new OrgUserItem();
    }
}

export class OrgUserItem extends TableItem {
    public readonly name : Label;
    public readonly info : Label;
    public readonly roleSelector : Button;
    public readonly roleMenu : GuiCommons;
    public readonly edit : Button;
    public readonly save : Button;
    public readonly delete : Button;

    constructor() {
        super();
        this.name = new Label();
        this.info = new Label();
        this.roleSelector = new Button();
        this.roleMenu = new GuiCommons();
        this.edit = new Button();
        this.save = new Button();
        this.delete = new Button();
    }

    public Restore() {
        super.Restore();
        this.Enabled(true);
        this.getRowModifiers();
        this.name.Text(" ");
        this.info.Text(" ");
        this.roleSelector.Data(null);
    }

    protected innerHtml() : string {
        const style : string = this.Enabled() ? "" : ` class="${this.getRowModifiers().join(" ")}"`;
        return `
<tr${style} data-oidis-bind="${this}">
    <td><span data-oidis-bind="${this.name}"></span></td>
    <td><span data-oidis-bind="${this.info}"></span></td>
    <td>
        <div class="dropdown"><button class="btn btn-outline-dark btn-sm dropdown-toggle text-start" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="width: 100%;" data-oidis-bind="${this.roleSelector}">select</button>
            <div class="dropdown-menu" style="width: 100%;" data-oidis-bind="${this.roleMenu}"><a class="dropdown-item" href="#">loading ...</a></div>
        </div>
    </td>
    <td class="text-end"><span data-oidis-bind="${this.edit}"><svg class="bi bi-file-earmark-person" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" data-bs-toggle="tooltip" title="Show user profile" style="margin-right: 10px;cursor: pointer;">
                <path d="M11 8a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"></path>
                <path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2zM9.5 3A1.5 1.5 0 0 0 11 4.5h2v9.255S12 12 8 12s-5 1.755-5 1.755V2a1 1 0 0 1 1-1h5.5v2z"></path>
            </svg></span><span data-oidis-bind="${this.save}"><svg class="bi bi-check2-circle" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" data-bs-toggle="tooltip" title="Save" style="margin-right: 10px;cursor: pointer;">
                <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"></path>
                <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"></path>
            </svg></span><span data-oidis-bind="${this.delete}"><svg class="bi bi-trash" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" data-bs-toggle="tooltip" title="Remove from organization" style="cursor: pointer;">
                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"></path>
            </svg></span></td>
</tr>`;
    }

    private getRowModifiers() : string[] {
        const classes : string[] = [];
        if (this.Enabled()) {
            classes.push("text-secondary");
        }

        const element : HTMLElement = this.InstanceOwner();
        if ((<any>this).completed && !ObjectValidator.IsEmptyOrNull(element)) {
            if (!this.Enabled()) {
                element.classList.add("text-secondary");
            } else {
                element.classList.remove("text-secondary");
            }
        }
        return classes;
    }
}
