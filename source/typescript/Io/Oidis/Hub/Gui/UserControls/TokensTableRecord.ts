/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { Button } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Button.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { TextField } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/TextField.js";

export class TokensTableRecord extends GuiCommons {
    public name : TextField;
    public token : Label;
    public date : TextField;
    public showButton : Button;
    public copyButton : Button;
    public revokeButton : Button;
    private authenticated : boolean;

    constructor() {
        super();
        this.name = new TextField();
        this.token = new Label();
        this.date = new TextField();
        this.showButton = new Button();
        this.copyButton = new Button();
        this.revokeButton = new Button();
        this.authenticated = false;
    }

    public ShowTokenValue($value? : boolean) : boolean {
        return this.authenticated = Property.Boolean(this.authenticated, $value);
    }

    protected innerHtml() : string {
        let icons : string;
        if (!this.authenticated) {
            icons = `
                    <svg data-oidis-bind="${this.showButton}" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-eye-fill float-end" style="margin-top: 5px;">
                        <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                        <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                    </svg>`;
        } else {
            icons = `
                    <svg data-oidis-bind="${this.copyButton}" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-code-square" data-bs-toggle="tooltip" title="Copy token in JSON format">
                        <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"></path>
                        <path fill-rule="evenodd" d="M6.854 4.646a.5.5 0 0 1 0 .708L4.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0zm2.292 0a.5.5 0 0 0 0 .708L11.793 8l-2.647 2.646a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708 0z"></path>
                    </svg>`;
        }

        return `
            <div class="input-group" style="margin-top: 10px;">
                <input data-oidis-bind="${this.name}" type="text" class="form-control" value="${this.name.Value()}" />
                <span class="input-group-text" style="width: 60%;">
                    <span data-oidis-bind="${this.token}" style="width: 100%; text-align: left;">${this.token.Text()}</span>
                    <span style="width: 15px;">${icons}</span>
                </span>
                <input data-oidis-bind="${this.date}" class="form-control" type="date" value="${this.date.Value()}"/>
                <button data-oidis-bind="${this.revokeButton}" class="btn btn-danger" type="button">Revoke</button>
            </div>`;
    }
}
