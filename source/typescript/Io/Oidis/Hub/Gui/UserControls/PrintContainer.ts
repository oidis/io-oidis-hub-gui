/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";

export class PrintContainer extends GuiCommons {
    public readonly ruler : GuiCommons;
    public readonly warning : GuiCommons;
    public readonly body : GuiCommons;
    protected localization : IPrintContainerLocalization;

    constructor() {
        super();

        this.ruler = new GuiCommons();
        this.warning = new GuiCommons();
        this.body = new GuiCommons();
        this.localization = {
            header : "Native page print is not supported by this system",
            message: "Please use print option from application user interface instead."
        };
    }

    protected innerHtml() : string {
        return `
<section class="${this.getClassNameWithoutNamespace()}">
    <section class="${GeneralCSS.DNONE}" data-oidis-bind="${this.body}"></section>
    <div data-oidis-bind="${this.warning}">
        <h2 style="text-align: center;">${this.localization.header}</h2>
        <h5 style="color: rgb(122,122,122);text-align: center;">${this.localization.message}</h5>
    </div>
    <div class="${GeneralCSS.DNONE}" style="height: 100mm;" data-oidis-bind="${this.ruler}"></div>
</section>`;
    }
}

export interface IPrintContainerLocalization {
    header : string;
    message : string;
}

// generated-code-start
export const IPrintContainerLocalization = globalThis.RegisterInterface(["header", "message"]);
// generated-code-end
