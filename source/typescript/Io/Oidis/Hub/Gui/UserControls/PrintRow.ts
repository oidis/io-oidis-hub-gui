/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class PrintRow extends GuiCommons {
    public readonly columns : GuiCommons[];
    private height : number;
    private padding : number;
    private withGrid : boolean;

    constructor($columnsCount : number = 2) {
        super();

        this.columns = [];
        for (let index : number = 0; index < $columnsCount; index++) {
            const column : GuiCommons = new GuiCommons();
            this[column.Id()] = column;
            this.columns.push(column);
        }
        this.height = 71.75;
        this.padding = 5;
        this.withGrid = false;
    }

    public Height($value? : number) : number {
        if (!ObjectValidator.IsEmptyOrNull($value) && this.height > 0) {
            this.height = $value;
        }
        return this.height;
    }

    public Padding($value? : number) : number {
        return this.padding = Property.PositiveInteger(this.padding, $value);
    }

    public WithGrid($value? : boolean) : boolean {
        return this.withGrid = Property.Boolean(this.withGrid, $value);
    }

    protected innerHtml() : string {
        const border : string = this.withGrid ? "border: 1px dashed #545454;" : "";
        const columnWidth : number = 100 / this.columns.length;
        let columnsHtml : string = "";
        for (const column of this.columns) {
            columnsHtml += `
    <div class="col" data-oidis-bind="${column}" style="height: 100%;width: ${columnWidth}%;margin: 0;padding: ${this.padding}mm;overflow: hidden;${border}"></div>`;
        }
        return `
<div class="row" style="padding: 0;margin: 0;height: ${this.height}mm;width: 100%;" data-oidis-bind="${this}">${columnsHtml}
</div>`;
    }
}
