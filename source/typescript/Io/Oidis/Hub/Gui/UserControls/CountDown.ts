/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";

export class CountDown extends GuiCommons {
    private static dependencyLoaded : boolean = false;
    private value : number;
    private min : number;
    private max : number;
    private component : GuiCommons;

    constructor() {
        super();
        this.value = 0;
        this.min = 0;
        this.max = 100;
        this.component = new GuiCommons();
    }

    public Value($value? : number) : number {
        this.value = Property.Integer(this.value, $value);
        this.component.InstanceOwner().value = this.value;
        return this.value;
    }

    public Min($value? : number) : number {
        this.min = Property.Integer(this.min, $value);
        this.component.InstanceOwner().min = this.min;
        return this.min;
    }

    public Max($value? : number) : number {
        this.max = Property.Integer(this.max, $value);
        this.component.InstanceOwner().max = this.max;
        return this.max;
    }

    protected innerCode() : string {
        if (!CountDown.dependencyLoaded) {
            this.getEvents().setOnComplete(() : void => {
                if (!CountDown.dependencyLoaded) {
                    CountDown.dependencyLoaded = true;
                    StaticPageContentManager.HeadScriptAppend("/resource/libs/js-circle-progress/circle-progress.min.js" +
                        "?v=" + this.getEnvironmentArgs().getProjectConfig().build.timestamp, "module");
                }
            });
        }
        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<circle-progress data-oidis-bind="${this.component}" value="${this.value}" min="${this.min}" max="${this.max}" text-format="value"></circle-progress>`;
    }
}
