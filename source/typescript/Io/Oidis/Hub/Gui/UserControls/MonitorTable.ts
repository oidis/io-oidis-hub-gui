/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Label } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Label.js";
import { ITableColumn, Table, TableItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/Table.js";

export class MonitorTable extends Table<MonitorItem> {

    constructor() {
        super();

        this.localization = {
            noData: "No monitoring data found for selected filter"
        };
    }

    protected getHeaderConfig() : ITableColumn[] {
        return [{text: "Message"}, {text: "User", width: "150px"}, {text: "Time", width: "260px"}];
    }

    protected getNewItemInstance() : MonitorItem {
        return new MonitorItem();
    }
}

export class MonitorItem extends TableItem {
    public readonly date : Label;
    public readonly message : Label;
    public readonly user : Label;
    private isError : boolean;
    private isWarning : boolean;

    constructor() {
        super();
        this.date = new Label();
        this.message = new Label();
        this.user = new Label();
        this.isError = false;
        this.isWarning = false;
    }

    public setError($value : boolean) : void {
        this.isError = Property.Boolean(this.isError, $value);
        if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
            this.InstanceOwner().classList.add("table-danger");
        }
    }

    public setWarning($value : boolean) : void {
        this.isWarning = Property.Boolean(this.isWarning, $value);
        if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
            this.InstanceOwner().classList.add("table-warning");
        }
    }

    public Restore() {
        super.Restore();
        this.isError = false;
        this.isWarning = false;
        this.getRowModifiers();
        this.date.Text(" ");
        this.message.Text(" ");
        this.user.Text(" ");
    }

    protected innerHtml() : string {
        const modifiers : string = this.getRowModifiers().join(" ");
        return `
<tr data-oidis-bind="${this}" class="${modifiers}">
    <td><span data-oidis-bind="${this.message}"></span></td>
    <td><span data-oidis-bind="${this.user}"></span></td>
    <td><span data-oidis-bind="${this.date}"></span></td>
</tr>`;
    }

    private getRowModifiers() : string[] {
        const classes : string[] = [];
        if (this.isError) {
            classes.push("table-danger");
        } else if (this.isWarning) {
            classes.push("table-warning");
        }

        const element : HTMLElement = this.InstanceOwner();
        if ((<any>this).completed && !ObjectValidator.IsEmptyOrNull(element)) {
            element.classList.remove("table-danger", "table-warning");
            if (this.isError) {
                element.classList.add("table-danger");
            }
            if (this.isWarning) {
                element.classList.add("table-warning");
            }
        }
        return classes;
    }
}
