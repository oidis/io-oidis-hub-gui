/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { DropDownListItem } from "@io-oidis-usercontrols/Io/Oidis/UserControls/Bootstrap/UserControls/DropDownList.js";

export class PermissionsItem extends DropDownListItem {
    private styleClass : string;

    constructor() {
        super();
        this.styleClass = "btn-outline-primary";
    }

    public setStyle($value : string) : void {
        this.styleClass = $value;
    }

    protected innerHtml() : string {
        return `
            <button class="btn ${this.styleClass} btn-sm float-start" type="button" style="border-radius: 25px;">${this.Text()}
                <svg data-oidis-bind="${this}" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-x-circle-fill" style="margin-left: 8px;margin-bottom: 3px;">
                    <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"></path>
                </svg>
            </button>`;
    }
}
