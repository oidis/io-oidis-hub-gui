/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";

export class BaseHubPageViewerArgs extends BasePanelViewerArgs {
    private headerText : string;
    private defaultContent : string;
    private footerText : string;

    constructor() {
        super();
        this.headerText = "";
        this.defaultContent = "";
        this.footerText = "";
    }

    public HeaderText($value? : string) : string {
        return this.headerText = Property.String(this.headerText, $value);
    }

    public DefaultContent($value? : string) : string {
        return this.defaultContent = Property.String(this.defaultContent, $value);
    }

    public FooterText($value? : string) : string {
        return this.footerText = Property.String(this.footerText, $value);
    }
}
