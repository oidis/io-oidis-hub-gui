/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Resolver, ResolverScope } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { IndexPage } from "@io-oidis-gui/Io/Oidis/Gui/Pages/IndexPage.js";
import { AsyncBasePageController } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/Resolvers/AsyncBasePageController.js";
import { AgentsRegisterTest } from "./RuntimeTests/AgentsRegisterTest.js";
import { ModelTest } from "./RuntimeTests/ModelTest.js";
import { Snippets } from "./RuntimeTests/Snippets.js";
import { DashboardPageViewer } from "./Viewers/Pages/DashboardPageViewer.js";
import { SSOPageViewer } from "./Viewers/Pages/SSOPageViewer.js";

@Resolver(["/", "/index", "/index.html", "/web/"], {scope: ResolverScope.DEV, forceRegister: true})
export class Index extends AsyncBasePageController {

    constructor() {
        super();
        this.setInstanceOwner(new IndexPage());
    }

    protected async onSuccess($instance : IndexPage) : Promise<void> {
        $instance.headerTitle.Content("Oidis Hub");
        $instance.headerInfo.Content("Oidis Framework synchronization hub");

        let content : string = "";
        /* dev:start */
        content += `
            <h3>Pages</h3>
            <a href="/#/main">Main Page</a>
            <a href="${DashboardPageViewer.CallbackLink(true)}">Dashboard page</a>
            <a href="${SSOPageViewer.CallbackLink(true)}">SSO page</a>
            <a href="${this.getRequest().getHostUrl() + "graphiql"}">GraphiQL</a>

            <h3>Runtime tests</h3>
            <a href="${Snippets.CallbackLink()}">Snippets</a>
            <a href="${AgentsRegisterTest.CallbackLink()}">AgentsRegisterTest</a>
            <a href="${ModelTest.CallbackLink()}">Model persistence on front-end test</a>
            `;
        /* dev:end */

        $instance.pageIndex.Content(content);
        $instance.footerInfo.Content("" +
            "version: " + this.getEnvironmentArgs().getProjectVersion() +
            ", build: " + this.getEnvironmentArgs().getBuildTime() +
            ", " + this.getEnvironmentArgs().getProjectConfig().target.copyright);
    }
}
