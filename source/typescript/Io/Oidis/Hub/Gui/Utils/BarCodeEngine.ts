/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";

declare const JsBarcode : any;
declare const Html5Qrcode : any;
declare const QRCode : any;

export class BarCodeEngine extends BaseObject {
    private scanner : any;
    private isRunning : boolean;
    private target : HTMLElement;

    public static async Generate($target : any, $value : string, $options? : IBarCodeOptions | IQRCodeOptions) : Promise<void> {
        $options = JsonUtils.Extend({
            format: "CODE128",
            height: 25,
            margin: 0
        }, $options);
        if ($options.format === "QRCODE") {
            if (ObjectValidator.IsEmptyOrNull(QRCode)) {
                throw new Error("QRCode generator requires usage of nodeQRcode.jsonp import and QRCODE PageContentBundle.");
            }
        } else if (ObjectValidator.IsEmptyOrNull(JsBarcode)) {
            throw new Error("BarCode generator requires usage of jsBarcode.jsonp import and BARCODE PageContentBundle.");
        }
        return new Promise<void>(($resolve, $reject) : void => {
            if ($options.format === "QRCODE") {
                $options = JsonUtils.Extend({
                    width: 250
                }, $options);
                QRCode.toCanvas($target, $value, $options, ($error : Error) : void => {
                    if ($error) {
                        $reject($error);
                    } else {
                        $resolve();
                    }
                });
            } else {
                try {
                    JsBarcode($target, $value, $options);
                    $resolve();
                } catch (ex) {
                    $reject(ex);
                }
            }
        });
    }

    constructor() {
        super();
        this.scanner = null;
        this.isRunning = false;

        if (ObjectValidator.IsEmptyOrNull(Html5Qrcode)) {
            throw new Error("BarCodeEngine scanner requires usage of html5QRcode.jsonp import and CODE_SCANNER PageContentBundle.");
        }
    }

    public async Init($target : any) : Promise<any[]> {
        if (ObjectValidator.IsEmptyOrNull(this.scanner)) {
            $target.id = this.getUID();
            this.scanner = new Html5Qrcode($target.id);
            this.target = $target;
        }
        return Html5Qrcode.getCameras();
    }

    public async Start($onDetect : ($data : string) => void, $cameraId? : string) : Promise<void> {
        if (ObjectValidator.IsEmptyOrNull(this.scanner)) {
            throw new Error("BarCode reader must be initialized by Init method before execution of Start");
        }
        let deviceConfig : any = {
            facingMode: "environment"
        };
        if (!ObjectValidator.IsEmptyOrNull($cameraId)) {
            deviceConfig = {
                deviceId: {exact: $cameraId}
            };
        }
        await this.scanner.start(
            deviceConfig,
            {
                fps  : 10,
                qrbox: () => {
                    try {
                        let width : number = Math.ceil(this.target.offsetWidth / 2);
                        let height : number = Math.ceil(this.target.offsetHeight / 2);
                        if (width < 50) {
                            width = 50;
                        }
                        if (height < 50) {
                            height = 50;
                        }
                        return {width, height};
                    } catch (ex) {
                        LogIt.Error("Failed to get qrbox dimensions", ex);
                    }
                    return {width: 350, height: 350};
                }
            },
            ($decodedText : string, $decodedResult : any) : void => {
                // LogIt.Info("BarCode detect: {0}", JSON.stringify($decodedResult));
                $onDetect($decodedText);
            },
            ($errorMessage : string) : void => {
                // LogIt.Debug($errorMessage);
            });
        this.isRunning = true;
    }

    public async Stop() : Promise<boolean> {
        if (ObjectValidator.IsEmptyOrNull(this.scanner) || !this.isRunning) {
            return false;
        }
        await this.scanner.stop();
        this.isRunning = false;
        return true;
    }

    public async ScanFile($data : any) : Promise<string> {
        if (ObjectValidator.IsEmptyOrNull(this.scanner)) {
            return null;
        }
        await this.Stop();
        return this.scanner.scanFile($data, true);
    }
}

export interface IBarCodeOptions {
    format? : string;
    height? : number;
    width? : number;
}

interface IQRCodeColorOptions {
    dark? : string;
    light? : string;
}

interface IQRCodeOptions {
    format? : string;
    modules? : any;
    version? : number;
    errorCorrectionLevel? : string;
    maskPattern? : number;
    segments? : string | any[];
    toSJISFunc? : any;
    margin? : number;
    scale? : number;
    small? : boolean;
    width? : number;
    color? : IQRCodeColorOptions;
}

// generated-code-start
export const IBarCodeOptions = globalThis.RegisterInterface(["format", "height", "width"]);
// generated-code-end
