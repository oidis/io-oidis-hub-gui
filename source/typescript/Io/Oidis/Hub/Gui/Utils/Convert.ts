/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Convert as Parent } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";

export class Convert extends Parent {

    public static DateToCalendarFormat($value : Date) : string {
        return $value.getFullYear() +
            "-" + (($value.getMonth() > 8) ? ($value.getMonth() + 1) : ("0" + ($value.getMonth() + 1))) +
            "-" + (($value.getDate() > 9) ? $value.getDate() : ("0" + $value.getDate()));
    }

    public static DateToCsFormat($value : Date) : string {
        return (($value.getDate() > 9) ? $value.getDate() : ("0" + $value.getDate())) +
            "." + (($value.getMonth() > 8) ? ($value.getMonth() + 1) : ("0" + ($value.getMonth() + 1))) +
            "." + $value.getFullYear();
    }

    public static DateToId($value : Date, $baseLineTime = 1640991600000 /* 0:0:0 1.1.2022 */) : string {
        // const fromDate : Date = new Date();
        // fromDate.setFullYear(2022, 0, 1);
        // fromDate.setHours(0, 0, 0, 0);
        // $baseLineTime = fromDate.getTime();
        return Convert.ToFixed(Convert.TimeToSeconds($value.getTime() - $baseLineTime), 0).toString();
    }
}
