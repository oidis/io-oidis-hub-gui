/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface ISendmailConfiguration {
    serviceType : string;
    pass : string;
    user : string;
    location : string;
    port : number;
    secure : boolean;
    defaultSender : string;
    dkim : ISendmailDkim[];
    domain : string;
    enabled : boolean;
    admins : string[];
    fetchTick : number;
    blackList : string[];
    onBehalfPatterns : string[];
}

export interface ISendmailDkim {
    keySelector : string;
    privateKey : string;
    domainName? : string;
}

// generated-code-start
/* eslint-disable */
export const ISendmailConfiguration = globalThis.RegisterInterface(["serviceType", "pass", "user", "location", "port", "secure", "defaultSender", "dkim", "domain", "enabled", "admins", "fetchTick", "blackList", "onBehalfPatterns"]);
export const ISendmailDkim = globalThis.RegisterInterface(["keySelector", "privateKey", "domainName"]);
/* eslint-enable */
// generated-code-end
