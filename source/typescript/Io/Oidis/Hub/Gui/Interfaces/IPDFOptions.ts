/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IPDFOptions {
    name? : string;
    format? : string;
    displayHeaderFooter? : boolean;
    landscape? : boolean;
    margin? : IPDFMargin;
    asStream? : boolean;
    language? : string;
    license? : string;
    css? : string;
    width? : string;
    height? : string;
    cookies? : any[];
    inputs? : any;
}

export interface IPDFMargin {
    top? : string | number;
    right? : string | number;
    bottom? : string | number;
    left? : string | number;
}

// generated-code-start
/* eslint-disable */
export const IPDFOptions = globalThis.RegisterInterface(["name", "format", "displayHeaderFooter", "landscape", "margin", "asStream", "language", "license", "css", "width", "height", "cookies", "inputs"]);
export const IPDFMargin = globalThis.RegisterInterface(["top", "right", "bottom", "left"]);
/* eslint-enable */
// generated-code-end
