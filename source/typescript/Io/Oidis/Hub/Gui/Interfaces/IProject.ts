/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IProject as Parent, IProjectTarget as ICommonsProjectTarget } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IProject.js";

export interface IProject extends Parent {
    target : IProjectTarget;
}

export interface IProjectTarget extends ICommonsProjectTarget {
    headerStrip : IHeaderStripType;
}

export interface IHeaderStripType {
    prod : IHeaderStripConfig;
    dev : IHeaderStripConfig;
    eap : IHeaderStripConfig;
    localhost : IHeaderStripConfig;
}

export interface IHeaderStripConfig {
    title : string;
    message : string;
}

// generated-code-start
export const IProject = globalThis.RegisterInterface(["target"], <any>Parent);
export const IProjectTarget = globalThis.RegisterInterface(["headerStrip"], <any>ICommonsProjectTarget);
export const IHeaderStripType = globalThis.RegisterInterface(["prod", "dev", "eap", "localhost"]);
export const IHeaderStripConfig = globalThis.RegisterInterface(["title", "message"]);
// generated-code-end
