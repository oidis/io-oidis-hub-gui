/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IRegistryItemGroup {
    name : string;
    items : IRegistryItem[];
    isAsyncRefreshDisabled? : boolean;
    isFilterDisabled? : boolean;
}

export interface IRegistryItem {
    link? : string;
    name? : string;
    info? : string;
    version? : string;
    platform? : string;
    type? : string;
}

// generated-code-start
export const IRegistryItemGroup = globalThis.RegisterInterface(["name", "items", "isAsyncRefreshDisabled", "isFilterDisabled"]);
export const IRegistryItem = globalThis.RegisterInterface(["link", "name", "info", "version", "platform", "type"]);
// generated-code-end
