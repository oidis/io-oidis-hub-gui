/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface ISendMailOptions {
    to : string | string[];
    subject : string;
    body : string;
    attachments? : string[];
    plainText? : string;
    copyTo? : string | string[];
    from? : string;
    sender? : string;
}

// generated-code-start
/* eslint-disable */
export const ISendMailOptions = globalThis.RegisterInterface(["to", "subject", "body", "attachments", "plainText", "copyTo", "from", "sender"]);
/* eslint-enable */
// generated-code-end
