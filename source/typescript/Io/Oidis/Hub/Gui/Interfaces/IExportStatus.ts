/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IExportStatus {
    id : string;
    isPartial : boolean;
    data? : IExportData;
}

export interface IExportData {
    name? : string;
    data : Buffer | string;
    encoding : string;
    format : string;
    asStream : boolean;
}

// generated-code-start
export const IExportStatus = globalThis.RegisterInterface(["id", "isPartial", "data"]);
export const IExportData = globalThis.RegisterInterface(["name", "data", "encoding", "format", "asStream"]);
// generated-code-end
