/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseHttpResolver } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { Resolver, ResolverType } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";

@Resolver("/ServerError/Http/NotAuthorized", {forceRegister: true, type: ResolverType.FRONTEND_ONLY})
export class NotAuthorizedResolver extends BaseHttpResolver {

    public Process() : void {
        this.resolver();
    }

    protected resolver() : void {
        if (this.getEnvironmentArgs().HtmlOutputAllowed()) {
            this.getHttpManager().ReloadTo("/login", this.RequestArgs().POST());
        } else {
            LogIt.Warning("This is Not authorized request is suitable only for front-end calls.");
        }
    }
}
