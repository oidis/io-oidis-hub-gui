# io-oidis-hub

> GUI for Oidis Framework synchronization hub

## Requirements

This library does not have any special requirements but it depends on the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder). See the Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`oidis docs` command from the {projectRoot} folder.

## History

### v2023.0.0
Initial release.

## License

This software is owned or controlled by Oidis.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
 
See the `LICENSE.txt` file for more details.

---

Copyright 2023-2025 [Oidis](https://www.oidis.io/)
